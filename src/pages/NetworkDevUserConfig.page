<!-- ==================================================================================
File Name:   NetworkDevUserConfig.cls
Author:      Michael Olson
Date:        5/19/2015
Description: 

Modification History
5/19/2015       Michael Olson       Created
9/28/2015		Michael Olson		Added support for FST auto assignment
10/15/2015		Michael olson		Added Auto Assign FST
====================================================================================== -->

<apex:page controller="NetworkDevUserConfig" sidebar="false">
    <apex:form >
       <head>
            <!-- JQuery API inclusion -->
            <!-- <apex:includescript value="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"/>-->
            <apex:includescript value="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.js"/>
            <!-- <apex:includescript value="{!$Resource.jQuery_v2_1_4}" />-->
            
            <script src="{!URLFOR($Resource.MPIdataTables, 'media/js/jquery.dataTables.min.js')}"></script>
            <apex:stylesheet value="{!URLFOR($Resource.jQueryUI_MPI,'/css/custom-theme/jquery-ui-1.8.18.custom.css')}" />
                        
            <apex:stylesheet value="{!URLFOR($Resource.MPIdataTables,'/media/css/jquery.dataTables_themeroller.css')}" />
            <apex:stylesheet value="{!URLFOR($Resource.MPIdataTables,'/extras/ColVis/media/css/ColVis.css')}" />
            <apex:stylesheet value="{!URLFOR($Resource.MPIdataTables,'/extras/ColReorder/media/css/ColReorder.css')}" />
            <apex:includescript value="{!$Resource.TableSorter}" />
            
            
            <script type="text/javascript" charset="UTF-8">
                 
                //--------------------------------------------------------------------------
                //Ready Functions
                //--------------------------------------------------------------------------
              
                    $(document).ready( function () {
        
                        //--------------------------------------------------------------------------
                        //Loads User Table
                        //--------------------------------------------------------------------------
        
                        var uTable = $("#usersTable").dataTable( {
        
                             //"bSort": true,
                             "aaSorting": [],
                             "sDom": 'WRC<"clear">lftip',
                             "bJQueryUI": true,
                             "sPaginationType": "full_numbers",
                             "iDisplayLength" : -1,
                             "aLengthMenu" : [ [ -1, 50, 100, 200 ],
                                             [ "All", 50, 100, 200 ] ],
                             "aoColumnDefs": [ { "bVisible": false, "aTargets": [] }],
                             "oColumnFilterWidgets": { "aiExclude": [ 0, 1, 3, 4, 5, 6 ] },                          
                             "fnInitComplete": function() { 
                                 $("#usersTable").show(); 
                             }          
                        });


						$(".multiSelectPicklistTable tbody > tr:first-child").hide();
                        $("#usersTable tr:even").css("background-color", "#eeeeee");
						$("#usersTable tr:odd").css("background-color", "#ffffff");
                        
                        //Apply line highlighting to tables
                        $("#usersTable tr").not(':first').hover(
                            function () {
                                $(this).css('background','lightgrey');
                            }, 
                            function () {
                                //$(this).css('background','');
								$("#usersTable tr:even").css("background-color", "#eeeeee");
								$("#usersTable tr:odd").css("background-color", "#ffffff");
                            }
                        );
                        
					});
                        
                        
                        //--------------------------------------------------------------------------
                        //Function to setup the ablity to properly sort user DataTable
                        //--------------------------------------------------------------------------             
                        //$(function() {
                        
                            //$("#uTable").tablesorter({
                                //textExtraction: function(node){
                                    //return $(node).text().replace(/^[(]/,'-');
                                //},
                                //sortList : [[0, 0]],
                                //sortList : [[ ]],
                                //debug: true, 
                                //headers: {
                                    //zero-based column index is used
                                    //1: { sorter:'isoDate'},
                                    //4:  { sorter:'currency'}, 
                                    //5: { sorter:'currency'}, 
                                    //14: { sorter:'isoDate'} //isoDate or usLongDate
                                //}
                            //});
                        //});                                     
             
            </script>
        </head> 
        
        <apex:pageblock title="Rate Operations - User Auto Assign Config">
        
            <apex:pageMessages />
        
            <apex:pageBlockButtons >
                <apex:commandButton value="Save Users" action="{!saveUsers}"/>
                <apex:commandButton value="Cancel" action="{!cancel}"/>
            </apex:pageBlockButtons>
        
            <apex:outputPanel rendered="{!configUsers.size>0}" id="uListOP">             
                <body>  
                    <table cellpadding="0" cellspacing="0" border="0" class="display" id="usersTable" style="margin-top:20px;display:none;">
                        <thead >
                            <tr>
                                <th>User Name</th>
                                <th>Auto Assign: Rate Ops Request Types</th>
                                <th>Auto Assign: HCFA RT Rate Rev</th>
                                <th>Auto Assign: FST</th>
                                <!-- <th>Rate Ops Backup Email</th>-->
                                <th>No Auto Assign: Date Range</th>
                             </tr>
                        </thead>
            
                        <tbody>
                            <apex:repeat value="{!configUsers}" var="uList">
                                <tr>
                                    <td style="vertical-align: text-top">
                                    	<apex:outputfield value="{!uList.name}" /><br/>
                                    	<apex:outputfield value="{!uList.email}"/><br/>
                                    	<br/>
                                    	<apex:outputtext value="Rate Ops Backup Email:" /><br/>
                                    	<apex:inputfield value="{!uList.Rate_Operations_Backup_Email__c}" style="width:75%"/>
                                    </td>
                                    <td><apex:inputfield value="{!uList.Auto_Assign_Rate_Operation_Request_Type__c}" id="aarort"/><br/></td>
                                    <td><apex:inputfield value="{!uList.Auto_Assign_Rate_Rev_HCFA_RT_Assignable__c}"/></td>
                                    <td><apex:inputfield value="{!uList.Auto_Assign_FST__c}"/></td>
                                    <!-- <td><apex:inputfield value="{!uList.Rate_Operations_Backup_Email__c}"/></td>-->
                                    <td><apex:inputfield value="{!uList.No_Auto_Assign_HCE_Request_Start_Date__c}"/> <apex:outputtext value=" through "/> <apex:inputfield value="{!uList.No_Auto_Assign_HCE_Request_End_Date__c}"/></td>
                                 </tr>
                             </apex:repeat>
                        </tbody>
                    </table>
                </body>
            </apex:outputPanel>
            <apex:outputLabel value="No User records to display." rendered="{!configUsers.size=0}" styleClass="noRowsHeader"></apex:outputLabel>
        
        </apex:pageBlock>
    
    
    </apex:form>
</apex:page>