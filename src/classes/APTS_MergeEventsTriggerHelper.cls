/*
    Author: Apttus, 02/23/2017 - MergeEventsTriggerHelper for after insert.

    Modification log:
    Kruti Shah, APTTUS, 02/23/2017 - Added Derivation for updating Agreement Clause field. 
	If user regenerates the agreement then, delete all the related agreement clauses for that agreement.
    Kruti Shah, APTTUS, 05/04/2017 - Added Derivation to update Agreement status Category, Status and Agreement Stage.

*/
public class APTS_MergeEventsTriggerHelper {

	//Constructor
    public APTS_MergeEventsTriggerHelper()
    {
        
    }

    public void updateAgreementClauseAsDeleted(List<Apttus__MergeEvent__c> newMergeEventsList){
        Set<ID> agrIDSet = new Set<ID>();
        Set<ID> agrClauseIDSet = new Set<ID>();
        Set<ID> agrClauseIDSet_Deleted = new Set<ID>();
        List<Apttus__APTS_Agreement__c> listOfAgreementsToBeUpdated = new List<Apttus__APTS_Agreement__c>();
        List<Apttus__Agreement_Clause__c> listOfAgreementCluases = new List<Apttus__Agreement_Clause__c>();
        Map<ID,List<Apttus__Agreement_Clause__c>> mapofAgrIDtoAgrClauseDeleted = new Map<ID,List<Apttus__Agreement_Clause__c>>();        
        List<Apttus__Agreement_Clause__c> listOfagrCluaseToBeUpdated = new List<Apttus__Agreement_Clause__c>();

        List<Apttus__MergeEvent__c> listOfMErgeEventsToDelete = new List<Apttus__MergeEvent__c>();
        Savepoint sp = Database.setSavepoint();
        try{
            for(Apttus__MergeEvent__c mergeEventSO : newMergeEventsList)
            {
                if(mergeEventSO.Name == CONSTANTS.REGENERATE){
                    agrIDSet.add(mergeEventSO.Apttus__AgreementId__c);                         
                } 
            }
            system.debug('APTTUS--> Agreement ID Set '+agrIDSet);

            listOfAgreementCluases = [SELECT Id,Name,Apttus__Action__c,Apttus__Status__c, Apttus__Clause__c, Apttus__Active__c
            							FROM Apttus__Agreement_Clause__c
            						        WHERE Apttus__Agreement__c IN :agrIDSet];   

            //Start: Update Status Category/Status as 'In Authoring/Author Contract' and Agreement Stage as 'Draft'
            listOfAgreementsToBeUpdated = [SELECT ID,Name,Apttus__Status_Category__c,Apttus__Status__c,Agreement_Stage__c 
                                            FROM Apttus__APTS_Agreement__c 
                                                WHERE ID IN :agrIDSet];    

            if(listOfAgreementsToBeUpdated.size() >0 && !listOfAgreementsToBeUpdated.isEmpty()){
                for(Apttus__APTS_Agreement__c objAgreement :listOfAgreementsToBeUpdated){
                    objAgreement.Apttus__Status_Category__c = CONSTANTS.STATUS_CATEGORY_IN_AUTHORING;
                    objAgreement.Apttus__Status__c = CONSTANTS.STATUS_AUTHOR_CONTRACT;
                    objAgreement.Agreement_Stage__c = CONSTANTS.AGREEMENT_STAGE_DRAFT;
                }
                
                update listOfAgreementsToBeUpdated;
            }             
           //End: Update Status Category/Status as 'In Authoring/Author Contract' and Agreement Stage as 'Draft'

            system.debug('APTTUS--> listOfAgreementCluases '+listOfAgreementCluases);
            delete listOfAgreementCluases;

            /*
            //delete all the Regenerated Merge Event records
            //get the list of regenerated MergeEvents records and delete them after all the process has been done.
            listOfMErgeEventsToDelete = [SELECT Id,Name,Apttus__Action__c FROM  Apttus__MergeEvent__c WHERE Name =:CONSTANTS.REGENERATE];
            system.debug('APTTUS--> listOfMErgeEventsToDelete '+listOfMErgeEventsToDelete);
            delete listOfMErgeEventsToDelete;
            */
            /*
            if(listOfAgreementCluases.size() >0 && !listOfAgreementCluases.isEmpty()){

            	for(Apttus__Agreement_Clause__c agrCluaseSO : listOfAgreementCluases){
            		agrCluaseSO.Apttus__Status__c = 'Deleted';
            		agrCluaseSO.Apttus__Action__c = 'Deleted';
            	}
            	update listOfAgreementCluases;
            }
    		*/
        }Catch(Exception ex){
            Database.rollback(sp);
            system.debug('Error while updating Agreement Record '+ex);
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error while updating Agreement Record '+ex));                        
        }
    }
}