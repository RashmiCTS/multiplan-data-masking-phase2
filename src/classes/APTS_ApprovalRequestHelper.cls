/*
Author: Apttus, 01/25/2017 - Helper class for Approval Request Trigger.
Modification log:
Kruti Shah, APTTUS, 01/25/2017 - Created Helper class for Approval Request Trigger.
*/
public with sharing class APTS_ApprovalRequestHelper {    
    
    public set<Id> setOfAgreementClauseId = new set<Id>();
    public map<Id,Apttus_Approval__Approval_Request__c> mapOfAppReqIdAppReq = new map<Id,Apttus_Approval__Approval_Request__c>();
    public Map<Id,Apttus__Agreement_Clause__c> mapOfIdToAgreementClause = new Map<Id,Apttus__Agreement_Clause__c>();
    
    public APTS_ApprovalRequestHelper(){
        
    }
    
    //Derivation to populate field Agreement Clause Name(Text) from the Child Object(Agreement Clause)
    public void populateAgreementClauseName(Apttus_Approval__Approval_Request__c[] appReqList, Map<Id, Apttus_Approval__Approval_Request__c> appReqIdtoAppReqOldMap){        
        for(Apttus_Approval__Approval_Request__c appReq : appReqList)
		{
            Apttus_Approval__Approval_Request__c appReqOld = appReqIdtoAppReqOldMap.get(appReq.Id);
            
            if(appReqOld == null
					|| (appReq.Apttus_Approval__ChildObjectType__c != null						
                    && appReq.Apttus_Approval__ChildObjectType__c == 'Apttus__Agreement_Clause__c'))
			{
                setOfAgreementClauseId.add((Id)appReq.Apttus_Approval__ChildObjectId__c);
                mapOfAppReqIdAppReq.put(appReq.Id,appReq);
            }            
        }

        if(setOfAgreementClauseId.size() >0 && !setOfAgreementClauseId.isEmpty()){                       
            for(Apttus__Agreement_Clause__c agrClause : [SELECT Id,Name,Apttus__Clause__c 
                            FROM Apttus__Agreement_Clause__c 
                            WHERE Id in :setOfAgreementClauseId]){
                mapOfIdToAgreementClause.put(agrClause.id, agrClause);      
            }
        }
      
        if(mapOfAppReqIdAppReq.size() >0 && !mapOfAppReqIdAppReq.Isempty()){
            for(Apttus_Approval__Approval_Request__c appReqObj : mapOfAppReqIdAppReq.Values()){
                Apttus__Agreement_Clause__c agrClauseRec = new Apttus__Agreement_Clause__c();
                agrClauseRec = mapOfIdToAgreementClause.get((ID)appReqObj.Apttus_Approval__ChildObjectId__c);
                system.debug('APTTUS--> Agreement Clause Record '+agrClauseRec);
                if(agrClauseRec != null){
                    appReqObj.APTS_Agreement_Clause_Name__c = agrClauseRec.Apttus__Clause__c;
                }                
            }
        }
       
    }
    
    /* This method is not in use now
     public void populateSubmissionComments(Apttus_Approval__Approval_Request__c[] appReqList){  
         
     }
	*/
}