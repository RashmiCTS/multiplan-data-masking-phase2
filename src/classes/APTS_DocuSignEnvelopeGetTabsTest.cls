/*
	Author: Apttus, 08/31/2016 - Test class for APTS_DocuSignEnvelopeGetTabs.
 */
@isTest
private class APTS_DocuSignEnvelopeGetTabsTest {
	static Account testAcc;
	static Contact testCon;
	
	static{
		testAcc = new Account();
		testAcc.name = 'testA';
    	insert testAcc;

    	testCon = new Contact();
    	testCon.AccountId = testAcc.Id;
    	testCon.lastName = 'agreementInsert_Test';
    	testCon.email = 'agreementInsert_Test@Test.com';
    	insert testCon;
	}

	@isTest static void test_method_one() {
		// Implement test code
		// Implement test code
		Id group5RTId = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('Group V5').getRecordTypeId();  

		Apttus__APTS_Agreement__c testAgr = new Apttus__APTS_Agreement__c();
		testAgr.Apttus__Account__c = testAcc.id;
		testAgr.recordtypeid = group5RTId;
		testAgr.Apttus__Primary_Contact__c = testCon.id;
		insert testAgr;

		// create a mock docusign tab wrapper.
		Apttus_DocuApi.DocuSignUtil2.dateTab dateTab = new Apttus_DocuApi.DocuSignUtil2.dateTab();
		dateTab.anchorString = '\\ef3\\';
		dateTab.value = '12/27/2020';
		List<Apttus_DocuApi.DocuSignUtil2.dateTab> dateTabs = new List<Apttus_DocuApi.DocuSignUtil2.dateTab>();
		dateTabs.add(dateTab);
		Apttus_DocuApi.DocuSignUtil2.RecipientTabs rTabs = new Apttus_DocuApi.DocuSignUtil2.RecipientTabs();
		rTabs.dateTabs = dateTabs;
		
		Apttus_DocuApi.DocuSignUtil2.DocuSignRecipientStatus objRecipeintStatus = new Apttus_DocuApi.DocuSignUtil2.DocuSignRecipientStatus();
		objRecipeintStatus.routingOrder = '3';
		objRecipeintStatus.tabs = rTabs;

		Apttus_DocuApi.GetRecipientTabsWrapper grtWrapper = new Apttus_DocuApi.GetRecipientTabsWrapper();
		grtWrapper.parentId = testAgr.Id;
		grtWrapper.recipientStatus = objRecipeintStatus;

		List<Apttus_DocuApi.GetRecipientTabsWrapper> iListGetRecipientTabsWrapper = new List<Apttus_DocuApi.GetRecipientTabsWrapper>();
		iListGetRecipientTabsWrapper.add(grtWrapper);

		APTS_DocuSignEnvelopeGetTabs con = new APTS_DocuSignEnvelopeGetTabs();
		con.getRecipientTabs(iListGetRecipientTabsWrapper);

		// validate that callback class is updating the start date of agreement based on date field from signer 3.
		testAgr = [Select Id, Apttus__Contract_Start_Date__c 
						From Apttus__APTS_Agreement__c
							Where Id = :testAgr.Id];
		System.assertEquals(Date.newInstance(2020, 12, 27), testAgr.Apttus__Contract_Start_Date__c);
	}	
}