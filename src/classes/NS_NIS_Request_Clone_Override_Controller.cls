/*
File Name:   NS_NIS_Request_Clone_Override_Controller.cls
Author:      Michael Olson
Date:        5/26/2015
Description: Test class for CaseTrigger.

Modification History
5/26/2015		Michael Olson		Created
*/

public without sharing class NS_NIS_Request_Clone_Override_Controller {

    /************************************************
     CONSTRUCTOR
    ************************************************/
    public final NS_NIS_Request__c r; 
    public NS_NIS_Request__c v2;
    id currRecord {get;set;}
    
    
    public NS_NIS_Request_Clone_Override_Controller(ApexPages.standardController controller) {	
    	currRecord = ApexPages.currentPage().getParameters().get('id=');
        
        r = (NS_NIS_Request__c)controller.getRecord(); 

        this.v2 = [	select 	id,
        					ownerid,
        					name, 
							recordtypeid, 
            				of_Medicare__c,
							of_RBRVS__c,
							Account__c,
							Additional_Details__c,
							User__c,
							Add_Ons__c,
							Address__c,
							Anesthesia_Conversion_Factor__c,
							Anesthesia_Conversion_Factor_Value__c,
							Annual_Charge_Volume__c,
							AP_Accelerated_Pay__c,
							Automatic_Rate_Escalator__c,
							Automatic_Rate_Escalator_P_Increase__c,
							Automatic_Rate_Escalator_Date__c,
							Auto_Medical_Rate_of_the_State_FS__c,
							Auto_Medical_Rate_Lesser_Of__c,
							//Auto_Medical_Rate_POC_Rate__c,
							Auto_Rate_Escalate_of_Years_to_Update__c,
							Band_Name__c,
							Beech_Current_of_Medicare__c,
							Beech_Current_Discount__c,
							Beech_Negotiated_of_Medicare__c,
							Beech_Negotiated_Discount__c,
							Begin_Date_Range_of_Results__c,
							Carrier_and_Local_Numbers__c,
							CBSA__c,
							CE_ID__c,
							Charge_Master_Cap__c,
							Charge_Master_Cap_P__c,
							City__c,
							Compare_FS_ID__c,
							Contract_Notes__c,
							Contract_Rate_Type__c,
							Contract_Rate_Update__c,
							Contract_Rate_Update_Date__c,
							Contract_Type__c,
							County__c,
							Current_FS_ID__c,
							//Desired_Completion_Date__c,
							//DTS_Tracking_ID__c,
							End_Date_Range_of_Results__c,
							//Entered_into_DTS_for_HCFA_By__c,
							//Enter_into_DTS_for_HCFA_Requested__c,
							//Enter_into_DTS_for_HCFA_Requested_Date__c,
							//Enter_into_DTS_for_HCFA_Status__c,
							Existing_FS_ID__c,
							Fee_Schedule_ID__c,
							Fee_Schedule_Types__c,
							FFS_1__c,
							FFS_2__c,
							FFS_3__c,
							FFS_4__c,
							File_Type__c,
							Geography__c,
							HB_Primary__c,
							//HCE_Analyst__c,
							//HCE_Analyst_Closed_Date__c,
							//HCE_Analyst_Status__c,
							//NIS_Explanation__c,
							//NIS_Modified_Due_Date__c,
							HCPCS_Drug_ASP__c,
							HCPCS_Drug_ASP_Value__c,
							HCPCS_Drug_AWP__c,
							HCPCS_Drug_AWP_Value__c,
							HCPCS_Level_I_and_II_CLAB__c,
							HCPCS_Level_I_and_II_CLAB_Value__c,
							HCPCS_Level_I_E_M__c,
							HCPCS_Level_I_E_M_Value__c,
							HCPCS_Level_II_AMB__c,
							HCPCS_Level_II_AMB_Value__c,
							HCPCS_Level_II_DME_POS__c,
							HCPCS_Level_II_DME_POS_Value__c,
							HCPCS_Level_II_Not_Specified__c,
							HCPCS_Level_II_Not_Specified_Value__c,
							HCPCS_Level_II_PEN__c,
							HCPCS_Level_II_PEN_Value__c,
							HCPCS_Level_II_RBRVS__c,
							HCPCS_Level_II_RBRVS_Value__c,
							HCPCS_Level_I_Medicine__c,
							HCPCS_Level_I_Medicine_Value__c,
							HCPCS_Level_I_Pathology__c,
							HCPCS_Level_I_Pathology_Value__c,
							HCPCS_Level_I_Radiology__c,
							HCPCS_Level_I_Radiology_Value__c,
							HCPCS_Level_I_Surgery__c,
							HCPCS_Level_I_Surgery_Value__c,
							HEOS_Current_pct_Medicare__c,
							HEOS_Current_Discount__c,
							HEOS_Negotiated_pct_of_Medicare__c,
							HEOS_Negotiated_Discount__c,
							Locality__c,
							Locality_Name__c,
							Locality_Name_Override__c,
							Map_Version__c,
							Market_Id__c,
							Market_Name__c,
							Market_Type__c,
							Market_Type_Description__c,
							Mid_Level_Reduction__c,
							MP3_Contract_or_Hcode__c,
							MPI_of_Medicare__c,
							Status_Current_1__c,
							MPI_Negotiated_of_Medicare__c,
							Status_Negotiated_1__c,
							Region__c,
							MSA__c,
							Negotiation_Sub_Type__c,
							Negotiation_Type__c,
							Network__c,
							Not_Otherwise_Specified_Default__c,
							Not_Otherwise_Specified_Default_Value__c,
							NIS_Analyst_Lead__c,
							NS_NIS_Req__c,
							Number_of_Facilities__c,
							PHCS_of_Medicare__c,
							Status_Current_2__c,
							PHCS_Negotiated_of_Medicare__c,
							Status_Negotiated_2__c,
							Product_Notes__c,
							Products__c,
							Product_Selection__c,
							PROV_ID__c,
							Provider_Name__c,
							Provider_TINs__c,
							Provider_Type__c,
							Rate_Data_Source_Year__c,
							//Rate_Exhibit_Review_Approval_By__c,
							//Rate_Exhibit_Review_Approval_Date__c,
							//Rate_Exhibit_Review_Approval_Status__c,
							//Rate_Exhibit_Review_Notes__c,
							//Rate_Exhibit_Review_Request_Date__c,
							//Rate_Negotiation_Status__c,
							Rate_Operations_Request_Type__c,
							Fee_Schedule_Year_Comparison__c,
							Region_Name__c,
							Reimbursement_Effective_Date__c,
							Reimbursement_Type__c,
							Reimbursement_Type_POC_Rate__c,
							//Request_Rate_Exhibit_Review__c,
							Request_Description__c,
							Request_Output__c,
							//Request_Status__c,
							Request__c,
							Savility_of_Medicare__c,
							Status_Current_3__c,
							Savility_Negotiated_of_Medicare__c,
							Status_Negotiated_3__c,
							Specialty__c,
							State__c,
							Summary_Details_del__c,
							U_S_National_Average__c,
							//Worker_Comp_Rate_POC_Rate__c,
							Workers_Comp_Rate_of_the_State_FS__c,
							Workers_Comp_Rate_Lesser_Of__c,
							WP_Wrap__c,
							ZIP_3__c,
							FS_ZIP_3__c,
							ZIP_5__c,
							Zip_Code__c,
							Cloned_From_ND_HCE_Request__c,
							ND_HCE_Request_Group__c,
							//Entered_into_DTS_for_HCFA_Date,
							Requester__c 
            		from NS_NIS_Request__c 
            		where id = : r.id]; 
				
				//--------------------------------------------------------------
  				// Set your own defaults
  				//--------------------------------------------------------------
  					v2.id = null;
  					v2.Cloned_From_ND_HCE_Request__c = r.id; 
  					//v2.Requester__c = r.Requester__c;
  					if(v2.Requester__c != null) {
  						v2.ownerid = v2.Requester__c;
  					}else{
  						v2.ownerid = UserInfo.getUserId();
  					}

    }  
       
    public pageReference cloneRecord() {
    	
    	//Create and associate Request Group
    	if(v2.ND_HCE_Request_Group__c == null) {
    		ND_HCE_Request_Group__c genRG = new ND_HCE_Request_Group__c(Name = v2.name);
    		
    		try{
    			insert genRG;
    		}catch(exception e) { ApexPages.addMessages(e); return null; }
    		
    		r.ND_HCE_Request_Group__c = genRG.id;
    		v2.ND_HCE_Request_Group__c = genRG.id;
    		
    		try{
    			update r;
    		}catch(exception e) { ApexPages.addMessages(e); return null; }
    	}
		
		//Insert the newly clones record
		try{
			insert v2;
		}catch(exception e){ ApexPages.addMessages(e); return null;}

        return new pagereference('/'+ v2.id+'/e?retURL=/'+v2.id);   
    }
}