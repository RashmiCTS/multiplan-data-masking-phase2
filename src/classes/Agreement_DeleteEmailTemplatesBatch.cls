/*
File Name:   Agreement_DeleteEmailTemplatesBatch
Author:      Rashmi T  
Date:        12/20/2016
Description: The docusign API implemented by Apptus could not resolve the Agreement Number present in the email template 
				of V5_Group_Email and hence the Agreement_CreateEmailTemplatesBatch batch serves as a workaround which creates an email template for every Agreement
				to embed the agreement number in itself. After the email has been sent out the redundant email templates have to 
				be delete to keep the data clean and therefore this batch deletes all the temporary email templates that were created
				to handle the problem of agreement number merge fields not being populated on the agreement.

Modification History
12/20/2016   Rashmi T   Created
*/ 
public class Agreement_DeleteEmailTemplatesBatch implements Database.Batchable<sObject> { 
    public Boolean isDeleteAllTemp = false;
    public Agreement_DeleteEmailTemplatesBatch(){
        //constructor
    }
    
    public Agreement_DeleteEmailTemplatesBatch(Boolean isDeleteAll){
        //constructor
        isDeleteAllTemp = isDeleteAll;
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC) {
    	
        String strQuery = 'SELECT Id, Name,DeveloperName FROM EmailTemplate WHERE Folder.DeveloperName=\'Temp_Email_Folder\'';
        return Database.getQueryLocator(strQuery);
    }
    
    public void execute(Database.BatchableContext BC, List<EmailTemplate> scope) {
        Map<String,List<EmailTemplate>> mapStrETNameVSLstEmailtemp = new Map<String,List<EmailTemplate>>();
        List<EmailTemplate> lstDeleteEmTemp = new List<EmailTemplate>();
        Set<EmailTemplate> setDeleteEmTemp = new Set<EmailTemplate>();
        for(EmailTemplate et : scope){
            if(isDeleteAllTemp){
                lstDeleteEmTemp.add(et);
            }
            else{
                if(mapStrETNameVSLstEmailtemp.containsKey(et.DeveloperName)){
                    mapStrETNameVSLstEmailtemp.get(et.DeveloperName).add(et);
                }
                else{
                    mapStrETNameVSLstEmailtemp.put(et.DeveloperName,new List<EmailTemplate> {et});
                }
            }            
        }
        if(!mapStrETNameVSLstEmailtemp.isEmpty()){
            for(Apttus_DocuApi__DocuSignDefaultRecipient2__c rc : [SELECT id,Email_Template_Prev_Assigned__c FROM Apttus_DocuApi__DocuSignDefaultRecipient2__c
                                                               WHERE Email_Template_Prev_Assigned__c IN: mapStrETNameVSLstEmailtemp.keySet()]){
        		
            	setDeleteEmTemp.addAll(mapStrETNameVSLstEmailtemp.get(rc.Email_Template_Prev_Assigned__c)); 	 
            }
            if(!setDeleteEmTemp.isEmpty()){
                lstDeleteEmTemp.addAll(setDeleteEmTemp);
                
            }
        }
        if(!lstDeleteEmTemp.isEmpty()){
            delete lstDeleteEmTemp;
        }        
    }
    
    public void finish(Database.BatchableContext BC) { 
        AsyncApexJob aaj = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
                                TotalJobItems, CreatedBy.Email
                            FROM AsyncApexJob WHERE Id = :BC.getJobId()];
        System.debug('****** aaj '+aaj);        
    }
}