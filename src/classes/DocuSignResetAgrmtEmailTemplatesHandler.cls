public class DocuSignResetAgrmtEmailTemplatesHandler {
    
    public static void resetEmailTemplates(List<Apttus_DocuApi__DocuSignEnvelope__c> lstDocSignEnvelope){
        Set<Id> setAgmtIds = new Set<Id>();
        List<Apttus_DocuApi__DocuSignDefaultRecipient2__c> lstDocSIgnRecp = new List<Apttus_DocuApi__DocuSignDefaultRecipient2__c>(); 
        Apttus_DocuApi__DocuSignDefaultRecipient2__c dsRec = new Apttus_DocuApi__DocuSignDefaultRecipient2__c();
        if(!lstDocSignEnvelope.isEmpty()){
            for(Apttus_DocuApi__DocuSignEnvelope__c appDoc : lstDocSignEnvelope){
                if(appDoc.Apttus_CMDSign__Agreement__c <> NULL && appDoc.Apttus_DocuApi__Status__c <> NULL &&
                   (appDoc.Apttus_DocuApi__Status__c.equalsIgnoreCase('sent'))){
                    setAgmtIds.add(appDoc.Apttus_CMDSign__Agreement__c);
                }
            }
            if(!setAgmtIds.isEmpty()){
                for(Apttus_DocuApi__DocuSignDefaultRecipient2__c recp :  [SELECT id,Name,Email_Template__c,Email_Template_Prev_Assigned__c,Apttus_CMDSign__AgreementId__c 
                                                                            FROM Apttus_DocuApi__DocuSignDefaultRecipient2__c 
                                                                          WHERE Apttus_CMDSign__AgreementId__c IN: setAgmtIds AND Apttus_CMDSign__AgreementId__r.RecordType.DeveloperName='Group_V5']){
                	
                    if(recp.Email_Template__c.startsWith('TEMP_') && String.isNotBlank(recp.Email_Template_Prev_Assigned__c)){
                    	dsRec = new Apttus_DocuApi__DocuSignDefaultRecipient2__c();
                        dsRec.Id = recp.Id;
                        dsRec.Email_Template_Prev_Assigned__c = recp.Email_Template__c;
                        dsRec.Email_Template__c = recp.Email_Template_Prev_Assigned__c;
                        lstDocSIgnRecp.add(dsRec);
                    }	                                                              
                }                                                                           
            }
            if(!lstDocSIgnRecp.isEmpty()){
                update lstDocSIgnRecp;
            }
        }
    }
}