/* APTS_AgreementApprovalEmailContTest
 * Test Class for APTS_AgreementApprovalEmailController.
 *
 * Developer: Kruti Shah, APTTUS
 * Business Owner: 
 *
 * Scenario:
 * 
 * History: 
 * 02/14/2017, Kruti Shah,  APTTUS - created APTS_AgreementApprovalEmailContTest.
 */
@isTest
public class APTS_AgreementApprovalEmailContTest {
    public static Account testAcc;
    public static Contact testCon;
    static Apttus__Agreement_Clause__c agrClause;
    static Apttus__APTS_Template__c testTemplate;
    
    static{
        
        testAcc = new Account();
        testAcc.name = 'testA';
        insert testAcc;
        
        testCon = new Contact();
        testCon.AccountId = testAcc.Id;
        testCon.lastName = 'agreementInsert_Test';
        testCon.email = 'agreementInsert_Test@Test.com';
        insert testCon;
        
        testTemplate = new Apttus__APTS_Template__c();
        testTemplate.Name = 'Section 1.02 Billed Charges - Option 02 (Group 5)';
        testTemplate.Apttus__Type__c = 'Clause';
        testTemplate.Apttus__IsActive__c = true;
        testTemplate.Apttus__Agreement_Types__c = 'Group V5 - Amendment; Group - Region; Group Region - Amendment';
        insert testTemplate;
        
        Id group5RTId = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('Group V5').getRecordTypeId();
        
        Apttus__APTS_Agreement__c testAgr = new Apttus__APTS_Agreement__c();
        testAgr.Apttus__Account__c = testAcc.id;
        testAgr.recordtypeid = group5RTId;
        testAgr.Apttus__Primary_Contact__c = testCon.id;
        testAgr.Market_State__c = 'AK';
        testAgr.Region__c = 'West';
        insert testAgr;      
        
        Zip_Code__c objZIPCode = new Zip_Code__c();
        objZIPCode.Ambulance_Locality__c = 'ambulanceLocality';
        objZIPCode.CLAB_Locality__c = 'CLABLocality';
        insert objZIPCode;        
        
        Id requestRecordTypeId = Schema.SObjectType.NS_NIS_Request__c.getRecordTypeInfosByName().get('Reimbursement and Implementation - HCFA').getRecordTypeId(); 
        //[Select Name, Id From RecordType where sObjectType='NS_NIS_Request__c' and Name='Reimbursement and Implementation - HCFA'];
                
        NS_NIS_Request__c testNISRequest = new NS_NIS_Request__c();
        testNISRequest.RecordTypeId = requestRecordTypeId;
        testNISRequest.Account__c = testAcc.Id;
        testNISRequest.User__c = UserInfo.getUserId();
        testNISRequest.Agreement__c = testAgr.Id;
        testNISRequest.Zip_Code__c = objZIPCode.Id;        
        testNISRequest.Name = 'test ND-HCE record';
        testNISRequest.Reimbursement_Type__c = 'Unique';
        testNISRequest.Anesthesia_Conversion_Factor_Value__c = 15;
        testNISRequest.Anesthesia_Conversion_Factor__c = '% of Billed Charges';
        testNISRequest.HCPCS_Level_I_Surgery__c = '% of Billed Charges';
        testNISRequest.HCPCS_Level_I_Radiology__c = '% of Billed Charges';
        testNISRequest.HCPCS_Level_I_Pathology__c = '% of Billed Charges';
        testNISRequest.HCPCS_Level_I_Medicine__c = '% of Billed Charges';
        testNISRequest.HCPCS_Level_I_E_M__c = '% of Billed Charges';
        testNISRequest.HCPCS_Level_II_RBRVS__c = '% of Billed Charges';
        testNISRequest.HCPCS_Level_I_and_II_CLAB__c = '% of Billed Charges';
        testNISRequest.HCPCS_Level_II_DME_POS__c = '% of Billed Charges';
        testNISRequest.HCPCS_Level_II_PEN__c = '% of Billed Charges';
        testNISRequest.HCPCS_Level_II_AMB__c = '% of Billed Charges';
        testNISRequest.HCPCS_Drug_ASP__c = '% of Billed Charges';
        testNISRequest.HCPCS_Drug_AWP__c = '% of Billed Charges';
        testNISRequest.HCPCS_Level_II_Not_Specified__c = '% of Billed Charges (BP)';
        testNISRequest.Not_Otherwise_Specified_Default__c = '% of Billed Charges (BP)';
        testNISRequest.HCPCS_Level_I_Surgery_Value__c = 105;
        testNISRequest.HCPCS_Drug_ASP_Value__c = 101;
        testNISRequest.HCPCS_Level_II_PEN_Value__c = 105;
        testNISRequest.HCPCS_Level_I_Radiology_Value__c = 25;
        testNISRequest.HCPCS_Level_I_Pathology_Value__c = 35;
        testNISRequest.HCPCS_Level_I_Medicine_Value__c = 45;
        testNISRequest.HCPCS_Level_I_E_M_Value__c = 55;
        testNISRequest.HCPCS_Level_II_RBRVS_Value__c = 65;
        testNISRequest.HCPCS_Level_I_and_II_CLAB_Value__c = 75;
        testNISRequest.HCPCS_Level_II_DME_POS_Value__c = 85;        
        testNISRequest.HCPCS_Level_II_AMB_Value__c = 15;        
        testNISRequest.HCPCS_Drug_AWP_Value__c = 105;
        testNISRequest.HCPCS_Level_II_Not_Specified_Value__c = 55;
        testNISRequest.Not_Otherwise_Specified_Default_Value__c = 5; 
        /*
        testNISRequest.State__c = 'AZ';
        testNISRequest.Locality_year__c = '2017';
        testNISRequest.State_and_Year__c = 'AZ-0000';
        */
        insert testNISRequest;
        
        agrClause = new Apttus__Agreement_Clause__c();
        agrClause.Apttus__Agreement__c = testAgr.id;
        agrclause.Apttus__Template__c = testTemplate.Id;
        agrClause.Apttus__Action__c = CONSTANTS.INSERTED;
        agrClause.Apttus__Clause__c = 'test clause to be insert';
        agrClause.Apttus__Active__c = true;       
        insert agrClause;
    } 
    
    public static testmethod void testmethod1(){                
        Apttus_Approval__Approval_Request__c approvalRequestTest = new Apttus_Approval__Approval_Request__c();
        approvalRequestTest.Apttus_Approval__Approval_Status__c = 'Assigned';
        approvalRequestTest.Apttus_Approval__ChildObjectId__c = String.valueOf(agrClause.Id);    
        approvalRequestTest.Apttus_Approval__ChildObjectType__c = 'Apttus__Agreement_Clause__c';
        approvalRequestTest.Apttus_Approval__ChildObjectName__c = 'Agreement Clause';
        approvalRequestTest.Apttus_Approval__Active__c = true;
        insert approvalRequestTest;
        
        Apttus__APTS_Agreement__c testAgr2 = [select ID,name from Apttus__APTS_Agreement__c WHERE Market_State__c='AK'];
        List<NS_NIS_Request__c> listOfNISRequests = new List<NS_NIS_Request__c>();
        List<NS_NIS_Request__c> listOfClosedNISRequests = new List<NS_NIS_Request__c>();
        String agreementID;
        
        Test.startTest();
        APTS_AgreementApprovalEmailController objController = new APTS_AgreementApprovalEmailController();
        objController.approvalRequestSO = approvalRequestTest;
        objController.agrID = testAgr2.ID;
        objController.setAgrID(testAgr2.ID);
        agreementID = objController.getAgrID();
        listOfNISRequests = objController.getlistOfNS_NIS_Request();
        listOfClosedNISRequests = objController.getlistOfClosedRequests();
        Test.stopTest();     
    }
    

}