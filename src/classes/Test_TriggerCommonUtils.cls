/*
File Name:   TriggerCommonUtils.cls
Author:      Jason Hartfield
Date:        8/9/2016
Description: Template triggers

Modification History
Modified By			Date			Description

*/

@isTest
private class Test_TriggerCommonUtils {
	
	@isTest static void testPreventDuplicateNames() {
		Apttus__APTS_Template__c t1 = new Apttus__APTS_Template__c(Name = 'MyName');
		insert t1;
		Apttus__APTS_Template__c t2 = new Apttus__APTS_Template__c(Name = 'MyName');
		try{
			
			insert t2;
			system.assert(true,'Expected an exception here!');
		} catch(exception ex){
			// Good, name duplication was caught.
			// Make sure it is the correct exception though...
			system.assert(ex.getMessage().contains('item alredy exists with this name'));
		}
		t2.Name = 'MyNewName';
		insert t2;
		// Expect no error
		try{
			 t1.Name = 'MyNewName';
			 update t1;
		 	 system.assert(true,'Expected an exception here!');
		 } catch(exception ex){
		 	// Good, duplicate was caught.
		 	system.assert(ex.getMessage().contains('item alredy exists with this name'));
		 }
	}	
}