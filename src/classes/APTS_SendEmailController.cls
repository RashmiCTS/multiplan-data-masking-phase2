/* APTPS_SendEmailController
 * Controller class for APTPS_SendEmail page. 
 *
 * Developer: Harish Emmadi, APTTUS
 * Business Owner: 
 *
 * Scenario:
 * 
 * 
 *
   ***************************************************************************************************
   Modification Log:
   *
   *    Harish Emmadi, 07/20/2016 - Created.
   *    
   ***************************************************************************************************
*/
public with sharing class APTS_SendEmailController {
	
	// public getter/setters.
	public Apttus__APTS_Agreement__c agreementSO{get;private set;}

	public Boolean isInitialized{get;set;}
	
	public List<AttachmentWrapper> attachments{get;set;}
	public String attachmentIds{get;set;}

	public String documentId{get;set;}

	public Id folderId{get;set;}
	public List<SelectOption> folders{get;set;}

	public List<EmailTemplateWrapper> templates{get;set;}
	public Id emailTemplateId{get;set;}
	public String emailAuthorURL{get;set;}

	public Boolean isSortByName{get;set;}
	public Boolean isSortByTemplateType{get;set;}
	public Boolean isSortByDescription{get;set;}
	public String sortImageURL{get;set;}
	public String sortImageTitle{get;set;}

	public Boolean protect{get;set;}
	public Boolean unprotect{get;set;}
	public String protectionLevel{get;set;}
	public List<SelectOption> protectionLevels{get;set;}

	// control the visibility of sections...AJAX behaviour.
	// public Boolean showStep2{get;set;}
	// public Boolean showStep3{get;set;}
	public Boolean showStep4{get;set;}
	public Boolean showPrevious{get;set;}
	public Boolean showAttachments{get;set;}
	public Boolean showTemplates{get;set;}
	public Boolean IsEmailAuthoringStep{get;set;}
	public Boolean IsLastStep{get;set;}

	/*public Boolean hasParent{get;set;}
	public Boolean includeParent{get;set;}
	public Boolean hasRelated{get;set;}
	public Boolean includeRelated{get;set;}
	public Boolean hasChildren{get;set;}
	public Boolean includeChildren{get;set;}
	public Boolean hasAmendRenew{get;set;}
	public Boolean includeAmendRenew{get;set;}
	public Boolean hasSiblings{get;set;}
	public Boolean includeSiblings{get;set;}*/

	// configurable settings using Admin setting....TBD
	public Boolean canEditAgreement{get;set;}
	public Boolean SkipEmailTemplateSelection{get;set;}

	public Id tempObjectId{get;set;}
	public Id tempEmailTemplateId{get;set;}
	public String stepTitle{get;set;}

	// private variables.
	static final String ASSOCIATION_TYPE_PRIMARY 		= 'Primary';
	/*static final String ASSOCIATION_TYPE_PARENT 		= 'Parent';
	static final String ASSOCIATION_TYPE_CHILDREN 		= 'Children';
	static final String ASSOCIATION_TYPE_RELATED 		= 'Related';
	static final String ASSOCIATION_TYPE_AMENDRENEW 	= 'AmendRenew';
	static final String ASSOCIATION_TYPE_SIBLINGS 		= 'Siblings';*/

	Map<Id, String> agreementIdtoRelationShipTypeMap;
	Map<String, List<AttachmentWrapper>> associationtoaWrapListMap;
	Map<Id, List<EmailTemplate>> folderIdtoEmailTemplatesMap;
	String actionType;

	public APTS_SendEmailController(ApexPages.StandardController controller) {
		this.agreementSO = (Apttus__APTS_Agreement__c)controller.getRecord();
		protectionLevel = 'Ignore';

		//actionType = System.currentPageReference().getParameters().get('actionType');
		actionType = 'Send_To_Other_Party_For_Review';		
		system.debug('APTTUS--> actionType '+ actionType);
		String retURL = '%2Fapex%2FAPTS_SendEmailPostProcessing%3FuserAction%3D'+actionType+'%26id%3D'+this.agreementSO.Id+
					 	'%26newTemplateId%3D__NEW_TEMPLATE_ID_16CHARS__';
                	
		emailAuthorURL = '/_ui/core/email/author/EmailAuthor?new_template=1&p3_lkid='+this.agreementSO.Id+
						 '&saveURL='+retURL+'&template_id=__NEW_TEMPLATE_ID_16CHARS__';		
		
		/*Map<String, Apttus__APTS_Admin__c> adminNametoAdminSOMap = new Map<String, Apttus__APTS_Admin__c>();
		Set<String> adminNames = new Set<String>{APTPS_CONSTANTS.ADM_EMAIL_TEMPLATE_REVIEW, APTPS_CONSTANTS.ADM_SKIPEMAILTEMPLATE_SELECTION};
		for(Apttus__APTS_Admin__c ad : APTPS_AdminSelector.selectActiveAdminByName(adminNames))
		{
			adminNametoAdminSOMap.put(ad.Name, ad);
		}

		Apttus__APTS_Admin__c skipEmailAdmin = adminNametoAdminSOMap.get(APTPS_CONSTANTS.ADM_SKIPEMAILTEMPLATE_SELECTION);
		SkipEmailTemplateSelection = false;
		if(skipEmailAdmin != null
			&& String.isNotBlank(skipEmailAdmin.Apttus__Value__c))
		{
			SkipEmailTemplateSelection = Boolean.valueOf(skipEmailAdmin.Apttus__Value__c);
		}
		
		Apttus__APTS_Admin__c emailTemplateNameAdmin = adminNametoAdminSOMap.get(APTPS_CONSTANTS.ADM_EMAIL_TEMPLATE_REVIEW);
		if(emailTemplateNameAdmin != null
			&& String.isNotBlank(emailTemplateNameAdmin.Apttus__Value__c))
		{
			List<EmailTemplate> etList = [Select Id, FolderId, Name, TemplateType, Description 
												From EmailTemplate
													Where Name = :emailTemplateNameAdmin.Apttus__Value__c limit 1];
			if(!etList.isEmpty())
				emailTemplateId = etList.get(0).Id;
		}*/

		// isInitialized = false;
	}

	public void doInit(){
		showAttachments = true;
		showTemplates = false;
		canEditAgreement = true;
		
		stepTitle = 'Select Attachments';
		
		agreementIdtoRelationShipTypeMap = new Map<Id, String>();
		
		agreementIdtoRelationShipTypeMap.put(this.agreementSO.Id, ASSOCIATION_TYPE_PRIMARY);

		/*
		hasParent = this.agreementSO.Apttus__Parent_Agreement__c != null ? true : false;
		agreementIdtoRelationShipTypeMap.put(this.agreementSO.Apttus__Parent_Agreement__c, ASSOCIATION_TYPE_PARENT);
		
		hasAmendRenew = false;
		for(Apttus__APTS_Related_Agreement__c relAgr : [Select Id, Apttus__APTS_Contract_From__c, Apttus__Relationship_From_Type__c, Apttus__APTS_Contract_To__c, Apttus__Relationship_To_Type__c 
																		From Apttus__APTS_Related_Agreement__c 
																			Where Apttus__APTS_Contract_From__c = :this.agreementSO.Id
																				  OR Apttus__APTS_Contract_To__c = :this.agreementSO.Id]){

			if(relAgr.Apttus__Relationship_From_Type__c.contains('Renewal'))
			{
				hasAmendRenew = true;
				agreementIdtoRelationShipTypeMap.put(relAgr.Apttus__APTS_Contract_To__c, ASSOCIATION_TYPE_AMENDRENEW);	
			}	
		}

		hasChildren = false;
		for(Apttus__APTS_Agreement__c agr:  [Select Id 
													From Apttus__APTS_Agreement__c
														Where Apttus__Parent_Agreement__c = :this.agreementSO.Id])
		{
			hasChildren = true;
			agreementIdtoRelationShipTypeMap.put(agr.Id, ASSOCIATION_TYPE_CHILDREN);	
		}*/	
		
		// Related Agreements and Siblings to be implemented.....Not sure How to find the related agreements and siblings.
		//hasRelated = false;
		//hasSiblings = false;

		//includeParent		=	false;
		//includeChildren		=	false;
		//includeRelated		=	false;
		//includeAmendRenew	=	false;
		//includeSiblings		=	false;

		// showStep2 = false;
		// showStep3 = false;
		showStep4 = false;

		associationtoaWrapListMap = new Map<String, List<AttachmentWrapper>>();
		Map<Id, Apttus__APTS_Agreement__c> agrIdtoAgreementSOMap = new Map<Id, Apttus__APTS_Agreement__c>([Select Id, Name, Apttus__Status_Category__c, Apttus__FF_Agreement_Number__c, Apttus__VersionAware__c, Apttus__Status__c 
																												From Apttus__APTS_Agreement__c
																													Where Id IN :agreementIdtoRelationShipTypeMap.KeySet()]);
		 
		for(Attachment attch : [Select Id, Name, BodyLength, LastModifiedDate, ParentId 
									From Attachment
										Where ParentId IN :agreementIdtoRelationShipTypeMap.KeySet()])
		{
			String relationship =  agreementIdtoRelationShipTypeMap.get(attch.ParentId);
			AttachmentWrapper aW = new AttachmentWrapper(attch, agrIdtoAgreementSOMap.get(attch.ParentId), relationship);
			List<AttachmentWrapper> aWList = associationtoaWrapListMap.containsKey(relationship) ? associationtoaWrapListMap.get(relationship) : new List<AttachmentWrapper>();
			aWList.add(aW);
			associationtoaWrapListMap.put(relationship, aWList);
		}
		
		String documentObjId = Document.sObjectType.getDescribe().getKeyPrefix();
		for(Apttus__Agreement_Document__c ad : [Select Id, Name, Apttus__URL__c, Apttus__Agreement__c, LastModifiedDate
													From Apttus__Agreement_Document__c
														Where Apttus__Agreement__c = :this.agreementSO.Id
															Order by CreatedDate DESC Limit 1])
		{
			String relationship =  ASSOCIATION_TYPE_PRIMARY;//agreementIdtoRelationShipTypeMap.get(attch.ParentId);
			String docId = ad.Apttus__URL__c.substringAfterLast('=');
			// we are not interested if the documentId is blank or not document.
			if(String.isBlank(docId)
				|| !docId.startsWith(documentObjId))
				continue;
			AttachmentWrapper aW = new AttachmentWrapper(ad, docId, agrIdtoAgreementSOMap.get(ad.Apttus__Agreement__c), relationship);
			List<AttachmentWrapper> aWList = associationtoaWrapListMap.containsKey(relationship) ? associationtoaWrapListMap.get(relationship) : new List<AttachmentWrapper>();
			aWList.add(aW);
			associationtoaWrapListMap.put(relationship, aWList);
		}

		folderIdtoEmailTemplatesMap = new Map<Id, List<EmailTemplate>>();
		for(EmailTemplate et : [Select Id, FolderId, Name, TemplateType, Description From EmailTemplate]){
			List<EmailTemplate> ets = folderIdtoEmailTemplatesMap.containsKey(et.folderId) ? folderIdtoEmailTemplatesMap.get(et.folderId) : new List<EmailTemplate>();
			ets.add(et);
			folderIdtoEmailTemplatesMap.put(et.folderId, ets);
		}
		
		folders = new List<SelectOption>();
		for(Folder f : [select Id, Name, DeveloperName 
							From Folder 
								Where Id = :folderIdtoEmailTemplatesMap.KeySet()
									Order by Name]){
			
			// default to ApttusEmailTemplates folder.
			if(f.DeveloperName == 'ApttusEmailTemplates')
				folderId = f.Id;
			
			folders.add(new SelectOption(f.Id, f.Name));
		}

		// default to random folder if not defaulted to anything.
		folderId = folderId == null ? folders.get(0).getValue() : folderId;
		
		doAttachmentSearch();

		isInitialized = true;
	}

	public PageReference doCancel(){
		return new PageReference('/'+this.agreementSO.Id);
	}

	public PageReference doPrevious(){
		// From 'Show Attachments From' to attachments.
		// hide step 2 and previous button and reload attachments.
		/*if(showStep2 == true)
		{
			showPrevious = false;
			showStep2 = false;
			
			doAttachmentSearch();
			stepTitle = getSetepTitle();
			return null;
		}*/
		
		/*// From 'Protection level' to 'Show Attachments From'. 
		stepTitle = 'Show Attachments From';
		showStep3 = false;
		return null;

		// from Email Template to 'Protection level'*/

		// from Email Template to 'Show Attachments From'
		if(showStep4 == true)
		{
			showStep4 = false;
			showPrevious = false;
			//showStep2 = true;

			showAttachments = true;
			doAttachmentSearch();
			showTemplates = false;
			stepTitle = getSetepTitle();
			
			return null;	
		}
		
		return null;
	}

	// Will not be called from Email template screen.
	public PageReference doNext(){
		// from 'Attachments' to 'Show Attachments From'
		/*if((hasParent
			|| hasRelated
			|| hasChildren
			|| hasAmendRenew
			|| hasSiblings)
			&& showStep2 == false)
		{
			showStep2 = true;
			showPrevious = true;

			doAttachmentSearch();
			stepTitle = getSetepTitle();
			return null;
		}*/

		// From 'Show Attachments From' to protection page.
		/*stepTitle = 'Select Protection Level';
		showStep2 = false;
		showAttachments = false;
		return null;

		// From 'Protection Page' to Email template page.*/
		

		// From 'Show Agreements From' to Email template page.
		if(showStep4 == false)
		{
			attachmentIds = '';
			for(List<AttachmentWrapper> aWList : associationtoaWrapListMap.values())
			{		
				for(AttachmentWrapper aw : aWList)
				{
					if(aw.selected){
						if(aw.isAttachment == true)
							attachmentIds += aw.attachment.Id+',';
						else
							documentId = aw.docId;
					}
				}
			}

			// remove the last comma if added.
			attachmentIds = String.isNotBlank(attachmentIds) ? attachmentIds.subString(0, attachmentIds.length()-1) : '';
			
			if(String.isBlank(attachmentIds)
				&& String.isBlank(documentId))
			{
				ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR, 'At least one document or attachment is required to continue.'));
				return null;
			}

			// search only if email template section is not skipped.
			if(SkipEmailTemplateSelection == true)
				IsEmailAuthoringStep = true;
			else{
				doEmailTemplateSearch();
				showStep4 = true;
			}
			
			// do not show step3.
			// showStep3 = false;
			// showStep2 = false;
			// showStep4 = true;

			// do not show attachments.
			showAttachments = false;
			showTemplates = true;

			stepTitle = getSetepTitle();
			return null;
		}
		return null;
	}

	public void doAttachmentSearch(){
		attachments = new List<AttachmentWrapper>();
		// commented to hide step2.
		/*if(showStep2 == true)
		{
			if(includeParent
				|| includeChildren
				|| includeRelated
				|| includeAmendRenew
				|| includeSiblings)
			{
				if(includeParent
					&& associationtoaWrapListMap.containsKey(ASSOCIATION_TYPE_PARENT))
				{
					for(AttachmentWrapper aW : associationtoaWrapListMap.get(ASSOCIATION_TYPE_PARENT)){
						attachments.add(aW);
					}
				}
				if(includeChildren
					&& associationtoaWrapListMap.containsKey(ASSOCIATION_TYPE_CHILDREN))
				{
					for(AttachmentWrapper aW : associationtoaWrapListMap.get(ASSOCIATION_TYPE_CHILDREN)){
						attachments.add(aW);
					}
				}
				if(includeRelated
					&& associationtoaWrapListMap.containsKey(ASSOCIATION_TYPE_RELATED))
				{
					for(AttachmentWrapper aW : associationtoaWrapListMap.get(ASSOCIATION_TYPE_RELATED)){
						attachments.add(aW);
					}
				}
				if(includeAmendRenew
					&& associationtoaWrapListMap.containsKey(ASSOCIATION_TYPE_AMENDRENEW))
				{
					for(AttachmentWrapper aW : associationtoaWrapListMap.get(ASSOCIATION_TYPE_AMENDRENEW)){
						attachments.add(aW);
					}
				}
				if(includeSiblings
					&& associationtoaWrapListMap.containsKey(ASSOCIATION_TYPE_SIBLINGS))
				{
					for(AttachmentWrapper aW : associationtoaWrapListMap.get(ASSOCIATION_TYPE_SIBLINGS)){
						attachments.add(aW);
					}
				}
			}
		}else if(associationtoaWrapListMap.containsKey(ASSOCIATION_TYPE_PRIMARY)){
			for(AttachmentWrapper aW : associationtoaWrapListMap.get(ASSOCIATION_TYPE_PRIMARY)){
				attachments.add(aW);
			}
		}*/
		// added to show attachments only for primary agreement by hiding step2.
		// added if to fix OS-887
		if(associationtoaWrapListMap.containsKey(ASSOCIATION_TYPE_PRIMARY))
		{
			for(AttachmentWrapper aW : associationtoaWrapListMap.get(ASSOCIATION_TYPE_PRIMARY)){
				attachments.add(aW);
			}	
		}
	}

	public void doEmailTemplateSearch(){
		templates = new List<EmailTemplateWrapper>();
		if(folderIdtoEmailTemplatesMap.containsKey(folderId))
			for(EmailTemplate et : folderIdtoEmailTemplatesMap.get(folderId)){
				EmailTemplateWrapper etWrap = new EmailTemplateWrapper(et);
				if(et.Id == emailTemplateId)
					etWrap.selected = true;
				templates.add(etWrap);
			}
	}

	// Sort Will be implemented later. - TBD
	public PageReference doSort(){
		return null;
	}

	public void doSelectEmailTemplate(){
		for(EmailTemplateWrapper etW : templates)
		{
			if(etW.emailTemplate.Id == emailTemplateId)
				etW.selected = true;
			else
				etW.selected = false;
		}
	}

	public PageReference doDeleteTempEmailTemplate(){
		// delete temporary email template.
		Delete [Select Id From EmailTemplate Where Id = :tempEmailTemplateId];
		return null;
	}

	public PageReference doDeleteTempObject(){
		// delete temporary object entry.
		Delete [Select Id From Apttus__TempObject__c Where Id = :tempObjectId];
		return null;
	}

	public void doValidateEmailTemplateSelection(){
		if(String.isBlank(emailTemplateId))
			ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.WARNING, 'Email template selection to be made to continue.'));
	}

	private String getSetepTitle(){
		// updated to hide step2.
		/*if(showStep2 == true)
			return 'Show Attachments From';*/
		/*else if(showStep3 == true)
			return 'Select Protection Level';*/
		if(showStep4 == true)
			return 'Select Email Template (Optional)';
		else return 'Select Attachments';

	}

	public class AttachmentWrapper{
		public Boolean selected{get;set;}
		public Attachment attachment{get;private set;}
		public Apttus__APTS_Agreement__c agmt{get;set;}
		public String agmtAssociationType{get;set;}
		public Apttus__Agreement_Document__c agrDocument{get;set;}
		public String docId{get;set;}
		public Boolean isAttachment{get;set;}
		public AttachmentWrapper(Attachment attachment, Apttus__APTS_Agreement__c agmt, String agmtAssociationType){
			this.selected = false;
			this.attachment = attachment;
			this.agmt = agmt;
			this.agmtAssociationType = agmtAssociationType;
			this.isAttachment = true;
		}
		public AttachmentWrapper(Apttus__Agreement_Document__c agrDocument, String documentId, Apttus__APTS_Agreement__c agmt, String agmtAssociationType){
			this.selected = false;
			this.agrDocument = agrDocument;
			this.docId = documentId;
			this.agmt = agmt;
			this.agmtAssociationType = agmtAssociationType;
			this.isAttachment = false;
		}
	}

	public class EmailTemplateWrapper{
		public Boolean selected{get;set;}
		public EmailTemplate emailTemplate{get;private set;}
		public EmailTemplateWrapper(EmailTemplate emailTemplate){
			this.selected = false;
			this.emailTemplate = emailTemplate;
		}
		
		/*global Integer compareTo(Object objToCompare) {
			//Sort by Template desription.
			if(isSortByDescription)
			{
				return emailTemplate.Description.compareTo(((EmailTemplateWrapper)objToCompare).emailTemplate.Description);
			}
			else if(isSortByTemplateType) //Sort by Book price
			{
				return emailTemplate.TemplateType.compareTo(((EmailTemplateWrapper)objToCompare).emailTemplate.TemplateType);
			}
			else{
				return emailTemplate.Name.compareTo(((EmailTemplateWrapper)objToCompare).emailTemplate.Name);
			}
		}*/
	}
}