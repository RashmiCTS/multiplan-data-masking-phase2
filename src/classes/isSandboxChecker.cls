/*
File Name:   isSandboxChecker
Author:      Michael Olson  
Date:        4/11/2016
Description: Tells you if you are in Dev Prod

Modification History
4/11/2016		Michael Olson		created
*/

public with sharing class isSandboxChecker {
	public static Boolean isSandboxChecker() {
  		return [SELECT Id, IsSandbox FROM Organization LIMIT 1].IsSandbox;
	}
}