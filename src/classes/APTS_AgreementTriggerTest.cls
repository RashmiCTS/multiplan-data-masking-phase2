/*
	Author: Apttus, 08/31/2016 - Test class for agreement trigger logic.
	01/03/2017 - Kruti Shah, APTTUS - Modified some methods to cover code for multiple derivation like (Create AgreementClauses/populate some of the fields on Agreement)
 */


@isTest
private class APTS_AgreementTriggerTest {
	
	static Account testAcc;
	static Contact testCon;
	static User testUser1;
    static User testUser2;
    static User testUser3;     
    static List<User> listOfUsers;
    static list<APTS_State_Based_Approver_Setting__c> stateBasedApproverCustomSetting;
    static APTS_State_Based_Approver_Setting__c stateBasedApprover1;    
	static{
		testAcc = new Account();
		testAcc.name = 'testA';
    	insert testAcc;

    	testCon = new Contact();
    	testCon.AccountId = testAcc.Id;
    	testCon.lastName = 'agreementInsert_Test';
    	testCon.email = 'agreementInsert_Test@Test.com';
    	insert testCon;

    	Profile sysAdmin = [Select Id 
        						From Profile 
        							Where Name = 'System Administrator' Limit 1];

        Profile NSsysAdmin = [Select Id 
        						From Profile 
        							Where Name = 'NS Sys Admin' Limit 1];

       	listOfUsers = new List<User>();
		testUser1 = new User(Alias = 'testu1',email = 'test@test.com', IsActive = true,LastName='Testing',UserName='testApttususer1@apttustest.com',EmailEncodingKey='UTF-8',LanguageLocaleKey='en_US',LocaleSidKey='en_US',TimeZoneSidKey='America/Los_Angeles',ProfileId = sysAdmin.ID);
        testUser1.Division = 'OSI-New York';
        testUser1.CompanyName ='Apttus'; 
		listOfUsers.add(testUser1);		

		testUser2 = new User(Alias = 'testu2',email = 'test2@test.com', IsActive = true,LastName='Testing2',UserName='testApttususer2@apttustest.com',EmailEncodingKey='UTF-8',LanguageLocaleKey='en_US',LocaleSidKey='en_US',TimeZoneSidKey='America/Los_Angeles',ProfileId = sysAdmin.ID);
        testUser2.Division = 'OSI-New York';
        testUser2.CompanyName ='Apttus'; 
		listOfUsers.add(testUser2);	

		testUser3 = new User(Alias = 'testu3',email = 'test3@test.com', IsActive = true,LastName='Testing3',UserName='testApttususer3@apttustest.com',EmailEncodingKey='UTF-8',LanguageLocaleKey='en_US',LocaleSidKey='en_US',TimeZoneSidKey='America/Los_Angeles',ProfileId = NSsysAdmin.ID);
        testUser3.Division = 'OSI-New York';
        testUser3.CompanyName ='Apttus'; 
		listOfUsers.add(testUser3);			
		insert listOfUsers;

		stateBasedApproverCustomSetting = new list<APTS_State_Based_Approver_Setting__c>();
		stateBasedApprover1 = new APTS_State_Based_Approver_Setting__c();
		stateBasedApprover1.Name = 'State_AK';
		stateBasedApprover1.APTS_State__c = 'AK';
		stateBasedApprover1.APTS_Approver_Name__c = 'Testing';
		stateBasedApprover1.APTS_Approver_Username__c = 'testApttususer1@apttustest.com';
		stateBasedApproverCustomSetting.add(stateBasedApprover1);
		insert stateBasedApproverCustomSetting;

		APTS_AgrAmendRecordTypeMapping__c amendRecordTypeCustSett = new APTS_AgrAmendRecordTypeMapping__c();
		amendRecordTypeCustSett.APTS_New_Record_Type__c = 'Group Region - Amendment';
		amendRecordTypeCustSett.APTS_Old_Record_Type__c = 'Group - Region';
		amendRecordTypeCustSett.Name = 'Group_Region';
		insert amendRecordTypeCustSett;

		APTS_Region_Based_Approver__c regionApproverCustSett = new APTS_Region_Based_Approver__c();
		regionApproverCustSett.APTPS_Region__c = 'West';
		regionApproverCustSett.APTS_Legal_Support_Name__c = 'Testing3';
		regionApproverCustSett.APTS_Legal_Support_Username__c = 'testApttususer3@apttustest.com';
		regionApproverCustSett.Name = 'Region_West';
		regionApproverCustSett.APTS_CIA_Review_Name__c = 'Testing2';
		regionApproverCustSett.APTS_CIA_Review_Username__c = 'testApttususer2@apttustest.com';
		regionApproverCustSett.APTS_SVP_RVP_Name__c = 'Testing1';
		regionApproverCustSett.APTS_SVP_RVP_Username__c ='testApttususer1@apttustest.com';
		insert regionApproverCustSett;
	}

	@isTest static void agreementInsert_Test() {
		// Implement test code
		Id group5RTId = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('Group V5').getRecordTypeId();  

		Apttus__APTS_Agreement__c testAgr = new Apttus__APTS_Agreement__c();
		testAgr.Apttus__Account__c = testAcc.id;
		testAgr.recordtypeid = group5RTId;
		testAgr.Apttus__Primary_Contact__c = testCon.id;
		testAgr.Market_State__c = 'AK';		
		insert testAgr;
	}

	@isTest static void agreementInsertMarketState_Test() {			
		Id group5RTId = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('Group V5').getRecordTypeId();  

		list<Apttus__APTS_Agreement__c> listOfAgreement = new list<Apttus__APTS_Agreement__c>();

		Apttus__APTS_Agreement__c testAgr = new Apttus__APTS_Agreement__c();
		testAgr.Apttus__Account__c = testAcc.id;
		testAgr.recordtypeid = group5RTId;
		testAgr.Apttus__Primary_Contact__c = testCon.id;
		testAgr.Market_State__c = 'AK';
		listOfAgreement.add(testAgr);

		Apttus__APTS_Agreement__c testAgr2 = new Apttus__APTS_Agreement__c();
		testAgr2.Apttus__Account__c = testAcc.id;
		testAgr2.recordtypeid = group5RTId;
		testAgr2.Apttus__Primary_Contact__c = testCon.id;
		testAgr2.Market_State__c = 'AL';
		listOfAgreement.add(testAgr2);
		insert listOfAgreement;

		testAgr.Market_State__c = 'AL';
		testAgr2.Market_State__c ='AK';
		update listOfAgreement;
	}
	
	@isTest static void agreementUpdate_Test_ApprovalFlags() {
		// Implement test code
		Id group5RTId = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('Group V5').getRecordTypeId();  
		Id agr_rt_grpregion =  Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get(CONSTANTS.AGREEMENT_RT_ROUP_REGION).getRecordTypeId();

		Apttus__APTS_Agreement__c testAgr = new Apttus__APTS_Agreement__c();
		testAgr.Apttus__Account__c = testAcc.id;
		testAgr.recordtypeid = agr_rt_grpregion;
		testAgr.Apttus__Primary_Contact__c = testCon.id;
		testAgr.Apttus__Perpetual__c = true;
		insert testAgr;

		testAgr.Auto_Medical_Plans__c = false;
		testAgr.AutoMedical__c = 'five (5%) percent';	
		testAgr.Notice_time_period_of_discretionary_term__c = 'ninety (90) days	';	
		testAgr.Fee_Schedule_Type__c = 'S72';	
		testAgr.Apttus__Non_Standard_Legal_Language__c = true;	
		testAgr.Apttus__Contract_Start_Date__c = Date.today().addDays(100);	
		testAgr.Period_Group_Submit_Claims_for_Payment__c = 'one hundred and eighty (180) days	';	
		testAgr.ValuePoint_Discount_Card_Plans__c = false;	
		testAgr.WorkComp__c = 'three (3%) percent';	
		testAgr.Workers_Compensation_Plans__c = false;
		testAgr.Period_Part_Prof_Continue_Provide_Health__c = 'ninety (90) days';
		testAgr.Prior_Notice_Period_to_Terminate_Groups__c = 'sixty (60) days';
		testAgr.APTS_Mutual_Indem_State_Limitation_Ref__c = 'test value';
		testAgr.APTS_Billing_Participant_DisputeTimefram__c = 'ninety (90) days';
		testAgr.APTS_Refund_to_Participant_Timeframe__c = 'sixty (60) days';
		testAgr.Overpayment_Refund_Timeframe__c = 'thirty (30) days';
		testAgr.Underpayment_Refund_Timeframe__c = 'thirty (30) days';	
		testAgr.Claim_Post_Pymt_Audit_Timeframe__c = 'thirty (30) days';
		testAgr.Prior_Notice_Group_MPI_Client_Challenge__c = 'three (3) years';
		testAgr.Modification_of_Contract_Rates_and_Terms__c = 'upon prior written consent of MPI and Group';
		testAgr.Notice_of_Name_Logo_Update__c = 'written consent of Group';
		testAgr.Emergency_Service_Time_Period_Limit__c = 'two (2) weeks';
		testAgr.Claim_submission_Time_Limit__c = 'ninety (90) days';
		testAgr.APTS_Delegation_Agreement_Effective_Date__c = Date.today();
		testAgr.APTS_Admin_Handbook_Modifications_To__c = 'prior written notice to Group';
		testAgr.Prior_Notice_to_Modify_Ntwk_Partn__c = 'prior written consent of Group';
		testAgr.Prior_Notice_to_Modify_Exhibit_B__c = 'prior written consent of Group';
		testAgr.Period_Part_Prof_Continue_Accept_Pay__c = 'ninety (90) days';
		testAgr.Contract_Rate_Timeframe_Limit__c = 'ninety (90) days';
		testAgr.Period_Part_Prof_Continue_Provide_Health__c = 'thirty (30) days';
		testAgr.Discretionary_Termination_Options__c = 'Option 1';
		testAgr.APTS_Notice_Network_Expansion_PriorNotic__c = 'ninety (90) days';
		update testAgr;

		testAgr = [Select Id, Auto_Medical_Opt_Out_Triggered_Flag__c, Auto_Medical_Increase_Trigger_Flag__c, 
							Discretionary_Termination_Trigger_Flag__c, Fee_Schedule_Change_Trigger_Flag__c,
							Non_Standard_Language_Trigger_Flag__c, Retroactive_Date_Trigger_Flag__c,
							Submission_of_Claims_Trigger_Flag__c, ValuePoint_Discount_Card_Trigger__c,
							Worker_s_Comp_Increase_Trigger_Flag__c, Worker_s_Comp_Opt_Out_Trigger_Flag__c,
							APTS_Period_PartProfContinue_ProvideFlag__c,APTS_Contract_Rate_Timeframe_Limit_Flag__c,
							APTS_Period_Part_ProfCont_AcceptPay_Flag__c,APTS_Prior_Notice_toModify_ExhibitB_Flag__c,
							APTS_Prior_Notice_Modify_Ntwk_Partn_Flag__c,APTS_Admin_Handbook_Modifications_ToFlag__c,
							APTS_Delegation_Agmt_Effective_Date_Flag__c,APTS_Claim_submission_Time_Limit_Flag__c,
							APTS_Emergency_Service_Time_Period_Limit__c,APTS_Notice_of_Name_Logo_Update_Flag__c,
							APTS_Modification_ContractRatesTermsFlag__c,APTS_Postpaymnt_disputed_claim_time_Flag__c,
							APTS_Claim_PostPymt_Audit_Timeframe_Flag__c,APTS_Underpayment_Refund_Timeframe_Flag__c,
							APTS_Overpayment_Refund_Timeframe_Flag__c,APTS_Refund_toParticipant_Timeframe_Flag__c,
							APTS_BillingParticipantDisputTimframFlag__c,APTS_MutualIndemStateLimitation_Ref_Flag__c,
							APTS_Notice_of_Network_Expansion_Flag__c,
							APTS_Prior_Notice_Period_of_Network_Term__c
							From Apttus__APTS_Agreement__c Where Id = :testAgr.Id];
		System.assertEquals(testAgr.Auto_Medical_Opt_Out_Triggered_Flag__c, true);
		System.assertEquals(testAgr.Auto_Medical_Increase_Trigger_Flag__c, true);
		System.assertEquals(testAgr.Discretionary_Termination_Trigger_Flag__c, true);
		System.assertEquals(testAgr.Fee_Schedule_Change_Trigger_Flag__c, true);
		System.assertEquals(testAgr.Non_Standard_Language_Trigger_Flag__c, true);
		System.assertEquals(testAgr.Retroactive_Date_Trigger_Flag__c, true);
		System.assertEquals(testAgr.Submission_of_Claims_Trigger_Flag__c, true);
		System.assertEquals(testAgr.ValuePoint_Discount_Card_Trigger__c, true);
		System.assertEquals(testAgr.Worker_s_Comp_Increase_Trigger_Flag__c, true);
		System.assertEquals(testAgr.Worker_s_Comp_Opt_Out_Trigger_Flag__c, true);

		System.assertEquals(testAgr.APTS_Period_PartProfContinue_ProvideFlag__c, true);
		System.assertEquals(testAgr.APTS_Contract_Rate_Timeframe_Limit_Flag__c, true);
		System.assertEquals(testAgr.APTS_Period_Part_ProfCont_AcceptPay_Flag__c, true);
		System.assertEquals(testAgr.APTS_Prior_Notice_Modify_Ntwk_Partn_Flag__c, true);
		System.assertEquals(testAgr.APTS_Delegation_Agmt_Effective_Date_Flag__c, true);
		System.assertEquals(testAgr.APTS_Emergency_Service_Time_Period_Limit__c, true);
		System.assertEquals(testAgr.APTS_Modification_ContractRatesTermsFlag__c, true);
		System.assertEquals(testAgr.APTS_Claim_PostPymt_Audit_Timeframe_Flag__c, true);
		System.assertEquals(testAgr.APTS_Overpayment_Refund_Timeframe_Flag__c, true);
		System.assertEquals(testAgr.APTS_BillingParticipantDisputTimframFlag__c, true);
		System.assertEquals(testAgr.APTS_Prior_Notice_Period_of_Network_Term__c, true);
		System.assertEquals(testAgr.APTS_MutualIndemStateLimitation_Ref_Flag__c, true);
		System.assertEquals(testAgr.APTS_Refund_toParticipant_Timeframe_Flag__c, true);
		System.assertEquals(testAgr.APTS_Underpayment_Refund_Timeframe_Flag__c, true);
		System.assertEquals(testAgr.APTS_Postpaymnt_disputed_claim_time_Flag__c, true);
		System.assertEquals(testAgr.APTS_Claim_submission_Time_Limit_Flag__c, true);
		System.assertEquals(testAgr.APTS_Admin_Handbook_Modifications_ToFlag__c, true);
		System.assertEquals(testAgr.APTS_Notice_of_Name_Logo_Update_Flag__c, true);
		System.assertEquals(testAgr.APTS_Prior_Notice_toModify_ExhibitB_Flag__c, true);
		System.assertEquals(testAgr.APTS_Notice_of_Network_Expansion_Flag__c, true);		
		// update the approval status to Approved and check the flags.
		testAgr.Apttus_Approval__Approval_Status__c = 'Approved';
		update testAgr;

		testAgr = [Select Id, Auto_Medical_Opt_Out_Triggered_Flag__c, Auto_Medical_Increase_Trigger_Flag__c, 
							Discretionary_Termination_Trigger_Flag__c, Fee_Schedule_Change_Trigger_Flag__c,
							Non_Standard_Language_Trigger_Flag__c, Retroactive_Date_Trigger_Flag__c,
							Submission_of_Claims_Trigger_Flag__c, ValuePoint_Discount_Card_Trigger__c,
							Worker_s_Comp_Increase_Trigger_Flag__c, Worker_s_Comp_Opt_Out_Trigger_Flag__c,
							APTS_Period_PartProfContinue_ProvideFlag__c,APTS_Contract_Rate_Timeframe_Limit_Flag__c,
							APTS_Period_Part_ProfCont_AcceptPay_Flag__c,APTS_Prior_Notice_toModify_ExhibitB_Flag__c,
							APTS_Prior_Notice_Modify_Ntwk_Partn_Flag__c,APTS_Admin_Handbook_Modifications_ToFlag__c,
							APTS_Delegation_Agmt_Effective_Date_Flag__c,APTS_Claim_submission_Time_Limit_Flag__c,
							APTS_Emergency_Service_Time_Period_Limit__c,APTS_Notice_of_Name_Logo_Update_Flag__c,
							APTS_Modification_ContractRatesTermsFlag__c,APTS_Postpaymnt_disputed_claim_time_Flag__c,
							APTS_Claim_PostPymt_Audit_Timeframe_Flag__c,APTS_Underpayment_Refund_Timeframe_Flag__c,
							APTS_Overpayment_Refund_Timeframe_Flag__c,APTS_Refund_toParticipant_Timeframe_Flag__c,
							APTS_BillingParticipantDisputTimframFlag__c,APTS_MutualIndemStateLimitation_Ref_Flag__c,
							APTS_Notice_of_Network_Expansion_Flag__c,
							APTS_Prior_Notice_Period_of_Network_Term__c
							From Apttus__APTS_Agreement__c Where Id = :testAgr.Id];
		System.assertEquals(testAgr.Auto_Medical_Opt_Out_Triggered_Flag__c, false);
		System.assertEquals(testAgr.Auto_Medical_Increase_Trigger_Flag__c, false);
		System.assertEquals(testAgr.Discretionary_Termination_Trigger_Flag__c, false);
		System.assertEquals(testAgr.Fee_Schedule_Change_Trigger_Flag__c, false);
		System.assertEquals(testAgr.Non_Standard_Language_Trigger_Flag__c, false);
		System.assertEquals(testAgr.Retroactive_Date_Trigger_Flag__c, false);
		System.assertEquals(testAgr.Submission_of_Claims_Trigger_Flag__c, false);
		System.assertEquals(testAgr.ValuePoint_Discount_Card_Trigger__c, false);
		System.assertEquals(testAgr.Worker_s_Comp_Increase_Trigger_Flag__c, false);
		System.assertEquals(testAgr.Worker_s_Comp_Opt_Out_Trigger_Flag__c, false);

		System.assertEquals(testAgr.APTS_Period_PartProfContinue_ProvideFlag__c, false);
		System.assertEquals(testAgr.APTS_Contract_Rate_Timeframe_Limit_Flag__c, false);
		System.assertEquals(testAgr.APTS_Period_Part_ProfCont_AcceptPay_Flag__c, false);
		System.assertEquals(testAgr.APTS_Prior_Notice_Modify_Ntwk_Partn_Flag__c, false);
		System.assertEquals(testAgr.APTS_Delegation_Agmt_Effective_Date_Flag__c, false);
		System.assertEquals(testAgr.APTS_Emergency_Service_Time_Period_Limit__c, false);
		System.assertEquals(testAgr.APTS_Modification_ContractRatesTermsFlag__c, false);
		System.assertEquals(testAgr.APTS_Claim_PostPymt_Audit_Timeframe_Flag__c, false);
		System.assertEquals(testAgr.APTS_Overpayment_Refund_Timeframe_Flag__c, false);
		System.assertEquals(testAgr.APTS_BillingParticipantDisputTimframFlag__c, false);
		System.assertEquals(testAgr.APTS_Prior_Notice_Period_of_Network_Term__c, false);
		System.assertEquals(testAgr.APTS_MutualIndemStateLimitation_Ref_Flag__c, false);
		System.assertEquals(testAgr.APTS_Refund_toParticipant_Timeframe_Flag__c, false);
		System.assertEquals(testAgr.APTS_Underpayment_Refund_Timeframe_Flag__c, false);
		System.assertEquals(testAgr.APTS_Postpaymnt_disputed_claim_time_Flag__c, false);
		System.assertEquals(testAgr.APTS_Claim_submission_Time_Limit_Flag__c, false);
		System.assertEquals(testAgr.APTS_Admin_Handbook_Modifications_ToFlag__c, false);
		System.assertEquals(testAgr.APTS_Notice_of_Name_Logo_Update_Flag__c, false);
		System.assertEquals(testAgr.APTS_Prior_Notice_toModify_ExhibitB_Flag__c, false);
		System.assertEquals(testAgr.APTS_Notice_of_Network_Expansion_Flag__c, false);
	}

	@isTest static void agreementUpdate_Test_NetworkTypes() {
		// Implement test code
		Id group5RTId = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('Group V5').getRecordTypeId();  

		Apttus__APTS_Agreement__c testAgr = new Apttus__APTS_Agreement__c();
		testAgr.Apttus__Account__c = testAcc.id;
		testAgr.recordtypeid = group5RTId;
		testAgr.Apttus__Primary_Contact__c = testCon.id;
		insert testAgr;

		testAgr.Primary_Network__c = true;
		testAgr.Complementary_Network__c = true;	
		testAgr.Workers_Compensation_Plans__c = true;
		testAgr.Auto_Medical_Plans__c = true;	
		testAgr.PSAV__c = true;
		testAgr.HealthEOS_Network__c = true;	
		testAgr.COE_Network__c = true;	
		update testAgr;

		testAgr = [Select Id, Network_types__c From Apttus__APTS_Agreement__c Where Id = :testAgr.Id];
		System.assert(testAgr.Network_types__c.containsIgnoreCase('the Centers of Excellence (“COE”) Transplant Network'), 'Network_types__c derivation is not working.');
	}

	@isTest static void agreementUpdate_Test_FeeScheduleDerivation() {
		// Implement test code
		Id group5RTId = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('Group V5').getRecordTypeId();  

		Banded_Fee_Schedules__c testFeeSchedule = new Banded_Fee_Schedules__c();
		testFeeSchedule.Band__c = 'S73';
		testFeeSchedule.MKT_ID__c = 'PORT';
		testFeeSchedule.State__c = 'ME';
		insert testFeeSchedule;

		Apttus__APTS_Agreement__c testAgr = new Apttus__APTS_Agreement__c();
		testAgr.Apttus__Account__c = testAcc.id;
		testAgr.recordtypeid = group5RTId;
		testAgr.Apttus__Primary_Contact__c = testCon.id;		
		insert testAgr;

		testAgr.Fee_Schedule_Type__c = testFeeSchedule.Band__c;
		testAgr.Market_Id__c = testFeeSchedule.MKT_ID__c;
		testAgr.Market_State__c = testFeeSchedule.State__c;		
		update testAgr;

		testAgr.Market_State__c = 'MT';
		update testAgr;

		testAgr = [Select Id, Complementary_Network__c, ValuePoint_Discount_Card_Plans__c 
							From Apttus__APTS_Agreement__c 
								Where Id = :testAgr.Id Limit 1];
		System.assertEquals(testAgr.Complementary_Network__c, false);
		System.assertEquals(testAgr.ValuePoint_Discount_Card_Plans__c, false);
	}

	@isTest static void agreementUpdate_Test_CreateAgrClauses() {		
		Id group5RTId = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('Group V5').getRecordTypeId();  

		Apttus__APTS_Agreement__c testAgr = new Apttus__APTS_Agreement__c();
		testAgr.Apttus__Account__c = testAcc.id;
		testAgr.recordtypeid = group5RTId;
		testAgr.Apttus__Primary_Contact__c = testCon.id;
		testAgr.Group_System_Option__c =true;
		insert testAgr;

		testAgr.Group_System_Option__c =false;
		Update testAgr;

		Apttus__APTS_Agreement__c testAgr2 = new Apttus__APTS_Agreement__c();
		testAgr2.Apttus__Account__c = testAcc.id;
		testAgr2.recordtypeid = group5RTId;
		testAgr2.Apttus__Primary_Contact__c = testCon.id;
		testAgr2.Group_System_Option__c =false;
		insert testAgr2;

		testAgr2.Group_System_Option__c =true;
		Update testAgr2;
	}

	@isTest static void agreementInsert_AmendRecordType() {		
		Id groupRegionRTId = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('Group - Region').getRecordTypeId();  
		Id ipaRegionRTId = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('IPA - Region').getRecordTypeId();  

		Apttus__APTS_Agreement__c testAgr = new Apttus__APTS_Agreement__c();
		testAgr.Apttus__Account__c = testAcc.id;
		testAgr.recordtypeid = groupRegionRTId;
		testAgr.Apttus__Primary_Contact__c = testCon.id;
		testAgr.Market_State__c = 'AK';
		testAgr.Apttus__Status_Category__c = 'Request';
		testAgr.Apttus__Status__c = 'In Amendment';
        testAgr.Apttus__Perpetual__c = true;
		insert testAgr;

		Apttus__APTS_Agreement__c testAgr2 = new Apttus__APTS_Agreement__c();
		testAgr2.Apttus__Account__c = testAcc.id;
		testAgr2.recordtypeid = ipaRegionRTId;
		testAgr2.Apttus__Primary_Contact__c = testCon.id;
		testAgr2.Market_State__c = 'AK';
		testAgr2.Apttus__Status_Category__c = 'Request';
		testAgr2.Apttus__Status__c = 'In Amendment';
        testAgr2.Apttus__Perpetual__c = true;
		insert testAgr2;

		Apttus__Agreement_Clause__c agrClause = new Apttus__Agreement_Clause__c();
		agrClause.Apttus__Agreement__c = testAgr2.id;
		agrClause.Apttus__Action__c = CONSTANTS.INSERTED;
		agrClause.Apttus__Clause__c = 'test clause to be insert';
		agrClause.Apttus__Active__c = true;
		insert agrClause;

		testAgr2.APTS_Director__c = testUser1.Id;
		testAgr2.SVP_RVP__c = testUser2.ID;
		testAgr2.Regulatory_Compliance__c = testUser2.Id;
		testAgr2.APTS_Legal_Support__c = testUser1.ID;
		update testAgr2;
	}

	@isTest static void agreementFeeScheduleValue() {		
		Id groupRegionRTId = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('Group - Region').getRecordTypeId();  

		Apttus__APTS_Agreement__c testAgr = new Apttus__APTS_Agreement__c();
		testAgr.Apttus__Contract_Start_Date__c = System.today();
		testAgr.Apttus__Account__c = testAcc.id;
		testAgr.recordtypeid = groupRegionRTId;
		testAgr.Apttus__Primary_Contact__c = testCon.id;		
		testAgr.Fee_Schedule_Type__c = 'S68';
        testAgr.Apttus__Perpetual__c = true;
		insert testAgr;

		testAgr.Fee_Schedule_Type__c = 'Unique';
		testAgr.ownerID = testUser2.Id;
		update testAgr;
	}

	//Test case : Activated/Terminated/Cancelled agreements should be locked
	@isTest static void agreementUpdate_Activate_Test() {		
		Id groupRegionRTId = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('Group - Region').getRecordTypeId();  

		Apttus__APTS_Agreement__c testAgr = new Apttus__APTS_Agreement__c();
		testAgr.Apttus__Contract_Start_Date__c = System.today();
		testAgr.Apttus__Account__c = testAcc.id;
		testAgr.recordtypeid = groupRegionRTId;
		testAgr.Apttus__Primary_Contact__c = testCon.id;		
		testAgr.Fee_Schedule_Type__c = 'S68';
        testAgr.Apttus__Perpetual__c = true;
        testAgr.Apttus__Status_Category__c = CONSTANTS.STATUS_CATEGORY_IN_EFFECT;
        testAgr.Apttus__Status__c = CONSTANTS.STATUS_ACTIVATED;
		insert testAgr;

		System.runAs(testUser3){
			try{
				testAgr.APTS_Notice_Network_Expansion_PriorNotic__c = 'thirty (30) days';
				testAgr.Apttus__Is_System_Update__c = false;
				update testAgr;
			}catch(Exception ex){				
				System.assert(testAgr.APTS_Notice_Network_Expansion_PriorNotic__c.containsIgnoreCase('thirty (30) days'), 'This record cannot be edited because it has been either Activated,Terminated,Superseded or Cancelled.');
			}
		}
	}
	
}