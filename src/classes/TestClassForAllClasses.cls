@isTest
public class TestClassForAllClasses {
    
    static testmethod void test_Agreement_CreateEmailTemplatesBatch(){       
        //Test methods for DSReciepient_UpdateEmailTemplateBatch and Agreement_CreateEmailTemplatesBatch
        List<Apttus__APTS_Template__c> lstTemps = UtilityForTestData.createTemplates();
        Contract_Language_Amendment_Tracking__c clat = UtilityForTestData.insertCLAT(NULL);
        List<Template_To_Gen__c> lstTemp = [select id,Status__c,Generated_Attachment_Id__c,Agreement__r.Flag_Auto_Generate_Documents__c,
                                            Agreement__r.recordType.DeveloperName ,Agreement__r.Apttus__Status_Category__c  
                                            from Template_To_Gen__c];
       
        
        Agreement_CreateEmailTemplatesBatch agBtch = new Agreement_CreateEmailTemplatesBatch();
        Test.startTest();
        Database.executeBatch(agBtch);
        Test.stopTest();
    }
    
    static testmethod void test_AttachDocsFromTemplateController(){
        // Create Agreement
        List<Apttus__APTS_Template__c> lstTemps = UtilityForTestData.createTemplates();
        Contract_Language_Amendment_Tracking__c clat = UtilityForTestData.insertCLAT(NULL);
        //Map<Id,List<Attachment>> mapAtt = UtilityForAttachments.getAllAttachmentsByParentId(CONSTANTS.APTTUS_APTS_TEMPLATE,CONSTANTS.APPLICATION_PDF);
        //System.debug('**** mapAtt '+mapAtt);
        List<Attachment> lstAttach = [select id,contentType from Attachment];
        System.debug('**** lstAttach '+lstAttach);
        List<Apttus__APTS_Agreement__c> lstAgree = [SELECT id FROM Apttus__APTS_Agreement__c WHERE CLAT__c=:clat.Id];
        ApexPages.StandardController stdCon = new ApexPages.StandardController(lstAgree[0]);
        Test.setCurrentPage(Page.AttachDocsFromTemplate);
        AttachDocsFromTemplateController tempCon = new AttachDocsFromTemplateController(stdCon);
        tempCon.strSearch = 'Application';
        List<AttachDocsFromTemplateController.TemplateWrapper> lstTempWrap = new List<AttachDocsFromTemplateController.TemplateWrapper>();
        for(AttachDocsFromTemplateController.TemplateWrapper tw : tempCon.getLstTempWrap()){
            tw.bIsSelected = true;
            lstTempWrap.add(tw);
        }
        tempCon.setLstTempWrap(lstTempWrap);
        tempCon.uploadAttachToAgreements();
        
        // Call all other methods
        
        tempCon.firstPage();
        tempCon.lastPage();
        tempCon.nextPage();
        tempCon.prevPage();
        Boolean b1 = tempCon.hasfirstLast;
        Boolean b2 =tempCon.hasPrevious;
        Boolean b3 =tempCon.hasNext;
        tempCon.pageNum =2;
        tempCon.getFilteredTemplates();
        
    }
    static testmethod void test_AttachDocsFromTemplateController2(){
        // Create Agreement
        ApexPages.StandardController stdCon = new ApexPages.StandardController(UtilityForTestData.createAgreement());
        Test.setCurrentPage(Page.AttachDocsFromTemplate);
        AttachDocsFromTemplateController tempCon = new AttachDocsFromTemplateController(stdCon);
        List<AttachDocsFromTemplateController.TemplateWrapper> lstTempWrap = new List<AttachDocsFromTemplateController.TemplateWrapper>();
        
        // Call all other methods
        
        tempCon.firstPage();
        tempCon.lastPage();
        tempCon.nextPage();
        tempCon.prevPage();
        tempCon.getFilteredTemplates();
        
    }
    
    static testmethod void test_DocuSignResetAgrmtEmailTemplatesHandler(){
        //Test methods for DSReciepient_UpdateEmailTemplateBatch and Agreement_CreateEmailTemplatesBatch
        List<Apttus__APTS_Template__c> lstTemps = UtilityForTestData.createTemplates();
        Contract_Language_Amendment_Tracking__c clat = UtilityForTestData.insertCLAT(NULL);
        List<Apttus__APTS_Agreement__c> lstAgree = [SELECT id FROM Apttus__APTS_Agreement__c WHERE CLAT__c=:clat.Id];
        Agreement_CreateEmailTemplatesBatch agBtch = new Agreement_CreateEmailTemplatesBatch();
        Test.startTest();
        Database.executeBatch(agBtch);
        Test.stopTest();
        Apttus_DocuApi__DocuSignEnvelope__c docEnv = new Apttus_DocuApi__DocuSignEnvelope__c();
        docEnv.Apttus_CMDSign__Agreement__c = lstAgree[0].Id;
        insert docEnv;
        docEnv.Apttus_DocuApi__Status__c = 'sent';
        update docEnv;
        
    }
    
    
    static testmethod void test_ErrorHandler(){
    	List<Contract_Language_Amendment_Tracking__c> lstClat = new List<Contract_Language_Amendment_Tracking__c>();
        List<Apttus__APTS_Template__c> lstTemps = UtilityForTestData.createTemplates();
        for(Integer i=0;i<10;i++){
            User usr = new User();
            usr.Username = 'ttmu'+i+'@multiplantest.com';
            usr.Alias = 'tthu'+i;
            usr.CommunityNickname = 'thrr'+i;
            usr = UtilityForTestData.createUser(usr);
            Contract_Language_Amendment_Tracking__c clat = new Contract_Language_Amendment_Tracking__c();
            clat.Negotiator__c = usr.Id;
            clat = UtilityForTestData.createCLAT(clat);   
            clat.Provider_TIN_Passthrough__c = '123';   
            lstClat.add(clat);
        }
        try{
            insert lstClat;
        }
        catch(exception e){
            System.debug('******* e '+e.getMessage());
        }
    }
    
    static testmethod void test_DatabaseOperation_Update(){
    	List<Apttus__APTS_Template__c> lstTemps = UtilityForTestData.createTemplates();
        try{
            Contract_Language_Amendment_Tracking__c clat = UtilityForTestData.insertCLAT(NULL);
        }
        catch(exception e){
            System.debug('******* e '+e.getMessage());
        }
    }
    
    /****************** test class for addDocuSignRecipients_Cls ******************/
     
    static testmethod void test_addDocuSignRecipients_Cls(){
        
        List<Apttus__APTS_Agreement__c> lstAgree = new List<Apttus__APTS_Agreement__c>();
        Apttus__APTS_Agreement__c apAgree1 = UtilityForTestData.createAgreement();
        Apttus__APTS_Agreement__c apAgree2 = UtilityForTestData.createAgreement();
        
        //Create Contact 
        Contact con = UtilityForTestData.createContact(null);
       
        apAgree2.Apttus__Primary_Contact__c = con.Id;
        update apAgree2;
        
        apAgree1.Assignment_Work_Type__c = 'Assignment and Amendment';
        apAgree1.APTS_Original_Agreement__c = apAgree2.Id;        
        update apAgree1;
        
        
        test.startTest();
        PageReference pr = Page.AgreementDocuSignRecipient;
        pr.getParameters().put('agreementid',apAgree1.Id);
        Test.setCurrentPage(pr);
        Apttus_DocuApi__DocuSignDefaultRecipient2__c docRep = new Apttus_DocuApi__DocuSignDefaultRecipient2__c();
        ApexPages.StandardController stdCon = new ApexPages.StandardController(docRep);
        addDocuSignRecipients_Cls addDocSign = new addDocuSignRecipients_Cls(stdCon);
        test.stopTest();
        
        
        
    }
}