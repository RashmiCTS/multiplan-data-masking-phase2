public class CONSTANTS { 
/*
File Name:   CONSTANTS
Author:      Rashmi T
Date:        11/7/2016
Description: This utility declares all the variables that are used as constants.
Modification History
11/07/2016      Rashmi T      Created 
02/09/2017      Kruti Shah, APTTUS    - Added some field values for Agreement Status And Status Category fields.
02/15/2017      Kruti Shah, APTTUS    - Added some values for Agreement Record Types.
*/    	
    public static final String APTTUS_APTS_TEMPLATE = 'Apttus__APTS_Template__c';
    public static final String APPLICATION_PDF = 'application/pdf';
    public static final String DOT_PDF = '.pdf';

    //Start: Added by Kruti Shah (APTTUS)
    public static final String GROUP_SYSTEM_OPTION 							    = 'Group_System_Option__c';
    public static final String INSERTED 		 								= 'Inserted';
    public static final String DELETED   		 								= 'Deleted';
    public static final String STATUS_CATEGORY_IN_SIGNATURES					= 'In Signatures';
    public static final String STATUS_FULLY_SIGNED								= 'Fully Signed';
    public static final String STATUS_OTHER_PARTY_REVIEW						= 'Other Party Review';
    public static final String STATUS_SIGNATURE_DECLINED						= 'Signature Declined';
    public static final String STATUS_OTHER_PARTY_SIGNATURES                    = 'Other Party Signatures';
    public static final String STATUS_REQUEST                                   = 'Request';
    public static final String STATUS_IN_AMENDMENT                              = 'In Amendment';    
    public static final String STATUS_ACTIVATED                                 = 'Activated';   
    public static final String STATUS_CANCELLED_REQUEST                         = 'Cancelled Request';   
    public static final String STATUS_TERMINATED                                = 'Terminated';   
    public static final String STATUS_SUPERSEDED                                = 'Superseded';       

    public static final String STATUS_CATEGORY_IN_AUTHORING                     = 'In Authoring';
    public static final String STATUS_AUTHOR_CONTRACT                           = 'Author Contract';        
    public static final String STATUS_CATEGORY_IN_EFFECT                        = 'In Effect';

    public static final String MAIL_OR_FAX_PROVIDER_REJECTED					= 'MailOrFaxProviderRejected';
    public static final String MAIL_OR_FAX_EXECUTIVE_SIGNATURE_COMPLETED		= 'MailorFaxExecutiveSignatureCompleted';
    public static final String SENT_VIA_MAIL_OR_FAX								= 'SentViaMailOrFax';
    public static final String ACTIVATE_LEGACY_AGREEMENT                        = 'ActivateLegacyAgreement';

    public static final String APPROVAL_REQUIRED								= 'Approval Required';
    public static final String CANCELLED										= 'Cancelled';    
    public static final String APPROVED                                         = 'Approved';

    public static final String AGREEMENT_RT_GROUPFIVE                           = 'Group V5';
    public static final String AGREEMENT_RT_GROUPFIVE_AMEND                     = 'Group V5 - Amendment';    
    public static final String AGREEMENT_RT_ROUP_REGION                         = 'Group - Region';
    public static final String AGREEMENT_RT_GROUP_REGION_AMEND                  = 'Group Region - Amendment';
    public static final String AGREEMENT_RT_IPA_REGION                          = 'IPA - Region';
    public static final String AGREEMENT_RT_IPA_REGION_AMEND                    = 'IPA Region - Amendment';
    public static final String AGREEMENT_RT_LEGACY                              = 'Legacy';
    

    public static final String REGENERATE                                       = 'Regenerate';

    public static final String AGREEMENT_STAGE_CLOSED_SUCCESSFUL                = 'Closed - Successful';
    public static final String AGREEMENT_STAGE_DRAFT                            = 'Draft';

    public static final String PROFILE_SYSADMIN                                 = 'System Administrator'; 
    //END: Added by Kruti Shah (APTTUS)
}