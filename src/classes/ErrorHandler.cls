public with sharing class ErrorHandler {
	private List<SObject> lstSobj = new List<SObject>();
	private String strObjectType = '';
	List<SObject> lstTriggerNew = new List<SObject>();
	Map<String,SObject> mapStrIdVSTriggNew = new Map<String,SObject>();
	public ErrorHandler(List<SObject> lstSObject, List<SObject> lstTriggerNew) {
		//constructor	
		
        if(!lstSObject.isEmpty()){
            lstSobj = lstSObject;
            strObjectType = String.valueOf(lstSObject[0].getSObjectType());
        }        
		for(Sobject s : lstTriggerNew){
			mapStrIdVSTriggNew.put(String.valueOf(s.get('id')),s);
		}
        System.debug('*** mapStrIdVSTriggNew '+mapStrIdVSTriggNew);
        System.debug('*** lstTriggerNew '+lstTriggerNew);
	}

	public List<SObject> processSaveResult(Database.SaveResult[] arrSaveResult){
		System.debug('*** lstSobj '+lstSobj);
        System.debug('*** strObjectType '+strObjectType);
        System.debug('*** mapStrIdVSTriggNew '+mapStrIdVSTriggNew);
		
		Set<String> setStrLookUp = new Set<String>();
		Id idErr;
		System.debug('*** arrSaveResult '+arrSaveResult);
		for(Integer i=0 ; i<arrSaveResult.size();i++){
			if(!arrSaveResult[i].isSuccess()){
				setStrLookUp = getLookUpFields(strObjectType);
                System.debug('*** setStrLookUp '+setStrLookUp);
				if(!setStrLookUp.isEmpty() && !mapStrIdVSTriggNew.isEmpty()){
					for(String lp : setStrLookUp){
						String strLp = (String)lstSobj[i].get(lp);
                        System.debug('*** strLp '+strLp);
						if(String.isNotBlank(strLp) &&  mapStrIdVSTriggNew.containsKey(strLp)){
							idErr = strLp;
							break;							
						}
					}
				}		
                System.debug('*** idErr '+idErr);
				for(Database.Error err : arrSaveResult[i].getErrors()){
                    if(idErr <> null){
                        mapStrIdVSTriggNew.get(idErr).addError(err.getMessage());
                    }
				}
			}
		}
		return lstSobj;
	}


	public Set<String> getLookUpFields(String strObjectType){
		Set<String> setStrLookUp = new Set<String>();
		String[] arrSobjTypes = new String[]{strObjectType};
		Schema.DescribeSobjectResult[] desResults = Schema.describeSObjects(arrSobjTypes);
		Map<String, Schema.SObjectField> mapFields = new Map<String, Schema.SObjectField>();
		for(Schema.DescribeSobjectResult ds : desResults){
			mapFields = ds.fields.getMap();
			for(String f : mapFields.keySet()){
				Schema.DescribeFieldResult 	sDesRes = mapFields.get(f).getDescribe();
				if(sDesRes.getType().name() == 'Reference'){
					setStrLookUp.add(f);
				}
			}
		}
        return setStrLookUp;
	}
}