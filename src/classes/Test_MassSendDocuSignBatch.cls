/*
File Name:   Test_MassSendDocuSignBatch
Author:      Michael Olson
Date:        5/26/2016
Description: Test's massSendDocuSignBatch

Modification History
5/26/2016       Michael Olson       Created.
8/18/2016       Michale Olson       Added parameter for session Id.
8/31/2016       Michael Olson       added email_Template__c
*/

@isTest
public with sharing class Test_MassSendDocuSignBatch {
    
    
    static testMethod void Test_MassSendDocuSignBatch_HappyPath() {


        //---------------------------------------------------
        // Variables
        //---------------------------------------------------    
        Id rtClatVAAG = Schema.SObjectType.Contract_Language_Amendment_Tracking__c.getRecordTypeInfosByName().get('VA Amendment - Group').getRecordTypeId();
        Id rtAgreeVAAG = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('VA Amendment - Group').getRecordTypeId();  
        Id rtRecipientCon = Schema.SObjectType.Apttus_DocuApi__DocuSignDefaultRecipient2__c.getRecordTypeInfosByName().get('Contact').getRecordTypeId();   
        Id rtRecipientGroup = Schema.SObjectType.Apttus_DocuApi__DocuSignDefaultRecipient2__c.getRecordTypeInfosByName().get('Signing Group').getRecordTypeId();        
        string sessionid = UserInfo.getSessionId();
        
        //---------------------------------------------------
        // Insert Records
        //---------------------------------------------------
            
            //INSERT EMAIL TEMPLATE
            EmailTemplate e = [select id, developerName from EmailTemplate where developerName = 'VA_Amendment_Group_Email'];
                
            //insert Account    
            Account a = new Account();
                a.name = 'testA';
            insert a;
            
            
            //insert Contacts
            list<Contact> cons = new List<Contact>();
                
                Contact c1 = new Contact();
                    c1.lastName = 'last';
                    c1.accountid = a.id;
                    c1.email = 'f@multiplan.com';
                cons.add(c1);
            
            insert cons;

    
            //insert Agreement
               
            Apttus__APTS_Agreement__c a1 = new Apttus__APTS_Agreement__c();
                a1.Apttus__Account__c = a1.id;
                a1.recordtypeid = rtAgreeVAAG;
                a1.Apttus__Primary_Contact__c = c1.id;
                a1.Flag_Auto_Generate_Documents__c = true;
                a1.Flag_Auto_Send_via_DocuSign__c = true;
                a1.Supplemental_Email_Template_Unique_Name__c = e.developername;
                a1.Apttus__Status_Category__c = 'Request - Auto Document Generation Successful; Auto DocuSign Send Pending';   
            insert a1;
                       
            //insert attachments
            list<attachment> aList = new list<attachment>();
            
                //insert template attachment
                Attachment attach1 = new Attachment();  
                    attach1.ParentId = a1.id;  
                    attach1.Name = 'DOCX Template.docx';
                      
                    Blob b = Blob.valueOf('Test Data');  
                    attach1.Body = b;             
                aList.add(attach1);  
                
                 //insert template attachment
                Attachment attach2 = new Attachment();  
                    attach2.ParentId = a1.id;  
                    attach2.Name = 'PDFTemplate.pdf';
                    attach2.contentType = 'application/pdf';
                    Blob b2 = Blob.valueOf('Test Data');  
                    attach2.Body = b2;             
                aList.add(attach2);  
            
            insert aList;

             //insert template to let us insert templatetogen
            Apttus__APTS_Template__c aTemp1 = new  Apttus__APTS_Template__c();
            aTemp1.name = 'Group V5';
            insert aTemp1;

            // Documents to send are attached to 'template to gen' records.
            Template_To_Gen__c templateGen_A1_1 = new Template_To_Gen__c(
                Send_In_Supplemental_Email_Only__c = false, 
                Sort_Order__c =1, 
                Generated_Attachment_Id__c = attach1.id,
                Status__c = 'Generated',
                Agreement__c = a1.id,
                Template__c = aTemp1.id);
            insert templateGen_A1_1;

            // 'Supplemental' attachment in pdf format
            Template_To_Gen__c templateGen_A1_2 = new Template_To_Gen__c(
                Send_In_Supplemental_Email_Only__c = true, 
                Sort_Order__c =2, 
                Generated_Attachment_Id__c = attach2.id,
                Status__c = 'Generated',
                Agreement__c = a1.id,
                Template__c = aTemp1.id);
            insert templateGen_A1_2;
            
            // Add a signing group.
            Apttus_DocuApi__DocuSignUser__c docuSignUser = new Apttus_DocuApi__DocuSignUser__c();
            docusignUser.Apttus_DocuApi__ContactLookup__c = c1.id;
            insert docusignuser;
            
           Apttus_DocuApi__DocuSignSigningGroup__c signingGroup = new Apttus_DocuApi__DocuSignSigningGroup__c ();
           signinggroup.Apttus_DocuApi__SigningGroupId__c = '1';
           signinggroup.Apttus_DocuApi__DocuSignAccountId__c = docusignuser.id;
           insert signingGroup ;
           Apttus_DocuApi__DocuSignSigningGroupMembers__c groupMember = new Apttus_DocuApi__DocuSignSigningGroupMembers__c();
           groupMember.Apttus_DocuApi__UserName__c = 'j';
           groupMember.Apttus_DocuApi__Email__c = 'user@multiplan.com';
           groupMember.Apttus_DocuApi__SigningGroupId__c = signingGroup .id;
            insert groupMember;
            
            //insert DocuSign Default Recipient
            
            
                Apttus_DocuApi__DocuSignDefaultRecipient2__c ar = new Apttus_DocuApi__DocuSignDefaultRecipient2__c();
                    ar.RecordTypeid = rtRecipientCon;
                    ar.Apttus_DocuApi__ContactId__c = c1.id;
                    ar.Apttus_DocuApi__RoleName__c = 'Signer 1';
                    ar.email_Template__c = e.DeveloperName;
                    ar.Apttus_DocuApi__RecipientType__c = 'Signer';
                    ar.Apttus_DocuApi__SigningOrder__c = 1;
                    ar.Apttus_CMDSign__AgreementId__c = a1.id;
                    ar.Send_Supplemental_Email__c = true;
                insert ar;
                
                // Signing group.
                 Apttus_DocuApi__DocuSignDefaultRecipient2__c ar2 = new Apttus_DocuApi__DocuSignDefaultRecipient2__c();
                    ar2.RecordTypeid = rtRecipientGroup ;
                    ar2.Apttus_DocuApi__RoleName__c = 'Signer 2';
                    ar2.email_Template__c = e.DeveloperName;
                    ar2.Apttus_DocuApi__RecipientType__c = 'Signer';
                    ar2.Apttus_DocuApi__SigningOrder__c = 2;
                    ar2.Apttus_CMDSign__AgreementId__c = a1.id;
                    ar2.Apttus_DocuApi__SigningGroupId__c = signingGroup.id;
                    ar2.Send_Supplemental_Email__c = true;
                insert ar2;
            
           
            
            //insert docusign connect
            list<DocuSignConnect__c> dscList = new list<DocuSignConnect__c>();
                DocuSignConnect__c dsc1 = new DocuSignConnect__c();
                    dsc1.Name = 'SANDBOX';
                    dsc1.Account_Id__c = 'x';
                    dsc1.Integrator_Key__c = 'x';
                    dsc1.Pass__c = 'x';
                    dsc1.User_Id__c = 'x';
                    dsc1.Web_Service_Url__c = 'x';
                dscList.add(dsc1);  
                
                DocuSignConnect__c dsc2 = new DocuSignConnect__c();
                    dsc2.Name = 'PROD';
                    dsc2.Account_Id__c = 'x';
                    dsc2.Integrator_Key__c = 'x';
                    dsc2.Pass__c = 'x';
                    dsc2.User_Id__c = 'x';
                    dsc2.Web_Service_Url__c = 'x';
                dscList.add(dsc2);  
            insert dscList;             
        
        Mass_Document_Send__c mds = new Mass_Document_Send__c();
        insert mds;
        a1.Mass_Document_Send__c = mds.id;
        update a1;
            
        ///Start of Tests
        Test.startTest();
            massSendDocuSignBatch msdsb = new massSendDocuSignBatch(sessionId);
            Database.executeBatch(msdsb,1);
        Test.stopTest();
        // To simulate a successful send, insert an envelope attached to the agreement.
        Apttus_DocuApi__DocuSignEnvelope__c envelopeSuccess = new Apttus_DocuApi__DocuSignEnvelope__c();
        envelopeSuccess.Apttus_CMDSign__Agreement__c = a1.id;
        insert envelopeSuccess;

        a1 = [SELECT Apttus__Status__c,Apttus__Status_Category__c, Apttus_CMDSign__DocuSignEnvelopeId__c from Apttus__APTS_Agreement__c WHERE ID=:a1.id];
        //system.assertEquals('Ready for Signatures',a1.Apttus__Status__c);
        //system.assertEquals('In Signatures',a1.Apttus__Status_Category__c);
       
       
        
        
    }
    
    // Test that an exception is thrown if an email address is invalid or missing.
     static testMethod void Test_MassSendDocuSignBatch_Failure_NoEmail() {


        //---------------------------------------------------
        // Variables
        //---------------------------------------------------    
        Id rtClatVAAG = Schema.SObjectType.Contract_Language_Amendment_Tracking__c.getRecordTypeInfosByName().get('VA Amendment - Group').getRecordTypeId();
        Id rtAgreeVAAG = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('VA Amendment - Group').getRecordTypeId();  
        Id rtRecipientCon = Schema.SObjectType.Apttus_DocuApi__DocuSignDefaultRecipient2__c.getRecordTypeInfosByName().get('Contact').getRecordTypeId();   
        Id rtRecipientGroup = Schema.SObjectType.Apttus_DocuApi__DocuSignDefaultRecipient2__c.getRecordTypeInfosByName().get('Signing Group').getRecordTypeId();        
        string sessionid = UserInfo.getSessionId();
        
        //---------------------------------------------------
        // Insert Records
        //---------------------------------------------------
            
            //INSERT EMAIL TEMPLATE
            EmailTemplate e = [select id, developerName from EmailTemplate where developerName = 'VA_Amendment_Group_Email'];
                
            //insert Account    
            Account a = new Account();
                a.name = 'testA';
            insert a;
            
            
            //insert Contacts
            list<Contact> cons = new List<Contact>();
                
                Contact c1 = new Contact();
                    c1.lastName = 'last';
                    c1.accountid = a.id;
                    c1.email = null; // INVALID
                cons.add(c1);
            
            insert cons;

    
            //insert Agreement
               
            Apttus__APTS_Agreement__c a1 = new Apttus__APTS_Agreement__c();
                a1.Apttus__Account__c = a1.id;
                a1.recordtypeid = rtAgreeVAAG;
                a1.Apttus__Primary_Contact__c = c1.id;
                a1.Flag_Auto_Generate_Documents__c = true;
                a1.Flag_Auto_Send_via_DocuSign__c = true;
                
                a1.Apttus__Status_Category__c = 'Request - Auto Document Generation Successful; Auto DocuSign Send Pending';   
            insert a1;
            
            
            //insert attachments
            list<attachment> aList = new list<attachment>();
            
                //insert template attachment
                Attachment attach1 = new Attachment();  
                    attach1.ParentId = a1.id;  
                    attach1.Name = 'DOCX Template.docx';
                      
                    Blob b = Blob.valueOf('Test Data');  
                    attach1.Body = b;             
                aList.add(attach1);  
                
                 //insert template attachment
                Attachment attach2 = new Attachment();  
                    attach2.ParentId = a1.id;  
                    attach2.Name = 'PDFTemplate.pdf';
                      
                    Blob b2 = Blob.valueOf('Test Data');  
                    attach2.Body = b2;             
                aList.add(attach2);  
            
            insert aList;

             //insert template to let us insert templatetogen
            Apttus__APTS_Template__c aTemp1 = new  Apttus__APTS_Template__c();
            aTemp1.name = 'Group V5';
            insert aTemp1;

            // Documents to send are attached to 'template to gen' records.
            Template_To_Gen__c templateGen_A1_1 = new Template_To_Gen__c(
                Send_In_Supplemental_Email_Only__c = false, 
                Sort_Order__c =1, 
                Generated_Attachment_Id__c = attach1.id,
                Status__c = 'Generated',
                Agreement__c = a1.id,
                Template__c = aTemp1.id);
            insert templateGen_A1_1;
             
            //insert DocuSign Default Recipient
            
                Apttus_DocuApi__DocuSignDefaultRecipient2__c ar = new Apttus_DocuApi__DocuSignDefaultRecipient2__c();
                    ar.RecordTypeid = rtRecipientCon;
                    ar.Apttus_DocuApi__ContactId__c = c1.id;
                    ar.Apttus_DocuApi__RoleName__c = 'Signer 1';
                    ar.email_Template__c = e.DeveloperName;
                    ar.Apttus_DocuApi__RecipientType__c = 'Signer';
                    ar.Apttus_DocuApi__SigningOrder__c = 1;
                    ar.Apttus_CMDSign__AgreementId__c = a1.id;
                    ar.Send_Supplemental_Email__c = true;
                insert ar;
                
                          
            //insert docusign connect
            list<DocuSignConnect__c> dscList = new list<DocuSignConnect__c>();
                DocuSignConnect__c dsc1 = new DocuSignConnect__c();
                    dsc1.Name = 'SANDBOX';
                    dsc1.Account_Id__c = 'x';
                    dsc1.Integrator_Key__c = 'x';
                    dsc1.Pass__c = 'x';
                    dsc1.User_Id__c = 'x';
                    dsc1.Web_Service_Url__c = 'x';
                dscList.add(dsc1);  
                
                DocuSignConnect__c dsc2 = new DocuSignConnect__c();
                    dsc2.Name = 'PROD';
                    dsc2.Account_Id__c = 'x';
                    dsc2.Integrator_Key__c = 'x';
                    dsc2.Pass__c = 'x';
                    dsc2.User_Id__c = 'x';
                    dsc2.Web_Service_Url__c = 'x';
                dscList.add(dsc2);  
            insert dscList;             
            
            
        ///Start of Tests
        Test.startTest();
            massSendDocuSignBatch msdsb = new massSendDocuSignBatch(sessionId);
            Database.executeBatch(msdsb,1);
        Test.stopTest();
        
        a1 = [SELECT Apttus__Status__c, Apttus__Status_Category__c, Apttus_CMDSign__DocuSignEnvelopeId__c from Apttus__APTS_Agreement__c WHERE ID=:a1.id];
        system.assertEquals('Ready for Signatures',a1.Apttus__Status__c);
        system.assertEquals('In Signatures',a1.Apttus__Status_Category__c);
        system.assertEquals(0,[SELECT Count() FROM Apttus_DocuApi__DocuSignEnvelope__c WHERE ID=:a1.Apttus_CMDSign__DocuSignEnvelopeId__c]);
                
        
    }
        
}