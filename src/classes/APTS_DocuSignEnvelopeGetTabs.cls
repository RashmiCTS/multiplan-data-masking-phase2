/*
    Author: Harish Emmadi, 07/18/2016 - Used to update the agreement based on docusign tabs after signing.
                                        This class should be updated in 'DocuSign Custom Classes' custom settings for this to work.
                                        This callback class only gets called on docusgin envelope status check batch job, does not work synchronous. 
 * Harish Emmadi, 07/27/2016 - Commented all tags except agreement start date according to Raul's email.
 */
global class APTS_DocuSignEnvelopeGetTabs implements Apttus_DocuApi.IDocuSignEnvelopeGetRecipientTabs2{

    global APTS_DocuSignEnvelopeGetTabs(){
        
    }

    global void getRecipientTabs(List<Apttus_DocuApi.GetRecipientTabsWrapper> iListGetRecipientTabsWrapper){
        
        Map<Id, Apttus__APTS_Agreement__c> agreementsTobeUpdatedMap = new Map<Id, Apttus__APTS_Agreement__c>();
        
        for(Apttus_DocuApi.GetRecipientTabsWrapper objGetRecipientTabsWrapper : iListGetRecipientTabsWrapper)
        {
            try{
                ID agreementId = ID.valueOf(objGetRecipientTabsWrapper.parentId);
                
                System.debug(System.LoggingLevel.ERROR, 'Parent Id == > '+agreementId);
                Apttus_DocuApi__DocuSignDefaultRecipient2__c docuSignDefaultRecipient = null;
                
                System.debug(System.LoggingLevel.ERROR, 'Step 001 ==>');
                Apttus_DocuApi.DocuSignUtil2.DocuSignRecipientStatus objRecipeintStatus = objGetRecipientTabsWrapper.recipientStatus;
                
                System.debug(System.LoggingLevel.ERROR, 'Step 001.1 == >');
                String routingOrder = objRecipeintStatus.routingOrder;
                // we are only interested in signer1, signer3.
                /*if(routingOrder != '1'
                    || routingOrder != '3')
                    continue;*/

                // get the parent agreement associated with the signers.
                Apttus__APTS_Agreement__c agreementSO = agreementsTobeUpdatedMap.containskey(agreementId) ? agreementsTobeUpdatedMap.get(agreementId) : new Apttus__APTS_Agreement__c(Id = agreementId);
                Boolean tabsExists = false;

                Apttus_DocuApi.DocuSignUtil2.RecipientTabs rTabs = objRecipeintStatus.tabs;
                
                System.debug('rTabs are : '+rTabs);
                System.debug('rTabs.textTabs are : '+rTabs.textTabs);
                System.debug('rTabs.dateTabs are : '+rTabs.dateTabs);
                /*
                        Tab             API Name                                    Docusign Role
                    Effective Date  Apttus__Contract_Start_Date__c              Signer3
                    NPI             Provider_NPI__c                             Signer1
                    Tax ID          Provider_TIN__c                             Signer1
                    Date            Apttus__Other_Party_Signed_Date__c          Signer1
                    Name            Apttus__Other_Party_Signed_By_Unlisted__c   Signer1
                    Title           Apttus__Other_Party_Signed_Title__c         Signer1
                */
                /*if(rTabs.textTabs.size() > 0){
                    // possible values for DocuSignUtil2.textTab are String anchorString, String mergeFieldXml, String name, String recipientId, String value
                    for(Apttus_DocuApi.DocuSignUtil2.textTab tTab : rTabs.textTabs){
                        if(tTab.anchorString.contains('\\npi1\\')){
                            agreementSO.Provider_NPI__c = String.valueOf(tTab.value);
                            tabsExists = true;
                        }else if(tTab.anchorString.contains('\\ta1\\')){
                            agreementSO.Provider_TIN__c = String.valueOf(tTab.value);
                            tabsExists = true;
                        }else if(tTab.anchorString.contains('\\na1\\')){
                            agreementSO.Apttus__Other_Party_Signed_By_Unlisted__c = String.valueOf(tTab.value);
                            tabsExists = true;
                        }else if(tTab.anchorString.contains('\\ti1\\')){
                            agreementSO.Apttus__Other_Party_Signed_Title__c = String.valueOf(tTab.value);
                            tabsExists = true;
                        }else if(tTab.anchorString.contains('\\gn1\\')){
                            agreementSO.Group_Name__c = String.valueOf(tTab.value);
                            tabsExists = true;
                        }else if(tTab.anchorString.contains('\\nan1\\')){
                            agreementSO.Notice_Account__c = String.valueOf(tTab.value);
                            tabsExists = true;
                        }else if(tTab.anchorString.contains('\\natt1\\')){
                            agreementSO.Notice_Attention__c = String.valueOf(tTab.value);
                            tabsExists = true;
                        }else if(tTab.anchorString.contains('\\nad1\\')){
                            agreementSO.Notice_Address__c = String.valueOf(tTab.value);
                            tabsExists = true;
                        }else if(tTab.anchorString.contains('\\nc1\\')){
                            agreementSO.Notice_City__c = String.valueOf(tTab.value);
                            tabsExists = true;
                        }else if(tTab.anchorString.contains('\\ns1\\')){
                            agreementSO.Notice_State__c = String.valueOf(tTab.value);
                            tabsExists = true;
                        }else if(tTab.anchorString.contains('\\nzc1\\')){
                            agreementSO.Notice_Zip_Code__c = String.valueOf(tTab.value);
                            tabsExists = true;
                        }
                    }
                }*/
                
                //check if the recipient tab has email tabs
                if(rTabs.emailTabs.size() > 0){
                    //populate the third custom field with the data in the first email tab
                    Apttus_DocuApi.DocuSignUtil2.emailTab eTab = rTabs.emailTabs[0];
                    ///agreementSO.CustomTab3__c = String.ValueOf(eTab.value);
                }

                //check if the recipient tab has date tabs
                if(rTabs.dateTabs.size() > 0){
                   // possible values for DocuSignUtil2.dateTab are String anchorString, String mergeFieldXml, String name, String recipientId, String value
                    for(Apttus_DocuApi.DocuSignUtil2.dateTab dTab : rTabs.dateTabs){
                        /*if(dTab.anchorString.contains('\\d1\\')){
                            agreementSO.Apttus__Other_Party_Signed_Date__c = Date.parse(dTab.value);
                            tabsExists = true;
                        }else */

                        if(dTab.anchorString.contains('\\ef3\\')){
                            agreementSO.Apttus__Contract_Start_Date__c = Date.parse(dTab.value);
                            tabsExists = true;
                        }
                    } 
                }
// Modified by Swarnalata Maruvad - 7/26/2017 --------------------------------------------

                if(rTabs.textTabs.size() > 0){
                    for(Apttus_DocuApi.DocuSignUtil2.textTab tTab : rTabs.textTabs){
                        if(tTab.anchorString.contains('\\ef3\\')){
                            agreementSO.Apttus__Contract_Start_Date__c = Date.parse(tTab.value);
                            tabsExists = true;
                        }
                    } 
                }

//----------------------------------------------------------------------------------------
                if(tabsExists == true)
                    agreementsTobeUpdatedMap.put(agreementId, agreementSO);
            } catch(Exception ex){
                System.debug('ERROR AT the :'+ex.getMessage());
            }
        }

        System.System.debug(loggingLevel.Error, '*** agreementsTobeUpdatedMap: ' + agreementsTobeUpdatedMap);
        try{
            if(!agreementsTobeUpdatedMap.isEmpty())
                update agreementsTobeUpdatedMap.values();
        }catch(Exception ex){
            System.debug(loggingLevel.Error, '*** Error updating docusign tabs on agreement.: Error \''+ex.getMessage()+' at ' + ex.getLineNumber());
        }
    }
}