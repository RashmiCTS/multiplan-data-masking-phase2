/*
File Name:   scheduleMassGenerateDocumentBatch
Author:      Michael Olson
Date:        5/7/2016
Description: Schedules batch apex for massGenerateDocumentBatch

Modification History
4/13/2016		Michael Olson		Created  
5/26/2016		Michael Olson		massGenerate batch was changed - query parameter removed.
6/6/2016		Michale Olson		Add V5 Group Logic
*/

global class scheduleMassGenerateDocumentBatch implements Schedulable{  

	private String sessionId;

	global scheduleMassGenerateDocumentBatch( String sessionId ) {
	    this.sessionId = sessionId;
	}
      
      global void execute(SchedulableContext sc) { 			 
      	massGenerateDocumentBatch  batchApex = new massGenerateDocumentBatch(sessionId);  
        Database.executeBatch(batchApex,1);
   } 
}