/*
File Name:   massSendDocuSignBatch
Author:      Michael Olson  
Date:        5/25/2016
Description: apex batch for DocusignAPI

Modification History
5/25/2016   Michael Olson   Created
6/6/2016    Michale Olson   Add V5 Group Logic
7/6/2016    Michael Olson   For V5 Group; created multiple DocuSign Recipients; Auto Generate the Exhibits B,D, and SLCP - stateId
9/14/2016   Jason Hartfield Updated to fix issues with going over the heap size
*/ 
public class massSendDocuSignBatch implements Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts { 

    private static final boolean isSandbox = isSandboxChecker.isSandboxChecker();
    private static string dscInstance = !isSandbox ? 'PROD' : 'SANDBOX'; 
    private static final string waitingToSendStatus = 'Request - Auto Document Generation Successful; Auto DocuSign Send Pending';
    //--------------------------------------------------------------------------
    //Method that is called to begin the document generation process!
    //--------------------------------------------------------------------------  
    string sessionId;
    
    public massSendDocuSignBatch(string sessionId) {
      this.sessionId = sessionId;
    }

    // This grabs all fields for a given object.   This could be moved to a separte utilities class...
    // This is no longer really necessary.  We put this in originally because we were letting users
    // merge their email templates with the agreement obect, but now we no longer do that so we could hard code
    // the necessary fields to reduce code complexity.
    public static string SelectStar(sObject newObject){
        string pAllFields ='';
                
        Map<String, Schema.SObjectField> fields = newObject.getSObjectType().getDescribe().fields.getMap();
        
        List<String> validFields = new List<string>();
        for(String s:fields.keySet())
        {
            SObjectField fieldToken = fields.get(s);
            DescribeFieldResult selectedField = fieldToken.getDescribe();
            if(selectedField.getType() != Schema.DisplayType.Location)
                validFields.add(selectedField.getName());
        }
        pAllFields = string.join(validFields,',');
        return pAllFields;
    }
    //--------------------------------------------------------------------------
    //Query is run to start process
    //--------------------------------------------------------------------------
    public Database.QueryLocator start(Database.BatchableContext BC) {

      string sQuery = 'SELECT Id , Apttus__Account__c , Apttus__FF_Agreement_Number__c ' +
              'from Apttus__APTS_Agreement__c ' +     
              'where Flag_Auto_Send_via_DocuSign__c = true ' +    
              'AND Apttus__Status_Category__c = :waitingToSendStatus ' +   
              'order by Id';
      
      return Database.getQueryLocator(sQuery); 
    }
  
  //--------------------------------------------------------------------------
    //Work through the scope to generate
    //--------------------------------------------------------------------------
    public void execute(Database.BatchableContext BC, list<Apttus__APTS_Agreement__c> scope) {
      // Scope is exactly 1 or this will fail
      // It needs to be one because we want to make callouts and do dml after the callout.  If we do a loop
      // it would break.
      system.assertEquals(1,scope.size(), 'Scope has to be exactly 1.');
      Apttus__APTS_Agreement__c agreement = scope[0];
      ID agreementId = agreement.id;
      // Get the full agreement fresh, also make sure the status is still correct.
      string allFields = SelectStar(new Apttus__APTS_Agreement__c()); 
      string sQuery = 'SELECT ' + allFields + ',RecordType.Name, Owner.Email,Owner.Name ' +
              'from Apttus__APTS_Agreement__c ' +     
              'where id=:agreementId';
      agreement =  (Apttus__APTS_Agreement__c) Database.query(sQuery)[0];
      // If the status somehow changed to any other status that would indicate there was a success or error 
      // between the batch job kickoff and execute, don't process it.  
      if(agreement.Apttus__Status_Category__c != waitingToSendStatus)
        return;

      try {
        doSend(agreement, SessionId); 
      } catch(Exception ex) {
        // If there is a problem sending, we just set the Agreement to a status to let a user manually do the send.
        string errorMessage = ex.getMessage() + ' \r\n' + ex.getStackTraceString();
        //envListError.add(sendResult);
        // Set agreement status so we know something broke and we can look into it.
        agreement.Apttus__Status_Category__c = 'In Signatures';
        agreement.Apttus__Status__c = 'Ready for Signatures'; 
        agreement.Auto_Generation_Errors__c = errorMessage;
        update agreement;
        return;
      }
       
      update agreement; 
      
   }

  //--------------------------------------------------------------------------
  // After batch is ran run, see if we need to kick of a new job.  
  //--------------------------------------------------------------------------
  public void finish(Database.BatchableContext BC) {    
      // If we see there are still agreements that need to be sent, we will restart this job at the beginning of the next hour to 
      // clean those up.  That takes care of misc Docusign API issues, like if we hit limits or the API is simply down.
      integer pendingAgreementsCount = [SELECT Count() FROM Apttus__APTS_Agreement__c WHERE Apttus__Status_Category__c = :waitingToSendStatus LIMIT 1000];
      system.debug('Pending Agreement count:' + pendingAgreementsCount);
      if(pendingAgreementsCount > 0){
         // If a job is already scheduled, cancel it
        for(CronTrigger ct : [SELECT Id, CronJobDetail.Name, CronJobDetail.JobType
                        FROM CronTrigger
                        WHERE CronJobDetail.Name like 'Mass Send Docusign Batch Retry' AND State != 'COMPLETE' ORDER BY CreatedDate DESC LIMIT 1])
         system.abortJob(ct.id);
         
        // How many minutes from now is the next hour?
        
        integer nextHourMinutesFromNow = (60 - DateTime.now().minute());
        integer nextJobMinutesFromNow = 5; // We want to wait at least five minutes before we start the next job.
        if(nextHourMinutesFromNow > nextJobMinutesFromNow)
          nextJobMinutesFromNow = nextHourMinutesFromNow;
        System.scheduleBatch(new massSendDocuSignBatch(sessionId), 'Mass Send Docusign Batch Retry', nextJobMinutesFromNow, 1);
      }
     
      
  }  


  // This is the meat: given an agreement, it will pull the details and send out documentation
  // via docusign as appropriate.
  // This procedure:
  // * Merges word documents with records and spits out a PDF via Apttus magic.
  // * Sends those documents out to appropriate recipients via docusign
  // * Sends out a second email through Salesforce with 'Supplemental' information that is not a part of the contract
  public void doSend(Apttus__APTS_Agreement__c agreeRec, string sessionId) {
    // Setup - Find the email address that our email service uses to send docusign documents. 
    // Figure out what the email address is that we need to send to.  Removes element of needing configuration setting.
    string triggerEmailAddress;
    try{
      ApexClass emailServiceClass = [SELECT ID FROM ApexClass WHERE Name='SendDocusignEmailHandler'];
      EmailServicesFunction emailService =[SELECT ID FROM EmailServicesFunction WHERE ApexClassId=:emailserviceClass.id];
      EmailServicesAddress serviceAddress = [SELECT ID,LocalPart,EmailDomainName FROM EmailServicesAddress WHERE IsActive =:true AND FunctionId =:emailService.id LIMIT 1];
      triggerEmailAddress = serviceAddress.LocalPart + '@' +serviceAddress.EmailDomainName;
    } catch(exception ex) {
      throw new CustomException('Email service is not set up correctly.   Make sure there is an email service built for "SendDocusignEmailHandler" with an active email.');
    }
     
    //------------------------------------------------------------
    // Find and validate all recipients
    //------------------------------------------------------------         
    list<Apttus_DocuApi__DocuSignDefaultRecipient2__c> apttusRecipients = [SELECT Apttus_DocuApi__ReadOnlyEmail__c, Apttus_DocuApi__ReadOnlyFirstName__c, Apttus_DocuApi__ReadOnlyLastName__c, 
                              Apttus_DocuApi__ContactId__c, Contact_Full_Name__c, Apttus_DocuApi__EmailTemplateUniqueName__c, 
                              Apttus_DocuApi__SigningOrder__c, Apttus_DocuApi__RecipientType__c, Apttus_DocuApi__RoleName__c,
                              Apttus_DocuApi__SigningGroupId__r.Apttus_DocuApi__SigningGroupId__c,
                              Apttus_DocuApi__SigningGroupId__r.Name,Send_Supplemental_Email__c 
                            FROM Apttus_DocuApi__DocuSignDefaultRecipient2__c 
                            WHERE Apttus_CMDSign__AgreementId__c =: agreeRec.id 
                            order by Apttus_DocuApi__SigningOrder__c, Apttus_DocuApi__RecipientType__c];
    
    // Bulkification: get all email templates we want to use.
    Map<string,EmailTemplate> emailTemplatesByName = new Map<string,EmailTemplate>();
    for(Apttus_DocuApi__DocuSignDefaultRecipient2__c apttusRecipient : apttusRecipients){
      emailTemplatesByName.put(apttusRecipient.Apttus_DocuApi__EmailTemplateUniqueName__c,null);
    }
    for(EmailTemplate et : [SELECT Subject,body,developerName FROM EmailTemplate WHERE developerName in: emailTemplatesByName.keySet()]){
      // TODO: Do we want to throw exception if we see a merge field, since APTTUS send function won't merge them for us?
      emailTemplatesByName.put(et.developerName,et);
    }
         
    // Check the list of recipients to make sure they are all valid for sending and that email templates exist for them.
    for(Apttus_DocuApi__DocuSignDefaultRecipient2__c apttusRecipient : apttusRecipients) {             
      string userName = apttusRecipient.Apttus_DocuApi__ReadOnlyFirstName__c + ' ' + apttusRecipient.Apttus_DocuApi__ReadOnlyLastName__c;
      // Don't add recipients that have no email address
      if(string.IsBlank(apttusRecipient.Apttus_DocuApi__ReadOnlyEmail__c)) 
           throw new CustomException('Recipient "' + UserName + '" does not have an email address.');
      // Check that all the email addresses we are using are valid, and if we are in a sandbox they are not going externally.
      if(apttusRecipient.Apttus_DocuApi__ReadOnlyEmail__c != '(Signing Group)'){
        // If anyone doesn't have a valid email address, throw an exception, as we don't want to send anything then.
        if(!IsValidEmail(apttusRecipient.Apttus_DocuApi__ReadOnlyEmail__c)){
          throw new CustomException('Recipient "' + UserName + '" does not have a valid email: "' + apttusRecipient.Apttus_DocuApi__ReadOnlyEmail__c + '"');
        }
        //Logic to avoid sending mass emails external contacts from a sandbox 
        if(!IsAllowedEmail(apttusRecipient.Apttus_DocuApi__ReadOnlyEmail__c)){
          throw new CustomException('Recipient "' + UserName + '" has an external email address, which is not allowed to be sent from a Sandbox: "' + apttusRecipient.Apttus_DocuApi__ReadOnlyEmail__c + '".  Only email addresses ending with "multiplan.com", "marsaudior.com", "apttus.com", or ".sandboxNoSend" are allowed.');
        }
      }
                      
      // Every signer can have their own email template.  And actually, according to documentation,
      // if one recipient has a personalized template, they all should have one...
      EmailTemplate et = emailTemplatesByName.get(apttusRecipient.Apttus_DocuApi__EmailTemplateUniqueName__c);
      if(et == null){
        // An email notification needs to be defined for every recipient, per Docusign requirements.
        // So if we are missing a template, we cannot proceed.
          throw new CustomException('Email template "' + apttusRecipient.Apttus_DocuApi__EmailTemplateUniqueName__c + '"" was not found for recipient "' + UserName + '"' );
      }   
    } 

  //------------------------------------------------------------
  // Add all Documents, generate pdfs from word documents.
  //------------------------------------------------------------    
    
    //create a list of Documents to create using the templates for extended information.
    list<id>  attachmentIdsForSending = new List<Id>();
    List<Template_To_Gen__c> allDocusignStubsForAgreement = [SELECT ID, Status__c, Generation_Error__c, Send_In_Supplemental_Email_Only__c,Sort_Order__c,Generated_Attachment_Id__c FROM Template_To_Gen__c WHERE Agreement__c = :agreeRec.id AND Status__c = 'Generated' AND Send_In_Supplemental_Email_Only__c = false ORDER BY Sort_Order__c];

    // Run through all of the documents that were generated and add them to the signing envelope 
    // as necessary.
    for(Template_To_Gen__c docStub : allDocusignStubsForAgreement){
      
      // This is done in an 'unbulkified' way to keep heap size down.
      Attachment att = [select id, name, body, contentType from Attachment where id = :docStub.Generated_Attachment_Id__c];  
      boolean isPdf = false;
      if(att.contentType == null) {
          isPdf = att.name.substringAfterLast('.').containsIgnoreCase('pdf') ? true : false;               
      } else if(att.contentType.containsIgnoreCase('pdf')) { 
          isPdf = true;
      }  

      if(isPdf){
          attachmentIdsForSending.add(docStub.Generated_Attachment_Id__c);
      } else { 
      // We assume any non-pdf is a word doc.  Maybe we should specifically make sure?
        //Use APTTUS API to convert docx to pdf
        Id docId = att.id;
        String filename = att.name;
        Boolean addWatermark = false;
        Boolean removeWatermark = false;
        String headerText = '';
        String footerText = '';

        String apiServerURL = System.Url.getSalesforceBaseUrl().toExternalForm() + '/services/Soap/u/14.0/' + userInfo.getOrganizationId().left(15); 
        
        //Make Webservice Call to convert this doc to PDF via apttus magic
        Id convertedDocId;
        if(!test.isRUnningTest()) {
            convertedDocId = Apttus.MergeWebService.convertWordDocToPDF(docId, fileName,addWatermark, removeWatermark, headerText, footerText, SessionId, apiServerURL);
         } else {
             Attachment insertedDoc = new Attachment(Name='ConvertedDOCX.pdf',Body=blob.valueOf('testbody'),ParentId=docStub.id);
             insert insertedDoc;
             convertedDocid = insertedDoc.id;
         }
         attachmentIdsForSending.add(convertedDocid);
      }
     
    }

    // Now all attachments have been converted to pdfs for docusign to manage.
    // HACK: Send an email out to trigger their batch job to fire that will actually do the send.
    SendDocusignEmailHandler.SendInfo info = new SendDocusignEmailHandler.SendInfo();
    info.agreementId = agreeRec.id;
    info.attachmentIds = attachmentIdsForSending;

    
    // HACK: to get around SFDC outbound email limits - create a user to send the email to. 
    User usr;
    List<User> users = [SELECT ID,Email FROM User WHERE email = :triggerEmailAddress];
    if(users.size() >0){
      usr = users[0];
    } else {
      Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Chatter Free User' LIMIT 1];
      usr = new User(LastName = 'Email Service Trigger User',
                       FirstName='',
                       Alias = 'docusign',
                       Email = triggerEmailAddress,
                       Username = triggerEmailAddress.substringBetween('@','.') + '@multiplan.com',
                       ProfileId = profileId.id,
                       TimeZoneSidKey = 'GMT',
                       LanguageLocaleKey = 'en_US',
                       EmailEncodingKey = 'UTF-8',
                       LocaleSidKey = 'en_US'
                       );
      insert usr;
    }

    Messaging.SingleEmailMessage triggerEmail = new Messaging.SingleEmailMessage();
    triggerEmail.setSubject(agreerec.id);
    triggerEmail.setPlainTextBody(JSON.serialize(info));
    triggerEmail.saveAsActivity = false;
    triggerEmail.setTargetObjectId(usr.id); 

    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { triggerEmail });   
  }

  // Should be called from the envelope insert trigger (ApttusDocuSignEnvelopeTriggers)
  // This is actually fired in a trigger when we detect that an envelope has been added to an agreement waiting
  // for the docusign job to send.  It send out extra documents via Salesforce that aren't really part of the Docusign
  // contract directly.
  public static void CheckAndSendSupplementalEmail(ID agreementId){

    try{
      if(agreementId != null){
        Apttus__APTS_Agreement__c agreeRec = [SELECT ID,Name, Mass_Document_Send__c, Apttus__Status_Category__c, Supplemental_Email_Template_Unique_Name__c, Owner.Name, Owner.Email  FROM Apttus__APTS_Agreement__c WHERE ID = :agreementId];
       
        if(agreeRec.Apttus__Status_Category__c == waitingToSendStatus && agreeRec.Mass_Document_Send__c != null){
          try{
            SendSupplementalEmail(agreeRec);
          } catch(Exception ex){
            // Add the error to the agreement record status so we can examine it.
            agreeRec.Auto_Generation_Errors__c = ex.getMessage() + ' \r\n' + ex.getStackTraceString();
            update agreeRec;
          }
          // See if the entire batch is done so we can send an email message about the status.
          CheckBatchAndSendEmailSummary(agreeRec.Mass_Document_Send__c);
        }
      }
    } catch (Exception ex){
       // Since this is part of a trigger, we need to make sure to capture and swallow errors rather than
       // making the trigger insert fail.
       // TODO: Send an email?
       system.debug(ex);
    }
  }
 

  private static void SendSupplementalEmail(Apttus__APTS_Agreement__c agreeRec){
     //------------------------------------------------------------
    // Find and validate all recipients
    //------------------------------------------------------------         
    list<Apttus_DocuApi__DocuSignDefaultRecipient2__c> apttusRecipients = [SELECT Apttus_DocuApi__ReadOnlyEmail__c, Apttus_DocuApi__ReadOnlyFirstName__c, Apttus_DocuApi__ReadOnlyLastName__c, 
                              Apttus_DocuApi__ContactId__c, Contact_Full_Name__c, Apttus_DocuApi__EmailTemplateUniqueName__c, 
                              Apttus_DocuApi__SigningOrder__c, Apttus_DocuApi__RecipientType__c, Apttus_DocuApi__RoleName__c,
                              Apttus_DocuApi__SigningGroupId__r.Apttus_DocuApi__SigningGroupId__c,
                              Apttus_DocuApi__SigningGroupId__r.Name,Send_Supplemental_Email__c 
                            FROM Apttus_DocuApi__DocuSignDefaultRecipient2__c 
                            WHERE Apttus_CMDSign__AgreementId__c =: agreeRec.id 
                            order by Apttus_DocuApi__SigningOrder__c, Apttus_DocuApi__RecipientType__c];

    // Get all stubs that need to be sent in the Supplemental email
    List<Template_To_Gen__c> allSupplementalStubsForAgreement = [SELECT ID, Status__c, Sort_Order__c,Generated_Attachment_Id__c FROM Template_To_Gen__c WHERE Agreement__c = :agreeRec.id AND Status__c = 'Generated' AND Send_In_Supplemental_Email_Only__c = true ORDER BY Sort_Order__c];

    // There are some items that were marked 'send via attachment' that were not sent as a docusign
    // document.  We need to send those as a separate email.
    List<Messaging.Emailfileattachment> fileAttachments = new List<Messaging.Emailfileattachment>();
    List<Email_Monitoring_User__mdt> mdtEmailMonitor = new List<Email_Monitoring_User__mdt>();
    List<String> lstStrAddr = new List<String>();
    for(Template_To_Gen__c docStub : allSupplementalStubsForAgreement){
      // This is done in an 'unbulkified' way to keep heap size down.
      Attachment att = [select id, name, body, contentType from Attachment where id = :docStub.Generated_Attachment_Id__c]; 
      // Add to attachment file list
      Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
      efa.setFileName(att.name);
      efa.setBody(att.Body);   
      fileAttachments.add(efa);
    }
    
    if(fileAttachments.size() > 0){

      Messaging.SingleEmailMessage mail1 = new Messaging.SingleEmailMessage();
      mail1.setFileAttachments(fileAttachments);
      
      
      //mail1.setSubject('Application for ' + agreeRec.name);
      //mail1.setPlainTextBody('Please review the attached documents.');
      // Get the template to use to send this email if one exists.
      List<EmailTemplate> ets = [SELECT id,Subject,body,developerName FROM EmailTemplate WHERE developerName =: agreeRec.Supplemental_Email_Template_Unique_Name__c];
      mail1.setTemplateID(ets[0].Id);
      mail1.setSenderDisplayName(agreerec.owner.Name);
      mail1.setReplyTo(agreerec.owner.email);
      mail1.saveAsActivity = true;        
      mdtEmailMonitor = [SELECT Mass_Send_Email_User__c FROM Email_Monitoring_User__mdt WHERE DeveloperName='MPIAutomation_User'];
      if(!mdtEmailMonitor.isEmpty()){
          lstStrAddr.add(mdtEmailMonitor[0].Mass_Send_Email_User__c);
          mail1.setCcAddresses(lstStrAddr);
      }  
      /********************* Start add target object Id to resolve merge fields-Added by Rashmi T *************************/
       Contact c = [select id, Email from Contact where email <> null limit 1];
      mail1.setTargetObjectId(c.Id);
        
      mail1.setWhatId(agreeRec.id);
        mail1.setTreatTargetObjectAsRecipient(false);
      /********************* End add target object Id to resolve merge fields-Added by Rashmi T *************************/
        
      // Do we want to set a target object id?  This was in old code but doesn't seem necessary.
      //mail1.setTargetObjectId(docuSignRecipient_List[0].Apttus_DocuApi__ContactId__c); //needs advanced logic
      List<string> recipientEmails = new List<string>();
      for(Apttus_DocuApi__DocuSignDefaultRecipient2__c apttusRecipient : apttusRecipients) {  
          if(!apttusRecipient.Send_Supplemental_Email__c) continue;
          
          string email = apttusRecipient.Apttus_DocuApi__ReadOnlyEmail__c;
          if(string.IsBlank(email)) 
            continue; 
          if(email == '(Signing Group)'){
            // Get the members of the signing group.  Nested FOR should be ok as we anticipate a very small number of groups.
            string signingGroupId = apttusRecipient.Apttus_DocuApi__SigningGroupId__c;
            for(Apttus_DocuApi__DocuSignSigningGroupMembers__c member : [SELECT Apttus_DocuApi__UserName__c, Apttus_DocuApi__Email__c FROM Apttus_DocuApi__DocuSignSigningGroupMembers__c WHERE Apttus_DocuApi__SigningGroupId__c = :signingGroupId]){
              recipientEmails.add(mangleEmail(member.Apttus_DocuApi__Email__c));
            }
            
          } else {
            recipientEmails.add(mangleEmail(email));
          }
      }

      if(recipientEmails.size() > 0) {
        mail1.setToAddresses(recipientEmails); 
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail1 });
      }
    }
    
    // Change the status so we know the process is completely done.                      
    agreeRec.Apttus__Status_Category__c = 'In Signatures';
    agreeRec.Apttus__Status__c = 'Other Party Signatures';  
    update agreeRec;
   }

  // As envelopes are processed, we should be done sending a complete agreement.  Check the batch that the agreement
  // was a part of and see if all of the agreements are done.  If so, send a summmary email out.
  private static void CheckBatchAndSendEmailSummary(ID MassDocumentSendID){
      
    // First, is the batch all done?  We should be able to simply check for agreements with envelopes.
    Mass_Document_Send__c massDocSend = [SELECT ID,Contracts_Sent__c,CreatedDate,CreatedBy.Email FROM Mass_Document_Send__c WHERE ID = :MassDocumentSendID];
    // There are three statuses that we set that indicate we are waiting for an agreement to finish.
    integer outstandingAgreements = [SELECT COUNT() FROM Apttus__APTS_Agreement__c WHERE Mass_Document_Send__c = :MassDocumentSendID 
      AND Flag_Auto_Send_via_DocuSign__c = true AND (Apttus__Status_Category__c = 'Request - Auto Document Generation Successful; Auto DocuSign Send Pending'
      OR Apttus__Status_Category__c = 'Request - Auto Document Generation Pending; Auto DocuSign Send Pending')];
    

    if(outstandingAgreements == 0){
      List<Apttus__APTS_Agreement__c> sentAgreements = [SELECT ID, Name, RecordType.Name, Auto_Generation_Errors__c FROM Apttus__APTS_Agreement__c WHERE Mass_Document_Send__c = :MassDocumentSendID AND Flag_Auto_Send_via_DocuSign__c = true ORDER BY Apttus__FF_Agreement_Number__c];
      List<Apttus__APTS_Agreement__c> failures = new List<Apttus__APTS_Agreement__c>();
      List<Apttus__APTS_Agreement__c> success = new List<Apttus__APTS_Agreement__c>();
      for(Apttus__APTS_Agreement__c agreement : sentAgreements){
        if(string.IsBlank(agreement.Auto_Generation_Errors__c))
          success.add(agreement);
        else
          failures.add(agreement);
      }

      String agreeList = '';

      integer i=1;
      if(!failures.isEmpty()) {  
        agreeList += 'List of Agreements with Failures: <br/>';                 
        for(Apttus__APTS_Agreement__c agreement : failures) {
          agreeList += i + '. (Agreement Name: ' + agreement.Name + 
                  ',  RT: ' + agreement.RecordType.Name +
                  ',  errorMessage: ' + agreement.Auto_Generation_Errors__c +  
                  ',  Link: '+ System.URL.getSalesforceBaseUrl().toExternalForm() + '/' + agreement.Id +
                  '); <br/>';
          i++;          
        } 
      } 

      if(!success.isEmpty()) { 
        agreeList += 'List of Agreements with Success: <br/>';                    
        for(Apttus__APTS_Agreement__c agreement : success) {
          agreeList += i + '. (Agreement Name: ' + agreement.Name + 
                  ',  RT: ' + agreement.RecordType.Name +  
                  ',  Link: '+ System.URL.getSalesforceBaseUrl().toExternalForm() + '/' + agreement.Id +
                  '); <br/>';
          i++;          
        } 
      }                        
                              
      // Send an email to the Apex job's submitter notifying of job completion.
      Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
      //Query the AsyncApexJob object to retrieve the current job's information.
      
      String[] toAddresses = new String[] {massDocSend.CreatedBy.Email};
      mail.setToAddresses(toAddresses);

      mail.setSubject('Auto Send of DocuSign Documentation is Complete');
      // We add the batch job errors to the captured errors, becuase the batch job errors were uncaught and so are not in our error list.
      mail.setHtmlBody('A total of ' + sentAgreements.size() + ' Agreements were processsed.  There were '+ failures.size() + ' failures.  More details can be found here: '+ System.URL.getSalesforceBaseUrl().toExternalForm() + '/' + massDocSend.Id +'<br/><br/>' + agreeList);
    
      Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); 

    }

      
  }

  // returns an email that won't send but only for people outside of the organization and only if we are in a sandbox.
  // This prevents us from sending email to actual customers when working in a full sandbox, for example, with real data
  public static string mangleEmail(string targetEmail){
    if( isSandbox ) {
        // Emails to external entities should all be mangled in the sandbox.
        if(!targetEmail.containsIgnoreCase('@multiplan.com') 
          && !targetEmail.containsIgnoreCase('@marsauditor.com') && !targetEmail.containsIgnoreCase('jasonhartfield@gmail.com')) {
            targetEmail += '.sandboxNoSend';
        }
      }
    return targetEmail;
  }

  public static boolean isAllowedEmail(string targetEmail){
     boolean isAllowedEmail = true;
     if( isSandbox ) {
        // Emails to external entities should all be mangled in the sandbox.
        if(!targetEmail.containsIgnoreCase('@multiplan.com') &&
           !targetEmail.containsIgnoreCase('@marsauditor.com') && 
          !targetEmail.containsIgnoreCase('jasonhartfield@gmail.com') && 
          !targetEmail.containsIgnoreCase('1771.co') &&
          !targetEmail.containsIgnoreCase('apttus.com') && 
          !targetEmail.containsIgnoreCase('sandboxNoSend') && 
          !targetEmail.containsIgnoreCase('example')) {

            isAllowedEmail = false;
        }
      }
    return isAllowedEmail;
  }

  // Makes sure that the email address is  not blank and has a valid format.
  public static boolean isValidEmail(string targetEmail){
     if(string.IsBlank(targetEmail)) return false;
     // Very basic email validation.
     // Is there a single '@' sign?
     if(targetEmail.split('@').size() !=  2) return false;
     // is there at least one dot after the '@' sign? (split is regex, so we escape it)
     if(targetEmail.split('@')[1].split('\\.').size() < 2) return false;
     // if we get this far, we are ok
     return true;
  } 
}