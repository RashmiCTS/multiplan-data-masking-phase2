/*==================================================================================
File Name:   NS_NIS_Request_TriggerHandler
Author:      Rashmi T
Date:        3/13/2017
Description: 

Modification History

Date            Editor              Description
3/13/2017       Rashmi T       		To handle the logic for updating the Locality_Name_ZipCode__c to match the locality name
									depending on the Rate_Data_Source_Year__c.
====================================================================================*/
public class NS_NIS_Request_TriggerHandler {

    public static void updateZipCodeBasedOnRateDataSourceYear(List<NS_NIS_Request__c> lstNDHCEReq){
        Map<String,List<NS_NIS_Request__c>> mapZipCodevsNDHCE = new Map<String,List<NS_NIS_Request__c>>();
		Map<String,Zip_Code_Override__c> mapZipOverVSZipOverLocality = new Map<String,Zip_Code_Override__c>();
        List<Zip_Code_Override__c> lstZipCodeOverride = new List<Zip_Code_Override__c>();
        String strZipRate = '';
        Set<String> setZipCodes = new Set<String>();
        if(!lstNDHCEReq.isEmpty()){
            
            for(NS_NIS_Request__c nr : lstNDHCEReq){
                if(nr.Zip_Code__c <> NULL && nr.Rate_Data_Source_Year__c <> NULL){
                    
                    strZipRate = nr.Zip_Code__c+nr.Rate_Data_Source_Year__c;
                    if(mapZipCodevsNDHCE.containsKey(nr.Zip_Code__c)){
                        mapZipCodevsNDHCE.get(strZipRate).add(nr);
                    }
                    else{
                        mapZipCodevsNDHCE.put(strZipRate, new List<NS_NIS_Request__c> {nr} );
                    }
                    setZipCodes.add(nr.Zip_Code__c);
                }                
            }
            System.debug('******* mapZipCodevsNDHCE '+mapZipCodevsNDHCE);

            if(!setZipCodes.isEmpty()){ 
                Map<Id,Zip_Code__c> mapZipCodes = new Map<Id,Zip_Code__c>([SELECT id,Name,RBRVS_Locality__c FROM Zip_Code__c 
                                                                          WHERE id IN :setZipCodes]);
                for(Zip_Code_Override__c zip : [SELECT id,Name,Rate_Data_Source_Year__c,RBRVS_Locality__c,Zip_Code__c,
                                                Zip_Code__r.Name,Zip_Code__r.RBRVS_Locality__c
                                                FROM Zip_Code_Override__c
                                                WHERE Zip_Code__c IN:setZipCodes]){
					
                	strZipRate = zip.Zip_Code__c + zip.Rate_Data_Source_Year__c;
					mapZipOverVSZipOverLocality.put(strZipRate,zip);                                                    
                }
                System.debug('******* mapZipOverVSZipOverLocality '+mapZipOverVSZipOverLocality);
				if(!mapZipOverVSZipOverLocality.isEmpty()){
                    for(String z : mapZipCodevsNDHCE.keySet()){
                        strZipRate = '';
                        if(mapZipOverVSZipOverLocality.containsKey(z)){                            
                            strZipRate = (mapZipOverVSZipOverLocality.get(z).RBRVS_Locality__c <> NULL ? mapZipOverVSZipOverLocality.get(z).RBRVS_Locality__c : '');
                        }
                        System.debug('******* strZipRate '+strZipRate);
                        for(NS_NIS_Request__c nd : mapZipCodevsNDHCE.get(z)){
                            if(String.isNotBlank(strZipRate)){
                                nd.Locality_Name_ZipCode__c = strZipRate;
                            }
                            else if(mapZipCodes.containsKey(nd.Zip_Code__c)){
                                nd.Locality_Name_ZipCode__c = mapZipCodes.get(nd.Zip_Code__c).RBRVS_Locality__c;
                            }
                        }
                    }
				}
            }
        }        
    }
}