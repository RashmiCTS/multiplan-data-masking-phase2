/*
File Name:   UtilityForTestData
Author:      Rashmi Thippeswamy
Date:        01/12/2017
Description: A utility class to create all the test data for usage in the test classes
Modification History
01/12/2016      Rashmi T       Created.
*/
@isTest
public class UtilityForTestData {

    //Create User
    public static User createUser(User usr){
        User runUser = new User();
        Profile prf = [SELECT id FROM Profile WHERE Name = 'System Administrator' LIMIT 1 ];
        if(usr == null){
            usr = new User();
        }
        runUser = usr;            
        runUser.TimeZoneSidKey = (usr.TimeZoneSidKey <> NULL ? usr.TimeZoneSidKey :'America/New_York');   
        runUser.LanguageLocaleKey = (usr.LanguageLocaleKey <> NULL ? usr.LanguageLocaleKey :'en_US');         
        runUser.LocaleSidKey = (usr.LocaleSidKey <> NULL ? usr.LocaleSidKey :'en_US');    
        runUser.EmailEncodingKey = (usr.EmailEncodingKey <> NULL ? usr.EmailEncodingKey :'UTF-8');  
        runUser.ProfileId = (usr.ProfileId <> NULL ? usr.ProfileId :prf.Id);
        runUser.Title  = (usr.Title <> NULL ? usr.Title :'testTitle');
        runUser.FirstName = (usr.FirstName <> NULL ? usr.FirstName :'testFName');
        runUser.LastName = (usr.LastName <> NULL ? usr.LastName :'testLName');
        runUser.email = (usr.email <> NULL ? usr.email :'testemail@testmultiplan.com');
        runUser.Username = (usr.Username <> NULL ? usr.Username :'tRUsr3333@testmultiplan.com'); 
        runUser.Alias = (usr.Alias <> NULL ? usr.Alias :'A123Nick');
        runUser.CommunityNickname =  (usr.CommunityNickname <> NULL ? usr.CommunityNickname :'nick1234');
        runUser.Phone = (usr.Phone <> NULL ? usr.Phone :'1234567890');
        runUser.Auto_Assign_Rate_Operation_Request_Type__c = (usr.Auto_Assign_Rate_Operation_Request_Type__c <> NULL ? usr.Auto_Assign_Rate_Operation_Request_Type__c :'Fee Schedule Request;Non-Standard Rate Exhibit Lang Rev HCFA');
        runUser.Auto_Assign_Rate_Rev_HCFA_RT_Assignable__c = (usr.Auto_Assign_Rate_Rev_HCFA_RT_Assignable__c <> NULL ? usr.Auto_Assign_Rate_Rev_HCFA_RT_Assignable__c :true);
        runUser.Allow_Kickoff_of_CLAT_Inload_Process__c = true;
        insert runUser;
        return runUser;
    }
      
    public static Account createAccount(Account acc){
    	Account acctCreate = new Account();
        if(acc == null){
            acc = new Account();
        }
        acctCreate = acc;            
        acctCreate.Name = (acc.Name <> NULL ? acc.Name :'TestAcc');   
        acctCreate.TIN__c = (acc.TIN__c <> NULL ? acc.TIN__c :'813907096');
        acctCreate.ShippingStreet = (acc.ShippingStreet <> NULL ? acc.ShippingStreet :'2925 DeBarr Rd Suite D 240');
        acctCreate.ShippingCity = (acc.ShippingCity <> NULL ? acc.ShippingCity :'Anchorage');
        acctCreate.ShippingState = (acc.ShippingState <> NULL ? acc.ShippingState :'AK');
        acctCreate.ShippingPostalCode = (acc.ShippingPostalCode <> NULL ? acc.ShippingPostalCode :'99508');
        acctCreate.ShippingCountry = (acc.ShippingCountry <> NULL ? acc.ShippingCountry :'United States');
        acctCreate.BillingStreet = (acc.BillingStreet <> NULL ? acc.BillingStreet :'2925 DeBarr Rd Suite D 240');
        acctCreate.BillingCity = (acc.BillingCity <> NULL ? acc.BillingCity :'Anchorage');
        acctCreate.BillingState = (acc.BillingState <> NULL ? acc.BillingState :'AK');
        acctCreate.BillingPostalCode = (acc.BillingPostalCode <> NULL ? acc.BillingPostalCode :'99508');
        acctCreate.BillingCountry = (acc.BillingCountry <> NULL ? acc.BillingCountry :'United States');
        acctCreate.Service_Location_City__c = (acc.Service_Location_City__c <> NULL ? acc.Service_Location_City__c :'Anchorage');
        acctCreate.Service_Location_Street__c = (acc.Service_Location_Street__c <> NULL ? acc.Service_Location_Street__c :'2925 DeBarr Rd Suite D 240');
        acctCreate.Service_Location_State__c = (acc.Service_Location_State__c <> NULL ? acc.Service_Location_State__c :'AK');
        acctCreate.Service_Location_Zip_Code__c = (acc.Service_Location_Zip_Code__c <> NULL ? acc.Service_Location_Zip_Code__c :'99508');
        acctCreate.Provider_Type__c = (acc.Provider_Type__c <> NULL ? acc.Provider_Type__c :'Hospital');
        insert acctCreate;
        return acctCreate;	    
    }        
    
    public static Contact createContact(Contact con){
    	Contact conCreate = new Contact();
        if(con == null){
            con = new Contact();
        }
        conCreate = con;            
        conCreate.FirstName = (con.FirstName <> NULL ? con.FirstName :'TestFirstName');  
		conCreate.LastName = (con.LastName <> NULL ? con.LastName :'TestLastName'); 	       
        conCreate.MailingStreet = (con.MailingStreet <> NULL ? con.MailingStreet :'2925 DeBarr Rd Suite D 240');
        conCreate.MailingCity = (con.MailingCity <> NULL ? con.MailingCity :'Anchorage');
        conCreate.MailingState = (con.MailingState <> NULL ? con.MailingState :'AK');
        conCreate.MailingPostalCode = (con.MailingPostalCode <> NULL ? con.MailingPostalCode :'99508');
        conCreate.MailingCountry = (con.MailingCountry <> NULL ? con.MailingCountry :'United States');        
        conCreate.Email = (con.Email <> NULL ? con.Email :'test@multiplan.com');
        insert conCreate;
        return conCreate;	    
    }        
    
    
    public static Contract_Language_Amendment_Tracking__c createCLAT(Contract_Language_Amendment_Tracking__c clat){
        Contract_Language_Amendment_Tracking__c clatCreate = new Contract_Language_Amendment_Tracking__c();
        if(clat == null){
            clat = new Contract_Language_Amendment_Tracking__c();
        }
        clatCreate = clat;
        clatCreate.RecordTypeId = Schema.SObjectType.Contract_Language_Amendment_Tracking__c.getRecordTypeInfosByName().get('Group V5').getRecordTypeId();
        Account acc = createAccount(NULL);
        clatCreate.Account__c = (clat.Account__c <> NULL ? clat.Account__c : acc.Id);  
        Contact c = new Contact();
        c.AccountId = acc.Id;
        c = createContact(c);  
        System.debug('*** c'+c.Email);
        System.debug('*** cAcc'+c.AccountId);
        COntact con1 = [select Email,AccountId,Account_and_Email_Key__c from contact where id=:c.Id ];
        System.debug('*** con'+con1);
        clatCreate.Provider_Contact__c = (clat.Provider_Contact__c <> NULL ? clat.Provider_Contact__c : c.Id);   
        clatCreate.email_Address__c = c.Email;
        clatCreate.Nomination_ID__c = (clat.Nomination_ID__c <> NULL ? clat.Nomination_ID__c : '1610416441197');   
        clatCreate.Negotiator__c = (clat.Negotiator__c <> NULL ? clat.Negotiator__c : createUser(NULL).Id);   
        clatCreate.DocumentType__c = (clat.DocumentType__c <> NULL ? clat.DocumentType__c : 'NewContract');   
        clatCreate.Provider_Types__c = (clat.Provider_Types__c <> NULL ? clat.Provider_Types__c : 'Group');   
        clatCreate.Contract_Initiative__c = (clat.Contract_Initiative__c <> NULL ? clat.Contract_Initiative__c : 'NEW_GROUP');   
        clatCreate.Provider_TIN_Passthrough__c = (clat.Provider_TIN_Passthrough__c <> NULL ? clat.Provider_TIN_Passthrough__c : '813907096');   
        clatCreate.Market_Id__c = (clat.Market_Id__c <> NULL ? clat.Market_Id__c : 'PHOE');     
        clatCreate.Market_State__c = (clat.Market_State__c <> NULL ? clat.Market_State__c : 'AZ');     
        clatCreate.Fee_Schedule_Type__c = (clat.Fee_Schedule_Type__c <> NULL ? clat.Fee_Schedule_Type__c : 'S68');        
        clatCreate.In_Negotiations__c = (clat.In_Negotiations__c <> NULL ? clat.In_Negotiations__c : 'Yes');        
        clatCreate.Date_Negotiations_Started__c = (clat.Date_Negotiations_Started__c <> NULL ? clat.Date_Negotiations_Started__c : Date.today()); 
        clatCreate.Negotiation_Type__c = (clat.Negotiation_Type__c <> NULL ? clat.Negotiation_Type__c : 'New');        
        clatCreate.Negotiation_Status__c = (clat.Negotiation_Status__c <> NULL ? clat.Negotiation_Status__c : 'Yes');               
        clatCreate.LL_Success_Probability__c = (clat.LL_Success_Probability__c <> NULL ? clat.LL_Success_Probability__c : 'Low');    
        clatCreate.Category__c = (clat.Category__c <> NULL ? clat.Category__c : 'Provider'); 
        return clatCreate;        
    }
    
    public static Contract_Language_Amendment_Tracking__c insertCLAT(Contract_Language_Amendment_Tracking__c clat){
        Contract_Language_Amendment_Tracking__c clatCreate = createCLAT(clat);
        insert clatCreate;
        return clatCreate;
    }
    
    public static List<Attachment> createAttachments(List<Apttus__APTS_Agreement__c> lstAgree){
        //insert template attachment
        List<Attachment> aList = new List<Attachment>();
        Attachment attach1 = new Attachment();  
        attach1.ParentId = lstAgree[0].id;  
        attach1.Name = 'DOCX Template.docx';        
        Blob b = Blob.valueOf('Test Data');  
        attach1.Body = b;             
        aList.add(attach1);  
        
        //insert template attachment
        Attachment attach2 = new Attachment();  
        attach2.ParentId = lstAgree[0].id;  
        attach2.Name = 'PDFTemplate.pdf';
        attach2.contentType = 'application/pdf';
        Blob b2 = Blob.valueOf('Test Data');  
        attach2.Body = b2;             
        aList.add(attach2);  
            
        insert aList;        
        return aList;
    }
    
   
    
    public static List<Apttus__APTS_Template__c> createTemplates(){
        //insert template to let us insert templatetogen
        List<Apttus__APTS_Template__c> lstApttusTemp = new List<Apttus__APTS_Template__c>();
        
        // This test will have to change as the required documents changes.
        Apttus__APTS_Template__c aTemp1 = new  Apttus__APTS_Template__c();
        aTemp1.name = 'Group V5';
        lstApttusTemp.add(aTemp1);

        Apttus__APTS_Template__c aTemp2 = new  Apttus__APTS_Template__c();
        aTemp2.name = 'Network Participation Exhibit';
        lstApttusTemp.add(aTemp2);

        Apttus__APTS_Template__c aTemp3 = new  Apttus__APTS_Template__c();
        aTemp3.name = 'Contract Rates Exhibit';
        lstApttusTemp.add(aTemp3);

        Apttus__APTS_Template__c aTemp4 = new  Apttus__APTS_Template__c();
        //dynamic template matching: 'FeeSample-{!Apttus__APTS_Agreement__c.Market_Id__c}-{!Apttus__APTS_Agreement__c.Fee_Schedule_Type__c}';
        aTemp4.name = 'FeeSample-PHOE-S68';
        aTemp4.Apttus__Type__c = 'Supporting Document';
        aTemp4.Market_Id__c = 'PHOE';
        aTemp4.Fee_Schedule_Type__c = 'S68';        
        lstApttusTemp.add(aTemp4);

        Apttus__APTS_Template__c aTemp5 = new  Apttus__APTS_Template__c();
        // dynamic template matching: 'Application-{!Apttus__APTS_Agreement__c.Market_State__c}';
        atemp5.name = 'Application-AZ';
        lstApttusTemp.add(aTemp5);

        Apttus__APTS_Template__c aTemp6 = new  Apttus__APTS_Template__c();
               //dynamic template matching: 'FeeSample-{!Apttus__APTS_Agreement__c.Market_Id__c}-{!Apttus__APTS_Agreement__c.Fee_Schedule_Type__c}';
        aTemp6.name = 'FeeSample-PHOE-S72';
        aTemp6.Apttus__Type__c = 'Supporting Document';        
        aTemp6.Market_Id__c = 'PHOE';
        aTemp6.Fee_Schedule_Type__c = 'S72';
        lstApttusTemp.add(aTemp6);

        Apttus__APTS_Template__c aTemp7 = new  Apttus__APTS_Template__c();
        //This is the fallback template to use if the 'Applicatin-{statecode}' template is not found
        aTemp7.name = 'Application-Multiplan';
        lstApttusTemp.add(aTemp7);
        
        insert lstApttusTemp;
        List<Attachment> lstAttach = new List<Attachment>();
        for(Integer i=0;i<7;i++){
            Attachment attach = new Attachment();  
            attach.ParentId = lstApttusTemp[i].id;  
            attach.Name = lstApttusTemp[i].name+'.pdf';
            attach.contentType = 'application/pdf';
            Blob bb = Blob.valueOf('Test Data');  
            attach.Body = bb;             
            lstAttach.add(attach);  
        }
        insert lstAttach;
        return lstApttusTemp;
    }
    
    public static List<Template_To_Gen__c> createTemplateToGen(List<Attachment> lstAttach, List<Apttus__APTS_Agreement__c> lstAgree, List<Apttus__APTS_Template__c> lstTemp){
        List<Template_To_Gen__c> lstTempToGen = new List<Template_To_Gen__c>();
        Template_To_Gen__c templateGen_A1_1 = new Template_To_Gen__c(
            Send_In_Supplemental_Email_Only__c = false, 
            Sort_Order__c =1, 
            Generated_Attachment_Id__c = lstAttach[0].id,
            Status__c = 'Generated',
            Agreement__c = lstAgree[0].id,
            Template__c = lstTemp[0].id);
        lstTempToGen.add(templateGen_A1_1);
        
        // 'Supplemental' attachment in pdf format
        Template_To_Gen__c templateGen_A1_2 = new Template_To_Gen__c(
            Send_In_Supplemental_Email_Only__c = true, 
            Sort_Order__c =2, 
            Generated_Attachment_Id__c = lstAttach[1].id,
            Status__c = 'Generated',
            Agreement__c = lstAgree[0].id,
            Template__c = lstTemp[0].id);
        lstTempToGen.add(templateGen_A1_2);
        insert lstTempToGen;  
        return lstTempToGen;
    }
    
    public static Apttus__APTS_Agreement__c createAgreement(){
        Apttus__APTS_Agreement__c a1 = new Apttus__APTS_Agreement__c();
        a1.Apttus__Account__c = createAccount(NULL).Id;
        a1.recordtypeid = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('Group V5').getRecordTypeId();
        a1.Apttus__Primary_Contact__c = createContact(NULL).id;
        a1.Apttus__Status_Category__c = 'Request - Auto Document Generation Successful; Auto DocuSign Send Pending';   
        insert a1;
        return a1;
    }
}