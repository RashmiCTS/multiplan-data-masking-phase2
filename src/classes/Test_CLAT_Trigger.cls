/*
File Name:   Test_CLAT_Trigger
Author:      Michael Olson
Date:        4/6/2016
Description: Test Class for CLATTrigger

Modification History
4/13/2016       Michael Olson       Created email notifiation for HCE Requests in Final Rates Status where new documents have been uploaded.
6/6/2016		Michale Olson		Add V5 Group Logic
8/17/2016		Jason Harfield		Updates for TriggerUtils class
*/

@istest 
public with sharing class Test_CLAT_Trigger {
    static testMethod void TestGroupV5() { 
        
        //---------------------------------------------------
        // Variables
        //---------------------------------------------------    
        Id rtClatV5= Schema.SObjectType.Contract_Language_Amendment_Tracking__c.getRecordTypeInfosByName().get('Group V5').getRecordTypeId();
        Id rtAgreeV5 = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('Group V5').getRecordTypeId();        

        //---------------------------------------------------
        // Insert Records
        //---------------------------------------------------
            
        //Insert Run As user
        User newU = new User(); 
            newU.TimeZoneSidKey = 'America/New_York';   
            newU.LanguageLocaleKey = 'en_US';           
            newU.LocaleSidKey = 'en_US';            
            newU.EmailEncodingKey = 'UTF-8';                    
            newU.ProfileId = '00e30000000eb6g';
            newU.Title  = 'testtitle';
            newU.FirstName = 'mar';
            newU.LastName = 'automation';
            newU.email = 'testtyfh@kjdhfk.com';
            newU.Username = 'mpiautomation@multiplan.com.xxx'; 
            newU.Alias = 'mars';
            newU.CommunityNickname =  'nickn';
            newU.Phone = '1234132';
            newU.Allow_Kickoff_of_CLAT_Inload_Process__c = true;
        insert newU;  
        
        
        //Insert Account    
        Account a = new Account();
            a.name = 'testA';
        insert a;
        
        //insert Contact
        Contact c = new Contact();
            c.lastName = 'last';
            c.accountid = a.id;
            c.email = 'f@f.com';
        insert c;
        
        //insert templates - there are five and two of them are 'dynamic'.
        // This test will have to change as the required documents changes.
        Apttus__APTS_Template__c aTemp1 = new  Apttus__APTS_Template__c();
        aTemp1.name = 'Group V5';
        insert aTemp1;

        Apttus__APTS_Template__c aTemp2 = new  Apttus__APTS_Template__c();
        aTemp2.name = 'Network Participation Exhibit';
        insert aTemp2;

        Apttus__APTS_Template__c aTemp3 = new  Apttus__APTS_Template__c();
        aTemp3.name = 'Contract Rates Exhibit';
        insert aTemp3;

        Apttus__APTS_Template__c aTemp4 = new  Apttus__APTS_Template__c();
        //dynamic template matching: 'FeeSample-{!Apttus__APTS_Agreement__c.Market_Id__c}-{!Apttus__APTS_Agreement__c.Fee_Schedule_Type__c}';
        aTemp4.name = 'FeeSample-PHOE-S68';
        aTemp4.Apttus__Type__c = 'Supporting Document';
       
        aTemp4.Market_Id__c = 'PHOE';
        aTemp4.Fee_Schedule_Type__c = 'S68';
        insert aTemp4;

        Apttus__APTS_Template__c aTemp5 = new  Apttus__APTS_Template__c();
        // dynamic template matching: 'Application-{!Apttus__APTS_Agreement__c.Market_State__c}';
        atemp5.name = 'Application-AZ';
        insert aTemp5;

        Apttus__APTS_Template__c aTemp6 = new  Apttus__APTS_Template__c();
               //dynamic template matching: 'FeeSample-{!Apttus__APTS_Agreement__c.Market_Id__c}-{!Apttus__APTS_Agreement__c.Fee_Schedule_Type__c}';
        aTemp6.name = 'FeeSample-PHOE-S72';
        aTemp6.Apttus__Type__c = 'Supporting Document';
        
        aTemp6.Market_Id__c = 'PHOE';
        aTemp6.Fee_Schedule_Type__c = 'S72';
        insert aTemp6;

        Apttus__APTS_Template__c aTemp7 = new  Apttus__APTS_Template__c();
        //This is the fallback template to use if the 'Applicatin-{statecode}' template is not found
        aTemp7.name = 'Application-Multiplan';
        insert aTemp7;

       

        //Start of Unit Tests
        test.startTest();
        Contract_Language_Amendment_Tracking__c clat1;
        Contract_Language_Amendment_Tracking__c clat2;
        Contract_Language_Amendment_Tracking__c clat3;
        System.runAs(newU) { 
            // Insert 3 at once to test bulkification at the same time.
            list<Contract_Language_Amendment_Tracking__c> clats = new list<Contract_Language_Amendment_Tracking__c>();   
                
            clat1 = new Contract_Language_Amendment_Tracking__c();
            clat1.account__c = a.id;
            clat1.contact_Name__c = 'jeo some';
            clat1.email_Address__c = 'f@f.com';
            clat1.recordtypeid = rtClatV5;
            clat1.Contract_Initiative__c = 'HealthNet VA';
            clat1.Provider_Types__c = 'Group';
            clat1.DocumentType__c = 'AmendmentOnly';
            clat1.Negotiator__c = userInfo.getUserId();
            clat1.Fee_Schedule_Type__c = 'S68';
            clat1.Market_Id__c = 'PHOE';
            clat1.Market_State__c = 'AZ';
            clats.add(clat1);
   
            clat2 = new Contract_Language_Amendment_Tracking__c();
            clat2.account__c = a.id;
            clat2.contact_Name__c = '';
            clat2.email_Address__c = 'f2@f.com';
            clat2.recordtypeid = rtClatV5;
            clat2.Contract_Initiative__c = 'HealthNet VA';
            clat2.Provider_Types__c = 'Group';
            clat2.DocumentType__c = 'AmendmentOnly';
            clat2.Negotiator__c = userInfo.getUserId();
            clat2.Fee_Schedule_Type__c = 'S72';
            clat2.Market_Id__c = 'PHOE';
            clat2.Market_State__c = 'AZ';
            clats.add(clat2);     
            
            // The market state here is not in the templates.  We expect this to do a fallback to 
            // to 'Application-Multiplan' for the 'Application' template
            clat3 = new Contract_Language_Amendment_Tracking__c();
            clat3.account__c = a.id;
            clat3.contact_Name__c = 'kjdsf dskjf kfdsa';
            clat3.email_Address__c = 'f23@f.com';
            clat3.recordtypeid = rtClatV5;
            clat3.Contract_Initiative__c = 'HealthNet VA';
            clat3.Provider_Types__c = 'Group';
            clat3.DocumentType__c = 'AmendmentOnly';
            clat3.Negotiator__c = userInfo.getUserId();
            clat3.Fee_Schedule_Type__c = 'S72';
            clat3.Market_Id__c = 'PHOE';
            clat3.Market_State__c = 'MN';
            clats.add(clat3);   
                            
            insert clats;                   
        
        }
            
        test.stopTest();
        
        // Now go and look and make sure the correct agreement and stubs got inserted.

        Apttus__APTS_Agreement__c agreement1 = [SELECT ID FROM Apttus__APTS_Agreement__c WHERE CLAT__c = :clat1.id];
        // Does the agreement have the right fields?
        // Does the agreement have the right template generation stubs?
        Apttus__APTS_Agreement__c agreement2 = [SELECT ID FROM Apttus__APTS_Agreement__c WHERE CLAT__c = :clat2.id];
        Apttus__APTS_Agreement__c agreement3 = [SELECT ID FROM Apttus__APTS_Agreement__c WHERE CLAT__c = :clat3.id];
        // Make sure no extra agreements got made!
        system.assertEquals(3,[SELECT COUNT() FROM Apttus__APTS_Agreement__c]);

        
        
    }

    static testMethod void TestCLAT() { 
        
        //---------------------------------------------------
        // Variables
        //---------------------------------------------------    
        Id rtClatVAAG = Schema.SObjectType.Contract_Language_Amendment_Tracking__c.getRecordTypeInfosByName().get('VA Amendment - Group').getRecordTypeId();
        Id rtAgreeVAAG = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('VA Amendment - Group').getRecordTypeId();        


        //---------------------------------------------------
        // Insert Records
        //---------------------------------------------------
			
			//Insert Run As user
            User newU = new User(); 
                newU.TimeZoneSidKey = 'America/New_York';   
                newU.LanguageLocaleKey = 'en_US';           
                newU.LocaleSidKey = 'en_US';            
                newU.EmailEncodingKey = 'UTF-8';                    
                newU.ProfileId = '00e30000000eb6g';
                newU.Title  = 'testtitle';
                newU.FirstName = 'mar';
                newU.LastName = 'automation';
                newU.email = 'testtyfh@kjdhfk.com';
                newU.Username = 'mpiautomation@multiplan.com.xxx'; 
                newU.Alias = 'mars';
                newU.CommunityNickname =  'nickn';
                newU.Phone = '1234132';
            insert newU;  
            
            
            //Insert Account    
            Account a = new Account();
                a.name = 'testA';
            insert a;
            
            
            //insert Contact
            Contact c = new Contact();
                c.lastName = 'last';
                c.accountid = a.id;
                c.email = 'f@f.com';
            insert c;
            
            
            //insert templates.  These have to match what is in the custom metadata, so as that metadata changes so will these tests.

         	Apttus__APTS_Template__c aTemp1 = new  Apttus__APTS_Template__c();
         		aTemp1.name = 'VA Amendment - Group v3.16';
  			insert aTemp1;

            Apttus__APTS_Template__c aTemp2 = new  Apttus__APTS_Template__c();
             aTemp2.Apttus__Type__c = 'Supporting Document';
        
                aTemp2.Market_Id__c = 'PHOE';
                aTemp2.Fee_Schedule_Type__c = 'S68';
                aTemp2.name = 'FeeSample-PHOE-S68';
            insert aTemp2;

			
			
			//Start of Unit Tests
            test.startTest();
            
                System.runAs(newU) { 
            
                    list<Contract_Language_Amendment_Tracking__c> clats = new list<Contract_Language_Amendment_Tracking__c>();   
                        
                        Contract_Language_Amendment_Tracking__c clat1 = new Contract_Language_Amendment_Tracking__c();
                            clat1.account__c = a.id;
                            clat1.contact_Name__c = 'jeo some';
                            clat1.email_Address__c = 'f@f.com';
                            clat1.recordtypeid = rtClatVAAG;
                            clat1.Contract_Initiative__c = 'HealthNet VA';
                            clat1.Provider_Types__c = 'Group';
                            clat1.DocumentType__c = 'AmendmentOnly';
                            clat1.Negotiator__c = userInfo.getUserId();
                            clat1.Fee_Schedule_Type__c = 'S68';
                            clat1.Market_Id__c = 'PHOE';
                            clat1.Market_State__c = 'AZ';
                        clats.add(clat1);
               
                        Contract_Language_Amendment_Tracking__c clat2 = new Contract_Language_Amendment_Tracking__c();
                            clat2.account__c = a.id;
                            clat2.contact_Name__c = '';
                            clat2.email_Address__c = 'f2@f.com';
                            clat2.recordtypeid = rtClatVAAG;
                            clat2.Contract_Initiative__c = 'HealthNet VA';
                            clat2.Provider_Types__c = 'Group';
                            clat2.DocumentType__c = 'AmendmentOnly';
                            clat2.Negotiator__c = userInfo.getUserId();
                             
                        clats.add(clat2);     
                        
                        Contract_Language_Amendment_Tracking__c clat3 = new Contract_Language_Amendment_Tracking__c();
                            clat3.account__c = a.id;
                            clat3.contact_Name__c = 'kjdsf dskjf kfdsa';
                            clat3.email_Address__c = 'f23@f.com';
                            clat3.recordtypeid = rtClatVAAG;
                            clat3.Contract_Initiative__c = 'HealthNet VA';
                            clat3.Provider_Types__c = 'Group';
                            clat3.DocumentType__c = 'AmendmentOnly';
                            clat3.Negotiator__c = userInfo.getUserId();
                             
                        clats.add(clat3);   
                                    
                    insert clats;               	
                
                }
                // Delete all but one document to generate template so we can test the batch job code...
                // TODO: If we do this, make sure we set document types
                //List<Template_To_Gen__c> genplates =[select ID FROM Template_To_Gen__c LIMIT 2];
                //delete genplates;
                //massGenerateDocumentBatch mgdb = new massGenerateDocumentBatch( UserInfo.getSessionId() );
                //Database.executeBatch(mgdb,1);     
                
                // TODO: Check for new contacts, agreements, recipients, and template stubs.
                /* // Build or associate contacts
                    List<Contact> newContacts = CreateAndAssociateContactsForClats(clatsWithAgreementsToCreate);
                    
                    // Build agreements 
                    List<Apttus__APTS_Agreement__c> newAgreements = CreateAgreementsFromClats(clatsWithAgreementsToCreate);
                    
                    // Build agreement recipients for the new agreements
                    List<Apttus_DocuApi__DocuSignDefaultRecipient2__c> agreementRecipients = CreateAgreementRecipientsFromAgreements(newAgreements);

                    // Build templates for document generation on the new agreements
                    List<Template_To_Gen__c> documentGenerationTemplates = CreateDocumentGenerationTemplatesForAgreements(newAgreements);
                    */ 
                test.stopTest();
        
    }
}