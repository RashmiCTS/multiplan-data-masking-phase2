/*
File Name:   Test_NS_NIS_Request_Clone_Override_Cont.cls
Author:      Michael Olson
Date:        5/26/2015
Description: Test class for NS_NIS_Request_Clone_Override_Cont.

Modification History
5/26/2015		Michael Olson		Creaeted
*/

@istest 
public class Test_NS_NIS_Request_Clone_Override_Cont {
    static testMethod void t1() {
	    
	    //---------------------------------------------------
        // Variables
        //---------------------------------------------------
		Id rtRIHCFA = Schema.SObjectType.NS_NIS_Request__c.getRecordTypeInfosByName().get('Reimbursement and Implementation - HCFA').getRecordTypeId();
        
        
        //---------------------------------------------------
        // Insert Records
        //---------------------------------------------------
        NS_NIS_Request__c req1 = new NS_NIS_Request__c();
        	req1.recordtypeid = rtRIHCFA;
        	req1.product_Selection__c = 'X';
        	req1.provider_Tins__c = '111111111';
        insert req1;
        
        
        test.startTest();
        	ApexPages.StandardController sc1 = new ApexPages.StandardController(req1);
        	NS_NIS_Request_Clone_Override_Controller oc = new NS_NIS_Request_Clone_Override_Controller(sc1);
        	
        	PageReference tpageRef = Page.NS_NIS_Request_Clone_Override_Page;
 			Test.setCurrentPage(tpageRef);
			ApexPages.currentPage().getParameters().put('Id=', req1.Id);
			
			oc.cloneRecord();
        
        test.stopTest();	
    }
}