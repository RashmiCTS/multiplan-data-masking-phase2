/*
	Author: Kruti Shah, Apttus 03/06/2017 - Test class for Merge Event Trigger.	
 */
@isTest
public class APTS_MergeEventsTriggerTest {
    
    static Account testAcc;
	static Contact testCon;
	static User testUser1;
    static Apttus__APTS_Template__c testTemplate;
    
    static{
		testAcc = new Account();
		testAcc.name = 'testA';
    	insert testAcc;

    	testCon = new Contact();
    	testCon.AccountId = testAcc.Id;
    	testCon.lastName = 'agreementInsert_Test';
    	testCon.email = 'agreementInsert_Test@Test.com';
    	insert testCon;

    	Profile sysAdmin = [Select Id 
        						From Profile 
        							Where Name = 'System Administrator' Limit 1];
       
		testUser1 = new User(Alias = 'testu1',email = 'test@test.com', IsActive = true,LastName='Testing',UserName='testApttususer1@apttustest.com',EmailEncodingKey='UTF-8',LanguageLocaleKey='en_US',LocaleSidKey='en_US',TimeZoneSidKey='America/Los_Angeles',ProfileId = sysAdmin.ID);
        testUser1.Division = 'OSI-New York';
        testUser1.CompanyName ='Apttus'; 
		insert testUser1;
		
        testTemplate = new Apttus__APTS_Template__c();
        testTemplate.Name = 'Section 1.02 Billed Charges - Option 02 (Group 5)';
        testTemplate.Apttus__Type__c = 'Clause';
        testTemplate.Apttus__IsActive__c = true;
        testTemplate.Apttus__Agreement_Types__c = 'Group V5 - Amendment; Group - Region; Group Region - Amendment';
        insert testTemplate;								
	}
	
    @isTest static void mergeEventsInsert_Test() {		
		list<Apttus__Agreement_Clause__c> agreementClauseList = new list<Apttus__Agreement_Clause__c>();

		Id groupRegionRTId = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('Group - Region').getRecordTypeId();  

		Apttus__APTS_Agreement__c testAgreement = new Apttus__APTS_Agreement__c();
		testAgreement.Apttus__Account__c = testAcc.id;
		testAgreement.recordtypeid = groupRegionRTId;
		testAgreement.Apttus__Primary_Contact__c = testCon.id;			
        testAgreement.Apttus__Perpetual__c = true;
		insert testAgreement;

		testAgreement = [select id,SVP_RVP__c,APTS_Director__c,APTS_Legal_Support__c,Regulatory_Compliance__c
						FROM Apttus__APTS_Agreement__c
							WHERE Id = :testAgreement.ID];

		Apttus__Agreement_Clause__c agrClause = new Apttus__Agreement_Clause__c();
		agrClause.Apttus__Agreement__c = testAgreement.id;
		agrClause.Apttus__Action__c = CONSTANTS.INSERTED;
		agrClause.Apttus__Clause__c = 'test clause to be insert';
		agrClause.Apttus__Active__c = true;
		agreementClauseList.add(agrClause);

		Apttus__Agreement_Clause__c agrClause2 = new Apttus__Agreement_Clause__c();
		agrClause2.Apttus__Agreement__c = testAgreement.id;
		agrClause2.Apttus__Action__c = CONSTANTS.INSERTED;
		agrClause2.Apttus__Clause__c = 'Section 1.04 Client - Option 03 (Group 5)';
		agrClause2.Apttus__Active__c = true;
		agrClause2.Apttus__Status__c = CONSTANTS.APPROVAL_REQUIRED;
		agreementClauseList.add(agrClause2);
		insert agreementClauseList;
        
        test.startTest();
		Apttus__MergeEvent__c mergeEventRec = new Apttus__MergeEvent__c();
        mergeEventRec.Name = CONSTANTS.REGENERATE;
        mergeEventRec.Apttus__AgreementId__c = testAgreement.id;
		mergeEventRec.Apttus__DocumentFormat__c = 'DOCX';
		mergeEventRec.Apttus__ProtectionLevel__c = 'Insert comments and tracked changes only';
        mergeEventRec.Apttus__TemplateId__c = testTemplate.Id;
		insert mergeEventRec;
        test.stopTest();
	}
}