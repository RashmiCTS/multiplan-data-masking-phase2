public class APTS_UtilityFns {
   
    private static final Map<String,Map<String, Schema.RecordTypeInfo>> sObjectRecordTypes = new Map<String,Map<String,Schema.RecordTypeInfo>>();
    
    private final static String[] units_19 = new String[]{'zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten',
                                                         'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen',
                                                          'sixteen', 'seventeen', 'eighteen', 'nineteen'};
    private final static String[] tens = new String[]{'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety'};
    private final static String[] denom = new string[]{ '', 'thousand', 'million', 'billion', 'trillion', 'quadrillion',  
                                                          'quintillion', 's!xtillion', 'septillion', 'octillion', 'nonillion',  
                                                          'decillion', 'undecillion', 'duodecillion', 'tredecillion', 'quattuordecillion',  
                                                          's!xdecillion', 'septendecillion', 'octodecillion', 'novemdecillion', 'vigintillion'};

    private static final Map<String, Profile> profileNametoProfileSOMap = new Map<String, Profile>();
    public class ProfileNotFoundException extends Exception{}
    public static String spellOutinWords(long val) {  
        if(val < 0) 
            return 'minus '+spellOutinWords(-val);
        if (val <=100) {  
            return convert_nn(val.intValue());  
        }  
        if (val < 1000) {  
            return convert_nnn(val.intValue());  
        }  
        for (integer v = 0; v < denom.size(); v++) {  
            integer didx = v - 1;  
            integer dval = (integer)Math.pow(1000, v);  
            if (dval > val) {  
                integer mod = (integer)Math.pow(1000, didx);  
                integer l = (integer) val / mod;  
                integer r = (integer) val - (l * mod);  
                String ret = convert_nnn(l) + ' ' + denom[didx];  
                if (r > 0) {  
                    ret += ', ' + spellOutinWords(r);  
                }  
                return ret;  
            }  
        }  
        return 'Should never get here, bottomed out in spellOutinWords_V2';  
    }

    // convert a value < 100 to English.
    private static String convert_nn(integer val) {  
        if (val < 20)  
            return units_19[val];  
        if(val == 100)  
            return 'one hundred';  
        for (integer v = 0; v < tens.size(); v++) {  
            String dcap = tens[v];  
            integer dval = 20 + 10 * v;  
            if (dval + 10 > val) {  
                if (Math.Mod(val,10) != 0)  
                    return dcap + ' ' + units_19[Math.Mod(val,10)];  
                return dcap;  
            }      
        }  
        return 'Should never get here, less than 100 failure';  
    } 

    // convert a value < 1000 to english, special cased because it is the level that kicks   
    // off the < 100 special case. The rest are more general. This also allows you to  
    // get strings in the form of "forty-five hundred" if called directly.  
    private static String convert_nnn(integer val) {  
        String word = '';  
        integer rem = val / 100;  
        integer mod = Math.mod(val,100);  
        if (rem > 0) {  
            word = units_19[rem] + ' hundred';  
            if (mod > 0) {  
                word += ' ';  
            }  
        }  
        if (mod > 0) {  
            word += convert_nn(mod);  
        }  
        return word;  
    }

    public static Id getProfileId(String profileName) {
        if (String.isNotBlank(profileName)) {
            if (profileNametoProfileSOMap.isEmpty()) {
                for(Profile p : [select Id, Name 
                                        From Profile])
                {
                    profileNametoProfileSOMap.put(p.Name, p);
                }
            }
            if(profileNametoProfileSOMap.containsKey(profileName))
                return profileNametoProfileSOMap.get(profileName).Id;
        }
        
        throw new ProfileNotFoundException('Profile \'' + profileName + '\' was not found in Org.');
    }
}