/*
File Name:   ClatTriggerUtils.cls
Author:      Jason Hartfield
Date:        8/17/2016
Description: Utility class for Clat_Trigger

Modification History
Modified By         Date            Description
Michael Olson       8/17/2016       Switched out the Email_Template_Unique_Name__c field to be part of Docusign Recipient
Michael Olson       8/31/2016       Added Email_Tempale field
Swarna Maruvad      1/9/2017        Change signer 4 to MPI Executive if Agreement is Group - Region
*/

public class ClatTriggerUtilsSM { 
    public static boolean bIsRecursive = false;
    public static List<SObject> lstTriggerNew = new List<SObject>();
    public static Map<Id,SObject> mapTrigNewSObject = new Map<Id,SObject>();
    public static void AutoConstructAgreements(List<Contract_Language_Amendment_Tracking__c> triggerNew, Map<Id,Contract_Language_Amendment_Tracking__c> mapTrigNewContTracking){
        
        lstTriggerNew = triggerNew;
        mapTrigNewSObject = mapTrigNewContTracking;
        System.debug('*** lstTriggerNew '+lstTriggerNew);
        // This trigger should only fire if the current user has permission to auto-build clats, which is indictaed
        // via checkbox 'Allow Kickoff of CLAT Inload Process' on the user.
        boolean allowInload = [SELECT Allow_Kickoff_of_CLAT_Inload_Process__c FROM User WHERE ID=:UserInfo.getUserId()].Allow_Kickoff_of_CLAT_Inload_Process__c;
        if(!allowInload) return;
        
        //-------------------------------------------
        //General Query get all inserted attachments with extended data AND allow us to update them.
        //-------------------------------------------
        list<Contract_Language_Amendment_Tracking__c> allCLATs = [select id, name, recordTypeId, Contract_Initiative__c, Account__c, Account__r.name, 
                            Nomination_Id__c,Negotiator__c, Contact_Name__c, email_Address__c, Provider_Contact__c,
                            Provider_Types__c, DocumentType__c, Default_P__c, Medicare_P__c, RecordType.Name,RecordType.DeveloperName,
                            //pass through additions
                            Fee_Schedule_Type__c, Mail_Address__c, Mail_City__c, Mail_State__c, Mail_Zip__c, Market_Id__c,
                            Market_State__c, Service_Address__c, Service_City__c, Service_County__c, Service_State__c, Service_Zip__c,
                            Target_Name__c, Account__r.TIN__c, Provider_TIN_Passthrough__c   
                        from Contract_Language_Amendment_Tracking__c
                        where id IN : triggernew];  


        // Gather CLATs that should have an agreement automatically created.
        list<Contract_Language_Amendment_Tracking__c> clatsWithAgreementsToCreate = new list<Contract_Language_Amendment_Tracking__c>();    
        for(Contract_Language_Amendment_Tracking__c clat : allCLATs) {  
            // OK to put this in a loop since it is custom metadata and has no governor limits. 
            // Any record type that is in this table implicitly means it needs an agreement built.  
            if(getAgreementRTSetting(clat.RecordType.Name) != null) 
                clatsWithAgreementsToCreate.add(clat);  
        }

        // Short circuit to increase efficiency and code readability (one less indent from an if statment.)
        if(clatsWithAgreementsToCreate.size() == 0) return;

        // Build or associate contacts
        List<Contact> newContacts = CreateAndAssociateContactsForClats(clatsWithAgreementsToCreate);
        
        // Build agreements 
        List<Apttus__APTS_Agreement__c> newAgreements = CreateAgreementsFromClats(clatsWithAgreementsToCreate);
        
        // Build agreement recipients for the new agreements
        List<Apttus_DocuApi__DocuSignDefaultRecipient2__c> agreementRecipients = CreateAgreementRecipientsFromAgreements(newAgreements);
        if(agreementRecipients.size() > 0){
            /*** Start Error Handling **/
            DatabaseInterface di = new DatabaseOperations(agreementRecipients,lstTriggerNew);
            di.insertData();


            /** End **/
            //insert agreementRecipients;
        }

        // Build templates for document generation on the new agreements
        List<Template_To_Gen__c> documentGenerationTemplates = CreateDocumentGenerationTemplatesForAgreements(newAgreements);
        

    }

    private static List<Contact> CreateAndAssociateContactsForClats(List<Contract_Language_Amendment_Tracking__c> clats){
        //setup sets to use in query to find exisitng contacts by email
        // We use the account plus the email to make a distinct key to efficiently find contacts under a specific account.
        Map<string,Contact> accountsWithEmailKeys = new Map<string,Contact>();
        for( Contract_Language_Amendment_Tracking__c clat : clats ) {                   
            if(string.isNotBlank(clat.email_Address__c)) {
                accountsWithEmailKeys.put(clat.Account__c + '_' + clat.Email_Address__c,null);
            }
        }

        //query existing Contacts to added to CLAT if email matches
        //if existing contacts are found, create map, then set the CLAT's 'Provider Contact'    
        for(Contact c :[select id, Accountid, email,Account_And_Email_key__c from Contact where Account_And_Email_key__c in: accountsWithEmailKeys.keySet()]){
            accountsWithEmailKeys.put(c.Account_And_Email_key__c,c);
        }
            
        // This loop looks for any contacts that need to be created and builds them when possible.
        List<Contact> newContacts = new List<Contact>();
        for( Contract_Language_Amendment_Tracking__c clat : clats) {
            if(clat.Provider_Contact__c == null && clat.email_address__c != null) {
                string key = clat.Account__r.id + '_' + clat.email_Address__c;
                Contact foundContact = accountsWithEmailKeys.get(key);
                if(foundContact == null) { // Contact not found, build a new one.
                    string lName;
                    string fName;
                    Contact c = new Contact();
                    
                    //parsing string to create a contact
                    if(clat.Contact_Name__c != null) {
                        if( clat.Contact_Name__c.containsWhitespace() ) {
                            fName = (clat.Contact_Name__c).substring(0, (clat.Contact_Name__c).lastIndexOfChar(32));
                            lName = (clat.Contact_Name__c).substring((clat.Contact_Name__c).lastIndexOfChar(32)+1, (clat.Contact_Name__c).length());
                        }else{
                            lName = clat.Contact_Name__c;
                        }
                    } else {
                        string lNameTemp;
                        lNameTemp = (clat.email_Address__c).substring(0, (clat.email_Address__c).indexOf('@'));
                        lName = lNameTemp;
                    }
                    
                    c.firstName = fName;
                    c.lastName = lName;
                    c.email = clat.email_Address__c;
                    c.Accountid = clat.Account__r.id;
                    c.title = 'unknown';
                    c.Phone = '1';
                    
                    accountsWithEmailKeys.put(key,c);
                    newContacts.add(c);
                    
                }
            }   
        }   

        // Insert any new contacts that need to be inserted
        if(newContacts.size() > 0){
            /*** Start Error Handling **/
            DatabaseInterface di = new DatabaseOperations(newContacts,lstTriggerNew);
            di.insertData();


            /** End **/
            //insert newContacts;
        }

        // Now all contacts that we can possibly build are in teh accountsWithEmailKeys map.
        // Go back through the CLATs and associate to the new or existing contacts.
        List<Contract_Language_Amendment_Tracking__c> clatsWithContactToUpdate = new List<Contract_Language_Amendment_Tracking__c>();
        for(Contract_Language_Amendment_Tracking__c clat : clats) {
            string key = clat.Account__r.id + '_' + clat.email_Address__c;
            Contact foundContact = accountsWithEmailKeys.get(key);
            if(foundContact != null) {
                clat.Provider_Contact__c = foundContact.id;
                clatsWithContactToUpdate.add(clat);
            } 
        }

        // ?? Do we want to do this if we aren't going to create an agreement?  Old code had that limitation, but not sure why as contacts are still generated.
        if(clatsWithContactToUpdate.size() > 0){
            TriggerCommonUtils.BypassAllTriggers = true;
            /*** Start Error Handling **/
            DatabaseInterface di = new DatabaseOperations(clatsWithContactToUpdate,lstTriggerNew);
            di.updateData();


            /** End **/
            //update clatsWithContactToUpdate;
            TriggerCommonUtils.BypassAllTriggers = false;
        }

        return newContacts;
    }

    private static List<Apttus__APTS_Agreement__c> CreateAgreementsFromClats(List<Contract_Language_Amendment_Tracking__c> clats){
        boolean isSandbox = isSandboxChecker.isSandboxChecker();    
        string autoGenerateDocuments = !isSandbox ? (system.label.autoGenerateDocuments_Prod) : (system.label.autoGenerateDocuments_Sandbox);
        string autoSendDocuSignDocuments = !isSandbox ? (system.label.autoSendDocumentViaDocuSign_Prod) : (system.label.autoSendDocumentViaDocuSign_Sandbox);
        User v5_ExecSigner = [select id, alias from user where alias =: system.label.v5_ExecutiveSignerAlias limit 1];
        
        List<Apttus__APTS_Agreement__c> newAgreements = new List<Apttus__APTS_Agreement__c>();
        //create a CLAT list for update; Create a Agreement list for insert
        for(Contract_Language_Amendment_Tracking__c clat : clats) {
            
            //shows where agreements need to be created.
            if( (clat.recordType.Name == 'VA Amendment - Group' && clat.Provider_Types__c == 'Group' && clat.DocumentType__c == 'AmendmentOnly') ||
                (clat.recordType.Name == 'Group V5') ||
                (clat.recordType.Name == 'Tricare Amendment - Group') ) {               

                Agreement_Record_Type_Setting__mdt recordTypeSetting = getAgreementRTSetting(clat.RecordType.Name);
                // Continue, or throw exception?
                if(recordTypeSetting == null) continue;

                
                // Build agreement from the CLAT data. 
                // Set elements common to all agreements, regardless of record type.
                Apttus__APTS_Agreement__c a = new Apttus__APTS_Agreement__c();
                string agreeName = clat.Target_Name__c != null ? clat.Target_Name__c + ' - ' + clat.Contract_Initiative__c : clat.Account__r.name + ' - ' + clat.Contract_Initiative__c;
                a.name = agreeName.left(80);
                a.Apttus__Account__c = clat.Account__c;
                a.CLAT__c = clat.id;
                a.Apttus__Requestor__c = clat.Negotiator__c;
                a.Apttus__Primary_Contact__c = clat.Provider_Contact__c;
                if(clat.Negotiator__c != null)
                    a.ownerid = clat.Negotiator__c;
                a.MPI_Executive__c = v5_ExecSigner.id;
                a.Region__c = 'Telerecruiting';

                // We rely on the record types for the clat and agreement to be exactly the same.
                
                a.RecordTypeID = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get(recordTypeSetting.Agreement_Record_Type_Name__c).getRecordTypeId();
                // If the record type is not found, throw an exception.
                if(a.RecordTypeID == null){
                    // Throw an exception
                    
                }                           
                //set value to generate documents
                //set the flags for the apex batch to consume later
                if(autoGenerateDocuments == 'true') { // Global bypass switch
                    a.Flag_Auto_Generate_Documents__c = recordTypeSetting.Auto_Generate_Documents__c;                   
                }
                    
                //set value to send via DocuSign
                //set the flags for the apex batch to consume later
                if(autoSendDocuSignDocuments == 'true') {  // Global bypass switch
                    a.Flag_Auto_Send_via_DocuSign__c = recordTypeSetting.Auto_Send_Documents_via_DocuSign__c;   
                }       
                    
                //Set Status Category
                if(a.Flag_Auto_Generate_Documents__c == true && a.Flag_Auto_Send_via_DocuSign__c == false) {
                    a.Apttus__Status_Category__c = 'Request - Auto Document Generation Pending';
                }else if(a.Flag_Auto_Generate_Documents__c == true && a.Flag_Auto_Send_via_DocuSign__c == true) {
                    a.Apttus__Status_Category__c = 'Request - Auto Document Generation Pending; Auto DocuSign Send Pending';
                }
                                                                                
                //Pass Through fields
                a.Target_Name__c = clat.Target_Name__c;
                a.Default_P__c = clat.Default_P__c;
                a.Medicare_P__c = clat.Medicare_P__c;
                a.DocumentType__c = clat.DocumentType__c;
                a.Provider_Types__c = clat.Provider_Types__c;
                a.Fee_Schedule_Type__c = clat.Fee_Schedule_Type__c;
                a.Mail_Address__c = clat.Mail_Address__c;
                a.Mail_City__c = clat.Mail_City__c;
                a.Mail_State__c  = clat.Mail_State__c;
                a.Mail_Zip__c = clat.Mail_Zip__c;
                a.Market_Id__c = clat.Market_Id__c;
                a.Market_State__c  = clat.Market_State__c;
                a.Service_Address__c = clat.Service_Address__c;
                a.Service_City__c = clat.Service_City__c;
                a.Service_County__c  = clat.Service_County__c;
                a.Service_State__c  = clat.Service_State__c;
                a.Service_Zip__c = clat.Service_Zip__c;
                a.Provider_TIN__c = clat.Provider_TIN_Passthrough__c;  //should this map through
                a.Supplemental_Email_Template_Unique_Name__c = recordTypeSetting.Supplemental_Email_Template_Unique_Name__c;
                
                newAgreements.add(a);
            }
        }

        

        //Insert all of our new agreements we just built off of the CLATs.
        /*** Start Error Handling **/
            DatabaseInterface di = new DatabaseOperations(newAgreements,lstTriggerNew);
            di.insertData();


        /** End **/
        return newAgreements;
    }

    public static List<Apttus_DocuApi__DocuSignDefaultRecipient2__c> CreateAgreementRecipientsFromAgreements(List<Apttus__APTS_Agreement__c> agreements){

        // Now we need to build docusign recipients for these agreements.
        list<Apttus_DocuApi__DocuSignDefaultRecipient2__c> agreementRecipients = new list<Apttus_DocuApi__DocuSignDefaultRecipient2__c>();
        
        // Bulkification: Grab all the DocuSign groups so we can look them up by name and get an Id.
        map<string, id> SignGroupNameToSignGroupId = new map<string, id>();                                         
        for(Apttus_DocuApi__DocuSignSigningGroup__c dsg : [select id, name,Apttus_DocuApi__SigningGroupId__c from Apttus_DocuApi__DocuSignSigningGroup__c]) {
            SignGroupNameToSignGroupId.put(dsg.Name, dsg.id); 
            SignGroupNameToSignGroupId.put(dsg.Apttus_DocuApi__SigningGroupId__c, dsg.id);                      
        }

        // Bulkification: Gather up all the users IDs that we will need by alias
        map<string,id> UserAliasesToIDs = new Map<string,id>();
        for(Docusign_Recipient_Default__mdt recipient : [SELECT Assignee__c FROM Docusign_Recipient_Default__mdt WHERE Record_Type_Name__c = 'User']){
            // Assignee could be a hard coded alias OR a User field on the agreement.  Nested for loop is not efficent but should be ok.
            for( Apttus__APTS_Agreement__c a : agreements ) {
                string mergedAssignee = mergeTemplateName(recipient.Assignee__c, a);
                UserAliasesToIDs.put(mergedAssignee,null); 
            }
        }
        system.debug('From metadata:' + UserAliasesToIDs);
        for(User u : [select id, name,alias from User WHERE alias in:UserAliasesToIDs.keySet() OR ID in:UserAliasesToIDs.keySet()]) {
            UserAliasesToIDs.put(u.Alias,u.id); 
            UserAliasesToIDs.put(u.Id,u.id);                        
        }
        system.debug('After user lookup:' + UserAliasesToIDs);


        for( Apttus__APTS_Agreement__c a : agreements ) {
            // Find the record type name of this agreement so we can search for it in our custom metadata by name. 
            // ?? Is this getting the label or the dev name?
            string recordTypeName = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosById().get(a.RecordTypeId).getName();   
            //        system.debug('-----------------Record type name is------------' + recordTypeName);

                        Agreement_Record_Type_Setting__mdt recordTypeSetting = getAgreementRTSetting(recordTypeName);
            // Continue, or throw exception?
            if(recordTypeSetting == null) continue;   

            for(Docusign_Recipient_Default__mdt docusignRecipientTemplate : recordTypeSetting.DocuSign_Recipient_Defaults__r) {
                 //Setup basic DocuSign Recipient for All RT's
                Apttus_DocuApi__DocuSignDefaultRecipient2__c ar = new Apttus_DocuApi__DocuSignDefaultRecipient2__c();
                // record type based on docusignRecipientTemplate.Record_Type_Name__c
                string docusignRecipientRecordTypeName = docusignRecipientTemplate.Record_Type_Name__c; // Expect Contact, User, Signing Group
                ar.RecordTypeid = Schema.SObjectType.Apttus_DocuApi__DocuSignDefaultRecipient2__c.getRecordTypeInfosByName().get(docusignRecipientRecordTypeName).getRecordTypeId();
                
                // The signer could be a user, a contact, or a groupdsr
 
                string signerRecordTypeName = docusignRecipientTemplate.Record_Type_Name__c;
                if(docusignRecipientTemplate.Record_Type_Name__c == 'Contact'){
                    // Figure out what field on the agreement to use to get the contact id.
                    //{!Apttus__APTS_Agreement__c.Apttus__Primary_Contact__c} is default...
                    ar.Apttus_DocuApi__ContactId__c = mergeTemplateName(docusignRecipientTemplate.Assignee__c, a);
                    
                } else if(docusignRecipientTemplate.Record_Type_Name__c == 'User') { 
                    // Users are all reference by Alias
                    system.debug('users on get' + UserAliasesToIDs);
                    system.debug('assignee: ' + docusignRecipientTemplate.Assignee__c);
                    // Could be a hard coded alias OR a lookup to a User field on the agreement.
                    string mergedUserFIeld = mergeTemplateName(docusignRecipientTemplate.Assignee__c, a);
                    ar.Apttus_DocuApi__UserId__c = UserAliasesToIDs.get(mergedUserFIeld);
                } else if(docusignRecipientTemplate.Record_Type_Name__c == 'Signing Group') {
                    ar.Apttus_DocuApi__SigningGroupId__c = SignGroupNameToSignGroupId.get(docusignRecipientTemplate.Assignee__c);
                } else if(docusignRecipientTemplate.Record_Type_Name__c == 'Email'){
                    ar.Apttus_DocuApi__Email__c = docusignRecipientTemplate.Assignee__c;
                    ar.Apttus_DocuApi__FirstName__c = '';
                    ar.Apttus_DocuApi__LastName__c = docusignRecipientTemplate.Assignee__c;
                }
                     
                 ar.Apttus_DocuApi__RoleName__c = docusignRecipientTemplate.Role_Name__c;
                 ar.Apttus_DocuApi__RecipientType__c = docusignRecipientTemplate.Recipient_Type__c;
                 ar.Apttus_DocuApi__SigningOrder__c = docusignRecipientTemplate.Routing_Order__c;
                 ar.Apttus_CMDSign__AgreementId__c = a.id;
                 ar.Apttus_DocuApi__IsTransient__c = false;
                 ar.Email_Template__c = docusignRecipientTemplate.Email_Template_Unique_Name__c;                         
                 ar.send_supplemental_email__c = docusignRecipientTemplate.send_supplemental_email__c;
                 
                 // Changes made for Group - Region SM 1-9/2017
                // if ( (recordTypeName == 'Group - Region' || recordTypeName == 'Group Region - Amendment'|| recordTypeName == 'IPA - Region') && ar.Apttus_DocuApi__RoleName__c == 'Signer 4')
                // {     
                //    Apttus__APTS_Agreement__c GrReg = [SELECT Id, Name, MPI_Executive__c from Apttus__APTS_Agreement__c WHERE Id = :a.Id];
                //    if (string.isBlank(GrReg.MPI_Executive__c)== False)
                //    {
                        //------ ar.RecordTypeid = Schema.SObjectType.Apttus_DocuApi__DocuSignDefaultRecipient2__c.getRecordTypeInfosByName().get('User').getRecordTypeId();

                        //User Gru = [SELECT Id, Name, FirstName, LastName, Email, Assistant__c, Assistant__r.Email from user WHERE Id = :GrReg.MPI_Executive__c];
                //       User Gru = [SELECT Id, Name, FirstName, LastName, Email, AssistantEmail__c from user WHERE Id = :GrReg.MPI_Executive__c];
                //        system.debug('--------------------------------' + Gru.Id);
                        // ----- ar.Apttus_DocuApi__UserId__c = Gru.Id;
                //        if (Gru.AssistantEmail__c != null )
                //        {
                            //ar.Apttus_DocuApi__Email__c = Gru.Assistant__r.Email;
                //            ar.Apttus_DocuApi__Email__c = Gru.AssistantEmail__c;
                //        }    
                //        else
                //        {
                //            ar.Apttus_DocuApi__Email__c = Gru.Email;
                //        }
                //        ar.Apttus_DocuApi__FirstName__c = Gru.FirstName;
                //        ar.Apttus_DocuApi__LastName__c = Gru.LastName;
                //    }    
                // }
                 agreementRecipients.add(ar);
            }                            
        }

        return agreementRecipients;
    }

    public static List<Template_To_Gen__c> CreateDocumentGenerationTemplatesForAgreements(List<Apttus__APTS_Agreement__c> agreements){
        // INSERT Templates to generate records
        // Bulkification: We need to get all the possible template Ids that we might need and make them easily findable by name.
        // But some template names are dynamically generated based on the agreement, so we need to go through every agreement.
        Map<string,id> templateIDsByName = new Map<string,id>();
        for(Apttus__APTS_Agreement__c agreement : agreements) {
            if(agreement.Flag_Auto_Generate_Documents__c == true) {
                string recordTypeName = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosById().get(agreement.RecordTypeId).getName();
                Agreement_Record_Type_Setting__mdt recordTypeSetting = getAgreementRTSetting(recordTypeName);
                //Create all template names.
                for(Agreement_Template_Default__mdt templateDefault : recordTypeSetting.Agreement_Template_Defaults__r){
                    string mergedTemplateName = mergeTemplateName(templateDefault.Template_Name__c,agreement);                  
                    templateIDsByName.put(mergedTemplateName,null);
                    // Some templates have a fallback template to use if the template isn't found
                    if(string.IsNotBlank(templateDefault.Fallback_Template_Name__c)){
                        string mergedFallbackTemplateName = mergeTemplateName(templateDefault.Fallback_Template_Name__c,agreement); 
                        templateIDsByName.put(mergedFallbackTemplateName,null);
                    }
                }   
            }
        }

        // Now that we have all the template names, go see if we can find their IDs.
        for(Apttus__APTS_Template__c aptTemplate : [SELECT ID,Name FROM Apttus__APTS_Template__c WHERE Name in :templateIdsByName.keySet()]){
            templateIDsByName.put(aptTemplate.Name, aptTemplate.id);
        }

        // Go through each agreement and build sObject stubs (Template_To_Gen__c) that we will look at in a later batch process
        // to build attachments via an APTTUS webservice
        list<Template_To_Gen__c> newTemplateStubs = new list<Template_To_Gen__c>();
        for(Apttus__APTS_Agreement__c agreement : agreements) {
            
            if(agreement.Flag_Auto_Generate_Documents__c == true) {
                string recordTypeName = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosById().get(agreement.RecordTypeId).getName();
                Agreement_Record_Type_Setting__mdt recordTypeSetting = getAgreementRTSetting(recordTypeName);
                //Create all RT centric Templates
                for(Agreement_Template_Default__mdt templateDefault : recordTypeSetting.Agreement_Template_Defaults__r){
                    Template_To_Gen__c template = new Template_To_Gen__c();                     
                    template.Agreement__c = agreement.id;
                    template.Status__c = 'Queued';

                    string mergedTemplateName = mergeTemplateName(templateDefault.Template_Name__c,agreement);
                    // Some templates have a generic fallback template to use if the template isn't found in the database.
                    if(templateIDsByName.get(mergedTemplateName) == null){
                        if(string.IsNotBlank(templateDefault.Fallback_Template_Name__c)){
                            mergedTemplateName = mergeTemplateName(templateDefault.Fallback_Template_Name__c,agreement);    
                        }
                    }
                    // If the template is not found, then Template__c is null an exception is thrown by a validation on insert.
                    template.Template__c = templateIDsByName.get(mergedTemplateName);
                    if(template.Template__c == null){
                        if(agreement.CLAT__c <> null && !mapTrigNewSObject.isEmpty() && mapTrigNewSObject.containskey(agreement.CLAT__c)){
                            mapTrigNewSObject.get(agreement.CLAT__c).addError('Template missing: ' + mergedTemplateName);   
                        }
                        else if(!mapTrigNewSObject.isEmpty() && mapTrigNewSObject.containsKey(agreement.id)){
                            mapTrigNewSObject.get(agreement.id).addError('Template missing: ' + mergedTemplateName);
                        }
                        else{
                            throw new CustomException('Template missing: ' + mergedTemplateName);
                        }                        
                    }   

                    template.Send_In_Supplemental_Email_Only__c = templateDefault.Send_In_Supplemental_Email_Only__c;

                    template.Sort_Order__c = templateDefault.Sort_Order__c;                         
                    newTemplateStubs.add(template);
                }       
            }
        }
        
        if(newTemplateStubs.size() > 0) {
            /*** Start Error Handling **/
            DatabaseInterface di = new DatabaseOperations(newTemplateStubs,lstTriggerNew);
            di.insertData();


            /** End **/
          //  insert newTemplateStubs;                                    
        } 
        bIsRecursive = true;
        return newTemplateStubs;
    }

    // Parse a template name using the agreement fields to get a 'merged' template name that we can 
    // use to pull a template from our database.
    public static string mergeTemplateName(string templateName,sObject targetObject){
        //Apttus__APTS_Agreement__c agreement
        // Step 1. What is the API name of the object? like 'Apttus__APTS_Agreement__c'
        string objectName = targetObject.getSObjectType().getDescribe().getName();
        //Example of what this will parse: 'FeeSample-{!Apttus__APTS_Agreement__c.Market_ID__c}-{!Apttus__APTS_Agreement__c.Fee_Schedule_Type__c}'
        string mergedTemplateName = templateName;
        // Find all {!} fields using the magic of regex.  { and . are escaped with backslashes.
        system.debug('matching template: ' + templateName);
        Pattern myPattern = Pattern.compile('\\{\\!' + objectName + '\\.(.*?)\\}');
        Matcher m = myPattern.matcher(templateName);
        while(m.find())
        {
            string mergeField = m.group().substringBetween('{!' + objectName + '.','}');
            // Grab the field value from the agreement object.  We are going to assume this is a text field...
            string mergeFieldValue = (string) (targetObject.get(mergeField) +'');
            // replace the {!template} with the value
            string mergeStringToReplace = '{!' + objectName + '.' + mergeField + '}';
            mergedTemplateName = mergedTemplateName.replace(mergeStringToReplace,mergeFieldValue);
        }
        
        return mergedTemplateName;
    }

    // We don't technically need to cache the custom metadata, but this helps us reduce our log size when we are looking for this data in loops.
    private static Map<string, Agreement_Record_Type_Setting__mdt> AgreementRTsByName;
    private static Agreement_Record_Type_Setting__mdt getAgreementRTSetting(string recordTypeName){ // Get configuration instructions on what to do with each record type.
            if(AgreementRTsByName == null){
                List<Agreement_Record_Type_Setting__mdt> recordTypeSettings = [SELECT 
                                        Agreement_Record_Type_Name__c,
                                        Auto_Generate_Documents__c,
                                        Auto_Send_Documents_via_DocuSign__c,
                                        Supplemental_Email_Template_Unique_Name__c,
                                        (SELECT Agreement_Record_Type_Setting__c,Assignee__c,Recipient_Type__c,Record_Type_Name__c,Role_Name__c,Routing_Order__c, Email_Template_Unique_Name__c,send_supplemental_email__c FROM Docusign_Recipient_Defaults__r ORDER BY Routing_Order__c),
                                        (SELECT Template_Name__c,Sort_Order__c,Fallback_Template_Name__c,Send_In_Supplemental_Email_Only__c FROM Agreement_Template_Defaults__r ORDER BY Sort_Order__c)
                                        FROM Agreement_Record_Type_Setting__mdt WHERE Agreement_Record_Type_Name__c = :recordTypeName ];
                
                AgreementRTsByName = new Map<string,Agreement_Record_Type_Setting__mdt>();
                for(Agreement_Record_Type_Setting__mdt arts : recordTypeSettings){
                    AgreementRTsByName.put(arts.Agreement_Record_Type_Name__c, arts);
                }   
            }
            return AgreementRTsByName.get(recordTypeName);
        }
}