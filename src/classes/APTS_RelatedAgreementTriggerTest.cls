/* APTS_RelatedAgreementTriggerTest. 
 * Test class for APTS_RelatedAgreementTrigger
 *
 * Developer: Kruti Shah, APTTUS
 * Business Owner: 
 *
 * Scenario:
 * 
 * History: 
 * 03/31/2017, Kruti Shah, APTTUS - Created APTS_RelatedAgreementTriggerTest 
 */
 @isTest
public class APTS_RelatedAgreementTriggerTest {
    
    static Account testAcc;
    static Contact testCon;

    static{
        testAcc = new Account();
        testAcc.name = 'testA';
        insert testAcc;

        testCon = new Contact();
        testCon.AccountId = testAcc.Id;
        testCon.lastName = 'agreementInsert_Test';
        testCon.email = 'agreementInsert_Test@Test.com';
        insert testCon;

        Profile sysAdmin = [Select Id 
                                From Profile 
                                    Where Name = 'System Administrator' Limit 1];
    }

    public static testmethod void testPopulateOriginalAgreement()
    {
        Id groupRegionRTId = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('Group - Region').getRecordTypeId();  
        Id groupRegionAmendRTId = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('Group Region - Amendment').getRecordTypeId();  

        Apttus__APTS_Agreement__c parentAgr = new Apttus__APTS_Agreement__c();
        parentAgr.Apttus__Account__c = testAcc.id;
        parentAgr.recordtypeid = groupRegionRTId;
        parentAgr.Apttus__Primary_Contact__c = testCon.id;
        parentAgr.Market_State__c = 'AK';
        parentAgr.Apttus__Status_Category__c = 'In Effect';
        parentAgr.Apttus__Status__c = 'Activated';
        parentAgr.Apttus__Perpetual__c = true;
        parentAgr.Apttus__Contract_Start_Date__c = System.today();
        insert parentAgr;

        Apttus__APTS_Agreement__c childAgr = new Apttus__APTS_Agreement__c();
        childAgr.Apttus__Account__c = testAcc.id;                
        childAgr.Apttus__Primary_Contact__c = testCon.id;
        childAgr.recordtypeid = groupRegionAmendRTId;
        childAgr.Market_State__c = 'AK';
        childAgr.Apttus__Status_Category__c = 'Request';
        childAgr.Apttus__Status__c = 'In Amendment';        
        childAgr.Apttus__Contract_Start_Date__c = System.today();
        childAgr.Apttus__Perpetual__c = true;
        insert childAgr;        
        
        Apttus__APTS_Related_Agreement__c relAgrObj = new Apttus__APTS_Related_Agreement__c();
        relAgrObj.Apttus__APTS_Contract_From__c = parentAgr.ID;
        relAgrObj.Apttus__APTS_Contract_To__c = childAgr.ID;
        Test.startTest();
        insert relAgrObj;        
        childAgr.Apttus__Description__c = 'test description';
        update childAgr;
        System.assertEquals(childAgr.APTS_Original_Agreement__r.Name,parentAgr.Name);
        Test.stopTest();
    }

}