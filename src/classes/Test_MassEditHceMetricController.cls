/*
File Name:   TEST_MassEditHceMetricController.cls
Author:      Micahel Olson
Date:        11/19/2015
Description: Allow users to mass edit Metrics

Modification History

*/ 

@istest
public class Test_MassEditHceMetricController {
	
	public static testmethod void testMetric1() {
		
		//get RT
		Id hcfaRT = Schema.SObjectType.NS_NIS_Request__c.getRecordTypeInfosByName().get('Reimbursement and Implementation - HCFA').getRecordTypeId();

		
		//insert ND Req
		list<NS_NIS_Request__c> ndList = new list<NS_NIS_Request__c>();
		
			NS_NIS_Request__c nd1 = new NS_NIS_Request__c(	name = 'x',
															Product_Selection__c = 'AMN;Beech',
															recordtypeid = hcfaRT);
			ndList.add(nd1);
				
		insert ndList;
	
		
		//insert Metric
		list<HCE_Metric__c> metList = new list<HCE_Metric__c>();
	
			HCE_Metric__c m1 = new HCE_Metric__c(	Product__c = 'AMN', 
													ND_HCE_Request__c = nd1.id);
			metList.add(m1);
			
			HCE_Metric__c m2 = new HCE_Metric__c(	Product__c = 'Beech', 
													ND_HCE_Request__c = nd1.id);
			metList.add(m2);
			
		insert(metList);
	
		
		//-------------------------------------------
		//Start of tests
		//-------------------------------------------
		Test.startTest();	            

			//test 1
			PageReference pageRef = Page.MassEditHceMetric;
			pageRef.getParameters().put('ndRequestID', nd1.id);
			Test.setCurrentPage(pageRef);
			MassEditHceMetricController ccon = new MassEditHceMetricController ();
			
				ccon.getAllMetrics();
				ccon.saveAll();
				ccon.cancels();
				ccon.copyFrom();
			
			
			//test1
			PageReference pageRef2 = Page.MassEditHceMetric;
			pageRef2.getParameters().put('ndRequestID', nd1.id);
			pageRef2.getParameters().put('copyFromID', m1.id);
			Test.setCurrentPage(pageRef2);
			MassEditHceMetricController ccon2 = new MassEditHceMetricController ();
	        
	        ccon2.copyFrom();
	        
        Test.stopTest();
			
	
	
	}
	
	
}