/*
File Name:   Test_FeeScheduleTrackingCreatorControler.cls
Author:      Michael Olson
Date:        10/14/2015
Description: Test class for FeeScheduleTrackingCreatorControler page.

Modification History

*/


@istest
public with sharing class Test_FeeScheduleTrackingCreatorControler {
	
	public static testmethod void fstTest1() {

        //Group SUQ = [SELECT Id, Name, DeveloperName FROM Group WHERE DeveloperName = 'Settlement_Unassigned_Queue' LIMIT 1];
		
		string dSADateStartStr = system.today().toStartOfMonth().addMonths(1).format();
		date dSADateStart = date.parse(dSADateStartStr);
		date dSADateEnd = date.parse(dSADateStartStr).addYears(20);
		
		//Create Historical FST's
		list<Fee_Schedule_Tracking__c> fstIns = new list<Fee_Schedule_Tracking__c> ();
	        
		 	Fee_Schedule_Tracking__c fst1 = new Fee_Schedule_Tracking__c();	        
				fst1.Status__c = 'Closed - Complete';
				
				//Required at Close - Complete
		        fst1.Reimbursement_Effective_Date__c = system.today();
		        fst1.ownerid = userinfo.getUserId();
		       	fst1.Contract_DTS__c = 'z';
		       	fst1.Existing_FS_ID__c = 'x';
		       	fst1.Product_Selection__c = 'x';
		       	fst1.FS_Implementation_Date__c = system.today();
		       	fst1.Medicare_Locality__c = 'x';
		       	fst1.MP3_CE_ID_Number__c = '1';
		       	fst1.Not_Otherwise_Specified_Default__c = 'x';
		       	fst1.Not_Otherwise_Specified_Default_Value__c = '1';
		       	fst1.Reqlog_Batch_MPI_File_ID__c = 'x';		       	
		       	fst1.Contract_Rate_Type__c = 'Hard Coded Contract Rates';
		       	fst1.DTS_Tracking_ID__c = 'x';		       			        
		        fst1.Automatic_Escalator_Update__c = 'Yes';
		        fst1.Automatic_Escalator_Percentage_Increased__c = '1';
		        fst1.Automatic_Escalator_of_Years_to_Update__c ='x';
		        fst1.Automatic_Escalator_Anniversary_Date__c = dSADateStart;
		        
		        fst1.Data_Source_Update__c = 'Yes';
		        fst1.Data_Source_Anniversary_Date__c = dSADateStart;
		        
		        fst1.FST_Cloned_To__c = null;
		 	fstIns.add(fst1);
	        
	    insert fstIns;

                
        // Begining of Tests
        Test.startTest();
	                                
	        FeeScheduleTrackingCreatorController fstcCont = new FeeScheduleTrackingCreatorController();
	        PageReference pageRef = Page.FeeScheduleTrackingCreator;
	        Test.setCurrentPage(pageRef);
				
				fstcCont.setPastFST(fstcCont.dateTypeStr);
				fstcCont.getPastFST();
				
				fstcCont.setSD(fstcCont.dSADateStartStr);
				fstcCont.getSD();
				
				fstcCont.setED(fstcCont.dSADateStartStr);
				fstcCont.getED();
				
				fstcCont.getItems();
				
				fstcCont.getItems2();
				
				fstcCont.setStatus(fstcCont.statusStr);
				fstcCont.getStatus();
				fstcCont.cloneSelectedFSTs();
				fstcCont.reQuery();
				fstcCont.doNothing1();
				fstcCont.cases();
				fstcCont.casesWithoutBatches();
				
				FeeScheduleTrackingCreatorController.wrapperFST WC = new FeeScheduleTrackingCreatorController.wrapperFST(fst1, fst1.ownerid);	        		     	        	
	        	fstcCont.wrapperListFST[0].selected = true;
				fstcCont.cloneSelectedFSTs();

	        
        Test.stopTest();
    
    }
}