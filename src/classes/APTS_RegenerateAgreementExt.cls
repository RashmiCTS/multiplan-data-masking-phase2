/**
* Description: Controller for APTS_RegenerateAgreement
* Created By: Kruti Shah
* Created Date: 01/04/2017

* History:
* Kruti Shah, APTTUS - 01/04/2017 - Created Class APTS_RegenrateAgreementExt
* Kruti Shah, APTTUS - 03/30/2017 - Update Agreement Status Category,Status and Agreement Stage fields on Regenerate Action.
* Kruti Shah, APTTUS - 05/04/2017 - Commented the code to update fields. Move this code to MergeEvent Trigger code.
**/
public class APTS_RegenerateAgreementExt {
    private ID agreementRecordId ;    
    private Apttus__APTS_Agreement__c agreementSO;     
    public String strMessage {get;set;}
        
    public class myException extends Exception {}
    
    public APTS_RegenerateAgreementExt(ApexPages.StandardController stdController){
        this.agreementSO = (Apttus__APTS_Agreement__c)stdController.getRecord();        
        this.agreementRecordId = System.currentPageReference().getParameters().get('Id');       
        strMessage = label.APTS_Regenerate_Messge;
    }
    
    public PageReference gotoAgreement(){
        PageReference pgRef = null;        
        pgRef = new PageReference('/' + this.agreementRecordId);  
        return pgRef;                    
    }
    
	public PageReference goToTemplatePage(){      
        PageReference pgRef = null;

        //Savepoint sp = Database.setSavepoint();
        try{
          
            /*
            //Start: Update Status Category/Status as 'In Authoring/Author Contract' and Agreement Stage as 'Draft'            
            Apttus__APTS_Agreement__c objAgreement = new Apttus__APTS_Agreement__c();
            objAgreement = [select ID,Name,Apttus__Status_Category__c,Apttus__Status__c,Agreement_Stage__c 
                                from Apttus__APTS_Agreement__c 
                                    WHERE ID =:this.agreementRecordId];

            objAgreement.Apttus__Status_Category__c = CONSTANTS.STATUS_CATEGORY_IN_AUTHORING;
            objAgreement.Apttus__Status__c = CONSTANTS.STATUS_AUTHOR_CONTRACT;
            objAgreement.Agreement_Stage__c = CONSTANTS.AGREEMENT_STAGE_DRAFT;

            //update objAgreement;
            //End: Update Status Category/Status as 'In Authoring/Author Contract' and Agreement Stage as 'Draft'
            */

            pgRef = new PageReference('/apex/Apttus__SelectTemplate');
            pgRef.getParameters().put('action', 'Regenerate_Agreement');
            pgRef.getParameters().put('templateType', 'Agreement');
            pgRef.getParameters().put('Id', this.agreementRecordId);			
            
            if(test.isRunningTest())
            { 
                throw new myException('Error');                
            } 
            
        }catch(Exception ex){
            //Database.rollback(sp);
            system.debug('Error while redirecting to template page '+ex);
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error while redirecting to template page '+ex));            
            return null;
        }              
    	return pgRef;                       
    }	
}