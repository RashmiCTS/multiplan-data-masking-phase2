// This class takes an email that the system sends into itself for the purpose of running 
// Apttus' docusign sending method, which kicks off a batch job.
global without sharing class  SendDocusignEmailHandler implements Messaging.InboundEmailHandler {
	
	global Messaging.InboundEmailResult handleInboundEmail(Messaging.inboundEmail email,Messaging.InboundEnvelope env) {
 
		// Create an InboundEmailResult object for returning the result of the
		// Apex Email Service
		
		Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
		
		// Read the contents of the email and figure out what we need to send.
		String myPlainText= '';
		myPlainText = email.plainTextBody;
		system.debug('Body:' + myPlainText);
		SendInfo info = (SendInfo) JSON.deserialize(myPlainText, SendInfo.class);
		system.debug('Info:' + info);

		Id jobId = Apttus_CMDSign.CMDocuSignSendForESignatureAPI.submitDocuSignBatchCreateEnvelopeJob(info.agreementid, info.attachmentIds);
        
		if(jobId != null)
			result.success = true;
		else 
			result.success = false;
		return result;
	}

	public class SendInfo {
		public ID agreementId;
		public List<ID> attachmentIds;
	}
}