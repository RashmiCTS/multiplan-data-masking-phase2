public class AgreementClauseCustomSettings {
    
    private APTS_Agreement_Clause_Conditional_Values__c oAgreementClauseConitionalValues = null;
    Map<String,APTS_Agreement_Clause_Conditional_Values__c> agreementClauseCustomSettingMap = new Map<String,APTS_Agreement_Clause_Conditional_Values__c>();
    
    public AgreementClauseCustomSettings(APTS_Agreement_Clause_Conditional_Values__c oAgreementClauseConitional){
        this.oAgreementClauseConitionalValues = oAgreementClauseConitional;
    }
    
    public void createMAP(){
        
        if(this.oAgreementClauseConitionalValues.APTS_Field_1_API_Name__c != null && this.oAgreementClauseConitionalValues.APTS_Field_1_Value__c != null){
            agreementClauseCustomSettingMap.put(this.oAgreementClauseConitionalValues.APTS_Field_1_API_Name__c + ':' + this.oAgreementClauseConitionalValues.APTS_Field_1_Value__c,this.oAgreementClauseConitionalValues);
        }
        
    }
    
    
}