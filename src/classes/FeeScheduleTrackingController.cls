/*
FileName:    FeeScheduleTrackingController
Author:      Michael Olson
Date:        9/28/2015
Description: This is the FeeScheduleTrackingController page custom visualforce page conteoller.

Modification History 
11/17/2015			Michael Olson			MINOR CHANGE: Render Clone even if reocrd not closed.	
12/3/2015			Michael Olson			MINOR CHANGE: Added Hidden Heat Fields	
12/10/2015			Michael Olson			MINOR CHANGE: Added Additional Hidden Heat Fields	
12/11/2015			Michael Olson			MINOR CHANGE: Added Additional Hidden Heat Fields	
12/18/2015			Michael Olson			MINOR CHANGE: Added Additional Hidden Heat Fields fst_type
12/29/2015			Michale Olson			MINOR CHANGE: Added Legacy Owner
1/4/2016			Michale Olson			MINOR CHANGE: Changed Legacy Owner to Legacy Requestor
*/

public with sharing class FeeScheduleTrackingController {

    /* ====================================================
    *	RECORDS, LISTS, SETS, MAPS
    ======================================================= */
		public Fee_Schedule_Tracking__c fst {get;set;}  
    	public Fee_Schedule_Tracking__c fst2 {get;set;}
    	public Fee_Schedule_Tracking__c v1 {get;set;}
        public User currentUser {get;set;}
        public Fee_Schedule_Tracking_Group__c fstgIns {get;set;}
        public list<Fee_Schedule_Tracking__c> relatedFstInFstGroup {get;set;} 
        
    /* ===================================================
    *	STRINGS, BOOLEAN...
    ====================================================== */
        public Boolean globalEditMode {get;set;}
        public string HCEReqIdClone {get;set;}
        public string FsIdClone {get;set;}
        public string oFstId {get;set;} 
        public string initialAction {get;set;}
        public string sButton {get;set;}
        public integer fstgCount {get;set;}
        public string renderAsPdf {get;set;}
        public boolean reqFsCU {get;set;}
    
    /* =====================================================
	* 	CONSTRUCTOR
    ======================================================== */
	    public FeeScheduleTrackingController(ApexPages.StandardController stdController) {
	        
	        //Constructor variables
	        string editMode = ApexPages.currentPage().getParameters().get('editMode');
	        currentUser = [select id, LastModifiedDate from User where id =: Userinfo.getUserId()];
	        oFstId = ApexPages.currentPage().getParameters().get('id');
	        HCEReqIdClone = ApexPages.currentPage().getParameters().get('HCEReqIdClone');
	        FsIdClone = ApexPages.currentPage().getParameters().get('FsIdClone'); 
	        renderAsPdf = ApexPages.currentPage().getParameters().get('renderAsPdf'); 
	        fstgCount = 0;
	        reqFsCU = true;       
	        
	        //Logic to check if a record is being displayed or Clone/Copied.
	        if(oFstId == null) {
	        	initialAction = 'Create';
	        	sButton = 'Create Record';
	        	reqFsCU = false;
				this.fst = (Fee_Schedule_Tracking__c)stdController.getRecord();
				this.fst = getfst();				
	        }else{
	        	if(editMode == 'true'){
	        		initialAction = 'Edit';
	        	}else{
	        		initialAction = '';
	        	}
	        	sButton = 'Save';
	            this.fst = (Fee_Schedule_Tracking__c)stdController.getRecord();
	            this.fst = [select id, name, ownerid, CreatedByid, lastmodifiedbyid, createddate,lastmodifieddate, Status__c, FST_Group__c,
								Assignment_Date__c, Automatic_Escalator_of_Years_to_Update__c, 
								Automatic_Escalator_Anniversary_Date__c, Automatic_Escalator_Evergreen__c, Automatic_Escalator_Percentage_Increased__c, 
								Automatic_Escalator_Update__c, Auto_Medical_Rate_of_the_State_FS__c, Auto_Medical_Rate_Lesser_Of__c, Contract_DTS__c, 
								Contract_Notes__c, Contract_Rate_Type__c, Data_Source_Anniversary_Date__c, Data_Source_Update__c, DTS_Tracking_ID__c, 
								Existing_FS_ID__c, Fee_Schedule_Creation_Update__c, FS_Implementation_Date__c, Anesthesia_Only_Fee_Schedule__c, 
								Anesthesia_Conversion_Factor__c, Anesthesia_Conversion_Factor_Value__c, 
								HCPCS_Drug_ASP__c, HCPCS_Drug_ASP_Value__c, 
								HCPCS_Drug_AWP__c, HCPCS_Drug_AWP_Value__c, 							
								HCPCS_Level_I_and_II_CLAB__c, HCPCS_Level_I_and_II_CLAB_Value__c, 
								HCPCS_Level_I_E_M__c, HCPCS_Level_I_E_M_Value__c, 
								HCPCS_Level_II_AMB__c, HCPCS_Level_II_AMB_Value__c, 
								HCPCS_Level_II_DME_POS__c, HCPCS_Level_II_DME_POS_Value__c, 
								HCPCS_Level_II_Not_Specified__c, HCPCS_Level_II_Not_Specified_Value__c, 
								HCPCS_Level_II_PEN__c, HCPCS_Level_II_PEN_Value__c, 
								HCPCS_Level_II_RBRVS__c, HCPCS_Level_II_RBRVS_Value__c, 
								HCPCS_Level_I_Medicine__c, HCPCS_Level_I_Medicine_Value__c, 
								HCPCS_Level_I_Pathology__c, HCPCS_Level_I_Pathology_Value__c, 
								HCPCS_Level_I_Radiology__c, HCPCS_Level_I_Radiology_Value__c, 
								HCPCS_Level_I_Surgery__c, HCPCS_Level_I_Surgery_Value__c, 
								Medicare_Locality__c, Market_Id__c, MP3_CE_ID_Number__c, 
								Not_Otherwise_Specified_Default__c, Not_Otherwise_Specified_Default_Value__c, 
								Optum_Gap_Codes_Removed__c, Originating_ND_HCE_Request__c, 
								POS_Differential_Removed__c, Product_Selection__c, Provider_Name__c, Quarter__c, 
								Rate_Data_Source_Year__c, Rate_Linking_Date__c, Region_Name__c, Reimbursement_Effective_Date__c, Reqlog_Batch_MPI_File_ID__c, 
								Requestor__c, State__c, Workers_Comp_Rate_of_the_State_FS__c, Workers_Comp_Rate_Lesser_Of__c, Zip_Code__c, Originating_ND_HCE_Request__r.name,
								Completed_Date__c, Pended_Date__c, Originating_ND_HCE_Request__r.NS_NIS_Req__c, FST_Cloned_From__c, FST_Cloned_To__c,
								Call_ID__c, Call_Type__c, Call_Sub_Type__c, Call_Description__c, Call_Close_Description__c, Zip__c, Call_Notes__c,
								FS_Type__c, Legacy_Requestor__c									 
	            			from Fee_Schedule_Tracking__c where id =: oFstId];	            			  			 
	        }
	        
	        //Setup related FST's in the same FST Group
			 relatedFstInFstGroup = new list<Fee_Schedule_Tracking__c>([select id, Name, FST_Group__c, createddate, status__c, 
			 																Fee_Schedule_Creation_Update__c, Provider_Name__c, OwnerID,
			 																Reimbursement_Effective_Date__c 
	        															from Fee_Schedule_Tracking__c 
	        															where FST_Group__c = :this.fst.FST_Group__c
	        															AND id != : this.fst.id
	        															AND FST_Group__c != null
	        															order by Name desc]);  
	        
	        //query to allow for checking of concurrent ser updates. If 'fst' is new then don't run.
	        if(fst.id != null) {
	            fst2 = [select id, LastModifiedById 
	                    from Fee_Schedule_Tracking__c 
	                    where id =: fst.id];
	        	
	        	//setup Fee Schedule Tracking Group Count
	        	if(fst.FST_Group__c != null) {
	        		list<Fee_Schedule_Tracking__c> tempFST = new list<Fee_Schedule_Tracking__c>([select FST_Group__c 
	        																						from Fee_Schedule_Tracking__c 
	        																						where FST_Group__c =: fst.FST_Group__c]);
					integer i = 0;
					for(Fee_Schedule_Tracking__c f : tempFST) {
						i++;
					}
					fstgCount = i;						        																				
	        	}
	        }
	        
	        //set global edit mode
	        if (editMode == null) {
	             globalEditMode = false;
	        }else{
	            if(editMode == 'true') { globalEditMode = true;
	            }else { globalEditMode = false; }
	        }
	        
	    }

    /* ===================================================
     Create record to assign to fst
    ======================================================= */
		public Fee_Schedule_Tracking__c getfst() {
			
			//setup Unassigned Queue Id
			id fstUnQ;
				list<Group> gList = [select id, name from Group where name = 'FST - Unassigned Q' limit 1];
					for(Group g : gList) {
						fstUnQ = g.id;
					}
				
			if(FsIdClone != null) {    
		 		
		 		//query record to clone
		 		v1 = new Fee_Schedule_Tracking__c();
		 			//If new fields are added to FST, then they must added here to be cloned. 
		 			v1 = [select id, ownerid, Status__c, Anesthesia_Conversion_Factor__c, FST_Group__c,FST_Cloned_From__c, FST_Cloned_To__c, 
		 						FST_Cloned_To__r.name, 
		 						Anesthesia_Conversion_Factor_Value__c, 
								Assignment_Date__c, Automatic_Escalator_of_Years_to_Update__c, Automatic_Escalator_Anniversary_Date__c, 
								Automatic_Escalator_Evergreen__c, Automatic_Escalator_Percentage_Increased__c, Pended_Date__c,
								Automatic_Escalator_Update__c, Auto_Medical_Rate_of_the_State_FS__c, Auto_Medical_Rate_Lesser_Of__c, Contract_DTS__c, 
								Contract_Notes__c, Contract_Rate_Type__c, Data_Source_Anniversary_Date__c, Data_Source_Update__c, DTS_Tracking_ID__c, 
								Existing_FS_ID__c, Fee_Schedule_Creation_Update__c, FS_Implementation_Date__c, 
								HCPCS_Drug_ASP__c, HCPCS_Drug_ASP_Value__c, HCPCS_Drug_AWP__c, HCPCS_Drug_AWP_Value__c, 
								HCPCS_Level_I_and_II_CLAB__c, HCPCS_Level_I_and_II_CLAB_Value__c, 
								HCPCS_Level_I_E_M__c, HCPCS_Level_I_E_M_Value__c, 
								HCPCS_Level_II_AMB__c, HCPCS_Level_II_AMB_Value__c, 
								HCPCS_Level_II_DME_POS__c, HCPCS_Level_II_DME_POS_Value__c, 
								HCPCS_Level_II_Not_Specified__c, HCPCS_Level_II_Not_Specified_Value__c, 
								HCPCS_Level_II_PEN__c, HCPCS_Level_II_PEN_Value__c, 
								HCPCS_Level_II_RBRVS__c, HCPCS_Level_II_RBRVS_Value__c, 
								HCPCS_Level_I_Medicine__c, HCPCS_Level_I_Medicine_Value__c, 
								HCPCS_Level_I_Pathology__c, HCPCS_Level_I_Pathology_Value__c, 
								HCPCS_Level_I_Radiology__c, HCPCS_Level_I_Radiology_Value__c, 
								HCPCS_Level_I_Surgery__c, HCPCS_Level_I_Surgery_Value__c, 
								Medicare_Locality__c, 
								Market_Id__c, MP3_CE_ID_Number__c, Not_Otherwise_Specified_Default__c, 
								Not_Otherwise_Specified_Default_Value__c, 
								Optum_Gap_Codes_Removed__c, Originating_ND_HCE_Request__c, POS_Differential_Removed__c, Product_Selection__c, Provider_Name__c, 
								Quarter__c, Rate_Data_Source_Year__c, Rate_Linking_Date__c, Region_Name__c, Reimbursement_Effective_Date__c, Reqlog_Batch_MPI_File_ID__c, 
								Requestor__c, State__c, Workers_Comp_Rate_of_the_State_FS__c, Workers_Comp_Rate_Lesser_Of__c, Zip_Code__c															
							from Fee_Schedule_Tracking__c where id =: FsIdClone];
				
				if(v1.FST_Cloned_To__c != null) {
                	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'This record has already been cloned to '+ v1.FST_Cloned_To__r.name + '.'));
                	return null;
            	}  
				
				//setup record to transfer cloned details from
				Fee_Schedule_Tracking__c v2 = new Fee_Schedule_Tracking__c();	 		
		 			v2 = v1.clone();
	
					/* Fields to change at clone */
					v2.Status__c = 'New';
					//v2.OwnerId = v1;
					v2.Assignment_Date__c = null;
					v2.FS_Implementation_Date__c = null;
					v2.Reimbursement_Effective_Date__c = null;
					v2.Rate_Data_Source_Year__c = '';
					v2.Quarter__c = '';
					v2.Data_Source_Anniversary_Date__c = null;
					v2.Automatic_Escalator_Anniversary_Date__c = null;
					v2.Pended_Date__c = null;
					v2.FST_Cloned_From__c = v1.id;
					
					//Set Fee Schedule Creation/Update 
					if(v1.Automatic_Escalator_Update__c == 'Yes' ) {
						v2.Fee_Schedule_Creation_Update__c = 'Contractual Rate Escalator';
					}else if(v1.Data_Source_Update__c == 'Yes' ) {
						v2.Fee_Schedule_Creation_Update__c = 'Contractual Data Source Update';				
					}else{
						v2.Fee_Schedule_Creation_Update__c = '';
					}
					
				
				//create a Fee Scheudule Tracking Group if needed
				if(v1.FST_Group__c == null) {
					fstgIns = new Fee_Schedule_Tracking_Group__c();
					fstgIns.name = v2.Provider_Name__c;	
				}
				
		 		return v2;
		 		
		  	}else if(HCEReqIdClone != null) {
			
				//query record to be cloned
				NS_NIS_Request__c cloner = new NS_NIS_Request__c();
					cloner = [select id, Name, Requester__c, Contract_Notes__c, DTS_Tracking_ID__c, Market_Id__c, 
									Region_Name__c, State__c, Zip_Code__c 
								from NS_NIS_Request__c where id =: HCEReqIdClone];
			    
			    //setup record to transfer cloned details from
			    Fee_Schedule_Tracking__c temp = new Fee_Schedule_Tracking__c();
					temp.Status__c = 'New';
					temp.Provider_Name__c = cloner.Name;
				    temp.Fee_Schedule_Creation_Update__c = '';
					temp.requestor__c = cloner.requester__c;
				    temp.Originating_ND_HCE_Request__c = HCEReqIdClone;
					temp.Contract_Notes__c = cloner.Contract_Notes__c;
					temp.DTS_Tracking_ID__c = cloner.DTS_Tracking_ID__c;
					temp.Market_Id__c = cloner.Market_Id__c;
					temp.Region_Name__c = cloner.Region_Name__c;
					temp.State__c = cloner.State__c;
					temp.Zip_Code__c = cloner.Zip_Code__c;
					temp.ownerId = fstUnQ;
					temp.Contract_Notes__c = null;
			    return temp;
			
			}else{
				ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'No record is able to be created.  Please Contact your System Administrator.');
				ApexPages.addMessage(myMsg);	 			
				return null;
			}
		
		}
	
	
    /* ===================================================
     SET Global Edit Mode
    ======================================================= */
	    public PageReference setGlobalEditMode() {                   
	        
	        //Logic to set edit mode
	        if (globalEditMode == true) { globalEditMode = false;
	        }else{ globalEditMode = true; } 
	            
	        PageReference newPage = new PageReference('/apex/FeeScheduleTrackingPage?id=' + fst.id + '&editMode=true');
	        newPage.setRedirect(true);
	        
	        return newPage; 	    
	    }
    
        
    /* ===================================================
     SAVE Fee Schedule Tracker
    ======================================================= */
	    public PageReference saveFST() {
	
	        //Block Concurrent User Saves
	        if(HCEReqIdClone == null && FsIdClone == null) {
	            list<string> uConflict = new list<string>( getFstChecker() );           
	                if( uConflict[0] == 'true' ) {
	                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, uConflict[1])); return null;  
	                } 
	        }      
	        
	        
	        Savepoint sp = Database.setSavepoint();            
	        //Process Save of record; upsert is used as a record may also be created. 
	        try{
	        	//Insert Fee Schedule Tracking Group and update cloned record and new record.
	        	
	        	//Insert Fee Schedule Group
	        	if(fstgIns != null) {
	        		insert fstgIns;
	        		
	        		fst.FST_Group__c = fstgIns.id;
	        		v1.FST_Group__c = fstgIns.id;	        	
	        	}
	        	
	        	upsert fst;
	        	
				if(v1 != null && v1.FST_Cloned_To__c == null) {
					v1.FST_Cloned_To__c = fst.id;	        			        	
	        	}
	        	   
	        	if(v1 != null) {  	
					update v1;
	        	}
	        	
	        }catch(Exception dmle) { 
	        	ApexPages.addMessages(dmle); 
	        	if(dmle != null) { 
	        		Database.rollback(sp); 
	        	}  
	        	return null; }                
	                
	        PageReference newPage = new PageReference('/apex/FeeScheduleTrackingPage?id=' + fst.id+ '&editMode=false');    
	        newPage.setRedirect(true);
	        return newPage;	
	    }
    
    /* ===================================================
     CLONE Fee Schedule Tracker
    ======================================================= */
	    public PageReference cloneR() {                           
	        
	        //Logic to clone existing FST        	            
	        PageReference newPage = new PageReference('/apex/FeeScheduleTrackingPage?FsIdClone=' + fst.id + '&editMode=true&retURL=' + fst.id );
	        newPage.setRedirect(true);
	        return newPage; 	    
	    }
    /* ===================================================
     RENDER ACCEPT/BEGIN BILL Review - In Use MJO 12/11/2013
    ======================================================= */
    public boolean getRenderClone() {      
		
		if (fst.FST_Cloned_To__c == null ) {  
            return true;
		}else {return false;}
    }     
    /* ===================================================
     On save of FST, check that no one else save saved first  
    ======================================================= */
	    public list<string> getFstChecker() { 
	        Fee_Schedule_Tracking__c fstChecker = new Fee_Schedule_Tracking__c();
	        fstChecker = [ select id, LastModifiedBy.name, LastModifiedbyId from Fee_Schedule_Tracking__c where id =: fst.id];
	        
	        list<string> retSet = new list<string>();
	        
	        string isError = (fstChecker.LastModifiedById != fst2.LastModifiedById && fstChecker.LastModifiedById != currentUser.id) ? 'true' : 'false';
	            retSet.add(isError);
	        
	        string eMessage = 'Woops: This record was just modified by, ' + fstChecker.LastModifiedBy.name + '.  Please click \'Cancel\' or \'Refresh Page\' to view changes, then proceed.';
	            retSet.add(eMessage);
	       
	        return retSet;	    
	    }
}