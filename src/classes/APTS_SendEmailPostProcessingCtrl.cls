/* APTS_SendEmailController
 * Controller class for APTS_SendEmailPostProcessing page.
 * Customized actions are possible based on UserAction param.
 *
 * Developer: Kruti Shah, APTTUS
 * Business Owner: 
 *
 * Scenario:
 * 
 ***************************************************************************************************
 Modification Log:
 *
 *    Kruti Shah, 05/18/2017 - Created.
 *    
 ***************************************************************************************************
*/
public with sharing class APTS_SendEmailPostProcessingCtrl {
	
	// public getter/setters.
	public Boolean showConfirmation{get;set;}
	public Id agreementId{get;set;}
	public Id templateId{get;set;}
	public String userAction{get;set;}

	// private variables
	Apttus__APTS_Agreement__c agreementSO;
	
	public APTS_SendEmailPostProcessingCtrl(ApexPages.StandardController controller) {
		this.agreementSO = (Apttus__APTS_Agreement__c)controller.getRecord();
		
		agreementId = this.agreementSO.Id;
		templateId = System.currentPageReference().getParameters().get('newTemplateId');
		userAction = System.currentPageReference().getParameters().get('userAction');
		showConfirmation = false;
	}

	public PageReference doCancel(){
		return new PageReference('/'+agreementId);
	}

	public PageReference afterSentToOtherPartyReview(){
        /*
		PageReference ref = Page.APTS_AgreementStatusChange;
		ref.getParameters().put('id', agreementId);
		ref.getParameters().put('StatusCategory', APTPS_CONSTANTS.AGR_STATUS_CATEGORY_DONOTCHANGE);
		ref.getParameters().put('Status', APTPS_CONSTANTS.AGR_STATUS_INTERNAL_REVIEW);
		return ref;
		*/
        return new PageReference('/'+agreementId);
	}
	
}