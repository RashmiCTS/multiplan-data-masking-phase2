public interface DatabaseInterface {

	//Define the method signature to be implemented in classes that implements the interface
	//Example method
	List<SObject> insertData();
	List<SObject> updateData();
	//List<SObject> updateData(List<SObject> lst_SOBJECT);
}