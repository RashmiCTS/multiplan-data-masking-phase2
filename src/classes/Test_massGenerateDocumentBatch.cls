/*
File Name:   Test_massGenerateDocumentBatch
Author:      Michael Olson
Date:        5/11/2016
Description: Test formassGenerateDocumentBatch

Modification History
5/11/2016       Michael Olson       Created.
5/26/2016       Michale Olson       Phase 0 update to remove Query paramerter
8/31/2016       Michael Olson       Added template name
*/

// TODO: Make tests for documents not all generated and for documents generated with errors.

@isTest
public with sharing class Test_massGenerateDocumentBatch {
    
    static testMethod void Test_massGenerateDocumentBatch() {


        //---------------------------------------------------
        // Variables
        //---------------------------------------------------    
        Id rtClatVAAG = Schema.SObjectType.Contract_Language_Amendment_Tracking__c.getRecordTypeInfosByName().get('VA Amendment - Group').getRecordTypeId();
        Id rtAgreeVAAG = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('VA Amendment - Group').getRecordTypeId();  
        Id rtRecipientCon = Schema.SObjectType.Apttus_DocuApi__DocuSignDefaultRecipient2__c.getRecordTypeInfosByName().get('Contact').getRecordTypeId();      
        string sessionid = UserInfo.getSessionId();


        //---------------------------------------------------
        // Insert Records
        //---------------------------------------------------

            EmailTemplate e = [select id, developerName from EmailTemplate where developerName = 'VA_Amendment_Group_Email'];
                
            //insert Account    
            Account a = new Account();
                a.name = 'testA';
            insert a;
            
            
            //insert Contacts
            list<Contact> cons = new List<Contact>();
                
                Contact c1 = new Contact();
                    c1.lastName = 'last';
                    c1.accountid = a.id;
                    c1.email = 'f@f.com';
                cons.add(c1);
            
            insert cons;

    
            //insert templates
            list<Apttus__APTS_Template__c> atList = new list<Apttus__APTS_Template__c>();
                Apttus__APTS_Template__c at1 = new Apttus__APTS_Template__c();
                    at1.Name = 'VA Amendment - Group';
                    atList.add(at1);
            insert atList;

            //insert template attachment
            Attachment attach1 = new Attachment();  
                attach1.ParentId = at1.id;  
                attach1.Name = 'Test Attachment for Parent.docx';
                  
                Blob b = Blob.valueOf('Test Data');  
                attach1.Body = b;             
            insert attach1;             
            
            
            //insert CLAT
            list<Contract_Language_Amendment_Tracking__c> clatList = new list<Contract_Language_Amendment_Tracking__c>();
                Contract_Language_Amendment_Tracking__c clat1 = new Contract_Language_Amendment_Tracking__c();
                    clat1.Account__c = a.id;
                    clat1.Default_P__c = 'asdf';
                    clat1.medicare_P__c = 'adsfads';
                    clat1.recordtypeid = rtClatVAAG;
                clatList.add(clat1);                                
            insert clatList;
            
            
            //insert Agreement
            list<Apttus__APTS_Agreement__c> agreesList = new list<Apttus__APTS_Agreement__c>(); 
                
                Apttus__APTS_Agreement__c a1 = new Apttus__APTS_Agreement__c();
                    a1.Apttus__Account__c = a1.id;
                    a1.recordtypeid = rtAgreeVAAG;
                    a1.Apttus__Primary_Contact__c = c1.id;
                    a1.Flag_Auto_Generate_Documents__c = true;
                    a1.clat__c = clat1.id;
                agreesList.add(a1);
                
            insert agreesList;


            //insert template attachment
            Attachment attach2 = new Attachment();  
                attach2.ParentId = a1.id;  
                attach2.Name = 'Test Attachment for Parent.docx';                 
                Blob b2 = Blob.valueOf('Test Data');  
                attach2.Body = b2;                
            insert attach2;  
            
            
            //insert DocuSign Default Recipient
            list<Apttus_DocuApi__DocuSignDefaultRecipient2__c> arList = new list<Apttus_DocuApi__DocuSignDefaultRecipient2__c>();
            
                Apttus_DocuApi__DocuSignDefaultRecipient2__c ar = new Apttus_DocuApi__DocuSignDefaultRecipient2__c();
                    ar.RecordTypeid = rtRecipientCon;
                    ar.Apttus_DocuApi__ContactId__c = c1.id;
                    ar.Apttus_DocuApi__RoleName__c = 'Signer 1';
                    ar.Email_Template__c = e.DeveloperName;
                    ar.Apttus_DocuApi__RecipientType__c = 'Signer';
                    ar.Apttus_DocuApi__SigningOrder__c = 1;
                    ar.Apttus_CMDSign__AgreementId__c = a1.id;
                arList.add(ar);
            
            insert arList;          
            
            
        ///Start of Tests
        Test.startTest();
            scheduleMassGenerateDocumentBatch smgdb = new scheduleMassGenerateDocumentBatch( sessionId );
            String sch = '0 0 23 * * ?';

            massGenerateDocumentBatch mgdb = new massGenerateDocumentBatch( sessionId );
            Database.executeBatch(mgdb);        

        Test.stopTest();

    }

    static testMethod void Test_massGenerateDocumentBatch_Docx() {

        //Insert Run As user
        User newU = new User(); 
            newU.TimeZoneSidKey = 'America/New_York';   
            newU.LanguageLocaleKey = 'en_US';           
            newU.LocaleSidKey = 'en_US';            
            newU.EmailEncodingKey = 'UTF-8';                    
            newU.ProfileId = '00e30000000eb6g';
            newU.Title  = 'testtitle';
            newU.FirstName = 'mar';
            newU.LastName = 'automation';
            newU.email = 'testtyfh@kjdhfk.com';
            newU.Username = 'mpiautomation@multiplan.com.xxx'; 
            newU.Alias = 'mars';
            newU.CommunityNickname =  'nickn';
            newU.Phone = '1234132';
            newU.Allow_Kickoff_of_CLAT_Inload_Process__c = true;
        insert newU;  

        //---------------------------------------------------
        // Variables
        //---------------------------------------------------    
        Id rtClatVAAG = Schema.SObjectType.Contract_Language_Amendment_Tracking__c.getRecordTypeInfosByName().get('VA Amendment - Group').getRecordTypeId();
        Id rtAgreeVAAG = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('VA Amendment - Group').getRecordTypeId();  
        Id rtRecipientCon = Schema.SObjectType.Apttus_DocuApi__DocuSignDefaultRecipient2__c.getRecordTypeInfosByName().get('Contact').getRecordTypeId();      
        string sessionid = UserInfo.getSessionId();


        //---------------------------------------------------
        // Insert Records
        //---------------------------------------------------

            EmailTemplate e = [select id, developerName from EmailTemplate where developerName = 'VA_Amendment_Group_Email'];
                
            //insert Account    
            Account a = new Account();
                a.name = 'testA';
            insert a;
            
            
            //insert Contacts
            list<Contact> cons = new List<Contact>();
                
                Contact c1 = new Contact();
                    c1.lastName = 'last';
                    c1.accountid = a.id;
                    c1.email = 'f@f.com';
                cons.add(c1);
            
            insert cons;

    
            //insert templates
            
            Apttus__APTS_Template__c aTemp2 = new  Apttus__APTS_Template__c();
            aTemp2.name = 'VA Amendment - Group v3.16';
            
            insert aTemp2;

                // The templates needs an attachment.
             Attachment attach = new Attachment(
                Name = 'VA Amendment Attachment',
                body = Blob.valueOf('Test Data'),
                parentId = aTemp2.id,
                contentType = 'docx');
            insert attach;
                        
            ///Start of Tests
        Test.startTest();
        massGenerateDocumentBatch mgdb;
         System.runAs(newU) { 
            //insert CLAT
                Contract_Language_Amendment_Tracking__c clat1 = new Contract_Language_Amendment_Tracking__c();
                clat1.Account__c = a.id;
                clat1.Default_P__c = 'asdf';
                clat1.medicare_P__c = 'adsfads';
                clat1.recordtypeid = rtClatVAAG;
                clat1.Provider_Types__c = 'Group';
                clat1.DocumentType__c = 'AmendmentOnly';
                clat1.account__c = a.id;

                clat1.contact_Name__c = 'jeo some';
                clat1.email_Address__c = 'f@f.com';
                clat1.Contract_Initiative__c = 'HealthNet VA';
                clat1.Negotiator__c = userInfo.getUserId();                                         
            insert clat1;
            
            Apttus__APTS_Agreement__c agreement = [SELECT ID,Apttus__Status_Category__c,Apttus__Status__c from Apttus__APTS_Agreement__c WHERE CLAT__c = :clat1.id];
            system.assertEquals('Request - Auto Document Generation Pending', agreement.Apttus__Status_Category__c);
            system.assertEquals(null, agreement.Apttus__Status__c);

            // We now will have one or more 'template_to_gen' stubs that will get fed into the batch job.  
            // But the job should 
            List<Template_To_Gen__c> templates = [SELECT ID FROM Template_To_Gen__c WHERE Sort_Order__c != 1];
            delete templates;
            system.assertEquals(1, [SELECT COUNT() FROM Template_To_Gen__c]);           
        
            massGenerateDocumentBatch.run();       
        }
        Test.stopTest(); // Force batch job to run

        // Now some assertions to make sure it passed.
        // The stub should have a status of 'Generated' and should point to the newly generated document.
        Template_To_Gen__c template = [SELECT ID, STatus__c, Generated_Attachment_Id__c FROM Template_To_Gen__c]; 
        system.assertEquals('Generated', template.STatus__c);
        Attachment generatedAttachment = [SELECT ID, Name FROM Attachment WHERE ID = :template.Generated_Attachment_Id__c];
        system.assertEquals('APPTUS GENERATED.docx', generatedAttachment.Name);
        // Since there was only one document to generate, we also expect the status of the agreement to reflect a success.
        // Which is that we are ready for signatures.
        Apttus__APTS_Agreement__c agreement = [SELECT ID,Apttus__Status_Category__c,Apttus__Status__c from Apttus__APTS_Agreement__c];
        system.assertEquals('In Signatures',agreement.Apttus__Status_Category__c);
        system.assertEquals('Ready for Signatures',agreement.Apttus__Status__c);
        //system.assertEquals(0, mgdb.agreementsWithFailure.size());
        //system.assertEquals(1, mgdb.agreementsWithSuccess.size());

    }

    static testMethod void Test_massGenerateDocumentBatch_PDF() {

        //Insert Run As user
        User newU = new User(); 
            newU.TimeZoneSidKey = 'America/New_York';   
            newU.LanguageLocaleKey = 'en_US';           
            newU.LocaleSidKey = 'en_US';            
            newU.EmailEncodingKey = 'UTF-8';                    
            newU.ProfileId = '00e30000000eb6g';
            newU.Title  = 'testtitle';
            newU.FirstName = 'mar';
            newU.LastName = 'automation';
            newU.email = 'testtyfh@kjdhfk.com';
            newU.Username = 'mpiautomation@multiplan.com.xxx'; 
            newU.Alias = 'mars';
            newU.CommunityNickname =  'nickn';
            newU.Phone = '1234132';
            newU.Allow_Kickoff_of_CLAT_Inload_Process__c = true;
        insert newU;  

        //---------------------------------------------------
        // Variables
        //---------------------------------------------------    
        Id rtClatVAAG = Schema.SObjectType.Contract_Language_Amendment_Tracking__c.getRecordTypeInfosByName().get('VA Amendment - Group').getRecordTypeId();
        Id rtAgreeVAAG = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('VA Amendment - Group').getRecordTypeId();  
        Id rtRecipientCon = Schema.SObjectType.Apttus_DocuApi__DocuSignDefaultRecipient2__c.getRecordTypeInfosByName().get('Contact').getRecordTypeId();      
        string sessionid = UserInfo.getSessionId();


        //---------------------------------------------------
        // Insert Records
        //---------------------------------------------------

            EmailTemplate e = [select id, developerName from EmailTemplate where developerName = 'VA_Amendment_Group_Email'];
                
            //insert Account    
            Account a = new Account();
                a.name = 'testA';
            insert a;
            
            
            //insert Contacts
            list<Contact> cons = new List<Contact>();
                
                Contact c1 = new Contact();
                    c1.lastName = 'last';
                    c1.accountid = a.id;
                    c1.email = 'f@f.com';
                cons.add(c1);
            
            insert cons;

    
            //insert templates
            
            Apttus__APTS_Template__c aTemp2 = new  Apttus__APTS_Template__c();
            aTemp2.name = 'VA Amendment - Group v3.16';
            
            insert aTemp2;

                // The templates needs an attachment.
             Attachment attach = new Attachment(
                Name = 'VA Amendment Attachment.pdf',
                body = Blob.valueOf('Test Data'),
                parentId = aTemp2.id,
                contentType = 'pdf');
            insert attach;
                        
            ///Start of Tests
        Test.startTest();
        massGenerateDocumentBatch mgdb;
         System.runAs(newU) { 
            //insert CLAT
            list<Contract_Language_Amendment_Tracking__c> clatList = new list<Contract_Language_Amendment_Tracking__c>();
                Contract_Language_Amendment_Tracking__c clat1 = new Contract_Language_Amendment_Tracking__c();
                clat1.Account__c = a.id;
                clat1.Default_P__c = 'asdf';
                clat1.medicare_P__c = 'adsfads';
                clat1.recordtypeid = rtClatVAAG;
                clat1.Provider_Types__c = 'Group';
                clat1.DocumentType__c = 'AmendmentOnly';
                clat1.account__c = a.id;

                clat1.contact_Name__c = 'jeo some';
                clat1.email_Address__c = 'f@f.com';
                clat1.Contract_Initiative__c = 'HealthNet VA';
                clat1.Negotiator__c = userInfo.getUserId();
                
                clatList.add(clat1);                                
            insert clatList;
            
            ID agreementId = [SELECT ID from Apttus__APTS_Agreement__c WHERE CLAT__c = :clat1.id].id;
            
            // We now will have one or more 'template_to_gen' stubs that will get fed into the batch job.  
            // But the job should 
            List<Template_To_Gen__c> templates = [SELECT ID FROM Template_To_Gen__c WHERE Sort_Order__c != 1];
            delete templates;
            system.assertEquals(1, [SELECT COUNT() FROM Template_To_Gen__c]);           
        
            scheduleMassGenerateDocumentBatch smgdb = new scheduleMassGenerateDocumentBatch( sessionId );
            String sch = '0 0 23 * * ?';

            mgdb = new massGenerateDocumentBatch( sessionId );
            Database.executeBatch(mgdb);        
        }
        Test.stopTest(); // Force batch job to run

        // Now some assertions to make sure it passed.
        // The stub should have a status of 'Generated' and should point to the newly generated document.
        Template_To_Gen__c template = [SELECT ID, STatus__c, Generated_Attachment_Id__c FROM Template_To_Gen__c]; 
        system.assertEquals('Generated', template.STatus__c);
        Attachment generatedAttachment = [SELECT ID, Name FROM Attachment WHERE ID = :template.Generated_Attachment_Id__c];
        system.assertEquals('Original VA Amendment Attachment.pdf', generatedAttachment.Name);
        //system.assertEquals(0, mgdb.agreementsWithFailure.size());
        //system.assertEquals(1, mgdb.agreementsWithSuccess.size());
    }
}