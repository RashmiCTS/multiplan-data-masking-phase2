/*
File Name:   Test_AgreeTrigger
Author:      Michael Olson
Date:        4/11/2016
Description: Test Class for AgreeTrigger

Modification History
4/13/2016		Michael Olson		Created Test class
6/6/2016		Michale Olson		Add V5 Group Logic
*/

@istest 
public with sharing class Test_AgreeTrigger {
	
	static testMethod void Test_AgreeTrigger() { 
/*		//---------------------------------------------------
        // Variables
        //---------------------------------------------------    
		Id rtClatVAAG = Schema.SObjectType.Contract_Language_Amendment_Tracking__c.getRecordTypeInfosByName().get('VA Amendment - Group').getRecordTypeId();
		Id rtAgreeVAAG = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('VA Amendment - Group').getRecordTypeId();  
		Id rtRecipientCon = Schema.SObjectType.Apttus_DocuApi__DocuSignDefaultRecipient2__c.getRecordTypeInfosByName().get('Contact').getRecordTypeId();      
        //---------------------------------------------------
        // Insert Records
        //---------------------------------------------------
   	
	    	Account a = new Account();
	    		a.name = 'testA';
	    	insert a;
	    	
	    	list<Contact> cons = new List<Contact>();
		    	
		    	Contact c1 = new Contact();
		    		c1.lastName = 'last';
		    		c1.accountid = a.id;
		    		c1.email = 'f@f.com';
		    	cons.add(c1);
		    	
		    	Contact c2 = new Contact();
		    		c2.lastName = 'last2';
		    		c2.accountid = a.id;
		    		c2.email = 'f@f2.com';
		    	cons.add(c2);
	    	
	    	insert cons;
	    	
	    	
	    	list<Apttus__APTS_Agreement__c> agreesList = new list<Apttus__APTS_Agreement__c>(); 
	    		
	    		//contact to change
	    		Apttus__APTS_Agreement__c a1 = new Apttus__APTS_Agreement__c();
		    		a1.Apttus__Account__c = a1.id;
		    		a1.recordtypeid = rtAgreeVAAG;
		    		a1.Apttus__Primary_Contact__c = c1.id;
	    		agreesList.add(a1);
	    		
	    		//Contact to add
	    		Apttus__APTS_Agreement__c a2 = new Apttus__APTS_Agreement__c();
		    		a2.Apttus__Account__c = a1.id;
		    		a2.recordtypeid = rtAgreeVAAG;
	    		agreesList.add(a2);
	    		
	    		//Contact to Del
	    		Apttus__APTS_Agreement__c a3 = new Apttus__APTS_Agreement__c();
		    		a3.Apttus__Account__c = a1.id;
		    		a3.recordtypeid = rtAgreeVAAG;
		    		a3.Apttus__Primary_Contact__c = c1.id;
	    		agreesList.add(a3);
	    		
	    	insert agreesList;
	    	
	    	
	    	list<Apttus_DocuApi__DocuSignDefaultRecipient2__c> arList = new list<Apttus_DocuApi__DocuSignDefaultRecipient2__c>();
	    	
				Apttus_DocuApi__DocuSignDefaultRecipient2__c ar = new Apttus_DocuApi__DocuSignDefaultRecipient2__c();
					ar.RecordTypeid = rtRecipientCon;
					ar.Apttus_DocuApi__ContactId__c = c1.id;
					ar.Apttus_DocuApi__RoleName__c = 'Signer 1';
					ar.Apttus_DocuApi__EmailTemplateUniqueName__c = 'VA_Amendment_Group_Email';
					ar.Apttus_DocuApi__RecipientType__c = 'Signer';
					ar.Apttus_DocuApi__SigningOrder__c = 1;
					ar.Apttus_CMDSign__AgreementId__c = a1.id;
				arList.add(ar);
				
				Apttus_DocuApi__DocuSignDefaultRecipient2__c ar2 = new Apttus_DocuApi__DocuSignDefaultRecipient2__c();
					ar2.RecordTypeid = rtRecipientCon;
					ar2.Apttus_DocuApi__ContactId__c = c1.id;
					ar2.Apttus_DocuApi__RoleName__c = 'Signer 1';
					ar2.Apttus_DocuApi__EmailTemplateUniqueName__c = 'VA_Amendment_Group_Email';
					ar2.Apttus_DocuApi__RecipientType__c = 'Signer';
					ar2.Apttus_DocuApi__SigningOrder__c = 1;
					ar2.Apttus_CMDSign__AgreementId__c = a3.id;
				arList.add(ar2);
	    	
	    	insert arList;
	    	
	    	//Start of Tests
			a1.Apttus__Primary_Contact__c = c2.id;
			update a1;
			
			a2.Apttus__Primary_Contact__c = c1.id;
			update a2;
			
			a3.Apttus__Primary_Contact__c = null;
			update a3;
*/	 	
	}  
}