/* APTS_RefreshContractRatesCtrl
 * Controller class for the page APTS_RefreshContractRates.
 *
 * Developer: Kruti Shah, APTTUS
 * Business Owner: 
 *
 * Scenario:
 * 
 * History: 
 * 11/16/2016, Kruti Shah,  APTTUS - created APTS_RefreshContractRatesCtrl.
 * 02/15/2017, Kruti Shah,  APTTUS - Updated code to convert it into string value as per the ND-HCE record field value. 
 *                                 - If it contains $ then it will be converted as '$41' and if it contains % then converted as 'Forty One (41%) percent'.
 */
public class APTS_RefreshContractRatesCtrl {

	Private ID ndHCERequestID;
    Private ID agreementID;   	
    
    public APTS_RefreshContractRatesCtrl(){        
        ndHCERequestID = System.currentPageReference().getParameters().get('Id');  
        agreementID = System.currentPageReference().getParameters().get('AgreementId');        
    }
    
    public PageReference doInit(){
        PageReference pageRef =null;   
        if(ndHCERequestID == null
            || agreementID == null)
        {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Required parameters are not found.'));
            return null;
        }
        try{
            sobjectType srcObjectType = Schema.getGlobalDescribe().get('NS_NIS_Request__c');
            sObjectType destinationObjType = Schema.getGlobalDescribe().get('Apttus__APTS_Agreement__c');

            String querySource = buildSelectClauseAllFields(srcObjectType);
            querySource += ' WHERE ID = \'' + ndHCERequestID + '\'';
            sObject sourceRecd = Database.query(querySource);

            String queryDest = buildSelectClauseAllFields(destinationObjType);
            queryDest += ' WHERE ID = \'' + agreementID + '\'';
            sObject destinationRecd = Database.query(queryDest);

            Map<String, APTS_Contract_Rates_Field_Mapping__c> mapCustSettingRecords = APTS_Contract_Rates_Field_Mapping__c.getAll();
            for(APTS_Contract_Rates_Field_Mapping__c custSettingRec : mapCustSettingRecords.values()){
                if(custSettingRec.Action_Type__c == 'Refresh Contract Rates' 
                    && custSettingRec.Source_Object__c == 'NS_NIS_Request__c' && custSettingRec.Destination_Object__c == 'Apttus__APTS_Agreement__c'
                    && custSettingRec.Source_Field_API_Name__c != null && custSettingRec.Destination_Field_API_Name__c != null)
                {
                    system.debug('APTTUS--> custSettingRec.Source_Field_API_Name__c '+sourceRecd.get(custSettingRec.Source_Field_API_Name__c));
                    if(sourceRecd.get(custSettingRec.Source_Field_API_Name__c) != null){
                       
                        //check the field type,if it is text then directly copy it to the target field
                        Schema.SObjectType sObjectType = Schema.getGlobalDescribe().get(custSettingRec.Source_Object__c);
                        Map<String, Schema.SObjectField> finalMap = sObjectType.getDescribe().fields.getMap();
                        string strSourceFieldAPIName = custSettingRec.Source_Field_API_Name__c;                        
                        Schema.DisplayType fieldType = finalMap.get(strSourceFieldAPIName).getDescribe().getType();
                        system.debug('APTTUS--> field Type '+fieldType);

                        if((fieldType == DisplayType.String) || (fieldType == DisplayType.textarea)){
                            destinationRecd.put(custSettingRec.Destination_Field_API_Name__c,sourceRecd.get(custSettingRec.Source_Field_API_Name__c));

                        }else if(custSettingRec.APTS_Source_Field_String_Value_API_Name__c != null){
                            Integer intFieldValue = Integer.valueOf(sourceRecd.get(custSettingRec.Source_Field_API_Name__c)) ;    
                            String strFieldValue; 
                            String strSourceFieldStringValue = String.valueOf(sourceRecd.get(custSettingRec.APTS_Source_Field_String_Value_API_Name__c));
                            if(strSourceFieldStringValue.contains('$')){
                                strFieldValue = '$'+intFieldValue; 
                            }else if(strSourceFieldStringValue.contains('%')){
                                strFieldValue = APTS_UtilityFns.spellOutinWords(intFieldValue) + ' (' + String.valueOf(intFieldValue)+'%)'+ ' percent';                                 
                            }
                            /*
                            if(custSettingRec.APTS_Is_Dollar_Value__c == true){
                                strFieldValue = '$'+intFieldValue; 
                            }else{                                
                                strFieldValue = APTS_UtilityFns.spellOutinWords(intFieldValue) + ' (' + String.valueOf(intFieldValue)+'%)'+ ' percent';                                 
                            }
                            */
                            destinationRecd.put(custSettingRec.Destination_Field_API_Name__c,strFieldValue);
                        }                         
                        
                    }else{
                        destinationRecd.put(custSettingRec.Destination_Field_API_Name__c,null);
                    }
                }
            }

            update destinationRecd;            

        }catch(Exception ex)
        {   
            system.debug('Error while refreshing contract rates.'+ex);
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error while refreshing contract rates.'+ex));            
            return null;
        }
        
        pageRef= new PageReference('/' + agreementID);    
        return pageRef;
    }

    //  --------------------------------------------------------
    // buildSelectClauseAllFields: Build select clause with all fields for the given sobject
    //  --------------------------------------------------------
    public static String buildSelectClauseAllFields(SObjectType sObjType)
    {   
        String query = 'Select ';
        try{
            List<String> fields = getFieldAPINameList(sObjType);
            
            query += String.join(fields, ', ');
            query = query + ' From '+ getsObjectName(sObjType);
        }catch(Exception e) 
        {
            system.debug('Error from buildSelectClauseAllFields: ' + e.getMessage());
            throw new BuildSelectClauseException('Unable to build select clause for SObjectType \'' + sObjType + '\'.');
        }
        return query;
    }
    
    public static List<String> getFieldAPINameList(SObjectType sObjType)
    {   
        List<String> res = new List<String>();
        Map<String, Map<String, Schema.SObjectField>> sObjectFieldResults = new Map<String,Map<String,Schema.SObjectField>>();
        if(sObjType != null){
            Schema.DescribeSObjectResult sObjRes = sObjType.getDescribe();
            if (sObjRes != null && !sObjectFieldResults.containsKey(sObjRes.getName()))
            {
                sObjectFieldResults.put(sObjRes.getName(), sObjRes.fields.getMap());
            }
            
            Map<String, Schema.SObjectField> fieldMap = sObjectFieldResults.get(sObjRes.getName());
            for(String fieldName : fieldMap.keyset()){
                res.add(fieldName);
            }
        }
        return res;
    }

    public static String getsObjectName(SObjectType sObjType)
    {   
        String res = null;
        if(sObjType != null){
            Schema.DescribeSObjectResult sObjectResultSO = sObjType.getDescribe();
            
            res = sObjectResultSO.getName();
        }
        return res;
    }
    public class BuildSelectClauseException extends Exception {}
}