/* APTS_RegenerateAgreementExt_Test
 * Test Class for APTS_RegenerateAgreementExt.
 *
 * Developer: Kruti Shah, APTTUS
 * Business Owner: 
 *
 * Scenario:
 * 
 * History: 
 * 01/09/2017, Kruti Shah,  APTTUS - created APTS_RegenerateAgreementExt_Test.
 */
@isTest
public class APTS_RegenerateAgreementExt_Test {
    public static Account testAcc;
    public static Contact testCon;
    
    @testsetup public static void createData(){
        testAcc = new Account();
		testAcc.name = 'testA';
    	insert testAcc;
        
        testCon = new Contact();
    	testCon.AccountId = testAcc.Id;
    	testCon.lastName = 'agreementInsert_Test';
    	testCon.email = 'agreementInsert_Test@Test.com';
    	insert testCon;
        
        Id groupRegionTId = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('Group - Region').getRecordTypeId();
        
        Apttus__APTS_Agreement__c testAgr = new Apttus__APTS_Agreement__c();
		testAgr.Apttus__Account__c = testAcc.id;
		testAgr.recordtypeid = groupRegionTId;
		testAgr.Apttus__Primary_Contact__c = testCon.id;
		testAgr.Market_State__c = 'AK';
        testAgr.Apttus__Status_Category__c = 'Request';
        testAgr.Apttus__Status__c = 'Request';
        testAgr.Apttus__Perpetual__c = true;
		insert testAgr;
    }

     public static testmethod void testmethodWithValidValues(){
        PageReference pageRef = Page.APTS_RegenerateAgreement;
        Test.setCurrentPage(pageRef);
        
        Apttus__APTS_Agreement__c testAgr2 = [select ID,name,ANES_CF__c from Apttus__APTS_Agreement__c WHERE Market_State__c='AK']; 
        ApexPages.currentPage().getParameters().put('Id', testAgr2.ID);        
        
        Test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(testAgr2);
        APTS_RegenerateAgreementExt objController = new APTS_RegenerateAgreementExt(sc);
        objController.gotoAgreement();
        objController.goToTemplatePage();
		Test.stopTest();     
    }

}