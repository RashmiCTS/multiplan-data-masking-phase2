/* APTS_SendEmailControllerTest
 * Test class for APTS_SendEmailController
 *
 * Developer: Kruti Shah, APTTUS
 * Business Owner: 
 *
 * Scenario:
 * 
 * 
 *
   ***************************************************************************************************
   Modification Log:
   *
   *    Kruti Shah, 06/28/2017 - Created APTS_SendEmailControllerTest.
   ***************************************************************************************************
*/
@isTest
private class APTS_SendEmailControllerTest {
	
	static Apttus__APTS_Agreement__c testAgreement;
	static Profile adminProfile;
	static User adminUser;

	static{
		Account testAccount = (Account) APTS_TestDataFactory.createSObject(new Account());
        insert testAccount;

		adminProfile = [Select Id 
                                From Profile 
                                    Where Name = 'System Administrator' Limit 1];

        adminUser = (User) APTS_TestDataFactory.createSObject(new User());
        adminUser.ProfileId = adminProfile.Id;       
        insert adminUser;

		testAgreement = (Apttus__APTS_Agreement__c) APTS_TestDataFactory.createSObject(new Apttus__APTS_Agreement__c());
        testAgreement.Apttus__Account__c = testAccount.Id;    	
        insert testAgreement;
		
        /*
        List<Apttus__APTS_Admin__c> testAdmins = new List<Apttus__APTS_Admin__c>();
        Apttus__APTS_Admin__c admin1 = (Apttus__APTS_Admin__c)APTS_TestDataFactory.createSObject(new Apttus__APTS_Admin__c());
        admin1.Name = 'APTS_SkipEmailTemplateSelection';
        admin1.Apttus__Value__c = 'false';
        testAdmins.add(admin1);
        
        Apttus__APTS_Admin__c admin2 = admin1.clone(false);
        admin2.Name = 'APTS_EmailTemplateForReview';
        admin2.Apttus__Value__c = 'Test template';
        testAdmins.add(admin2);
        insert testAdmins;
		*/
        // create attachments.
        Attachment attch = new Attachment(parentId = testAgreement.Id);
        List<Attachment> testAttachments = (List<Attachment>)APTS_TestDataFactory.createSObjectList(attch, 20);
        insert testAttachments;

        Apttus__Agreement_Document__c agreementDoc = new Apttus__Agreement_Document__c(Apttus__Agreement__c = testAgreement.Id);
        agreementDoc.Apttus__Path__c = 'http://Attachment_1.docx';
        agreementDoc.Name = 'Attachment_1.docx'; 	
        agreementDoc.Apttus__Type__c = 'Final Electronic Copy';
        List<Apttus__Agreement_Document__c> testagreementDocs = (List<Apttus__Agreement_Document__c>)APTS_TestDataFactory.createSObjectList(agreementDoc, 5);
        insert testagreementDocs;
	}

	@isTest static void Initialization_Test() {
		// Implement test code
		
		ApexPages.StandardController stdCtrl = new ApexPages.StandardController(testAgreement);

		APTS_SendEmailController ctrlExt = new APTS_SendEmailController(stdCtrl);
		System.assertEquals(null, ctrlExt.isInitialized);

		ctrlExt.doInit();
		System.assertEquals(true, ctrlExt.isInitialized);
		System.assertEquals(true, ctrlExt.showAttachments);
		System.assertEquals(false, ctrlExt.showTemplates);
		System.assertEquals(false, ctrlExt.showStep4);
		System.assertEquals(true, ctrlExt.canEditAgreement);
		System.assertNotEquals(0, ctrlExt.attachments.size());

		System.assertEquals(null, ctrlExt.isSortByName);
		System.assertEquals(null, ctrlExt.isSortByTemplateType);
		System.assertEquals(null, ctrlExt.isSortByDescription);
		System.assertEquals(null, ctrlExt.sortImageURL);
		System.assertEquals(null, ctrlExt.sortImageTitle);

		System.assertEquals(null, ctrlExt.protect);
		System.assertEquals(null, ctrlExt.unprotect);
		System.assertEquals(null, ctrlExt.protectionLevels);
		System.assertEquals(null, ctrlExt.IsEmailAuthoringStep);
		System.assertEquals(null, ctrlExt.IsLastStep);
	}
	
	@isTest static void doNext_Test() {
		// Implement test code
		ApexPages.StandardController stdCtrl = new ApexPages.StandardController(testAgreement);

		APTS_SendEmailController ctrlExt = new APTS_SendEmailController(stdCtrl);
		ctrlExt.doInit();
		
		System.assertEquals(false, ctrlExt.showStep4);
		System.assertEquals(null, ctrlExt.showPrevious);
		ctrlExt.doNext();
		System.assertEquals(false, ctrlExt.showStep4);// because we have not made the attachment selection yet.

		// make attachment selection - first attachment.
		ctrlExt.attachments.get(0).selected = true;
		ctrlExt.doNext();
		System.assertEquals(true, ctrlExt.showStep4);
		System.assertEquals(false, ctrlExt.showAttachments);
		System.assertEquals(true, ctrlExt.showTemplates);
		System.assertEquals(null, ctrlExt.showPrevious);
	}
	
	@isTest static void doPrevious_Test() {
		// Implement test code
		ApexPages.StandardController stdCtrl = new ApexPages.StandardController(testAgreement);

		APTS_SendEmailController ctrlExt = new APTS_SendEmailController(stdCtrl);
		ctrlExt.doInit();
		
		// make attachment selection - first attachment.
		ctrlExt.attachments.get(0).selected = true;
		ctrlExt.doNext();
		System.assertEquals(true, ctrlExt.showStep4);
		System.assertEquals(false, ctrlExt.showAttachments);
		System.assertEquals(true, ctrlExt.showTemplates);
		System.assertEquals(null, ctrlExt.showPrevious);

		ctrlExt.doPrevious();
		System.assertEquals(false, ctrlExt.showStep4);
		System.assertEquals(true, ctrlExt.showAttachments);
		System.assertEquals(false, ctrlExt.showTemplates);
		System.assertEquals(false, ctrlExt.showPrevious);		
	}

	@isTest static void EmailTemplateMethods_Test() {
		// Implement test code
		ApexPages.StandardController stdCtrl = new ApexPages.StandardController(testAgreement);

		APTS_SendEmailController ctrlExt = new APTS_SendEmailController(stdCtrl);
		ctrlExt.doInit();

		ctrlExt.doEmailTemplateSearch();

		ctrlExt.emailTemplateId = ctrlExt.templates.get(0).emailTemplate.Id;
		ctrlExt.doSelectEmailTemplate();

		ctrlExt.doValidateEmailTemplateSelection();

		System.runAs(adminUser){// to avoid MIXED_DML_OPERATION
			Folder fld = [select Id, Name 
								From Folder 
									Where Name = 'Apttus Email Templates' Limit 1];

			// create email template.
			EmailTemplate testEmailTemplate = new EmailTemplate();
			testEmailTemplate.DeveloperName = 'EmailTemplateMethods_Test';
			testEmailTemplate.Name = 'EmailTemplateMethods_Test';
			testEmailTemplate.TemplateType = 'Text';
			testEmailTemplate.FolderId = fld.Id;
			insert testEmailTemplate;
		
			Apttus__TempObject__c testTempObj = new Apttus__TempObject__c();
			testTempObj.Apttus__DeveloperName__c = 'EmailTemplateMethods_Test';
			testTempObj.Apttus__AgreementId__c = testAgreement.Id;
			// testTempObj.Apttus__TemplateId__c = null;
			insert testTempObj;

			ctrlExt.tempObjectId = testTempObj.Id;
			ctrlExt.doDeleteTempObject();
			
			ctrlExt.tempEmailTemplateId = testEmailTemplate.Id;
			ctrlExt.doDeleteTempEmailTemplate();
		}
	}

	@isTest static void doCancel_Test() {
		// Implement test code
		ApexPages.StandardController stdCtrl = new ApexPages.StandardController(testAgreement);

		APTS_SendEmailController ctrlExt = new APTS_SendEmailController(stdCtrl);
		ctrlExt.doInit();

		System.assertNotEquals(null, ctrlExt.doCancel());
	}

	@isTest static void doSort_Test() {
		// Implement test code
		ApexPages.StandardController stdCtrl = new ApexPages.StandardController(testAgreement);

		APTS_SendEmailController ctrlExt = new APTS_SendEmailController(stdCtrl);
		ctrlExt.doInit();

		System.assertEquals(null, ctrlExt.doSort());
	}
}