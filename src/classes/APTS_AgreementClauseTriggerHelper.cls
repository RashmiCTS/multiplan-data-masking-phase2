/*
    Author: Apttus, 12/27/2016 - AgreementClauseTrigger for before insert.

    Modification log:
    Kruti Shah, APTTUS, 12/27/2016 - Added Derivation for updating fields(APTS_Region_Based_Approver__c) on Agreement based on Agreement's Market_State__c field.
    Kruti Shah, APTTUS, 01/10/2017 - Added Derivation for updating agreement clause status to 'Cancelled' from 'Approval Required' if that clause is having another record as 'Deleted' for that Agreement.
*/
public class APTS_AgreementClauseTriggerHelper {
    public static Boolean approvalCheckisInvoked = false;
    //Constructor
    public APTS_AgreementClauseTriggerHelper()
    {
        
    }
    
    //Populate the Approvers on Agreement Clauses from Agreement
    public void populateApproversFromAgreement(List<Apttus__Agreement_Clause__c> newAgrClauseList) 
    {
        Set<ID> agrIDSet = new Set<ID>();
        
        for(Apttus__Agreement_Clause__c agrClause : newAgrClauseList)
        {
            agrIDSet.add(agrClause.Apttus__Agreement__c);
        }
        
        Map<ID,Apttus__APTS_Agreement__c> agrIdToAgrMap = new Map<ID,Apttus__APTS_Agreement__c>([SELECT ID, SVP_RVP__c, APTS_Director__c, Regulatory_Compliance__c, 
                                                                                                    APTS_Legal_Support__c 
                                                                                                        FROM Apttus__APTS_Agreement__c 
                                                                                                            WHERE ID IN :agrIDSet]);
        
        //Update the Approvers on the Agreement Clauses
        for(Apttus__Agreement_Clause__c agrClause : newAgrClauseList) {
            Apttus__APTS_Agreement__c relatedAgreement = agrIdToAgrMap.get(agrClause.Apttus__Agreement__c);
            
            agrClause.SVP_RVP__c = relatedAgreement.SVP_RVP__c;
            agrClause.APTS_Director__c = relatedAgreement.APTS_Director__c;
            agrClause.APTS_Regulatory_Compliance__c = relatedAgreement.Regulatory_Compliance__c;
            agrClause.APTS_Legal_Support__c = relatedAgreement.APTS_Legal_Support__c;
        }
    }

    public void updateAgreementClauseAsCancelled(List<Apttus__Agreement_Clause__c> newAgrClauseList){
        Set<ID> agrIDSet = new Set<ID>();
        //List<Apttus__APTS_Agreement__c> listOfAgreement = new List<Apttus__APTS_Agreement__c>();
        Set<ID> agrClauseIDSet = new Set<ID>();
        Set<ID> agrClauseIDSet_Deleted = new Set<ID>();        
        Map<ID,List<Apttus__Agreement_Clause__c>> mapofAgrIDtoAgrClauseApprovalReq = new Map<ID,List<Apttus__Agreement_Clause__c>>();
        Map<ID,List<Apttus__Agreement_Clause__c>> mapofAgrIDtoAgrClauseDeleted = new Map<ID,List<Apttus__Agreement_Clause__c>>();        
        List<Apttus__Agreement_Clause__c> listOfagrCluaseToBeUpdated = new List<Apttus__Agreement_Clause__c>();

        for(Apttus__Agreement_Clause__c agrClause : newAgrClauseList)
        {
            if(agrClause.Apttus__Action__c == CONSTANTS.DELETED){                
                agrIDSet.add(agrClause.Apttus__Agreement__c);    
                agrClauseIDSet_Deleted.add(agrClause.ID);
            } 
        }

        system.debug('APTTUS--> Agreement ID Set '+agrIDSet);
        system.debug('APTTUS--> Agreement Clause ID Set '+agrClauseIDSet_Deleted);

        /*
        listOfAgreement = [SELECT Id,Name
                            FROM Apttus__APTS_Agreement__c 
                            WHERE ID in:agrIDSet];
        */

        Map<ID,Apttus__APTS_Agreement__c> agrIdToAgrMapForApprovalReq = new Map<ID,Apttus__APTS_Agreement__c>([SELECT ID, SVP_RVP__c, APTS_Director__c, Regulatory_Compliance__c, APTS_Legal_Support__c, 
                                                                                                (Select Id, Apttus__Action__c, Apttus__Status__c, Apttus__Clause__c, Apttus__Active__c 
                                                                                                From Apttus__R00N50000001os4tEAA__r
                                                                                                WHERE Apttus__Status__c =:CONSTANTS.APPROVAL_REQUIRED)
                                                                                                    FROM Apttus__APTS_Agreement__c 
                                                                                                    WHERE ID IN :agrIDSet]);

        Map<ID,Apttus__APTS_Agreement__c> agrIdToAgrMapForDeleted = new Map<ID,Apttus__APTS_Agreement__c>([SELECT ID, SVP_RVP__c, APTS_Director__c, Regulatory_Compliance__c, APTS_Legal_Support__c, 
                                                                                                (Select Id, Apttus__Action__c, Apttus__Status__c, Apttus__Clause__c, Apttus__Active__c 
                                                                                                From Apttus__R00N50000001os4tEAA__r
                                                                                                WHERE Apttus__Action__c =:CONSTANTS.DELETED)
                                                                                                    FROM Apttus__APTS_Agreement__c 
                                                                                                    WHERE ID IN :agrIDSet]);

       
        for(Apttus__APTS_Agreement__c agr:agrIdToAgrMapForApprovalReq.values()){
            mapofAgrIDtoAgrClauseApprovalReq.put(agr.ID, agr.Apttus__R00N50000001os4tEAA__r);
        }

        for(Apttus__APTS_Agreement__c agr:agrIdToAgrMapForDeleted.values()){
            mapofAgrIDtoAgrClauseDeleted.put(agr.ID, agr.Apttus__R00N50000001os4tEAA__r);
        }
        
        system.debug('APTTUS--> mapofAgrIDtoAgrClauseApprovalReq '+mapofAgrIDtoAgrClauseApprovalReq);
        system.debug('APTTUS--> mapofAgrIDtoAgrClauseDeleted '+mapofAgrIDtoAgrClauseDeleted);

        if(!mapofAgrIDtoAgrClauseApprovalReq.isEmpty()){
            for(ID agrId :mapofAgrIDtoAgrClauseApprovalReq.keySet())
            {
                List<Apttus__Agreement_Clause__c> listOfAgrClausesApprovalReq = new List<Apttus__Agreement_Clause__c>(mapofAgrIDtoAgrClauseApprovalReq.get(agrId));
                List<Apttus__Agreement_Clause__c> listOfAgrClausesDeleted = new List<Apttus__Agreement_Clause__c>(mapofAgrIDtoAgrClauseDeleted.get(agrId));
                Set<String> setOfAgrClausesDeleted = new Set<String>();
                if(listOfAgrClausesDeleted.size() >0 && !listOfAgrClausesDeleted.isEmpty()){
                    for(Apttus__Agreement_Clause__c agrClause : listOfAgrClausesDeleted){ 
                        String strClauseName = agrClause.Apttus__Clause__c;
                        setOfAgrClausesDeleted.add(strClauseName);
                    }
                }  

                for(Apttus__Agreement_Clause__c agrClause : listOfAgrClausesApprovalReq)
                {   
                    system.debug('APTTUS--> agrClause '+agrClause.Apttus__Clause__c);
                    if(setOfAgrClausesDeleted.contains(agrClause.Apttus__Clause__c)){            
                        //Don't add Agreement Clause which have status deleted. We don't need to update those agreement clauses
                        if(!agrClauseIDSet_Deleted.contains(agrClause.ID)){
                            agrClauseIDSet.add(agrClause.ID);
                        }                        
                    }
                }               

            }
        }
        system.debug('APTTUS--> agrClauseIDSet '+agrClauseIDSet);

        if(!agrClauseIDSet.isEmpty()){
            listOfagrCluaseToBeUpdated = [SELECT Id,Name,Apttus__Action__c,Apttus__Active__c,Apttus__Agreement__c,Apttus__Status__c,Apttus__Category__c
                                            FROM Apttus__Agreement_Clause__c 
                                            WHERE ID in:agrClauseIDSet];
            system.debug('APTTUS--> listOfagrCluaseToBeUpdated '+ listOfagrCluaseToBeUpdated);

            if(!listOfagrCluaseToBeUpdated.isEmpty()){
                for(Apttus__Agreement_Clause__c agrClauseSO : listOfagrCluaseToBeUpdated){
                    agrClauseSO.Apttus__Status__c = CONSTANTS.DELETED;//CONSTANTS.CANCELLED;
                    agrClauseSO.APTS_Approval_Flag__c = true;
                }
                update listOfagrCluaseToBeUpdated;               
            }  

            /*
            if(!listOfAgreement.isEmpty()){
                for(Apttus__APTS_Agreement__c agrSO : listOfAgreement){ 
                    agrSO.APTS_Dummy_Agrement_Clause_Approval_Flag__c = true;
                }

                //update listOfAgreement;
            }

            //update listOfagrCluaseToBeUpdated;            

            if(!listOfAgreement.isEmpty()){
                for(Apttus__APTS_Agreement__c agrSO : listOfAgreement){                   
                    Apttus_Approval.ApprovalRequiredCheck checker = new Apttus_Approval.ApprovalRequiredCheck();
                    Boolean result = checker.doCheck(agrSO.Id);
                    system.debug('APTTUS--> result '+result); 
                }
            }           
            */
        }

    }
}