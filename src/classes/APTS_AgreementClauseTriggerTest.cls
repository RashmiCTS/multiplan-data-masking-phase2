/*
	Author: Kruti Shah, Apttus, 01/02/2017 - Test class for Agreement Clause trigger logic.
*/

@isTest
private class APTS_AgreementClauseTriggerTest {
	
	static Account testAccount;
	static Contact testContact;		
	static User testUser1;
    static User testUser2;
    static User testUser3;
    static User testUser4;    
    static List<User> listOfUsers;

	static{
		testAccount = (Account) APTS_TestDataFactory.createSObject(new Account());
        insert testAccount;

        testContact = (Contact) APTS_TestDataFactory.createSObject(new Contact());
        testContact.AccountId = testAccount.Id;
        insert testContact;
        
		Profile sysAdmin = [Select Id 
        						From Profile 
        							Where Name = 'System Administrator' Limit 1];

       	User dummyUser1 = new User();
       	dummyUser1 = (User)APTS_TestDataFactory.createSObject(new User());
		dummyUser1.ProfileId = sysAdmin.ID;
		insert dummyUser1;

       	listOfUsers = new List<User>();
		testUser1 = new User(Alias = 'testu1',email = 'test@test.com', IsActive = true,LastName='Testing',UserName='testApttususer1@apttustest.com',EmailEncodingKey='UTF-8',LanguageLocaleKey='en_US',LocaleSidKey='en_US',TimeZoneSidKey='America/Los_Angeles',ProfileId = sysAdmin.ID);
        testUser1.Division = 'OSI-New York';
        testUser1.CompanyName ='Apttus'; 
		listOfUsers.add(testUser1);		

		testUser2 = new User(Alias = 'testu2',email = 'test2@test.com', IsActive = true,LastName='Testing2',UserName='testApttususer2@apttustest.com',EmailEncodingKey='UTF-8',LanguageLocaleKey='en_US',LocaleSidKey='en_US',TimeZoneSidKey='America/Los_Angeles',ProfileId = sysAdmin.ID);
        testUser2.Division = 'OSI-New York';
        testUser2.CompanyName ='Apttus'; 
		listOfUsers.add(testUser2);	

		testUser3 = new User(Alias = 'testu3',email = 'test3@test.com', IsActive = true,LastName='Testing3',UserName='testApttususer3@apttustest.com',EmailEncodingKey='UTF-8',LanguageLocaleKey='en_US',LocaleSidKey='en_US',TimeZoneSidKey='America/Los_Angeles',ProfileId = sysAdmin.ID);
        testUser3.Division = 'OSI-New York';
        testUser3.CompanyName ='Apttus'; 
		listOfUsers.add(testUser3);		

		testUser4 = new User(Alias = 'testu4',email = 'test4@test.com', IsActive = true,LastName='Testing4',UserName='testApttususer4@apttustest.com',EmailEncodingKey='UTF-8',LanguageLocaleKey='en_US',LocaleSidKey='en_US',TimeZoneSidKey='America/Los_Angeles',ProfileId = sysAdmin.ID);
        testUser4.Division = 'OSI-New York';
        testUser4.CompanyName ='Apttus'; 
		listOfUsers.add(testUser4);	
		insert listOfUsers;

	}

	@isTest static void agreementClauseInsert_Test() {
		// Implement test code
		list<Apttus__Agreement_Clause__c> agreementClauseList = new list<Apttus__Agreement_Clause__c>();

		Id group5RTId = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('Group V5').getRecordTypeId();  

		Apttus__APTS_Agreement__c testAgreement = new Apttus__APTS_Agreement__c();
		testAgreement.Apttus__Account__c = testAccount.id;
		testAgreement.recordtypeid = group5RTId;
		testAgreement.Apttus__Primary_Contact__c = testContact.id;
		testAgreement.APTS_Director__c = testUser4.Id;
		testAgreement.SVP_RVP__c = testUser3.Id;
		testAgreement.Regulatory_Compliance__c = testUser2.Id;
		testAgreement.APTS_Legal_Support__c = testUser1.Id;		
		insert testAgreement;

		testAgreement = [select id,SVP_RVP__c,APTS_Director__c,APTS_Legal_Support__c,Regulatory_Compliance__c
						FROM Apttus__APTS_Agreement__c
							WHERE Id = :testAgreement.ID];

		Apttus__Agreement_Clause__c agrClause = new Apttus__Agreement_Clause__c();
		agrClause.Apttus__Agreement__c = testAgreement.id;
		agrClause.Apttus__Action__c = CONSTANTS.INSERTED;
		agrClause.Apttus__Clause__c = 'test clause to be insert';
		agrClause.Apttus__Active__c = true;
		agreementClauseList.add(agrClause);

		Apttus__Agreement_Clause__c agrClause2 = new Apttus__Agreement_Clause__c();
		agrClause2.Apttus__Agreement__c = testAgreement.id;
		agrClause2.Apttus__Action__c = CONSTANTS.INSERTED;
		agrClause2.Apttus__Clause__c = 'Section 1.04 Client - Option 03 (Group 5)';
		agrClause2.Apttus__Active__c = true;
		agrClause2.Apttus__Status__c = CONSTANTS.APPROVAL_REQUIRED;
		agreementClauseList.add(agrClause2);

		Apttus__Agreement_Clause__c agrClause4 = new Apttus__Agreement_Clause__c();
		agrClause4.Apttus__Agreement__c = testAgreement.id;		
		agrClause4.Apttus__Clause__c = 'Section 1.04 Client - Option 03 (Group 5)';
		agrClause4.Apttus__Active__c = true;
		agrClause4.Apttus__Action__c = CONSTANTS.DELETED;
		agreementClauseList.add(agrClause4);
		insert agreementClauseList;

		Apttus__Agreement_Clause__c agrClause3 = new Apttus__Agreement_Clause__c();
		agrClause3.Apttus__Agreement__c = testAgreement.id;		
		agrClause3.Apttus__Clause__c = 'Section 1.04 Client - Option 03 (Group 5)';
		agrClause3.Apttus__Active__c = true;		
		insert agrClause3;

		agrClause3.Apttus__Action__c = CONSTANTS.DELETED;
		update agrClause3;

		agrClause = [select id,SVP_RVP__c,APTS_Director__c,APTS_Regulatory_Compliance__c,APTS_Legal_Support__c 
						FROM Apttus__Agreement_Clause__c
							WHERE Id = :agrClause.ID];		
		
		System.assertEquals(testAgreement.SVP_RVP__c,agrClause.SVP_RVP__c);
		System.assertEquals(testAgreement.APTS_Director__c,agrClause.APTS_Director__c);
		System.assertEquals(testAgreement.Regulatory_Compliance__c,agrClause.APTS_Regulatory_Compliance__c);
		System.assertEquals(testAgreement.APTS_Legal_Support__c,agrClause.APTS_Legal_Support__c);		
		
	}

	@isTest static void agreementUpdate_Test() {
		// Implement test code
		list<Apttus__Agreement_Clause__c> agreementClauseList = new list<Apttus__Agreement_Clause__c>();

		Id group5RTId = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('Group V5').getRecordTypeId();  

		Apttus__APTS_Agreement__c testAgreement = new Apttus__APTS_Agreement__c();
		testAgreement.Apttus__Account__c = testAccount.id;
		testAgreement.recordtypeid = group5RTId;
		testAgreement.Apttus__Primary_Contact__c = testContact.id;	
		testAgreement.APTS_Director__c = testUser2.Id;
		testAgreement.SVP_RVP__c = testUser1.Id;
		testAgreement.Regulatory_Compliance__c = testUser3.Id;
		testAgreement.APTS_Legal_Support__c = testUser4.Id;			
		insert testAgreement;

		testAgreement = [select id,SVP_RVP__c,APTS_Director__c,APTS_Legal_Support__c,Regulatory_Compliance__c
						FROM Apttus__APTS_Agreement__c
							WHERE Id = :testAgreement.ID];

		Apttus__Agreement_Clause__c agrClause = new Apttus__Agreement_Clause__c();
		agrClause.Apttus__Agreement__c = testAgreement.id;
		agrClause.Apttus__Action__c = CONSTANTS.INSERTED;
		agrClause.Apttus__Clause__c = 'test clause to be insert';
		agrClause.Apttus__Active__c = true;
		agreementClauseList.add(agrClause);

		Apttus__Agreement_Clause__c agrClause2 = new Apttus__Agreement_Clause__c();
		agrClause2.Apttus__Agreement__c = testAgreement.id;
		agrClause2.Apttus__Action__c = CONSTANTS.INSERTED;
		agrClause2.Apttus__Clause__c = 'test clause to be insert 2';
		agrClause2.Apttus__Active__c = true;
		agreementClauseList.add(agrClause2);
		insert agreementClauseList;

		testAgreement.APTS_Director__c = testUser4.Id;
		testAgreement.SVP_RVP__c = testUser3.Id;
		testAgreement.Regulatory_Compliance__c = testUser2.Id;
		testAgreement.APTS_Legal_Support__c = testUser1.Id;
		update testAgreement;

		testAgreement = [select id,SVP_RVP__c,APTS_Director__c,APTS_Legal_Support__c,Regulatory_Compliance__c
						FROM Apttus__APTS_Agreement__c
							WHERE Id = :testAgreement.ID];		
		
		agrClause = [select id,SVP_RVP__c,APTS_Director__c,APTS_Regulatory_Compliance__c,APTS_Legal_Support__c 
						FROM Apttus__Agreement_Clause__c
							WHERE Id = :agrClause.ID];		
		
		System.assertEquals(testAgreement.SVP_RVP__c,agrClause.SVP_RVP__c);
		System.assertEquals(testAgreement.APTS_Director__c,agrClause.APTS_Director__c);
		System.assertEquals(testAgreement.Regulatory_Compliance__c,agrClause.APTS_Regulatory_Compliance__c);
		System.assertEquals(testAgreement.APTS_Legal_Support__c,agrClause.APTS_Legal_Support__c);		
		
	}

}