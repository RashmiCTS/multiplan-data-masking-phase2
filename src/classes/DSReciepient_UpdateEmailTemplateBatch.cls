/*
File Name:   DSReciepient_UpdateEmailTemplateBatch
Author:      Rashmi T  
Date:        11/14/2016
Description: The docusign API implaemented by Apptus could not resolve the Agreement Number present in the email template 
				of V5_Group_Email and hence this batch serves as a workaround which creates an email template for every Agreement
				to embed the agreement number in itself.

Modification History
11/14/2016   Rashmi T   Created
*/ 
public class DSReciepient_UpdateEmailTemplateBatch implements Database.Batchable<sObject> { 
    
    public DSReciepient_UpdateEmailTemplateBatch(){
        //constructor
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC) {
    	String strQuery = 'SELECT id,Name,Email_Template__c,Email_Template_Prev_Assigned__c,Apttus_CMDSign__AgreementId__c,Apttus_CMDSign__AgreementId__r.Apttus__FF_Agreement_Number__c '+
            					'FROM Apttus_DocuApi__DocuSignDefaultRecipient2__c WHERE Email_Template__c=\'V5_Group_Email\' AND Apttus_CMDSign__AgreementId__c!=NULL';
        return Database.getQueryLocator(strQuery);
    }
    
    public void execute(Database.BatchableContext BC, List<Apttus_DocuApi__DocuSignDefaultRecipient2__c> scope) {
        System.debug('******** scope '+scope);
        String strTempName = '';
        String strTemp = 'TEMP_';
    	EmailTemplate emTemp = new EmailTemplate();
        List<EmailTemplate> lstEmailTemp = new List<EmailTemplate>();
        Apttus_DocuApi__DocuSignDefaultRecipient2__c docRep = new Apttus_DocuApi__DocuSignDefaultRecipient2__c();
        List<Apttus_DocuApi__DocuSignDefaultRecipient2__c> lstDocuRepUpdate = new List<Apttus_DocuApi__DocuSignDefaultRecipient2__c>();
        Map<String,EmailTemplate> mapCopyEmailTempNameVSEmailTemp = new Map<String,EmailTemplate>();
        List<Apttus_DocuApi__DocuSignDefaultRecipient2__c> lstDocusignReciepe = (List<Apttus_DocuApi__DocuSignDefaultRecipient2__c>) scope;
        for(Apttus_DocuApi__DocuSignDefaultRecipient2__c doc: lstDocusignReciepe){
            docRep = new Apttus_DocuApi__DocuSignDefaultRecipient2__c();
            docRep.id = doc.id;
            strTempName = strTemp+doc.Apttus_CMDSign__AgreementId__r.Apttus__FF_Agreement_Number__c;
            strTempName = strTempName.replace('.','_');
            docRep.Email_Template_Prev_Assigned__c = doc.Email_Template__c;
            docRep.Email_Template__c = strTempName;
            lstDocuRepUpdate.add(docRep);
        }
        System.debug('***** lstDocuRepUpdate '+lstDocuRepUpdate);
        update lstDocuRepUpdate;
    }
    
    public void finish(Database.BatchableContext BC) {     	
        
        AsyncApexJob a = [SELECT id, ApexClassId, 
                       JobItemsProcessed, TotalJobItems, 
                       NumberOfErrors, CreatedBy.Email 
                       FROM AsyncApexJob 
                       WHERE id = :BC.getJobId()];
        
        String emailMessage = 'Your batch job '
             + 'to create temporary email templates has been completed with the following results :\n'
             + a.totalJobItems 
             + ' batches.  Of which, ' + a.jobitemsprocessed 
             + ' processed without any exceptions thrown and ' 
             + a.numberOfErrors +
             ' batches threw unhandled exceptions.'
             + '  Of the batches that executed without error, ' ;
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {a.createdBy.email};
        mail.setTargetObjectId(UserInfo.getUserId());
        mail.saveAsActivity = false;
        mail.setSubject('Temporary Email Batch job completed');
        mail.setPlainTextBody(emailMessage);
        mail.setHtmlBody(emailMessage);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] 
                           { mail });
    }
}