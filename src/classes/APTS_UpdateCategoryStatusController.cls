/* APTS_UpdateCategoryStatusController
 * Controller class for the page APTS_UpdateCategoryStatus.
 * When Agreement Delivery Method - Fax/Mail ,then update Status and Status Category fields on different Actions.
 *
 * Developer: Kruti Shah, APTTUS
 * Business Owner: 
 *
 * Scenario:
 * 
 * History: 
 * 12/23/2016, Kruti Shah,  APTTUS - created APTS_UpdateCategoryStatusController.
 * 04/12/2017, Kruti Shah,  APTTUS - added additional condition for 'ActivateLegacyAgreement' to handle Activate Legacy Agreement use-case.
 */
public class APTS_UpdateCategoryStatusController {
    Private ID agreementID; 
    private String actionType;
    
    //Constructor
    public APTS_UpdateCategoryStatusController(){            
        actionType = System.currentPageReference().getParameters().get('actionType');  
        agreementID = System.currentPageReference().getParameters().get('AgreementId');         
    }
    
    //Init Method 
    public PageReference doInit(){
        PageReference pageRef =null;
        try{
            //retrieving Agreement fields via ID
            Apttus__APTS_Agreement__c objAgreement = new Apttus__APTS_Agreement__c();
            objAgreement = [select ID,Name,Apttus__Status_Category__c,Apttus__Status__c,Agreement_Stage__c
                                from Apttus__APTS_Agreement__c 
                                    WHERE ID =:agreementID];
            
            if(actionType.equalsIgnoreCase(CONSTANTS.SENT_VIA_MAIL_OR_FAX))
            {
                objAgreement.Apttus__Status_Category__c = CONSTANTS.STATUS_CATEGORY_IN_SIGNATURES;
                objAgreement.Apttus__Status__c = CONSTANTS.STATUS_OTHER_PARTY_SIGNATURES;                   
            }
            
            else if(actionType.equalsIgnoreCase(CONSTANTS.MAIL_OR_FAX_PROVIDER_REJECTED))
            {
                objAgreement.Apttus__Status_Category__c = CONSTANTS.STATUS_CATEGORY_IN_SIGNATURES;
                objAgreement.Apttus__Status__c = CONSTANTS.STATUS_SIGNATURE_DECLINED;       
            }
            
            else if(actionType.equalsIgnoreCase(CONSTANTS.MAIL_OR_FAX_EXECUTIVE_SIGNATURE_COMPLETED))
            {
                objAgreement.Apttus__Status__c = CONSTANTS.STATUS_FULLY_SIGNED;
            }
            
            /* commented by Kruti Shah : this functionality is not part of Apttus implementation
            else if(actionType.equalsIgnoreCase(CONSTANTS.ACTIVATE_LEGACY_AGREEMENT))
            {
                objAgreement.Apttus__Status_Category__c = CONSTANTS.STATUS_CATEGORY_IN_EFFECT;
                objAgreement.Apttus__Status__c = CONSTANTS.STATUS_ACTIVATED;       
                objAgreement.Agreement_Stage__c = CONSTANTS.STATUS_ACTIVATED;       
            }
            */
            update objAgreement; 
            
        }catch(Exception ex)
        {   
            system.debug('Error while updating Agreement Record '+ex);
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error while updating Agreement Record '+ex));            
            return null;
        }
        
        pageRef= new PageReference('/' + agreementID);    
        return pageRef; 
    }
    
}