@isTest
public class APTS_UpdateCategoryStatusControllerTest {
    public static Account testAcc;
    public static Contact testCon;
    
    @testsetup public static void createData(){
        
        testAcc = new Account();
		testAcc.name = 'testA';
    	insert testAcc;
        
        testCon = new Contact();
    	testCon.AccountId = testAcc.Id;
    	testCon.lastName = 'agreementInsert_Test';
    	testCon.email = 'agreementInsert_Test@Test.com';
    	insert testCon;
        
        Id group5RTId = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('Group V5').getRecordTypeId();
        
        Apttus__APTS_Agreement__c testAgr = new Apttus__APTS_Agreement__c();
		testAgr.Apttus__Account__c = testAcc.id;
		testAgr.recordtypeid = group5RTId;
		testAgr.Apttus__Primary_Contact__c = testCon.id;
		testAgr.Market_State__c = 'AK';
		insert testAgr;      
    } 
    
    public static testmethod void testmethod1(){
        PageReference pageRef = Page.APTS_UpdateCategoryStatus;
        Test.setCurrentPage(pageRef);
        
        Apttus__APTS_Agreement__c testAgr2 = [select ID,name from Apttus__APTS_Agreement__c WHERE Market_State__c='AK'];
        
        ApexPages.currentPage().getParameters().put('actionType', 'SentViaMailOrFax');
        ApexPages.currentPage().getParameters().put('AgreementId', testAgr2.ID);
        
        Test.startTest();
        APTS_UpdateCategoryStatusController objController = new APTS_UpdateCategoryStatusController();
        objController.doInit();
		Test.stopTest();     
    }
    
    public static testmethod void testmethod2(){
        PageReference pageRef = Page.APTS_UpdateCategoryStatus;
        Test.setCurrentPage(pageRef);
        
        Apttus__APTS_Agreement__c testAgr2 = [select ID,name from Apttus__APTS_Agreement__c WHERE Market_State__c='AK'];
        
        ApexPages.currentPage().getParameters().put('actionType', 'MailOrFaxProviderRejected');
        ApexPages.currentPage().getParameters().put('AgreementId', testAgr2.ID);
        
        Test.startTest();
        APTS_UpdateCategoryStatusController objController = new APTS_UpdateCategoryStatusController();
        objController.doInit();
		Test.stopTest();     
    }
    
    public static testmethod void testmethod3(){
        PageReference pageRef = Page.APTS_UpdateCategoryStatus;
        Test.setCurrentPage(pageRef);
        
        Apttus__APTS_Agreement__c testAgr2 = [select ID,name from Apttus__APTS_Agreement__c WHERE Market_State__c='AK'];
        
        ApexPages.currentPage().getParameters().put('actionType', 'MailorFaxExecutiveSignatureCompleted');
        ApexPages.currentPage().getParameters().put('AgreementId', testAgr2.ID);
        
        Test.startTest();
        APTS_UpdateCategoryStatusController objController = new APTS_UpdateCategoryStatusController();
        objController.doInit();
		Test.stopTest();     
    }
    
    public static testmethod void testExcetption(){
        PageReference pageRef = Page.APTS_UpdateCategoryStatus;
        Test.setCurrentPage(pageRef);
        
        Apttus__APTS_Agreement__c testAgr2 = [select ID,name from Apttus__APTS_Agreement__c WHERE Market_State__c='AK'];
        
        ApexPages.currentPage().getParameters().put('actionType', 'MailorFaxExecutiveSignatureCompleted');
        ApexPages.currentPage().getParameters().put('AgreementId',null);
        
        Test.startTest();
        APTS_UpdateCategoryStatusController objController = new APTS_UpdateCategoryStatusController();
        objController.doInit();
		Test.stopTest();     
    }

    
}