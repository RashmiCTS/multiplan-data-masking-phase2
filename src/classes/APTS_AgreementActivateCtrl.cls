/* APTS_AgreementActivateCtrl
* Controller class for the page APTS_AgreementActivate.
*
* Developer: Kruti Shah, APTTUS
* Business Owner: 
*
* Scenario:
* 
* History: 
* 02/17/2017, Kruti Shah,  APTTUS - created APTS_AgreementActivateCtrl. 
*/
public class APTS_AgreementActivateCtrl {
    
    Private ID agreementID;
    Private Apttus__APTS_Agreement__c objAgreement = new Apttus__APTS_Agreement__c();
    
    public APTS_AgreementActivateCtrl(){         
        
        agreementID = System.currentPageReference().getParameters().get('agreementId');        
        
        objAgreement = new Apttus__APTS_Agreement__c();
        objAgreement = [SELECT Id,Name,Apttus__Status_Category__c,Apttus__Status__c,Agreement_Stage__c
                        	FROM Apttus__APTS_Agreement__c 
                        		WHERE Id =:agreementID ];
    }
    
    
    public PageReference doActivate() {
        
        //Use the webservice to Activate the agreement.
        //Also provide appropriate parameter to add document for contract,
        //add for content search and for removal.
        
        PageReference pageRef =null;    
        
        Boolean toActivate=false;
        //validate agreement id
        try {
            
            String[] remDocIds = new String[] {};               
			List<String> activateDocIds = new List<String>();
            List<String> publishedDocIds = new List<String>();
            
            List <Attachment> listOfAtts = [SELECT Id, ParentId, Name, Body from Attachment where ParentId =:agreementID];    
            
            if (listOfAtts.size() > 0 && !listOfAtts.isEmpty()) {
               
                for (Attachment attFile: listOfAtts) {
                    if(attFile.Name.endsWith('__signed.docx') || attFile.Name.endsWith('__signed.pdf') )
                    {
                        toActivate=true;
                    }
                }
                
                //if a file found with __signed.pdf only then activate
                if(toActivate==true)
                {
                    for (Attachment attFile: listOfAtts) {                        
                        
                        if(attFile.Name.endsWith('__signed.docx') || attFile.Name.endsWith('__signed.pdf') )
                        {
                            activateDocIds.add(attFile.Id); 
                        }
                        else
                        {
                            //remDocIds.add(attFile.Id);
                        }                        
                    }                     
                    publishedDocIds=activateDocIds;
                }
            }
            
            system.debug('APTTUS--> activateDocIds '+activateDocIds);
            
            if (activateDocIds.size() > 0) {                    
                Boolean activateResponse = Apttus.AgreementWebService.activateAgreement(AgreementId, activateDocIds, remDocIds);
                
                system.debug('APTTUS--> activateResponse '+activateResponse);
                
                if (activateResponse && publishedDocIds != null) {
                    for(Id publishedDocId : publishedDocIds)
                    {
                        //publish document
                        Boolean publishDocumentResponse = Apttus.AgreementWebService.publishToDocumentRepository(AgreementId, publishedDocId);
                    }  
                }else{
                    // return 'ACTIVATION_FAILED';
                    system.debug('ACTIVATION_FAILED.');
            		ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'ACTIVATION_FAILED.'));            
            		return null;
                }
                
            }else{
                // return 'No Documents found for activation';
                system.debug('No Documents found for activation.');
            	ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'No Documents found for activation.'));            
            	return null;
            }
            
            //update objAgreement;
        }        
        
        catch (system.exception ex) {
            System.debug('Exception has occurred in activation : ' + ex.getMessage());
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Exception has occurred in activation :'+ ex.getMessage())); 
            return null;
        }
        
        pageRef= new PageReference('/' + agreementID);    
        return pageRef;
    }    
    
}