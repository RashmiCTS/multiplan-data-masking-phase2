/* APTS_AgreementActivate_OOB_ctrl
 * Controller class for APTS_AgreementActivate_OOB page. 
 *
 * Developer: Kruti Shah, APTTUS
 *
 * Scenario:
   ***************************************************************************************************
   Modification Log:
   *
   *    Kruti Shah, 02/21/2017 - Created.
   *    
   ***************************************************************************************************
*/
public class APTS_AgreementActivate_OOB_ctrl {
   
   
    private Apttus__APTS_Agreement__c agreementSO;  

    public Boolean isInitialized{get; private set;}
    public Boolean canEditAgreement{get; private set;}
    public Boolean isErrorPage{get; private set;}

    public Boolean isSelectionPage{get; private set;} 
    public Boolean isReviewPage{get; private set;}

    public Boolean hasSelectedDocs{get; private set;}   
    
    public Boolean isFirstPage{get; private set;}
    public Integer currentPageNumber{get; private set;}
    
    private Map<Id, DocInfoWrapper> allDocumentsMap;

    public List<SelectOption> availableDocItems{get; private set;}    

    public Boolean hasAvailableDocItems{get; private set;}   

    public List<String> selectedDocs{get; set;}   
    public List<String> removableDocs{get; set;}      
    public List<DocInfoWrapper> selectedDocInfos{get; set;}  

    public APTS_AgreementActivate_OOB_ctrl(ApexPages.StandardController stdController) {
        this.agreementSO = (Apttus__APTS_Agreement__c)stdController.getRecord();
        isInitialized = false;       
    }
    
    public PageReference doInit(){
        PageReference pgRef;
        
        canEditAgreement = true;
        isErrorPage = false;
        List<UserRecordAccess> userAccessRecords = [Select RecordId, HasReadAccess, HasEditAccess
                                                            From UserRecordAccess 
                                                                Where UserId=:UserInfo.getUserId() 
                                                                      AND RecordId = :agreementSO.Id];
        if(userAccessRecords.isEmpty()
           || userAccessRecords.get(0).HasEditAccess == false)
        {
            canEditAgreement = false;
            isErrorPage = true;
        }
        
        isInitialized = true;
        currentPageNumber = 1;
        isFirstPage = true;
        
        hasAvailableDocItems = false;
        isSelectionPage = true;       

        allDocumentsMap = new Map<Id, DocInfoWrapper>();
        for(Attachment a : [select Id, Name, createddate, ContentType 
                                    From Attachment 
                                        Where ParentId =:this.agreementSO.Id order by createddate desc]){
            allDocumentsMap.put(a.Id, new DocInfoWrapper(a));
        }

        availableDocItems = getavailableDocuments();
        selectedDocs    	= new List<String>();       
        selectedDocInfos    = new List<DocInfoWrapper>();        
        return pgRef;
    }
    
    public PageReference doCancel(){
        return new PageReference('/'+ agreementSO.Id);
    }
    
    public PageReference doNext(){
        PageReference pgRef = null;
        populateSummary();
        if(isSelectionPage == true){
            if(selectedDocs == null 
                || selectedDocs.isEmpty()){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'At least one document or attachment is required to continue.'));
                return null;
            }else{
                isSelectionPage = false;
                isFirstPage = false;

                // switch to review page.
                isReviewPage = true;                
            }
        }
        currentPageNumber++;
        return pgRef;
    }
    
    public PageReference doPrevious(){
        PageReference pgRef = null;
        if(isReviewPage == true){
            isReviewPage = false;

            //isSearchablePage = true;
            isSelectionPage = true;
            isFirstPage = true;
        }       
        currentPageNumber--;
        return pgRef;
    }
    
    public PageReference doActivate(){
        PageReference pgRef = new PageReference('/'+this.agreementSO.Id);
        // Activate the agreement using API call.
        try{
            Boolean activationResponse = Apttus.AgreementWebService.activateAgreement (this.agreementSO.Id, selectedDocs, removableDocs);
            if(activationResponse == true)
            {
                                               
                Set<String> selectedDocIdSet = new Set<String>();
                selectedDocIdSet.addAll(selectedDocs);
                pgRef = Page.Apttus__PublishDocument;
                pgRef.getParameters().put('id', this.agreementSO.Id);
                pgRef.getParameters().put('actionName', 'publish_document');
                pgRef.getParameters().put('documentIdsCsv', APTS_Agreement_Helper.flattenSet(selectedDocIdSet, ','));
             
            /*  
                Set<String> searchableDocIdSet = new Set<String>();
                searchableDocIdSet.addAll(searchableDocs);
                pgRef = Page.Apttus_Content__PublishContent;
                pgRef.getParameters().put('id', this.agreementSO.Id);
                pgRef.getParameters().put('actionName', 'publish_content');
                pgRef.getParameters().put('documentIdsCsv', APTS_Agreement_Helper.flattenSet(searchableDocIdSet, ','));
            */

            }else{
                system.debug('APTTUS--> Error '+'Error Activation Agreement. Please contact Admin.');
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error Activation Agreement. Please contact Admin.'));
                isErrorPage = true;
                return null;    
            }
        }catch(Exception ex){
            system.debug('APTTUS--> Exception '+ex.getMessage());
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
            isErrorPage = true;
            return null;
        }
        return pgRef;
    }
    
    public List<SelectOption> getavailableDocuments(){
        List<SelectOption> res = new List<SelectOption>();
        if(allDocumentsMap != null 
            && !allDocumentsMap.isEmpty()){
            for(DocInfoWrapper docInfo : allDocumentsMap.values()){
                res.add(new SelectOption(docInfo.docId, docInfo.Name));        
            }
        }

        hasAvailableDocItems = res.isEmpty() ? false : true;
        return res;
    }
    
    private void populateSummary(){
        // prepare Set's out of Lists to use contains.
        Set<String> selectedDocIdSet = new Set<String>();
        selectedDocIdSet.addAll(selectedDocs);                   
        
        // clear all DocInfo lists.
        selectedDocInfos.clear();       
        //removableDocInfos.clear();
        for(DocInfoWrapper docInfo : allDocumentsMap.values()){
            if(selectedDocIdSet.contains(docInfo.docId))
                selectedDocInfos.add(docInfo);                        
        }

        hasSelectedDocs     = selectedDocInfos.isEmpty() ? false : true;              
    }

    // wrapper for attachment....no need of content.
    public class DocInfoWrapper{
        public string Name{get;private set;}
        public String docId{get;private set;}
        public String docType{get;private set;}
        public DocInfoWrapper(Attachment a){
            this.Name = a.Name;
            this.docId = a.Id;
            this.docType = a.ContentType;
        }
    }

}