public class UtilityForAttachments {
/*
File Name:   UtilityForAttachments
Author:      Rashmi T
Date:        11/7/2016
Description: This utility is provides all the methods necessary to operate on attachments
Usage: AttachDocsFromTemplate vf page that can be triggered via custom button 'Add Supporting Docs' on the Agreement record. 

Modification History
11/07/2016      Rashmi T      Created 
*/    
    public static Map<Id,Attachment> getAllAttachmentsForSObject(String strSObjectType,String strContentType){
        Map<Id,Attachment> mapAttach = new Map<Id,Attachment>();
        if(String.isNotBlank(strSObjectType) && String.isNotBlank(strContentType)){
            mapAttach = new Map<Id,Attachment>([SELECT id,parent.Name,Body,Name,parentId FROM Attachment 
                         WHERE ContentType =: strContentType AND parent.type =: strSObjectType]);
        }
        else if(String.isNotBlank(strSObjectType)){
            mapAttach = new Map<Id,Attachment>([SELECT id,parent.Name,Body,Name,parentId FROM Attachment 
                                                WHERE parent.type =: strSObjectType]);
        }
        else if(String.isNotBlank(strContentType)){
            mapAttach = new Map<Id,Attachment>([SELECT id,parent.Name,Body,Name,parentId FROM Attachment 
                                                WHERE ContentType =: strContentType]);
        }
        else{
            mapAttach = new Map<Id,Attachment>([SELECT id,parent.Name,Body,Name,parentId FROM Attachment LIMIT 49999]);
        }
        return mapAttach;
    }
    
    public static Map<Id,List<Attachment>> getAllAttachmentsByParentId(String strSObjectType,Set<String> setStrContentType){
        Map<Id,List<Attachment>> mapParentIdVSAttach = new Map<Id,List<Attachment>>();
        if(String.isNotBlank(strSObjectType) && !setStrContentType.isEmpty()){
            for(Attachment att :[SELECT id,parent.Name,Body,Name,parentId FROM Attachment WHERE ContentType IN: setStrContentType AND parent.type =: strSObjectType]){
                if(mapParentIdVSAttach.containsKey(att.ParentId)){
                    mapParentIdVSAttach.get(att.ParentId).add(att);
                }    
                else{
                    mapParentIdVSAttach.put(att.ParentId, new List<Attachment>{att});
                }
        	}
        }
        return mapParentIdVSAttach;
    }
    
}