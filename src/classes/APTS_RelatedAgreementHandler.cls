/* APTS_RelatedAgreementHandler. 
* Trigger Handler for APTS_RelatedAgreementTrigger
*
* Developer: Kruti Shah, APTTUS
* Business Owner: 
*
* Scenario:
* 
* History: 
* 03/24/2017,Kruti Shah,  APTTUS - created APTS_RelatedAgreementHandler. 
*/
public class APTS_RelatedAgreementHandler {
    
    public void populateOriginalAgreementAfterAmend(List<Apttus__APTS_Related_Agreement__c> newRelAgrList)
    {
        map<Id,Id> mapChildAgrToParentAgr = new map<Id,ID>();
        map<Id,Apttus__APTS_Agreement__c> mapChildAgrIDToAgrRec = new map<Id,Apttus__APTS_Agreement__c>();
        set<Id> setOfChildAgreementIDs = new set<Id>();       
        List<Apttus__APTS_Agreement__c> listOfChildAgreements = new List<Apttus__APTS_Agreement__c>();
        
        for(Apttus__APTS_Related_Agreement__c relatedAgreement: newRelAgrList)
        {
            mapChildAgrToParentAgr.put(relatedAgreement.Apttus__APTS_Contract_To__c,relatedAgreement.Apttus__APTS_Contract_From__c);
            setOfChildAgreementIDs.add(relatedAgreement.Apttus__APTS_Contract_To__c);
        }
        System.debug('APTS_RelatedAgreementHandler - mapChildAgrToParentAgr : ' + mapChildAgrToParentAgr);       
        
        if(setOfChildAgreementIDs.size() >0 && !setOfChildAgreementIDs.isEmpty()){
            listOfChildAgreements = [SELECT ID,Name,APTS_Original_Agreement__c 
                                     FROM Apttus__APTS_Agreement__c 
                                     WHERE ID in :setOfChildAgreementIDs];
        }
        System.debug('APTS_RelatedAgreementHandler - listOfChildAgreements : ' + listOfChildAgreements);       
        
        if(listOfChildAgreements.size() >0 && !listOfChildAgreements.isEmpty()){
            for(Apttus__APTS_Agreement__c childAgreementRecord :listOfChildAgreements){
                childAgreementRecord.APTS_Original_Agreement__c = mapChildAgrToParentAgr.get(childAgreementRecord.Id) != null ? mapChildAgrToParentAgr.get(childAgreementRecord.Id) : null;
            }
            
            update listOfChildAgreements;
        }        
    }    
}