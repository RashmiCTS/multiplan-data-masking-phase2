/*
File Name:   TriggerCommonUtils.cls
Author:      Jason Hartfield
Date:        8/9/2016
Description: Template triggers

Modification History
Modified By			Date			Description

*/

public class TriggerCommonUtils {
	
	// Can be set to globally not run all triggers we are adding.  Helps us prevent looping triggers when we don't need to.
	public static boolean BypassAllTriggers = false;
	
	// Prevents duplicate names on the object being updated/inserted.  Should be called on 'before'.  (Does not support undelete)
	public static void PreventDuplicateNames(List<sObject> triggerNew, Map<ID,sObject> triggerOldMap){
		
		// First, only check the items that have had a name change.  Improves performance, reduces Soql queries.
		Map<string,sObject> itemsToCheckByName = new Map<string,sObject>();
		for(sObject newItem : triggerNew){
			sObject oldItem;
			if(triggerOldMap != null)
				oldItem = triggerOldMap.get(newItem.id);
			if(oldItem == null || newItem.get('Name') != oldItem.get('Name'))
				itemsToCheckByName.put((string)newItem.get('Name'),newItem);
		}

		if(itemsToCheckByName.size() > 0){
			// Get the api name for the object so we can run a select.
			string ObjectAPIName = itemsToCheckByName.values()[0].getsObjectType().getDescribe().getName();
			
			Set<string> nameList = itemsToCheckByName.keySet();
			string selectSoql = 'SELECT ID, Name FROM '+ ObjectAPIName +' WHERE Name in :nameList';
			for(sObject duplicateItem : Database.query(selectSoql)){
				string duplicateItemName = (string) duplicateItem.get('Name');
				itemsToCheckByName.get(duplicateItemName).addError('An item alredy exists with this name, duplicate Id=' + duplicateItem.id);
			}
		}
	
	}
}