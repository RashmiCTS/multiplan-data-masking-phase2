/*
File Name:   Test_TaskTrigger_Trigger
Author:      Michael Olson
Date:        4/6/2016
Description: Test Class for TaskTrigger

Modification History
4/13/2016		Michael Olson		Created
*/

@istest 
public with sharing class Test_TaskTrigger {
    
    static testMethod void TestTaskTrig() { 
		//---------------------------------------------------
        // Variables
        //---------------------------------------------------    
		Id rtAgreeVAAG = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('VA Amendment - Group').getRecordTypeId();   
		     
        //---------------------------------------------------
        // Insert Records
        //---------------------------------------------------
   	
	    	Account a = new Account();
	    		a.name = 'testA';
	    	insert a;
	    	
			list<Apttus__APTS_Agreement__c> agreesList = new list<Apttus__APTS_Agreement__c>(); 
	    		
	    		//contact to change
	    		Apttus__APTS_Agreement__c a1 = new Apttus__APTS_Agreement__c();
		    		a1.Apttus__Account__c = a1.id;
		    		a1.recordtypeid = rtAgreeVAAG;
	    		agreesList.add(a1);	
	 		
	    	insert agreesList;
	    	
	    	
	    	Task T = new Task();
	    		T.Subject = 'Get Internal Signatures';
	    		T.status = 'Not Started';
	    		T.Whatid = a1.id;
			insert T;
	 	
	}
}