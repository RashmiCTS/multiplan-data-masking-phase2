public with sharing class AgreementTriggerHandler {
	
    public static void regenerateAgreements(List<Apttus__APTS_Agreement__c> lstTriggerNew ,
                                            Map<Id,Apttus__APTS_Agreement__c> mapTriggerNewMap,
                                           Map<Id,Apttus__APTS_Agreement__c> mapTriggerOldMap){
        List<Apttus__APTS_Agreement__c> lstAgreeTrigNew = new List<Apttus__APTS_Agreement__c>();
        Map<Id,Apttus__APTS_Agreement__c> mapAgreeTrigNew = new Map<Id,Apttus__APTS_Agreement__c>();
        for(Id i : mapTriggerNewMap.keySet()){
            if(mapTriggerOldMap.get(i).Market_Id__c <> mapTriggerNewMap.get(i).Market_Id__c ||
               mapTriggerOldMap.get(i).Fee_Schedule_Type__c <> mapTriggerNewMap.get(i).Fee_Schedule_Type__c){
                   
                   mapAgreeTrigNew.put(i, mapTriggerNewMap.get(i));
                   lstAgreeTrigNew.add(mapTriggerNewMap.get(i));
                   
               }                   
        }
        if(!lstAgreeTrigNew.isEmpty() && !mapAgreeTrigNew.isEmpty()){
            ClatTriggerUtils.lstTriggerNew = lstAgreeTrigNew;
            ClatTriggerUtils.mapTrigNewSObject = mapAgreeTrigNew;
            List<Id> lstId = new List<Id>();
            //Delete Existing attachments
            deleteExistingOldGeneratedDocs(mapAgreeTrigNew.keySet());
            // Delete Existing Templates
            deleteAgreementTemplates(mapAgreeTrigNew.keySet());
            //Create New Templates
            List<Template_To_Gen__c> lstTempGen = ClatTriggerUtils.CreateDocumentGenerationTemplatesForAgreements(lstTriggerNew);
            if(!lstTempGen.isEmpty()){
                for(Template_To_Gen__c temp : lstTempGen){
                    lstId.add(temp.id);
                    if(Limits.getFutureCalls() < Limits.getLimitFutureCalls() && Limits.getCallouts() < Limits.getLimitCallouts()){
                        MPN_GenerateDocument_Future.GenerateDocument(temp.id,UserInfo.getSessionId());
                    } 
                } 
            }
        } 
    }
    
    private static List<Template_To_Gen__c> getAgreementTemplates(Set<Id> setAgreeIds){
        List<Template_To_Gen__c> lstTempToDel = new List<Template_To_Gen__c>();
        if(!setAgreeIds.isEmpty()){
            lstTempToDel = [SELECT id,Generated_Attachment_Id__c FROM Template_To_Gen__c WHERE Agreement__c IN : setAgreeIds];            
        }  
        return lstTempToDel;
    }
    private static void deleteAgreementTemplates(Set<Id> setAgreeIds){
        List<Template_To_Gen__c> lstTempToDel = getAgreementTemplates(setAgreeIds);
        if(!lstTempToDel.isEmpty()){
            delete lstTempToDel;
        }  
    }
    
    private static void deleteExistingOldGeneratedDocs(Set<Id> setAgreeIds){
        Set<Id> setGenAttchId = new Set<Id>();
        List<Template_To_Gen__c> lstTempToDel = getAgreementTemplates(setAgreeIds);
        List<Attachment> lstAgreeAttachs = new List<Attachment>();
        if(!lstTempToDel.isEmpty()){
            for(Template_To_Gen__c temp : lstTempToDel){
                setGenAttchId.add(temp.Generated_Attachment_Id__c);
            }
            lstAgreeAttachs = [SELECT id FROM Attachment WHERE id IN:setGenAttchId];
            if(!lstAgreeAttachs.isEmpty()){
                delete lstAgreeAttachs;
            }
        }
        /*
        //These docs are determined by dates that are less than today
        List<Attachment> lstAgreeAttachs = new List<Attachment>();
        List<Attachment> lstAgreeDelAttachs = new List<Attachment>();
        Date dt = Date.today();
        String strTodayDateSubs = String.valueOf(dt.year())+'-'+String.valueOf(dt.month())+'-'+String.valueOf(dt.day());
        lstAgreeAttachs = [SELECT id,Name FROM Attachment WHERE parentId=:idAgree];
        String strRegex = '[A-Z,a-z]\\d[_]\\d{4}[-]\\d{2}[-]\\d{2}';
        String strName;
        String strMatch;
        if(!lstAgreeAttachs.isEmpty()){
            for(Attachment ach : lstAgreeAttachs){
                strName = ach.Name;
                if(!strName.contains(strTodayDateSubs)){
                    strMatch = matchStringPatterns(strRegex,strName);
                    System.debug('***** strMatch '+strMatch);
                    if(String.isNotBlank(strMatch)){
                        //Add the file name to the delete list;
                        lstAgreeDelAttachs.add(ach);
                    }
                }
            }
            if(!lstAgreeDelAttachs.isEmpty()){
                System.debug('****** lstAgreeDelAttachs '+lstAgreeDelAttachs);
                delete lstAgreeDelAttachs;
            }            
        } */       
    }    
}