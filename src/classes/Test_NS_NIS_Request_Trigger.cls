/*
File Name:   Test_NS_NIS_Request_Trigger.cls
Author:      Michael Olson
Date:        5/26/2015
Description: Test class for CaseTrigger.

Modification History
5/26/2015		Michael Olson		Creaeted
8/18/2015		Michael Olson		Added Logic to assign Rate Approval By based on Assinged date vs Created date.
10/15/2015		Michael olson		Added Completed Date
3/24/2016		Michael Olson		Added Logic for Fee Schedule Approval Processs - Apttus Project Phase 0
*/

@istest 
public class Test_NS_NIS_Request_Trigger {
    	
    static testMethod void t1() {
	    
	    //---------------------------------------------------
        // Variables
        //---------------------------------------------------
		Id rtRIHCFA = Schema.SObjectType.NS_NIS_Request__c.getRecordTypeInfosByName().get('Reimbursement and Implementation - HCFA').getRecordTypeId();
		Id rtRO = Schema.SObjectType.NS_NIS_Request__c.getRecordTypeInfosByName().get('Rate Operations').getRecordTypeId();
        
        
        //---------------------------------------------------
        // Insert Records
        //---------------------------------------------------

       	User newUapp2 = new User(); 
	    	newUapp2.TimeZoneSidKey = 'America/New_York';   
	    	newUapp2.LanguageLocaleKey = 'en_US';           
	      	newUapp2.LocaleSidKey = 'en_US';            
	     	newUapp2.EmailEncodingKey = 'UTF-8';                    
	  		newUapp2.ProfileId = '00e80000000lB1v'; //00e80000000lB1vAAE
	    	newUapp2.Title  = 'testtitle';
	      	newUapp2.FirstName = 'fname';
	       	newUapp2.LastName = 'lname';
	    	newUapp2.email = 'testtsyfh@kjdhfk.com';
	     	newUapp2.Username = 'testserd@tesdter1111.com'; 
	      	newUapp2.Alias = 'A1';
	       	newUapp2.CommunityNickname =  'nicakn2';
	       	newUapp2.Phone = '1234132';
	       	newUapp2.Auto_Assign_Rate_Operation_Request_Type__c = 'Fee Schedule Request;Non-Standard Rate Exhibit Lang Rev HCFA';
	       	newUapp2.Auto_Assign_Rate_Rev_HCFA_RT_Assignable__c = true;
	    insert newUapp2;   	
	    
	    
       	User newUapp = new User(); 
	    	newUapp.TimeZoneSidKey = 'America/New_York';   
	    	newUapp.LanguageLocaleKey = 'en_US';           
	      	newUapp.LocaleSidKey = 'en_US';            
	     	newUapp.EmailEncodingKey = 'UTF-8';                    
	  		newUapp.ProfileId = '00e80000000lB1v'; //00e80000000lB1vAAE
	    	newUapp.Title  = 'testtitle';
	      	newUapp.FirstName = 'fname';
	       	newUapp.LastName = 'lname';
	    	newUapp.email = 'testtyfhs@kjdhfk.com';
	     	newUapp.Username = 'testsser@tesdter1111.com'; 
	      	newUapp.Alias = 'A1';
	       	newUapp.CommunityNickname =  'nicakn3';
	       	newUapp.Phone = '1234132';
	       	newUapp.Auto_Assign_Rate_Operation_Request_Type__c = 'Fee Schedule Request;Non-Standard Rate Exhibit Lang Rev HCFA';
	       	newUapp.Auto_Assign_Rate_Rev_HCFA_RT_Assignable__c = true;
	       	newUapp.HCE_Request_Approver__c = newUapp2.id;
	    insert newUapp;   	


       	User newU = new User(); 
	    	newU.TimeZoneSidKey = 'America/New_York';   
	    	newU.LanguageLocaleKey = 'en_US';           
	      	newU.LocaleSidKey = 'en_US';            
	     	newU.EmailEncodingKey = 'UTF-8';                    
	  		newU.ProfileId = '00e80000000lB1v'; //00e80000000lB1vAAE
	    	newU.Title  = 'testtitle';
	      	newU.FirstName = 'fname';
	       	newU.LastName = 'lname';
	    	newU.email = 'testtyfh@kjdhfk.com';
	     	newU.Username = 'testser@tesdter1111.com'; 
	      	newU.Alias = 'A1';
	       	newU.CommunityNickname =  'nicakn';
	       	newU.Phone = '1234132';
	       	newU.Auto_Assign_Rate_Operation_Request_Type__c = 'Fee Schedule Request;Non-Standard Rate Exhibit Lang Rev HCFA';
	       	newU.Auto_Assign_Rate_Rev_HCFA_RT_Assignable__c = true;
	       	newU.HCE_Request_Approver__c = newUapp.id;
	    insert newU;	

	    
	    list<NS_NIS_Request__c> reqList = new list<NS_NIS_Request__c>();    
	        
	        //req1
	        NS_NIS_Request__c req1 = new NS_NIS_Request__c();
	        	req1.recordtypeid = rtRIHCFA;
	        	req1.product_Selection__c = 'X';
	        	req1.provider_Tins__c = '111111111';  	
	        reqList.add(req1);
            
            //req2   
	        NS_NIS_Request__c req2 = new NS_NIS_Request__c();
	        	req2.recordtypeid = rtRO;
	        	req2.HCE_Analyst__c = newU.id;
	        	req2.Rate_Operations_Request_Type__c = 'x';
	        	req2.product_Selection__c = 'X';
	        	req2.provider_Tins__c = '111111111';
	        	req2.rate_Operations_request_Type__c = 'Fee Schedule Request';	        
			reqList.add(req2);
			
			//req3	
	        NS_NIS_Request__c req3 = new NS_NIS_Request__c();
	        	req3.recordtypeid = rtRIHCFA;
	        	req3.product_Selection__c = 'X';
	        	req3.provider_Tins__c = '111111111';
	        	req3.Rate_Exhibit_Review_Approval_By__c = UserInfo.getUserId();
	        	req3.Rate_Exhibit_Review_Request_Date_Time__c = system.now();
	        	req3.Request_Rate_Exhibit_Review__c = 'Request Rate Exhibit Review';       	
	        reqList.add(req3);
       	
       		//req4
	        NS_NIS_Request__c req4 = new NS_NIS_Request__c();
	        	req4.recordtypeid = rtRO;
	        	req4.product_Selection__c = 'X';
	        	req4.provider_Tins__c = '111111111';
	        	req4.HCE_Analyst__c = UserInfo.getUserId();
	        	req4.Rate_Exhibit_Review_Request_Date_Time__c = system.now();
				req4.rate_Operations_request_Type__c = 'Non-Standard Rate Exhibit Lang Rev HCFA'; 
				req4.Rate_Exhibit_Review_Approval_By__c = newU.id;   				
				req4.HCE_Analyst_Status__c = 'Closed - Complete';
				req4.HCE_Analyst_Closed_Date__c = system.today();
				req4.Request_Rate_Exhibit_Review__c = 'Request Rate Exhibit Review';
				req4.Rate_Exhibit_Review_Approval_Status__c = 'Closed - x';
				req4.Rate_Exhibit_Review_Approval_Date__c = system.today();	    	
			reqList.add(req4);

			//req5
        	NS_NIS_Request__c req5 = new NS_NIS_Request__c();
	        	req5.recordtypeid = rtRIHCFA;
	        	req5.Negotiation_Sub_Type__c = 'New Agreement';
	        	req5.Requester__c = newU.id;	        	
	        	req5.product_Selection__c = 'MPI;W/C;AM';
	        	req5.Workers_Comp_Rate_Lesser_Of__c = 'Yes';
	        	req5.Workers_Comp_Rate_of_the_State_FS__c = '100';
	        	req5.Auto_Medical_Rate_Lesser_of__c = 'Yes';
	        	req5.Auto_Medical_Rate_of_the_State_FS__c = '100';
	        	req5.Charge_Master_Cap__c = 'Exclude';	      	        	
	        	req5.provider_Tins__c = '111111111';	        	
	        	req5.Negotiated_Fee_Schedule_ID__c = 'x';       	
	        	req5.Metric_Analysis_Not_Required__c = true;    	
        	reqList.add(req5);

			//req6
	        NS_NIS_Request__c req6 = new NS_NIS_Request__c();
	        	req6.recordtypeid = rtRIHCFA;
	        	req6.Negotiation_Sub_Type__c = 'Renegotiation / Add Product';
	        	req6.Requester__c = newU.id;        	
	        	req6.product_Selection__c = 'MPI;W/C';
	        	req6.Workers_Comp_Rate_Lesser_Of__c = 'Yes';
	        	req6.Workers_Comp_Rate_of_the_State_FS__c = '100';
	        	req6.Charge_Master_Cap__c = 'Percent';
	        	req6.Charge_Master_Cap_P__c = 6;	      	        	
	        	req6.provider_Tins__c = '111111111';        	
	        	req6.Negotiated_Fee_Schedule_ID__c = 'x';	        	
	        	req6.Metric_Analysis_Not_Required__c = true;       	
        	reqList.add(req6);
	      
        insert reqList;  
       
        
        list<HCE_Metric__c> hceMetric = new list<HCE_Metric__c>();
        	
        	HCE_Metric__c m1 = new HCE_Metric__c();
        		m1.Product__c = 'MPI';
        		m1.ND_HCE_Request__c = req5.id;
        		m1.Negotiated_P_of_Medicare_P__c = 600;
        		m1.Negotiated_P_of_Medicare__c = 'Percent';
        		m1.Negotiated_Discount_P__c = 1;
        		m1.Revenue_Corridor__c = 'Positive Revenue Impact';
        		m1.Revenue_Corridor_P__c = 5;        	
        	hceMetric.add(m1);
        	
        	HCE_Metric__c m2 = new HCE_Metric__c();
        		m2.Product__c = 'MPI';
        		m2.ND_HCE_Request__c = req6.id;
        		m2.Negotiated_P_of_Medicare_P__c = 600;
        		m2.Negotiated_P_of_Medicare__c = 'Percent';
        		m2.Negotiated_Discount_P__c = 1;
        		m2.Current_P_of_Medicare__c = 'No Medicare % able to be calculated';
        		m2.Revenue_Change__c = 'Decrease in Revenue';
        		m2.Revenue_Change_D__c = -17000;
        		m2.Savings__c = 'Point Decrease in Savings';
        		m2.Savings_Points__c = -11;       	
        	hceMetric.add(m2);        	
        	
        insert hceMetric;


        //Start of testing
        test.startTest();
		
			req4.Enter_into_DTS_for_HCFA_Requested__c = true;
			req4.Rate_Exhibit_Review_Approval_By__c = newU.id;
			req4.Rate_Exhibit_Review_Approval_Status__c = 'Closed - Approved';
			req4.Rate_Exhibit_Review_Approval_Date__c = system.today();
			//update req4;
	       			
		   	req5.HCE_Analyst__c = newU.id;
		    req5.HCE_Analyst_Status__c = 'Closed - Complete';
		    req5.HCE_Analyst_Closed_Date__c = system.today();     
		   	update req5;         			
		    
		    req5.Rate_Exhibit_Review_Approval_By__c = newU.id;
		    req5.Rate_Exhibit_Review_Approval_Status__c = 'Closed - Approved';  
		    req5.Rate_Exhibit_Review_Approval_Date__c = system.today();			
		    update req5;
	
		   	req6.HCE_Analyst__c = newU.id;
		    req6.HCE_Analyst_Status__c = 'Closed - Complete';
		    req6.HCE_Analyst_Closed_Date__c = system.today();     
		   	update req6;         			
		    
		    req6.Rate_Exhibit_Review_Approval_By__c = newU.id;
		    req6.Rate_Exhibit_Review_Approval_Status__c = 'Closed - Approved';  
		    req6.Rate_Exhibit_Review_Approval_Date__c = system.today();			
		    update req6;       			 
        
			req1.product_Selection__c = 'X;Y';
			req1.HCE_Analyst__c = UserInfo.getUserId();
        	update req1;
        	
        	req1.product_Selection__c = '';
        	update req1;
        	
        	req1.HCE_Analyst_Closed_Date__c = system.today();
        	req1.HCE_Analyst_Status__c = 'Closed - Complete';
        	req1.Request_Rate_Exhibit_Review__c = 'Request Rate Exhibit Review';
        	req1.HCE_Analyst_Closed_Date__c = system.today();
        	update req1;
        
        test.stopTest();	
    }

}