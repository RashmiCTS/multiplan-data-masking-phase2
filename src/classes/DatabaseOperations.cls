public with sharing class DatabaseOperations implements DatabaseInterface{
	private List<SObject> lstSobj = new List<SObject>();
    private List<SObject> lstTriggerNew = new List<SObject>();
	private Boolean bIsReturn = false;
	public DatabaseOperations(List<SObject> lstSObject,List<SObject> lstTriggNew) {
		//constructor	
		getSObjectDataType(lstSObject); 
        System.debug('*** lstTriggerNew DB'+lstTriggerNew);
        
        lstTriggerNew = lstTriggNew;
         System.debug('*** lstTriggerNew DB'+lstTriggerNew);

	}

	public List<SObject> insertData(){
        System.debug('*** lstSobj'+lstSobj);
		if(bIsReturn){
			return lstSobj;
		}
		else{	
            System.debug('*** lstTriggerNew DB'+lstTriggerNew);
			Database.SaveResult[] dbSaveResult = Database.insert(lstSobj,false);
			ErrorHandler eh = new ErrorHandler(lstSobj,lstTriggerNew);
			return eh.processSaveResult(dbSaveResult); 
		}
	}

	public List<SObject> updateData(){
		if(bIsReturn){
			return lstSobj;
		}
		else{			
			Database.SaveResult[] dbSaveResult = Database.update(lstSobj,false);
			ErrorHandler eh = new ErrorHandler(lstSobj,lstTriggerNew);
			return eh.processSaveResult(dbSaveResult);
		}
	}


	public void getSObjectDataType(List<SObject> lstSObject){
		Schema.SObjectType schSobjInsertType;
		String strLstSobjType = '';
		if(!lstSObject.isEmpty()){			
			schSobjInsertType = lstSObject[0].getSObjectType();
			strLstSobjType = 'List<'+schSobjInsertType+'>';
			lstSobj = (List<SObject>) Type.forName(strLstSobjType).newInstance();
			lstSobj.addAll(lstSObject);			
		}
		else{
			bIsReturn = true;
			//Common Error Handling
		}
	}

}