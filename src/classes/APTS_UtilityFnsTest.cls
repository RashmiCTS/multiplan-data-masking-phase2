/* APTS_UtilityFnsTest
* Test class for APTS_UtilityFns class
*
* History:
* 04/11/2017, Kruti Shah, APTTUS - created APTS_UtilityFnsTest class.
*/
@isTest
private class APTS_UtilityFnsTest {
    
    // test method for getProfileId.
	@isTest static void getProfileId_Test() {
		// Implement test code
		String profileName = CONSTANTS.PROFILE_SYSADMIN;
		Id profileId = [Select Id From Profile Where Name = :profileName Limit 1].Id;
		
		System.assertEquals(profileId, APTS_UtilityFns.getProfileId(profileName));
	}
	
    // test method for spellOutinWords_V2(long).	
	@isTest static void spellOutinWords_Long_Test() {
		// Implement test code
		long input = 987653210;
		String res = APTS_UtilityFns.spellOutinWords(input);
		//System.assertEquals('Nine hundred Eighty Seven Million, Six hundred Fifty Three Thousand, Two hundred Ten', res);
		
		input = 1087653210;
		res = APTS_UtilityFns.spellOutinWords(input);
		//System.assertEquals('One Billion, Eighty Seven Million, Six hundred Fifty Three Thousand, Two hundred Ten', res);
		
		input = -100;
		res = APTS_UtilityFns.spellOutinWords(input);
		//System.assertEquals('minus One hundred', res);
	}
    
    
}