/* APTS_TestDataFactory
* Test factory to used in unit tests to create test data.
*
* History:
* 01/03/2017, Kruti Shah, APTTUS - created APTS_TestDataFactory class.
*/
public with sharing class APTS_TestDataFactory {   
    
    public static SObject createSObject(SObject sObj) {
        // Check what type of object we are creating and add any defaults that are needed.
        String objectName = String.valueOf(sObj.getSObjectType());
        System.Debug('APTPS_TestDataFactory - objectName: ' + objectName);
        // Construct the default values class. Salesforce doesn't allow '__' in class names
        String defaultClassName = 'APTS_TestDataFactory.' + objectName.replace('Apttus__', '').replace('Apttus_Revenue2__', '').replace('Apttus_DocuApi__', '').replace('Apttus_Config2__', '').replace('__c', '') + 'Defaults';
        System.Debug('APTPS_TestDataFactory - defaultClassName: ' + defaultClassName);
        // If there is a class that exists for the default values, then use them
        if (Type.forName(defaultClassName) != null) {
            sObj = createSObject(sObj, defaultClassName);
        }
        return sObj;
    }

    public static SObject createSObject(SObject sObj, String defaultClassName) {
        // Create an instance of the defaults class so we can get the Map of field defaults
        Type t = Type.forName(defaultClassName);
        if (t == null) {
            Throw new APTS_TestFactoryException('Invalid defaults class.');
        }
        FieldDefaults defaults = (FieldDefaults)t.newInstance();
        addFieldDefaults(sObj, defaults.getFieldDefaults());
        return sObj;
    } 

    public static SObject[] createSObjectList(Sobject sObj, Integer numberOfObjects) {
        return createSObjectList(sObj, numberOfObjects, null);
    }

    public static SObject[] createSObjectList(Sobject sObj, Integer numberOfObjects, String defaultClassName) {
        SObject[] sObjs = new SObject[]{};
        SObject newObj;

        // Get one copy of the object
        if (defaultClassName == null) {
            newObj = createSObject(sObj);
        } else {
            newObj = createSObject(sObj, defaultClassName);
        }

        // Get the name field for the object
        String nameField = nameFieldMap.get(String.valueOf(sObj.getSObjectType()));
        if (nameField == null) {
            nameField = 'Name';
        }
        
        // Clone the object the number of times requested. Increment the name field so each record is unique
        for (Integer i = 0; i < numberOfObjects; i++) {
            SObject clonedSObj = newObj.clone(false, true);
            if(String.isNotBlank(nameField))
                clonedSObj.put(nameField, (String)clonedSObj.get(nameField) + ' ' + i);
            sObjs.add(clonedSObj);
        }
        return sObjs; 
    }
 

    private static void addFieldDefaults(SObject sObj, Map<String, Object> defaults) {
        // Loop through the map of fields and if they are null on the object, fill them.
        for (String field : defaults.keySet()) {
            if (sObj.get(field) == null) {
                sObj.put(field, defaults.get(field));
            }
        }
    }

    // When we create a list of SObjects, we need to keep name field unique.
    private static Map<String, String> nameFieldMap = new Map<String, String> {
        'Contact' => 'LastName',
        'Case' => 'Subject',
        'Task' => 'Subject',
        'User' => 'CommunityNickname',
        'Group' => 'Name',
        'Apttus__AgreementLineItem__c' => '',
        'APTPS_Currency__c' => '',        
        'Apttus_DocuApi__DocuSignDefaultRecipient2__c' => '',        
        'MapCountry__c' => 'Name'
    };

    public class APTS_TestFactoryException extends Exception {}

    // Use the FieldDefaults interface to set up values you want to default in for all objects.
    public interface FieldDefaults {
        Map<String, Object> getFieldDefaults();
    }

    // To specify defaults for objects, use the naming convention [ObjectName]Defaults.
    // For custom objects, omit the __c from the Object Name
    // For Apttus Packaged objects, omit the Apttus__ and __c from the Object Name
    
    public class UserDefaults implements FieldDefaults{
        public Map<String, Object> getFieldDefaults() {
            Decimal counter = Math.random();
            return new Map<String, Object> {
                'IsActive' => TRUE,
                'LastName' => 'Test User: '+counter,
                'Alias'  => 'ut'+(String.valueof(counter).length() > 6 ? String.valueOf(counter).substring(0,3) : String.valueOf(counter)),
                'Email'  => 'noreply@email.com',
                'Username'  => 'unittester'+counter+'@testerpro.com',
                'CommunityNickname' => 'unittester'+counter,
                'TimeZoneSidKey' => 'America/Los_Angeles',
                'LocaleSidKey' => 'en_US',
                'EmailEncodingKey' => 'UTF-8',
                'LanguageLocaleKey' => 'en_US',
                'CompanyName' => 'Nothing', // to skip Company and Division/office fields are required fields validation
                'Division' => 'Finance'// to skip Company and Division/office fields are required fields validation               
            };
        }
    }

    public class AccountDefaults implements FieldDefaults{
        public Map<String, Object> getFieldDefaults() {
            return new Map<String, Object> {
                'Name' => 'Test Account' + Math.random(), 
                'Phone' => '(212)548-0600',
                'BillingStreet' => '224 W 57th St',
                'BillingCity' => 'San Francisco',
                'BillingState' => 'California',
                'BillingPostalCode' => '10019',
                'BillingCountry' => 'United States'
            };
        }
    }

    public class ContactDefaults implements FieldDefaults{
        public Map<String, Object> getFieldDefaults() {
            return new Map<String, Object> {
                'FirstName' => 'First',
                'LastName' => 'Last'
            };
        }
    }  

    public class APTS_AgreementDefaults implements FieldDefaults{
        public Map<String, Object> getFieldDefaults() {
            return new Map<String, Object> {
                'Name' => 'Test Agreement'+Math.random(),
                'Apttus__Contract_Start_Date__c' => Date.today(),
                'Apttus__Contract_End_Date__c' => Date.today().addMonths(12),
                'Apttus__Term_Months__c' => 12
            };
        }
    }

     public class AttachmentDefaults implements FieldDefaults{
        public Map<String, Object> getFieldDefaults() {
            return new Map<String, Object> {
                'Name' => 'Test attachment'+math.random(),
                'Body' =>  Blob.valueOf('Unit Test Attachment Body')
            };
        }
    }
}