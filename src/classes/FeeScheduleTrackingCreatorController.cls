/*
File Name:   FeeScheduleTrackingCreatorController.cls
Author:      Micahel Olson
Date:        9/28/2015
Description: 

Modification History
12/11/2015			Michael Olson			Removed Market Name
12/18/2015			michael Olson			Updates to allow legacy FST to be created without error
2/10/2016			Michael Olson			Update email message
2/16/2016			Michael Olson			Stopped sending email, email now being sent form trigger. 
*/

public with sharing class FeeScheduleTrackingCreatorController {

    /* ===================================================== 
     Variables
    ===================================================== */   
	public date dSADateStart {get;set;}
	public string dSADateStartStr {get;set;}
	public date dSADateEnd {get;set;}
	public string dSADateEndStr {get;set;}
	public String dateTypeStr {get;set;}  //should these be getset
	public String statusStr {get;set;} //should these be getset

    /* =======================================================
     Constructor
    ========================================================= */
    public FeeScheduleTrackingCreatorController() {

    	dSADateStartStr = system.today().toStartOfMonth().addMonths(1).format();
    	dSADateEndStr = system.today().toStartOfMonth().addMonths(2).addDays(-1).format();   //dSADateEndStr = '12/1/2019';
    	dateTypeStr = 'Data_Source_Anniversary_Date__c';
    	statusStr = 'Closed - Complete';
    	
    	//call to load cases
		casesWithoutBatches();
    }


    /* ================================================    
     Wrapper Class to FST creation
    =================================================== */  
    public class wrapperFST {
        public Fee_Schedule_Tracking__c thisFST {get; set;}
        public Fee_Schedule_Tracking__c thatFST {get; set;}
        public boolean selected {get; set;}

      	
        public wrapperFST(Fee_Schedule_Tracking__c s, id i) { 
            thisFST = s;
            thatFST = new Fee_Schedule_Tracking__c(ownerid = i);  
            selected = false;
        }

    }
    

    /* ================================================    
     Return's a list of records to display on page
    =================================================== */    
    public Fee_Schedule_Tracking__c[] queriedFST = new Fee_Schedule_Tracking__c[]{};   

    public Fee_Schedule_Tracking__c[] cases() {
    	
    	//set query parameters
    	dSADateStart = date.parse(dSADateStartStr);
    	dSADateEnd = date.parse(dSADateEndStr);       	 	
		
		//Dynamic Query of FST's
		queriedFST = Database.query('select id, Name, ownerid, Status__c, Anesthesia_Conversion_Factor__c, Anesthesia_Conversion_Factor_Value__c, '+ 
										'Assignment_Date__c, Automatic_Escalator_of_Years_to_Update__c, Automatic_Escalator_Anniversary_Date__c, '+
										'Automatic_Escalator_Evergreen__c, Automatic_Escalator_Percentage_Increased__c, Pended_Date__c, '+
										'Automatic_Escalator_Update__c, Auto_Medical_Rate_of_the_State_FS__c, Auto_Medical_Rate_Lesser_Of__c, Contract_DTS__c, '+ 
										'Contract_Notes__c, Contract_Rate_Type__c, Data_Source_Anniversary_Date__c, Data_Source_Update__c, DTS_Tracking_ID__c, '+ 
										'Existing_FS_ID__c, Fee_Schedule_Creation_Update__c, FS_Implementation_Date__c, '+ 
										'HCPCS_Drug_ASP__c, HCPCS_Drug_ASP_Value__c, HCPCS_Drug_AWP__c, HCPCS_Drug_AWP_Value__c, '+ 
										'HCPCS_Level_I_and_II_CLAB__c, HCPCS_Level_I_and_II_CLAB_Value__c, '+ 
										'HCPCS_Level_I_E_M__c, HCPCS_Level_I_E_M_Value__c, HCPCS_Level_II_AMB__c, HCPCS_Level_II_AMB_Value__c, HCPCS_Level_II_DME_POS__c, '+ 
										'HCPCS_Level_II_DME_POS_Value__c, HCPCS_Level_II_Not_Specified__c, HCPCS_Level_II_Not_Specified_Value__c, HCPCS_Level_II_PEN__c, '+ 
										'HCPCS_Level_II_PEN_Value__c, HCPCS_Level_II_RBRVS__c, HCPCS_Level_II_RBRVS_Value__c, HCPCS_Level_I_Medicine__c, '+ 
										'HCPCS_Level_I_Medicine_Value__c, HCPCS_Level_I_Pathology__c, HCPCS_Level_I_Pathology_Value__c, HCPCS_Level_I_Radiology__c, '+ 
										'HCPCS_Level_I_Radiology_Value__c, HCPCS_Level_I_Surgery__c, HCPCS_Level_I_Surgery_Value__c, Medicare_Locality__c, '+
										'Market_Id__c, MP3_CE_ID_Number__c, Not_Otherwise_Specified_Default__c, Not_Otherwise_Specified_Default_Value__c, '+ 
										'Optum_Gap_Codes_Removed__c, Originating_ND_HCE_Request__c, POS_Differential_Removed__c, Product_Selection__c, Provider_Name__c, '+ 
										'Quarter__c, Rate_Data_Source_Year__c, Rate_Linking_Date__c, Region_Name__c, Reimbursement_Effective_Date__c, Reqlog_Batch_MPI_File_ID__c, '+ 
										'Requestor__c, State__c, Workers_Comp_Rate_of_the_State_FS__c, Workers_Comp_Rate_Lesser_Of__c, Zip_Code__c, FST_Group__c, '+
										'FST_Cloned_From__c, FST_Cloned_To__c, FST_Cloned_To__r.createddate '+  
                    				'from Fee_Schedule_Tracking__c ' +
                    				'where ' + dateTypeStr + ' >= :dSADateStart AND '+
                    					dateTypeStr + ' <= :dSADateEnd AND ' +
                    					'Status__c = :statusStr ' +
                    				'order by ownerid nulls first, createdDate asc '+
                    				'limit 1000'); 
        
        return queriedFST;
    }



    /* ================================================    
     Return list of records and assigns to wrapper class
    =================================================== */     
	public list<wrapperFST> wrapperListFST {get; set;}    
    
    public list<wrapperFST> casesWithoutBatches() {      		
       		
        wrapperListFST = new list<wrapperFST>();
            
        for(Fee_Schedule_Tracking__c s : cases() ) {
        	wrapperListFST.add(new wrapperFST(s, s.ownerid)); 
        }      

		return wrapperListFST;
    }


	/* ================================================
     Create picklist for Report date
    =================================================== */ 
	  	public string getPastFST() {
	    	return dateTypeStr;
	  	}
	  	
	  	public void setPastFST(String dateTypeStr) {
	    	this.dateTypeStr = dateTypeStr;
	  	}
  
	  	public List<SelectOption> getItems() {
	    	
	    	list<SelectOption> op = new List<SelectOption>();
	    		op.add(new SelectOption('Data_Source_Anniversary_Date__c', 'Data Source Anniversary Date'));
	    		op.add(new SelectOption('Automatic_Escalator_Anniversary_Date__c', 'Automatic Escalator Anniversary Date'));
	    	return op;
	  	}


	/* ================================================
     Create picklist for Status
    =================================================== */ 
	  	public string getStatus() {
	    	return statusStr;
	  	}
	  	
	  	public void setStatus(String statusStr) {
	    	this.statusStr = statusStr;
	  	}
  
	  	public list<SelectOption> getItems2() {
	    	
	    	list<SelectOption> options = new List<SelectOption>();
	    	    Schema.DescribeFieldResult fieldResult = Fee_Schedule_Tracking__c.Status__c.getDescribe();
        		list<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
	    	
				for( Schema.PicklistEntry f : ple) {
            		options.add(new SelectOption(f.getLabel(), f.getValue()));
				}

	    	return options;
	  	}


	/* ================================================
     Create picklist for Date Range
	=================================================== */ 
	  	public string getSD() {
	    	return dSADateStartStr;
	  	}
	  	
	  	public void setSD(String dSADateStartStr) {
	    	this.dSADateStartStr = dSADateStartStr;
	  	}
	  	
	  	public string getED() {
	    	return dSADateEndStr;
	  	}
	  	
	  	public void setED(String dSADateEndStr) {
	    	this.dSADateEndStr = dSADateEndStr;
	  	}  

    
	/* ================================================
     Re-Queries the data after filtering
    =================================================== */ 
    public pagereference reQuery() {
		casesWithoutBatches(); 
        return null;
    }

    /* ================================================
     Null Method for Action Functions
    =================================================== */      
    public void doNothing1() {
	}
	  
    /* ================================================
     ASSIGN SELECTED SETTLEMENTS BUTTON
    =================================================== */  
     public pageReference cloneSelectedFSTs() { 
                       
            list<Fee_Schedule_Tracking__c> selectedFST = new list<Fee_Schedule_Tracking__c>();
            list<Fee_Schedule_Tracking_Group__c> FSTGroupInsert = new list<Fee_Schedule_Tracking_Group__c>();
            set<id> orginalFSTs = new set<id>();
            
            for (wrapperFST sw : wrapperListFST) {
            	
            	Fee_Schedule_Tracking__c newFstInserts = new Fee_Schedule_Tracking__c();
            	Fee_Schedule_Tracking_Group__c newFstGroupInserts = new Fee_Schedule_Tracking_Group__c();
            	
            	//Creates new FST's when record marked for creation
                if (sw.selected == true && sw.thisFST.FST_Cloned_To__c == null) {
                	
                	newFstInserts = sw.thisFST.Clone();
                	
					/* Fields to change at clone */
					newFstInserts.OwnerId = sw.thatFST.ownerid;
					newFstInserts.id = null;
					newFstInserts.Status__c = 'New';
					newFstInserts.Assignment_Date__c = null;
					newFstInserts.FS_Implementation_Date__c = null;
					newFstInserts.Reimbursement_Effective_Date__c = null;
					newFstInserts.Rate_Data_Source_Year__c = '';
					newFstInserts.Quarter__c = '';
					newFstInserts.Data_Source_Anniversary_Date__c = null;
					newFstInserts.Automatic_Escalator_Anniversary_Date__c = null;
					newFstInserts.Pended_Date__c = null;
					newFstInserts.FST_Cloned_From__c = sw.thisFST.id;
					
					//Set Fee Schedule Creation/Update 
					if(sw.thisFST.Automatic_Escalator_Update__c == 'Yes' ) {
						newFstInserts.Fee_Schedule_Creation_Update__c = 'Contractual Rate Escalator';
					}else if(sw.thisFST.Data_Source_Update__c == 'Yes' ) {
						newFstInserts.Fee_Schedule_Creation_Update__c = 'Contractual Data Source Update';				
					}else{
						newFstInserts.Fee_Schedule_Creation_Update__c = '';
					}
					
					//newFstInserts.HCPCS_Level_II_Not_Specified__c = null;
               		
                	selectedFST.add(newFstInserts);
					selectedFST.add(sw.thisFST);
					orginalFSTs.add(sw.thisFST.id);
                 
 					//create a Fee Scheudule Tracking Group if needed
					if(sw.thisFST.FST_Group__c == null) {
						newFstGroupInserts = new Fee_Schedule_Tracking_Group__c();						
						newFstGroupInserts.Name = sw.thisFST.id + sw.thisFST.Provider_Name__c;
						
						FSTGroupInsert.add(newFstGroupInserts);
					}          
                }            
            }
            
            //Throw error if no records were selected.
            if(selectedFST.size() == 0) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'No records have been selected for creation.'));
                return null;
            }       

				
				//---------------------------------------------------------------------------------------                       
        		// Final Updates
        		//---------------------------------------------------------------------------------------   
				Savepoint sp = Database.setSavepoint();
	    		
	    		try {                    
					
					//------------------------------------------------
					//Insert FST Groups if needed and assign to FST's
					//------------------------------------------------
					
					if(FSTGroupInsert.size() > 0) {
				
						insert FSTGroupInsert;
						
						//create map to tie FST groups to old an dnew records
						map<string,id> groupIdMap = new map<string,id>();	
							for(Fee_Schedule_Tracking_Group__c fstg : FSTGroupInsert){
								groupIdMap.put( (fstg.name).substring(0,18), fstg.id);
							}
												
						for(Fee_Schedule_Tracking__c fst : selectedFST) {
									
							if(fst.FST_Group__c == null) {
										
								id tempGid;
								if(fst.id != null) {
									tempGid = groupIdMap.get(fst.id);
									fst.FST_Group__c = tempGid;
								}else{										
									tempGid = groupIdMap.get(fst.FST_Cloned_From__c);
									fst.FST_Group__c = tempGid;									
								}
							}
						}
						
						//Rename the FST Group		
						for(Fee_Schedule_Tracking_Group__c fstg : FSTGroupInsert) {
							fstg.name = (fstg.name).substring(18, fstg.name.length());
						}
						
						//renaming update
						update FSTGroupInsert; 						
					}
					
					
					//-----------------------------------
					//update or insert new FST's
					//-----------------------------------
					
					if(selectedFST.size() > 0) {
	        			
	        			upsert selectedFST;
	        			
	        			//get FST.Name for use in Mass email template
	        			map<id,string> nameMap = new map<id,string>();
	        			set<id> insId = new set<id>();
	        				for(fee_schedule_tracking__c f : selectedFST) {
	        					insId.add(f.id);
	        				}
	        				list<fee_schedule_Tracking__c> fstNameSetup = new list<fee_schedule_Tracking__c>();
	        					fstNameSetup = [select id, name from fee_schedule_tracking__c where id IN : insId];
	        					for(fee_schedule_Tracking__c f : fstNameSetup) {
	        						nameMap.put(f.id, f.name);
	        					}
	        			
	        			
	        			//Asssign the original FST's 'Clone To' value
	        			map<id, id> clonedToMap = new map<id, id>();
	        			map<id, list<Fee_Schedule_Tracking__c>> uidToInsertedFST = new map<id, list<Fee_Schedule_Tracking__c>>();
	        			
	        			for(Fee_Schedule_Tracking__c fstUp : selectedFST) {
	        				
	        				if(!orginalFSTs.contains(fstUp.id)) {
	        					
	        					clonedToMap.put(fstUp.FST_Cloned_From__c, fstUp.id);	        					
	        					
	        					//prepare mass email list
	        					if(uidToInsertedFST.keyset().contains(fstUp.ownerid)) {
	        						uidToInsertedFST.get(fstUp.ownerid).add(fstUP);
	        					}else {
	        						list<Fee_Schedule_Tracking__c> tempFstInsList = new list<Fee_Schedule_Tracking__c>();
	        							tempFstInsList.add(fstup);
	        							
	        						uidToInsertedFST.put(fstUp.ownerid, tempFstInsList);
	        					}	        					        						        					
	        				}	        				
	        			}


						//Create and Send emails
				        if(uidToInsertedFST.size() > 0) {
				        				
				        	list<Messaging.SingleEmailMessage> mailsToMassSend = new list<Messaging.SingleEmailMessage>(); 
				        	map<id, string> uidToEmail = new map<id, string>();
				        	list<User> U = new list<User>();
				        	string cUserEmail;				        					
				        		
				        		//get users
				        		U = [select id, email, name from User where id IN : uidToInsertedFST.keySet() OR id =: UserInfo.getUserId() ];
				        		
				        		//setup email addresses for use later		
				        		for(User Usr : U) {
				        			uidToEmail.put(Usr.id, Usr.email);
				        			
				        			//setup current user as the email's CC
				        			if(Usr.id == UserInfo.getUserId()) {
				        				cUserEmail = Usr.email;
				        			}
				        		}	
				        		
				        		//Loop though map to generate the individual emails and set variables				
				        		for(id i : uidToInsertedFST.keySet()) {
		
				        			Messaging.SingleEmailMessage mail1 = new Messaging.SingleEmailMessage();
				        					
				        			string Subject = 'You were mass assigned \'Fee Schedule Tracking\' records.  Please see below for details. (source: FeeScheduleTrackingCreatorController)';				        					
				        			
				        			string tempBody = '';
				        			
				        			for(Fee_Schedule_tracking__c fst : uidToInsertedFST.get(i)) {
				        				tempBody = tempBody + 'FST #: ' + nameMap.get(fst.id) + ' for ' + fst.Provider_Name__c + '<br/>' +
				        										'Link to Record: ' + System.URL.getSalesforceBaseUrl().toExternalForm() + '/' + fst.id + '<br/><br>';
				        			}		        																
						 						
						 			list<string> toAddress = new list<string>();
						 				toAddress.add(uidToEmail.get(i));
						 			list<string> ccAddress = new list<string>();
						 				ccAddress.add(cUserEmail);	
									                
									mail1.setToAddresses(toAddress);
									mail1.setccAddresses(ccAddress);
									mail1.setSubject(Subject);
									mail1.setHtmlBody(tempBody); 
									mail1.saveAsActivity = false;
													
									mailsToMassSend.add(mail1);										        									        				
				        		}
				        				
				        		//Send Emails
				        		//2.16.16 no longer sending this email; code can probably be removed in the future.
								//Messaging.sendEmail( mailsToMassSend);			
				        }	
				        			

	        				        			
	        			//Final assignment of 'Cloned To' after Insert
	        			list<Fee_Schedule_Tracking__c> orginalFSTCloneTo = new list<Fee_Schedule_Tracking__c>();
	        			
	        			for(Fee_Schedule_Tracking__c fstUp : selectedFST) {
	        				
	        				if(orginalFSTs.contains(fstUp.id)) {
	        					fstUp.FST_Cloned_To__c = clonedToMap.get(fstUp.id);
								orginalFSTCloneTo.add(fstUp);
	        				}	
	        			}
						
						if(orginalFSTCloneTo.size() > 0) {
							update orginalFSTCloneTo;
						}     				        			
					}
	           	
	           	}catch(exception Ex) { ApexPages.addMessages(ex); if(Ex != null) { Database.rollback(sp); }  }                                 
            
            //return null for refreshed view of created.
            return null;
    }                          
}