public class AttachDocsFromTemplateController {
/*
File Name:   AttachDocsFromTemplateController
Author:      Rashmi T
Date:        11/7/2016
Description: This apex class takes care of bringing the pdf attachments from the Templates__c , so that users can choose the 
			 docs that need to be attached on an agreement. This is due to the limitation in the Apttus 'generate supporting documents'
				feature which does not list out the pdf docs from the template and lists only the word docs. 
Usage: AttachDocsFromTemplate vf page that can be triggered via custom button 'Add Supporting Docs' on the Agreement record. 

Modification History
11/07/2016      Rashmi T      Created 
*/    
/**** PRIVATE VARIABLES ****/   
	private Id idAgree;
    private Integer intPageSize = 5;
    private List<TemplateWrapper> lstTempWrap = new List<TemplateWrapper>();
    private List<Apttus__APTS_Template__c> lstTemplates = new List<Apttus__APTS_Template__c>();
    private Map<Id,List<Attachment>> mapTempIdVSAttach = UtilityForAttachments.getAllAttachmentsByParentId(CONSTANTS.APTTUS_APTS_TEMPLATE,
                                                                                                           new Set<String>{CONSTANTS.APPLICATION_PDF,CONSTANTS.DOT_PDF});
/**** END PRIVATE VARIABLES ****/   
/**** PUBLIC VARIABLES ****/   
/**** END PUBLIC VARIABLES ****/   
/**** CONSTRUCTORS ****/
    
    public AttachDocsFromTemplateController(ApexPages.StandardController stdCon){
        idAgree = stdCon.getId();
    }
/**** END CONSTRUCTORS ****/
/**** PROPERTIES ****/ 
    public List<TemplateWrapper> getLstTempWrap(){ 
        TemplateWrapper tw = new TemplateWrapper();
        lstTempWrap = new List<TemplateWrapper>();
        if(stdSetCon <> null){
            for(Apttus__APTS_Template__c temp : (List<Apttus__APTS_Template__c>)stdSetCon.getRecords()){
                if(mapTempIdVSAttach.containsKey(temp.Id)){
                    for(Attachment att : mapTempIdVSAttach.get(temp.Id)){
                        tw = new TemplateWrapper();
                        tw.templ = temp;
                        tw.attach = att;
                        lstTempWrap.add(tw);
                    }
                }
                
            }
        }
        return lstTempWrap;
    }
    public void setLstTempWrap(List<TemplateWrapper> argLstTempWrap){
        lstTempWrap = argLstTempWrap;
    }
    
    public ApexPages.StandardSetController stdSetCon{   
        get{
            Set<Id> setTempIds = new Set<Id>();
            String strLIKE = '';
            if(stdSetCon == null){
                if(!mapTempIdVSAttach.isEmpty()){
                    if(String.isBlank(strSearch)){
                       strLIKE = '%%'; 
                    }
                    else{
                       strLIKE = '%'+strSearch+'%';
                    }
                   stdSetCon = new ApexPages.StandardSetController([SELECT id,Name,Apttus__Category__c,Apttus__Subcategory__c 
                                                 FROM Apttus__APTS_Template__c WHERE id IN:mapTempIdVSAttach.keySet() AND Name LIKE: strLIKE ORDER BY Name]);
                    
                   stdSetCon.setPageSize(intPageSize);
                }
            }
            return stdSetCon;
        }
        set{stdSetCon = value;}
    }
    
    public String strSearch{
        get{
            return strSearch;
        }
        set{
            strSearch = value;
        }
    }
    /*******PAGINATION PROPERTIES******/
    
    public Boolean hasfirstLast{
        get{
            if(stdSetCon.getResultSize()> intPageSize)
                return true;
            else
                return false;
        }
        set;
    }
    
    public boolean hasNext{
    get{    	
        return stdSetCon.getHasNext();
        } set;
    }
    
    public boolean hasPrevious{
        get{
            return stdSetCon.getHasPrevious();
        } set;
    }
    
    public Integer pageNum{get{return stdSetCon.getpageNumber();} set;}
    
    public void firstPage(){
       try{
           stdSetCon.first();
       }
        catch(Exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,e.getMessage()));
        }
    }
        
    public void lastPage(){
        try{
            stdSetCon.last();
        }
        catch(Exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,e.getMessage()));
        }
    }
    
    public void prevPage(){
        try{
        	stdSetCon.previous();
		}
        catch(Exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,e.getMessage()));
        }
    }
    
    public void nextPage(){
        try{
        	stdSetCon.next();
        }
        catch(Exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,e.getMessage()));
        }
    }
    
    
    //End Pagination Properties
/**** END PROPERTIES ****/  
/**** FUNCTIONS ****/
    public void getFilteredTemplates(){    
        stdSetCon = null;
        //call Template getter
        getLstTempWrap();
    }
    public PageReference uploadAttachToAgreements(){   
        PageReference pgRef;
        Set<Id> setTempId = new Set<Id>();
        List<Attachment> lstAttachInsert = new List<Attachment>();
        Attachment attch = new Attachment();
        if(!lstTempWrap.isEmpty() && idAgree <> NULL){
            for(TemplateWrapper tw : lstTempWrap){
                if(tw.bIsSelected){
                    attch = new Attachment();
                    attch.Name = tw.attach.Name;
                    attch.Body = tw.attach.Body;
                    attch.ParentId = idAgree;
                    lstAttachInsert.add(attch);
                }
            }
            if(!lstAttachInsert.isEmpty()){
                try{
                    insert lstAttachInsert;
                    pgRef = new PageReference('/'+idAgree);
                }
                catch(Exception ex){
                    ApexPages.Message apxMess = new ApexPages.Message(ApexPages.Severity.ERROR,ex.getMessage());
                    ApexPages.addMessage(apxMess);
                }
            }
        }
        return pgRef;
    }
/**** END FUNCTIONS ****/
/**** WRAPPER CLASS ****/
    public class TemplateWrapper{
        public Apttus__APTS_Template__c templ {get;set;}
        public Attachment attach {get;set;}
        public Boolean bIsSelected {get;set;}
        public TemplateWrapper(){
            bIsSelected = false;
            templ = new Apttus__APTS_Template__c();	
        }
    }
/**** END WRAPPER CLASS ****/
}