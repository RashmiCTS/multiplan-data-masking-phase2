/* APTS_RefreshContractRatesCtrl_Test
 * Test Class for APTS_RefreshContractRatesCtrl.
 *
 * Developer: Kruti Shah, APTTUS
 * Business Owner: 
 *
 * Scenario:
 * 
 * History: 
 * 01/09/2017, Kruti Shah,  APTTUS - created APTS_RefreshContractRatesCtrl_Test.
 */
@isTest
public class APTS_RefreshContractRatesCtrl_Test {
    
    public static Account testAcc;
    public static Contact testCon;
    public static List<APTS_Contract_Rates_Field_Mapping__c> listOfCustomSettings;
    public static List<NS_NIS_Request__c> listOfNISRequests;
    @testsetup public static void createData(){
        
        testAcc = new Account();
        testAcc.name = 'testA';
        insert testAcc;
        
        testCon = new Contact();
        testCon.AccountId = testAcc.Id;
        testCon.lastName = 'agreementInsert_Test';
        testCon.email = 'agreementInsert_Test@Test.com';
        insert testCon;
        
        Id group5RTId = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('Group V5').getRecordTypeId();
        
        Apttus__APTS_Agreement__c testAgr = new Apttus__APTS_Agreement__c();
        testAgr.Apttus__Account__c = testAcc.id;
        testAgr.recordtypeid = group5RTId;
        testAgr.Apttus__Primary_Contact__c = testCon.id;
        testAgr.Market_State__c = 'AK';
        insert testAgr; 
        
        Zip_Code__c objZIPCode = new Zip_Code__c();
        objZIPCode.Ambulance_Locality__c = 'ambulanceLocality';
        objZIPCode.CLAB_Locality__c = 'CLABLocality';
        insert objZIPCode;        
        
        Id requestRecordTypeId = Schema.SObjectType.NS_NIS_Request__c.getRecordTypeInfosByName().get('Reimbursement and Implementation - HCFA').getRecordTypeId(); 
        //[Select Name, Id From RecordType where sObjectType='NS_NIS_Request__c' and Name='Reimbursement and Implementation - HCFA'];
        
        listOfCustomSettings = new List<APTS_Contract_Rates_Field_Mapping__c>();
        listOfNISRequests = new List<NS_NIS_Request__c>();

        NS_NIS_Request__c testNISRequest = new NS_NIS_Request__c();
        testNISRequest.RecordTypeId = requestRecordTypeId;
        testNISRequest.Account__c = testAcc.Id;
        testNISRequest.User__c = UserInfo.getUserId();
        testNISRequest.Agreement__c = testAgr.Id;
        testNISRequest.Zip_Code__c = objZIPCode.Id;        
        testNISRequest.Name = 'test ND-HCE record';
        testNISRequest.Reimbursement_Type__c = 'Unique';
        testNISRequest.Anesthesia_Conversion_Factor_Value__c = 15;
        testNISRequest.Anesthesia_Conversion_Factor__c = '% of Billed Charges';
        testNISRequest.HCPCS_Level_I_Surgery__c = '% of Billed Charges';
        testNISRequest.HCPCS_Level_I_Radiology__c = '% of Billed Charges';
        testNISRequest.HCPCS_Level_I_Pathology__c = '% of Billed Charges';
        testNISRequest.HCPCS_Level_I_Medicine__c = '% of Billed Charges';
        testNISRequest.HCPCS_Level_I_E_M__c = '% of Billed Charges';
        testNISRequest.HCPCS_Level_II_RBRVS__c = '% of Billed Charges';
        testNISRequest.HCPCS_Level_I_and_II_CLAB__c = '% of Billed Charges';
        testNISRequest.HCPCS_Level_II_DME_POS__c = '% of Billed Charges';
        testNISRequest.HCPCS_Level_II_PEN__c = '% of Billed Charges';
        testNISRequest.HCPCS_Level_II_AMB__c = '% of Billed Charges';
        testNISRequest.HCPCS_Drug_ASP__c = '% of Billed Charges';
        testNISRequest.HCPCS_Drug_AWP__c = '% of Billed Charges';
        testNISRequest.HCPCS_Level_II_Not_Specified__c = '% of Billed Charges (BP)';
        testNISRequest.Not_Otherwise_Specified_Default__c = '% of Billed Charges (BP)';
        testNISRequest.HCPCS_Level_I_Surgery_Value__c = 1001;
        testNISRequest.HCPCS_Drug_ASP_Value__c = 101;
        testNISRequest.HCPCS_Level_II_PEN_Value__c = 105;
        testNISRequest.HCPCS_Level_I_Radiology_Value__c = 25;
        testNISRequest.HCPCS_Level_I_Pathology_Value__c = 35;
        testNISRequest.HCPCS_Level_I_Medicine_Value__c = 45;
        testNISRequest.HCPCS_Level_I_E_M_Value__c = 55;
        testNISRequest.HCPCS_Level_II_RBRVS_Value__c = 65;
        testNISRequest.HCPCS_Level_I_and_II_CLAB_Value__c = 75;
        testNISRequest.HCPCS_Level_II_DME_POS_Value__c = 85;        
        testNISRequest.HCPCS_Level_II_AMB_Value__c = 15;        
        testNISRequest.HCPCS_Drug_AWP_Value__c = 105;
        testNISRequest.HCPCS_Level_II_Not_Specified_Value__c = 55;
        testNISRequest.Not_Otherwise_Specified_Default_Value__c = 5;                
        listOfNISRequests.add(testNISRequest);

        NS_NIS_Request__c testNISRequest2 = new NS_NIS_Request__c();
        testNISRequest2.RecordTypeId = requestRecordTypeId;
        testNISRequest2.Account__c = testAcc.Id;
        testNISRequest2.User__c = UserInfo.getUserId();
        testNISRequest2.Agreement__c = testAgr.Id;
        testNISRequest2.Zip_Code__c = objZIPCode.Id;        
        testNISRequest2.Name = 'test ND-HCE record-Band';        
        listOfNISRequests.add(testNISRequest2);

        APTS_Contract_Rates_Field_Mapping__c objCustomSettingRec = new APTS_Contract_Rates_Field_Mapping__c(Name='ANES_CF__c');
        objCustomSettingRec.Action_Type__c = 'Refresh Contract Rates';
        objCustomSettingRec.Destination_Field_API_Name__c = 'ANES_CF__c';
        objCustomSettingRec.Destination_Object__c = 'Apttus__APTS_Agreement__c';
        objCustomSettingRec.Source_Field_API_Name__c = 'Anesthesia_Conversion_Factor_Value__c';
        objCustomSettingRec.Source_Object__c = 'NS_NIS_Request__c';
        objCustomSettingRec.APTS_Source_Field_String_Value_API_Name__c = 'Anesthesia_Conversion_Factor__c';
        listOfCustomSettings.add(objCustomSettingRec);        

        APTS_Contract_Rates_Field_Mapping__c objCustomSetting2 = new APTS_Contract_Rates_Field_Mapping__c(Name='ND - HCE Request Name');
        objCustomSetting2.Action_Type__c = 'Refresh Contract Rates';
        objCustomSetting2.Destination_Field_API_Name__c = 'APTS_ND_HCE_Request_Name__c';
        objCustomSetting2.Destination_Object__c = 'Apttus__APTS_Agreement__c';
        objCustomSetting2.Source_Field_API_Name__c = 'Name';
        objCustomSetting2.Source_Object__c = 'NS_NIS_Request__c';
        objCustomSetting2.APTS_Source_Field_String_Value_API_Name__c = '';
        listOfCustomSettings.add(objCustomSetting2);

        APTS_Contract_Rates_Field_Mapping__c objCustomSettingRec3 = new APTS_Contract_Rates_Field_Mapping__c(Name='ASP');
        objCustomSettingRec3.Action_Type__c = 'Refresh Contract Rates';
        objCustomSettingRec3.Destination_Field_API_Name__c = 'ASP__c';
        objCustomSettingRec3.Destination_Object__c = 'Apttus__APTS_Agreement__c';
        objCustomSettingRec3.Source_Field_API_Name__c = 'HCPCS_Drug_ASP_Value__c';
        objCustomSettingRec3.Source_Object__c = 'NS_NIS_Request__c';
        objCustomSettingRec3.APTS_Source_Field_String_Value_API_Name__c = 'HCPCS_Drug_ASP__c';
        listOfCustomSettings.add(objCustomSettingRec3);

        APTS_Contract_Rates_Field_Mapping__c objCustomSettingRec4 = new APTS_Contract_Rates_Field_Mapping__c(Name='PEN');
        objCustomSettingRec4.Action_Type__c = 'Refresh Contract Rates';
        objCustomSettingRec4.Destination_Field_API_Name__c = 'PEN__c';
        objCustomSettingRec4.Destination_Object__c = 'Apttus__APTS_Agreement__c';
        objCustomSettingRec4.Source_Field_API_Name__c = 'HCPCS_Level_II_PEN_Value__c';
        objCustomSettingRec4.Source_Object__c = 'NS_NIS_Request__c';
        objCustomSettingRec4.APTS_Source_Field_String_Value_API_Name__c = 'HCPCS_Level_II_PEN__c';
        listOfCustomSettings.add(objCustomSettingRec4);

        APTS_Contract_Rates_Field_Mapping__c objCustomSettingRec5 = new APTS_Contract_Rates_Field_Mapping__c(Name='RBRVS_Surgery');
        objCustomSettingRec5.Action_Type__c = 'Refresh Contract Rates';
        objCustomSettingRec5.Destination_Field_API_Name__c = 'RBRVS_Surgery__c';
        objCustomSettingRec5.Destination_Object__c = 'Apttus__APTS_Agreement__c';
        objCustomSettingRec5.Source_Field_API_Name__c = 'HCPCS_Level_I_Surgery_Value__c';
        objCustomSettingRec5.Source_Object__c = 'NS_NIS_Request__c';
        objCustomSettingRec5.APTS_Source_Field_String_Value_API_Name__c = 'HCPCS_Level_I_Surgery__c';
        listOfCustomSettings.add(objCustomSettingRec5);

        insert listOfNISRequests;
        insert listOfCustomSettings;
    }
    
    public static testmethod void testmethodWithValidValues(){
        PageReference pageRef = Page.APTS_RefreshContractRates;
        Test.setCurrentPage(pageRef);
        
        Apttus__APTS_Agreement__c testAgr2 = [select ID,name,ANES_CF__c from Apttus__APTS_Agreement__c WHERE Market_State__c='AK'];
        NS_NIS_Request__c objNISRequest = [select ID,Name,Anesthesia_Conversion_Factor_Value__c from NS_NIS_Request__c LIMIT 1];
        
        ApexPages.currentPage().getParameters().put('Id', objNISRequest.ID);
        ApexPages.currentPage().getParameters().put('AgreementId', testAgr2.ID);
        
        Test.startTest();
        APTS_RefreshContractRatesCtrl objController = new APTS_RefreshContractRatesCtrl();
        objController.doInit();
        Test.stopTest();     
    }
    
    public static testmethod void testmethodWithoutRequestID(){
        PageReference pageRef = Page.APTS_RefreshContractRates;
        Test.setCurrentPage(pageRef);
        
        Apttus__APTS_Agreement__c testAgr2 = [select ID,name from Apttus__APTS_Agreement__c WHERE Market_State__c='AK'];
        NS_NIS_Request__c objNISRequest = [select ID,Name from NS_NIS_Request__c LIMIT 1];

        ApexPages.currentPage().getParameters().put('Id', null);
        ApexPages.currentPage().getParameters().put('AgreementId', testAgr2.ID);        
        
        Test.startTest();
        APTS_RefreshContractRatesCtrl objController = new APTS_RefreshContractRatesCtrl();
        objController.doInit();
        Test.stopTest();     
    }

    public static testmethod void testmethodWithNullValuesInSource(){
        PageReference pageRef = Page.APTS_RefreshContractRates;
        Test.setCurrentPage(pageRef);

        Apttus__APTS_Agreement__c testAgr2 = [select ID,name from Apttus__APTS_Agreement__c WHERE Market_State__c='AK'];
        NS_NIS_Request__c objNISRequest = [select ID,Name from NS_NIS_Request__c WHERE Name = 'test ND-HCE record-Band'];

        ApexPages.currentPage().getParameters().put('Id', objNISRequest.Id);
        ApexPages.currentPage().getParameters().put('AgreementId', testAgr2.ID);        
        
        Test.startTest();
        APTS_RefreshContractRatesCtrl objController = new APTS_RefreshContractRatesCtrl();
        objController.doInit();
        Test.stopTest();     
    }

     public static testmethod void testmethodForException(){
        PageReference pageRef = Page.APTS_RefreshContractRates;
        Test.setCurrentPage(pageRef);

        APTS_Contract_Rates_Field_Mapping__c objCustomSetting3 = new APTS_Contract_Rates_Field_Mapping__c(Name='Default % OF Charges');
        objCustomSetting3.Action_Type__c = 'Refresh Contract Rates';
        objCustomSetting3.Destination_Field_API_Name__c = 'Default_OF_Charges__c';
        objCustomSetting3.Destination_Object__c = 'Apttus__APTS_Agreement__c';
        objCustomSetting3.Source_Field_API_Name__c = 'Not_Otherwise_Specified_Default_Value';
        objCustomSetting3.Source_Object__c = 'NS_NIS_Request__c';
        insert objCustomSetting3;
        
        Apttus__APTS_Agreement__c testAgr2 = [select ID,name from Apttus__APTS_Agreement__c WHERE Market_State__c='AK'];
        NS_NIS_Request__c objNISRequest = [select ID,Name from NS_NIS_Request__c LIMIT 1];

        ApexPages.currentPage().getParameters().put('Id', objNISRequest.ID);
        ApexPages.currentPage().getParameters().put('AgreementId', testAgr2.ID);        
        
        Test.startTest();
        APTS_RefreshContractRatesCtrl objController = new APTS_RefreshContractRatesCtrl();
        objController.doInit();
        Test.stopTest();     
    }

}