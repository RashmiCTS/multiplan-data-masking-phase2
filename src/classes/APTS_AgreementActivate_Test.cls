/*
	Author: Kruti Shah, Apttus 03/15/2017 - Test class for agreement Activate logic.	
*/
@isTest
public class APTS_AgreementActivate_Test {
    
    static Account testAcc;
	static Contact testCon;
    static Apttus__APTS_Agreement__c testAgreement;    
    
     @isTest static void testAgreementActivation_success_NoRemoval() {

    	testAcc = new Account();
		testAcc.name = 'testA';
    	insert testAcc;

    	testCon = new Contact();
    	testCon.AccountId = testAcc.Id;
    	testCon.lastName = 'agreementInsert_Test';
    	testCon.email = 'agreementInsert_Test@Test.com';
    	insert testCon;
         
        Id groupRegionRTId = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('Group - Region').getRecordTypeId();  

		testAgreement = new Apttus__APTS_Agreement__c();
		testAgreement.Apttus__Account__c = testAcc.id;
		testAgreement.recordtypeid = groupRegionRTId;
		testAgreement.Apttus__Primary_Contact__c = testCon.id;
		testAgreement.Market_State__c = 'AK';
		testAgreement.Apttus__Status_Category__c = 'In Signatures';
		testAgreement.Apttus__Status__c = 'Fully Signed';
        testAgreement.Agreement_Stage__c = 'Implementation';
        testAgreement.Apttus__Contract_Start_Date__c =date.today();
        testAgreement.Apttus__Perpetual__c = true;
		insert testAgreement;
    	
		List<Attachment> testattachments = APTS_TestDataFactory.createSObjectList(new Attachment(parentId = testAgreement.Id), 2);
        insert testattachments;

        Test.setCurrentPageReference(new PageReference('/apex/APTS_AgreementActivate_OOB'));
        APTS_AgreementActivate_OOB_ctrl extension = new APTS_AgreementActivate_OOB_ctrl(new ApexPages.StandardController(testAgreement));
               
        Test.startTest();
        extension.doInit(); 
        extension.selectedDocs.add(testattachments.get(0).Id);        
        extension.doNext();
        extension.doPrevious();
        extension.doActivate();
        extension.doCancel();
        Test.stopTest();
    }    
    
    
    @isTest static void testAgreementActivation_Success_Sentive_WRemoval() {

        testAcc = (Account) APTS_TestDataFactory.createSObject(new Account());
        insert testAcc;        
        
        testCon = new Contact();
    	testCon.AccountId = testAcc.Id;
    	testCon.lastName = 'agreementInsert_Test';
    	testCon.email = 'agreementInsert_Test@Test.com';
    	insert testCon;

        Id groupRegionRTId = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('Group - Region').getRecordTypeId();  

		testAgreement = new Apttus__APTS_Agreement__c();
		testAgreement.Apttus__Account__c = testAcc.id;
		testAgreement.recordtypeid = groupRegionRTId;
		testAgreement.Apttus__Primary_Contact__c = testCon.id;
		testAgreement.Market_State__c = 'AK';
		testAgreement.Apttus__Status_Category__c = 'In Signatures';
		testAgreement.Apttus__Status__c = 'Fully Signed';
        testAgreement.Agreement_Stage__c = 'Implementation';
        testAgreement.Apttus__Contract_Start_Date__c =date.today();
        testAgreement.Apttus__Perpetual__c = true;
		insert testAgreement;    

        List<Attachment> testattachments = APTS_TestDataFactory.createSObjectList(new Attachment(parentId = testAgreement.Id), 2);
        insert testattachments;

        Test.setCurrentPageReference(new PageReference('/apex/APTS_AgreementActivate_OOB'));
        APTS_AgreementActivate_OOB_ctrl extension = new APTS_AgreementActivate_OOB_ctrl(new ApexPages.StandardController(testAgreement));
        
        Test.startTest();
        extension.doInit();        
        extension.selectedDocs.add(testattachments.get(0).Id);
       
        extension.doNext();
        extension.doPrevious();
		extension.doNext();
        extension.doActivate();
        extension.doCancel();
        Test.stopTest();
    }
    
    @isTest static void testAgreementActivation_failure() {

    	testAcc = (Account) APTS_TestDataFactory.createSObject(new Account());
        insert testAcc;        
        
        testCon = new Contact();
    	testCon.AccountId = testAcc.Id;
    	testCon.lastName = 'agreementInsert_Test';
    	testCon.email = 'agreementInsert_Test@Test.com';
    	insert testCon;

        Id groupRegionRTId = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('Group - Region').getRecordTypeId();  

		testAgreement = new Apttus__APTS_Agreement__c();
		testAgreement.Apttus__Account__c = testAcc.id;
		testAgreement.recordtypeid = groupRegionRTId;
		testAgreement.Apttus__Primary_Contact__c = testCon.id;
		testAgreement.Market_State__c = 'AK';
		testAgreement.Apttus__Status_Category__c = 'In Signatures';
		testAgreement.Apttus__Status__c = 'Fully Signed';
        testAgreement.Agreement_Stage__c = 'Implementation';
        testAgreement.Apttus__Contract_Start_Date__c =date.today();
        testAgreement.Apttus__Perpetual__c = true;
		insert testAgreement;    

        Test.setCurrentPageReference(new PageReference('/apex/APTS_AgreementActivate_OOB'));        
        APTS_AgreementActivate_OOB_ctrl extension = new APTS_AgreementActivate_OOB_ctrl(new ApexPages.StandardController(testAgreement));
        
        Test.startTest();
        extension.doInit();        
        extension.doNext();
        extension.doPrevious();
        extension.doNext();
        extension.doActivate();
        extension.doCancel();
        Test.stopTest();
    }
    
}