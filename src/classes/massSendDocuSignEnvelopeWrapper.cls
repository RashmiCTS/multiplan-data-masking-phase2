/*
File Name:   massSendDocuSignEnvelopeWrapper
Author:      Michael Olson  
Date:        5/25/2016
Description: wrapper class to DocusignAPI

Modification History
5/25/2016		Michael Olson		Created
6/6/2016		Michale Olson		Add V5 Group Logic
*/

global class massSendDocuSignEnvelopeWrapper {
	
	public Apttus__APTS_Agreement__c  agreeRec 	{ get; set; }
    public String envelopeId        			{ get; set; }
    public String agreementId       			{ get; set; }
    public String recipientName     			{ get; set; }
    public String recipientId       			{ get; set; }
    public String recipientEmail    			{ get; set; }
    public String documentNames     			{ get; set; }
    public String errorMsg          			{ get; set; } 
    public integer docCount     				{ get; set; } 
}