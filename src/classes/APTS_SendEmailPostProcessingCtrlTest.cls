/* APTS_SendEmailPostProcessingCtrlTest
 * Test class for APTS_SendEmailPostProcessingCtrl.
 *
 * Developer: Kruti Shah, APTTUS
 ***************************************************************************************************
 Modification Log:
 *
 *     Kruti Shah, 05/19/2017 - Created APTS_SendEmailPostProcessingCtrlTest.
 *    
 ***************************************************************************************************
*/
@isTest
private class APTS_SendEmailPostProcessingCtrlTest {
	
	static Apttus__APTS_Agreement__c testAgreement;
	static Profile adminProfile;
	static User adminUser;

	static{
		Account testAccount = (Account) APTS_TestDataFactory.createSObject(new Account());
        insert testAccount;

		adminProfile = [Select Id 
                                From Profile 
                                    Where Name = 'System Administrator' Limit 1];

        adminUser = (User) APTS_TestDataFactory.createSObject(new User());
        adminUser.ProfileId = adminProfile.Id;        
        insert adminUser;

		testAgreement = (Apttus__APTS_Agreement__c) APTS_TestDataFactory.createSObject(new Apttus__APTS_Agreement__c());
        testAgreement.Apttus__Account__c = testAccount.Id;    	    	
        insert testAgreement;
	}

	@isTest static void Initialization_Test() {
		// Implement test code
		
		Test.setCurrentPage(Page.APTS_SendEmailPostProcessing);
		System.currentPageReference().getParameters().put('newTemplateId', adminUser.Id);// suppose to be templateId but anything can be passed.
		System.currentPageReference().getParameters().put('userAction', 'Initialization_Test');
		ApexPages.StandardController stdCtrl = new ApexPages.StandardController(testAgreement);

		APTS_SendEmailPostProcessingCtrl ctrlExt = new APTS_SendEmailPostProcessingCtrl(stdCtrl);
		System.assertEquals(false, ctrlExt.showConfirmation);
		System.assertEquals(adminUser.Id, ctrlExt.templateId);
		System.assertEquals(testAgreement.Id, ctrlExt.agreementId);
		System.assertEquals('Initialization_Test', ctrlExt.userAction);
	}	
	
	@isTest static void afterSentToOtherPartyReview_Test() {
		// Implement test code
		Test.setCurrentPage(Page.APTS_SendEmailPostProcessing);
		System.currentPageReference().getParameters().put('newTemplateId', adminUser.Id);// suppose to be templateId but anything can be passed.
		System.currentPageReference().getParameters().put('userAction', 'Initialization_Test');
		ApexPages.StandardController stdCtrl = new ApexPages.StandardController(testAgreement);

		APTS_SendEmailPostProcessingCtrl ctrlExt = new APTS_SendEmailPostProcessingCtrl(stdCtrl);
		PageReference ref = ctrlExt.afterSentToOtherPartyReview();
	}

	@isTest static void doCancel_Test() {
		// Implement test code
		Test.setCurrentPage(Page.APTS_SendEmailPostProcessing);
		System.currentPageReference().getParameters().put('newTemplateId', adminUser.Id);// suppose to be templateId but anything can be passed.
		System.currentPageReference().getParameters().put('userAction', 'Initialization_Test');
		ApexPages.StandardController stdCtrl = new ApexPages.StandardController(testAgreement);

		APTS_SendEmailPostProcessingCtrl ctrlExt = new APTS_SendEmailPostProcessingCtrl(stdCtrl);
		PageReference ref = ctrlExt.doCancel();

		System.assertNotEquals(null, ref);
	}
	
}