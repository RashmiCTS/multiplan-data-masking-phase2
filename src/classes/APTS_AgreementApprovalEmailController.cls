/*
Author: Apttus, 02/03/2017 - APTS_AgreementApprovalEmailController.
Modification log:
Kruti Shah, APTTUS, 02/03/2017 - Controller for Visualforce Component - APTS_AgreementApprovalNotification
Kruti Shah, APTTUS, 03/21/2017 - Added method getlistOfClosedRequests for Pre-Signture Approval Email Template
*/
public with sharing class APTS_AgreementApprovalEmailController {     
  
    public Apttus_Approval__Approval_Request__c approvalRequestSO {get; set;}
    public String agrID {get; set;}
    //public String agreementName {get;set;}    
    
    public APTS_AgreementApprovalEmailController(){ 
       
    }
    
    public void setAgrID(String aID)
    {
        AgrID = aID;
    }
    public String getAgrID()
    {
        return AgrID;
    }
    public List<NS_NIS_Request__c> getlistOfNS_NIS_Request(){
               
        return [SELECT Id,Name,RecordType.Name,Reimbursement_Type__c,Request_Status__c,Requester__r.Name,HCE_Analyst__r.Name,
                HCE_Analyst_Status__c,ND_HCE_Request_Group__r.Name,CreatedDate,Agreement__r.Name
                    FROM NS_NIS_Request__c 
                        WHERE Agreement__c =:AgrID];
        
    }
    
     public List<NS_NIS_Request__c> getlistOfClosedRequests(){
               
        return [SELECT Id,Name,RecordType.Name,Reimbursement_Type__c,Request_Status__c,Requester__r.Name,HCE_Analyst__r.Name,
                HCE_Analyst_Status__c,ND_HCE_Request_Group__r.Name,CreatedDate,Agreement__r.Name,NS_NIS_Req__c, Agreement__r.region__c
                    FROM NS_NIS_Request__c 
                        WHERE Agreement__c =:AgrID ];
                        //AND Request_Status__c =:CONSTANTS.AGREEMENT_STAGE_CLOSED_SUCCESSFUL];
        
    }
    
}