/*
File Name:   Agreement_CreateEmailTemplatesBatch
Author:      Rashmi T  
Date:        11/14/2016
Description: The docusign API implaemented by Apptus could not resolve the Agreement Number present in the email template 
				of V5_Group_Email and hence this batch serves as a workaround which creates an email template for every Agreement
				to embed the agreement number in itself.

Modification History
11/14/2016   Rashmi T   Created
*/ 
public class Agreement_CreateEmailTemplatesBatch implements Database.Batchable<sObject> { 
    
    public Agreement_CreateEmailTemplatesBatch(){
        //constructor
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC) {
    	
        String strQuery = 'SELECT Id, Apttus__Status_Category__c,Name,Apttus__FF_Agreement_Number__c '+
                'FROM Apttus__APTS_Agreement__c ' +
                'WHERE Flag_Auto_Generate_Documents__c = true '+
            	'AND recordType.DeveloperName=\'Group_V5\' '+
            	'AND Apttus__Status_Category__c = \'Request - Auto Document Generation Pending; Auto DocuSign Send Pending\' '+
                'ORDER by Apttus__FF_Agreement_Number__c desc';
        System.debug('*** strQuery '+strQuery);
        return Database.getQueryLocator(strQuery);
    }
    
    public void execute(Database.BatchableContext BC, List<Apttus__APTS_Agreement__c> scope) {
        System.debug('*** scope '+scope);
        String strTempName = '';
        String strTemp = 'TEMP_';
        String sErrTemp = '';
        String sErrMsg = '';
    	EmailTemplate emTemp = new EmailTemplate();
        List<EmailTemplate> lstEmailTemp = new List<EmailTemplate>();
        //Query Temp Email Folder
        Folder fold = [SELECT id FROM Folder WHERE DeveloperName='Temp_Email_Folder' LIMIT 1];
        if(fold <> NULL){
            for(Apttus__APTS_Agreement__c ag : scope){
                emTemp = new EmailTemplate();
                strTempName = strTemp+ag.Apttus__FF_Agreement_Number__c.replace('.','_');
                emTemp.Name = strTempName;
                emTemp.DeveloperName = strTempName;
                emTemp.Body = 'Dear Reviewer: \n\n\n Please review the attached contract amendment.\n\n\n Thanks,Multiplan';
                emTemp.Subject = 'Provider Signed Group Agreement ['+ag.Apttus__FF_Agreement_Number__c+'] For Review';
                emTemp.FolderId = fold.Id;
                emTemp.TemplateType = 'Text';
                lstEmailTemp.add(emTemp); 
            }
            system.debug('*** '+lstEmailTemp);
            List<Database.SaveResult> lstETSaveRes = Database.insert(lstEmailTemp,false);
            Map<String,String> mapError = new Map<String,String>();
            for(Integer i=0;i<lstETSaveRes.size();i++){
                if(!lstETSaveRes[i].isSuccess()){
                    sErrTemp = lstEmailTemp[i].DeveloperName;
                    for(Database.Error de : lstETSaveRes[i].getErrors()){
                        sErrMsg = de.getMessage(); 
                    }
                    mapError.put(sErrTemp, sErrMsg);
                }
            }
            if(!mapError.isEmpty()){
                throw new applicationException(String.valueOf(mapError));
            }
        }        
    }
    
    public void finish(Database.BatchableContext BC) { 
        AsyncApexJob aaj = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
                                TotalJobItems, CreatedBy.Email
                            FROM AsyncApexJob WHERE Id = :BC.getJobId()];
        if(aaj.NumberOfErrors == 0){
            //If applicable, invoke DocuSign Batch to mass send documents
        	Database.executeBatch(new DSReciepient_UpdateEmailTemplateBatch(), 200);       
        }
    }
}