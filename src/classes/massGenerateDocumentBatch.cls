/*
File Name:   massGenerateDocumentBatch
Author:      Michael Olson
Date:        5/7/2016
Description: Batch to generate and send documents
Usage: massGenerateDocumentBatch.run() from an execute anonymous window.

Modification History
4/13/2016       Michael Olson       Created 
5/24/2016       Michael Olson       Update VA template name to v3.16
6/6/2016        Michale Olson       Add V5 Group Logic
6/23/2016       Michael Olson       Handled null pointer exception when not sending via docuSign 
7/6/2016        Michael Olson       For V5 Group; created multiple DocuSign Recipients; Auto Generate the Exhibits B,D, and SLCP - stateId
8/15/2016       Jason Hartfield     Refactored to remove hard coded features in favor of configuration via custom metadata
8/18/2016       Michael Olson       Handle Attachments where content type was null
10/20/2016      Jason Hartfield     Better error handling, easier to kick off
*/

// TODO: Handle use case where Session ID becomes invalid.  WE should abort the job and email the user so they know....
public class massGenerateDocumentBatch implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful { 
    
    public string SessionID;
    public  Map<ID,Apttus__APTS_Agreement__c> agreementsWithSuccess = new Map<ID,Apttus__APTS_Agreement__c>();
    public  Map<ID,Apttus__APTS_Agreement__c> agreementsWithFailure = new Map<ID,Apttus__APTS_Agreement__c>();
    public Mass_Document_Send__c massDocumentSend;

    // Make this a bit easier to run.
    public static void run(){
        
        Database.executeBatch(new massGenerateDocumentBatch(),1);
    }
    //--------------------------------------------------------------------------
    //Method that is called to begin the document generation process
    //--------------------------------------------------------------------------   
    public massGenerateDocumentBatch(){
        this(UserInfo.getSessionId());
    }
    public massGenerateDocumentBatch(String SessionID) {  

        this.SessionId = SessionID;
        //agreementsWithFailure = new Map<ID,Apttus__APTS_Agreement__c>();
        //agreementsWithSuccess = new Map<ID,Apttus__APTS_Agreement__c>();
    }

    
    //--------------------------------------------------------------------------
    //Query is run to start process
    //--------------------------------------------------------------------------
    public Database.QueryLocator start(Database.BatchableContext BC) {
        massDocumentSend = new Mass_Document_Send__c(Total_Agreements_Generated__c = 0);
        massDocumentSend.Session_ID__c = SessionId;
        insert massDocumentSend;  

        string Query = 'select Id, name, Status__c, Agreement__c, Agreement__r.Name, Template__c, Generated_Attachment_Id__c, Template__r.id, Agreement__r.id, ' +
                'Agreement__r.Flag_Auto_Generate_Documents__c, Agreement__r.Flag_Auto_Send_via_DocuSign__c, Agreement__r.RecordType.Name, ' +
                'Agreement__r.Apttus__Status_Category__c, Agreement__r.Apttus__Status__c,Send_In_Supplemental_Email_Only__c ' +
                'FROM Template_To_Gen__c ' +
                'WHERE Status__c = \'Queued\' ' +
                'AND Generated_Attachment_Id__c = null ' +
                'AND Agreement__r.Flag_Auto_Generate_Documents__c = true ' +
                'ORDER by Agreement__r.id, Sort_Order__c, name desc';

        Database.QueryLocator queryLocator = Database.getQueryLocator(query); 
        return queryLocator;
    }


    //--------------------------------------------------------------------------
    //Work through the scope to generate
    //--------------------------------------------------------------------------
    public void execute(Database.BatchableContext BC, list<Template_To_Gen__c> scope) {
        // Scope size can only be 1 record because we need to make a web callout for each generation.
        system.assertEquals(1,scope.size(),'Batch size cannot be larger than 1!');
        Template_To_Gen__c docStub = scope[0];

        if(GenerateDocument(docStub)){
            // Record the success for our summary email at the end of the batch.
            agreementsWithSuccess.put(docStub.Agreement__c,docStub.Agreement__r);
        } else {
            // Record the failure for our summary email at the end of the batch.
            agreementsWithFailure.put(docStub.Agreement__c,docStub.Agreement__r);
        }
        
        // IF all the documents for an agreement have been processed,
        // set the agreement status so we know.
        SetAgreementStatus(docStub.Agreement__r);
    }

    // Now that we have successfully generated our document, we want to check to see if all
    // the documents to be generated for the agreement have been generated.  If so, set the status
    // to let us know.
    private void SetAgreementStatus(Apttus__APTS_Agreement__c agreement){
        agreement.Auto_Generation_Errors__c = '';
        boolean allAgreementDocsGenerated = true;
        boolean allAgreementDocsSuccessful = true;
        List<string> attachmentIds = new List<string>();
        
        // TODO: What if this process runs twice, like for re-generate?  Do we need to mark the stubs as belonging to the same batch somehow?
        List<Template_To_Gen__c> allStubsForAgreement = [SELECT ID, Status__c,Generated_Attachment_Id__c,Generation_Error__c FROM Template_To_Gen__c WHERE Agreement__c = :agreement.id];
        for(Template_To_Gen__c docStub : allStubsForAgreement){
            if(docStub.Status__c == 'Queued'){
                allAgreementDocsGenerated = false;
            }

            if(docStub.Status__c != 'Generated'){
                allAgreementDocsSuccessful = false; 
                agreement.Auto_Generation_Errors__c += docStub.Generation_Error__c + '\r\n\r\n';
            }

            attachmentIds.add(docStub.Generated_Attachment_Id__c);
        }

        // Other processes (like the APTTUS doc generation) may have updated the agreement while the job has been running, so we get it fresh for our update.
        Apttus__APTS_Agreement__c agreementForUpdate = [SELECT ID FROM Apttus__APTS_Agreement__c WHERE ID =:agreement.id FOR UPDATE];
        // Associate the agreement to the 'send batch' that it is a part of so we can keep track of it.
        agreementForUpdate.Mass_Document_Send__c = massDocumentSend.id;


        if(allAgreementDocsGenerated){
            massDocumentSend.Total_Agreements_Generated__c++;
            if(AllAgreementDocsSuccessful){
                // Everything is processed and everything was successful in generating
                if(agreement.Flag_Auto_Send_via_DocuSign__c == true) {                              
                    agreementForUpdate.Apttus__Status_Category__c = 'Request - Auto Document Generation Successful; Auto DocuSign Send Pending';
                    agreementForUpdate.Apttus__Status__c = '';                                
                }else{
                    agreementForUpdate.Apttus__Status_Category__c = 'In Signatures';
                    agreementForUpdate.Apttus__Status__c = 'Ready for Signatures';          
                }   
            } else {
                agreementForUpdate.Apttus__Status__c = ''; 
                // Everything was processed, but something went wrong with one or more of the docs.
                if(agreement.Flag_Auto_Send_via_DocuSign__c == true) {
                    agreementForUpdate.Apttus__Status_Category__c = 'Request - Auto Document Generation Failed; Auto DocuSign Send Aborted';
                    
                } else {
                    agreementForUpdate.Apttus__Status_Category__c = 'Request - Auto Document Generation Failed';
                }   

            }
            
            // Original code did this, but I don't see it as being necessary.
            // agreementForUpdate.Flag_Auto_Generate_Documents__c = false;
        } else {
            //revert Agreement Status because Apttus.MergeWebService.generateDoc() updates Status automatically
            agreementForUpdate.Apttus__Status_Category__c = agreement.Apttus__Status_Category__c;
            agreementForUpdate.Apttus__Status__c = agreement.Apttus__Status__c;     
        }   
        update agreementForUpdate;  
        update massDocumentSend;
    }

    private boolean GenerateDocument(Template_To_Gen__c docStub){
        boolean bSuccessful = true;
        try{
            Attachment attach = [select id, name, body, parentid, contentType from attachment where parentid =: docStub.Template__r.id ORDER BY CreatedDate DESC limit 1];  
            //handle null content type
            boolean isPdf = false;
            if(attach.contentType == null) {
                isPdf = attach.name.substringAfterLast('.').containsIgnoreCase('pdf') ? true : false;               
            }else if(attach.contentType.containsIgnoreCase('pdf')) { 
                isPdf = true;
            }
            
            if(isPdf){ // Should be 'application/pdf'
                // For PDFs that don't actually get merged, copy them over to the agreement for historical reasons
                
                //string todayAsString = string.valueOf(system.today()).substring(0,9);
                string nameWithoutExtension = attach.name.substringBeforeLast('.');
                Attachment attachmentCopy = new Attachment();
                attachmentCopy.name = 'Original ' + nameWithoutExtension + '.pdf';
                attachmentCopy.body = attach.body;
                attachmentCopy.contentType = attach.contentType;
                attachmentCopy.parentId = docStub.agreement__c;
                insert attachmentCopy;
                docStub.Generated_Attachment_Id__c = attachmentCopy.id;
                
            } else {
                // For DOCX documents, merge them with the agreement and template records using the APTTUS engine via webservice.
                String pLevel = 'Full access';
                String docFormatDocx = 'DOCX';
                String ServerURL = System.Url.getSalesforceBaseUrl().toExternalForm() + '/services/Soap/u/14.0/' + UserInfo.getOrganizationId().left(15);
                //Apptus WebService call to generate Attachment 
                if(!test.isRunningTest()) { // 'generateDoc' is managed code, no way to mock this that I can think of.
                    // Test if we can get a session ID in a batch job
                    //SessionId = UserInfo.getSessionId();
                    system.debug('Template:' + docStub.Template__c);
                    system.debug('Agreement:' + docStub.Agreement__c);
                    system.debug('SessionID:' + SessionID);
                    system.debug('ServerURL:' + ServerURL);
                    docStub.Generated_Attachment_Id__c = Apttus.MergeWebService.generateDoc(docStub.Template__c, docStub.agreement__c, pLevel, docFormatDocx, SessionId, ServerURL);
                } else {
                    Attachment autoGeneratedDoc = new Attachment();
                    autoGeneratedDoc.ParentId = docStub.agreement__c;  
                    autoGeneratedDoc.Name = 'APPTUS GENERATED.docx';                   
                    autoGeneratedDoc.Body = Blob.valueOf('Test Data'); 
                    insert autoGeneratedDoc; 
                    docStub.Generated_Attachment_Id__c = autoGeneratedDoc.id;   
                }
            }
            docStub.Status__c = 'Generated';
        } catch(exception ex){
            // record the error but continue on so we can record it 
            // and process the status of the agreement
            bSuccessful = false;
            docStub.Status__c = 'Error';
            docStub.Generation_Error__c = ex.getMessage() + ' \r\n' + ex.getStackTraceString();
        }
        update docStub;
        return bSuccessful;
    }


    //--------------------------------------------------------------------------
    //After batch is ran run code to show success
    //--------------------------------------------------------------------------
    public void finish(Database.BatchableContext BC) {
        
        //----------------------------------------------------------------
        //Send an email to show sucess/error
        //----------------------------------------------------------------
        
        // Query the AsyncApexJob object to retrieve the current job's information.
        AsyncApexJob aaj = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
                                TotalJobItems, CreatedBy.Email
                            FROM AsyncApexJob WHERE Id = :BC.getJobId()];
        

        //create list of Agreements to show Success and Failure
        String agreeList = '';
        
        integer i=1;
        if(!agreementsWithFailure.isEmpty()) {  
            agreeList += 'List of Agreements with Failures: <br/>';                 
            for(Apttus__APTS_Agreement__c agreement : agreementsWithFailure.values()) {
                agreeList += i + '. (Agreement Name: ' + agreement.Name + 
                                ',  RT: ' + agreement.RecordType.Name +  
                                ',  Link: '+ System.URL.getSalesforceBaseUrl().toExternalForm() + '/' + agreement.Id +
                                '); <br/>';
                i++;  
                // It could be that an agreement had successes and failures.  
                // Remove any failures from the success list...
                agreementsWithSuccess.remove(agreement.id);         
            } 
        } 
                
        if(!agreementsWithSuccess.isEmpty()) {  
            agreeList += 'List of Agreements with Success: <br/>';                    
            for(Apttus__APTS_Agreement__c agreement : agreementsWithSuccess.values()) {
                agreeList += i + '. (Agreement Name: ' + agreement.Name + 
                                ',  RT: ' + agreement.RecordType.Name +  
                                ',  Link: '+ System.URL.getSalesforceBaseUrl().toExternalForm() + '/' + agreement.Id +
                                '); <br/>';
                i++;            
            } 
        }
          
        integer numberOfAgreementsProcessed = agreementsWithSuccess.size() + agreementsWithFailure.size();                  
        // Send an email to the Apex job's submitter notifying of job completion.
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {aaj.CreatedBy.Email};
            mail.setToAddresses(toAddresses);
            mail.setSubject('Auto Generation of Agreement related Documentation is Complete.  Status:  ' + aaj.Status);
            mail.setHtmlBody('A total of ' + numberOfAgreementsProcessed + ' Agreements were processed.  The Apex job processed ' + aaj.TotalJobItems + ' documents with '+ aaj.NumberOfErrors + ' failures.  (source: massGenerateDocumentBatch) <br/><br/>' + agreeList);
        
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        
        
        //----------------------------------------------------------------
        //Send a DocuSign ready contract to contact via Email
        //----------------------------------------------------------------
                
        //If applicable, invoke DocuSign Batch to mass send documents
        Database.executeBatch(new massSendDocuSignBatch(SessionId), 1);         
    }
   
}