/*
File Name:   Test_FeeScheduleTrackingController.cls
Author:      Michael Olson
Date:        9/28/2015
Description: Test Class for FeeScheduleTrackingController.cls

Modification History

*/

@istest 
public with sharing class Test_FeeScheduleTrackingController {
	
	static testMethod void TestFST1() { 
		
		Fee_Schedule_Tracking_Group__c fstG = new Fee_Schedule_Tracking_Group__c();
			fstG.Name = 'Test1';
		insert fstG;
		
		Fee_Schedule_Tracking__c fst = new Fee_Schedule_Tracking__c();
	 	 	fst.FST_Group__c = fstG.id;
	 	 	fst.Status__c = 'New';
		insert fst;
		
	 
	 	test.startTest();
	 		ApexPages.StandardController sc1 = new ApexPages.StandardController(fst);
         	ApexPages.currentPage().getParameters().put('id', fst.id);
   			ApexPages.currentPage().getParameters().put('FsIdClone', null);
   			ApexPages.currentPage().getParameters().put('HCEReqIdClone', null);	  
	        FeeScheduleTrackingController fstCon = new FeeScheduleTrackingController(sc1);
	        
	        fstCon.oFstId = fst.id;
	        //fstCon.getRenderClone();			
			fstCon.setGlobalEditMode();			
			fstCon.saveFST();
			fstCon.cloneR();
			fstCon.getFstChecker();				
			fstCon.getfst();
			fstCon.getRenderClone();		
				
     
        test.stopTest();	
	}
	
	static testMethod void TestFST2() { 
			
		Fee_Schedule_Tracking__c fst = new Fee_Schedule_Tracking__c();
			fst.Provider_Name__c = 'TesterProv';
		insert fst;
		 
		test.startTest();
			ApexPages.StandardController sc1 = new ApexPages.StandardController(fst);
			ApexPages.currentPage().getParameters().put('FsIdClone', fst.id);
			ApexPages.currentPage().getParameters().put('editMode', 'true');		  
			FeeScheduleTrackingController fstCon = new FeeScheduleTrackingController(sc1);
		        
			fstCon.oFstId = null;				
			fstCon.setGlobalEditMode();
			fstCon.saveFST();
			fstCon.cloneR();
		test.stopTest();
	}
	
	static testMethod void TestFST3() {
				
		Fee_Schedule_Tracking__c fst = new Fee_Schedule_Tracking__c();
			fst.Provider_Name__c = 'TesterProv';
		insert fst;
			
		NS_NIS_Request__c hceReq = new NS_NIS_Request__c();				
		insert hceReq;
		 
		test.startTest();
			ApexPages.StandardController sc1 = new ApexPages.StandardController(fst);
	        ApexPages.currentPage().getParameters().put('HCEReqIdClone', hceReq.id);	
	        ApexPages.currentPage().getParameters().put('editMode', 'false');  
			FeeScheduleTrackingController fstCon = new FeeScheduleTrackingController(sc1);
		        
		    fstCon.oFstId = null;				
			fstCon.setGlobalEditMode();
			fstCon.saveFST();
			fstCon.cloneR();	           	
		test.stopTest();
	}
}