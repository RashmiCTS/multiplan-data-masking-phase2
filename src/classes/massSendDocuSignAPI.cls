/*
File Name:   massSendDocuSignAPI
Author:      Michael Olson  
Date:        5/25/2016
Description: DocusignAPI used by apex batch

Modification History
5/25/2016		Michael Olson		Created
6/6/2016		Michale Olson		Add V5 Group Logic
7/6/2016		Michael Olson		For V5 Group; created multiple DocuSign Recipients; Auto Generate the Exhibits B,D, and SLCP - stateId
9/2/2016		Jason Hartfield	     This class is no longer used and has been absorbed into massSendDocusignBatch
*/

global with sharing class massSendDocuSignAPI {
	/*
	//Variables
	private static final boolean isSandbox = isSandboxChecker.isSandboxChecker();
    private static string dscInstance = !isSandbox ? 'PROD' : 'SANDBOX'; 
    
    String orgId = UserInfo.getOrganizationId(); 
    private static DocuSignConnect__c dscRecord;
    private static string Document_Format = 'pdf';


    //send for Signature
    global static massSendDocuSignEnvelopeWrapper doSend(Apttus__APTS_Agreement__c agreeRec, list<Id> generatedAgreementDocIds, string s) {
		
		string SessionId = s; 
  	
    	//local List, Maps,Set
        massSendDocuSignEnvelopeWrapper envWrapper = new massSendDocuSignEnvelopeWrapper();
        list<Attachment> attList;
        list<Apttus_DocuApi__DocuSignDefaultRecipient2__c>  docuSignRecipient_List = new list<Apttus_DocuApi__DocuSignDefaultRecipient2__c>();
        list<attachment> attachDels = new list<attachment>();
        
        //-------------------------------------------------------------
        //Run initial validation to ensure properly API setup
        //------------------------------------------------------------- 
        
	        //validate agreement id
	        if(agreeRec.id == null) { envWrapper.errorMsg = 'Agreement Id is Missing.'; return envWrapper; }
	
	        //validate agreement
	        if(AgreeRec == null) { envWrapper.errorMsg = 'Agreement Record is Missing.'; return envWrapper; }
	                
	        //validate agreement document id
	        if(generatedAgreementDocIds == null) { envWrapper.errorMsg = 'Generated Agreement Document Ids are Missing.'; return envWrapper; }        
            
            //get and validate attachments
            try {
	            attList = [select id, name, body, contentType 
	            			from Attachment 
	            			where id IN :generatedAgreementDocIds];	            			
	            			
	        }catch(System.QueryException qe1) { envWrapper.errorMsg = 'Invalid Attachment Ids.'; return envWrapper; }  
	       	
	       	//Get and validate DocuSign Connect
			try{
				dscRecord = [select id, Name, Account_Id__c, Integrator_Key__c, Pass__c, User_Id__c, Web_Service_Url__c 
							from DocuSignConnect__c 
							where Name =: dscInstance limit 1];
							
			}catch(System.QueryException qe) { envWrapper.errorMsg = 'DocuSignConnect Object issue.'; return envWrapper; } 
			
			//get and validate all Docusign recipients
			try {       	
	            docuSignRecipient_List = [SELECT Apttus_DocuApi__ReadOnlyEmail__c, Apttus_DocuApi__FirstName__c, Apttus_DocuApi__LastName__c, 
	            								Apttus_DocuApi__ContactId__c, Contact_Full_Name__c, Apttus_DocuApi__EmailTemplateUniqueName__c, 
	            								Apttus_DocuApi__SigningOrder__c, Apttus_DocuApi__RecipientType__c, Apttus_DocuApi__RoleName__c
						            		FROM Apttus_DocuApi__DocuSignDefaultRecipient2__c 
						            		WHERE Apttus_CMDSign__AgreementId__c =: agreeRec.id 
						            		order by Apttus_DocuApi__SigningOrder__c, Apttus_DocuApi__RecipientType__c];	
            		 		       
        	}catch(System.QueryException qe2) { envWrapper.errorMsg = 'DocuSign Recipients Object Error.'; return envWrapper; } 
        	

        //-------------------------------------------------------------
        //Setup Conection To DocuSign
        //-------------------------------------------------------------       	
	
	        //Docusign account login details
	        String accountId = dscRecord.Account_Id__c;
	       	String userId = dscRecord.User_Id__c;
	        String password = dscRecord.Pass__c;
	        String integratorsKey = dscRecord.Integrator_Key__c;
	        String webServiceUrl = dscRecord.Web_Service_Url__c;                                
	        String envelopeId;
	        
	        //Instantiate API Call
	        DocuSignAPI.APIServiceSoap dsApiSend = new DocuSignAPI.APIServiceSoap();
	        dsApiSend.endpoint_x = webServiceUrl;
	       
	        //Get setup call
	        String requestorDSID = userInfo.getUserEmail(); //probably should change to Agreement.Requestor__e.email
	        
	        String auth = 	'<DocuSignCredentials>' +
								'<Username>'+ userId +'</Username>' +
								'<Password>' + password + '</Password>' +
								'<IntegratorKey>' + integratorsKey + '</IntegratorKey>' +
	            			'</DocuSignCredentials>';
	            
	        dsApiSend.inputHttpHeaders_x = new Map<String, String>();
	        dsApiSend.inputHttpHeaders_x.put('X-DocuSign-Authentication', auth);
        

        //-------------------------------------------------------------
        //Setup DocuSign Envelope
        //-------------------------------------------------------------         
	        
	        //create envelope
	        DocuSignAPI.Envelope envelope = new DocuSignAPI.Envelope();
	        	envelope.AccountId = accountId;
		        envelope.Subject = 'Document ready for DocuSign eSignature';
		        envelope.EmailBlurb = 'Please review the attached Document and take the appropriate action.';
        		envelope.AllowReassign = true;
	        
		        String documentNames = '';
		        envelope.Documents = new DocuSignAPI.ArrayOfDocument();
		        envelope.Documents.Document = new DocuSignAPI.Document[attList.size()];                 	        
    
		        Integer i=1;
		        for (Attachment att : attList) {
      
		            DocuSignAPI.Document document = new DocuSignAPI.Document();
			            
			            document.ID = i;
			            
			            if(att.contentType == 'application/pdf'){
			            	document.pdfBytes = EncodingUtil.base64Encode(att.body);
			            }else{
			            	//Use APTTUS API to convert docx to pdf
			            	Id docId = att.id;
							String filename = att.name;
							Boolean addWatermark = false;
							Boolean removeWatermark = false;
							String headerText = '';
							String footerText = '';
							//String apiServerURL = System.Url.getSalesforceBaseUrl().toExternalForm() + '/services/Soap/u/14.0/' + userInfo.getOrganizationId();	
							String apiServerURL = System.Url.getSalesforceBaseUrl().toExternalForm() + '/services/Soap/u/14.0/00D19000000EsWn';		//cant' be hard coded					
			            	
			            	//Make Webservice Call
			            	Id convertedDocId = Apttus.MergeWebService.convertWordDocToPDF(docId, fileName,addWatermark, removeWatermark, 
			            																	headerText, footerText, SessionId, apiServerURL);
			            	//retrieve Atachment for details
			            	attachment gen = new attachment();
			            		gen = [select id, contentType, name, body from attachment where id =: convertedDocId limit 1];														

			            	document.pdfBytes = EncodingUtil.base64Encode(gen.body);
			            	
			            	//add to list to potentialy delete in the end
			            	attachDels.add(gen);
			            	
			            }
			            
			            document.Name = att.Name;
			            documentNames += (documentNames == '' ? '' : '###')+ document.Name; //why the ## format?
			            document.FileExtension = Document_Format;  //should auto convert
			            
			            envelope.Documents.Document[i-1] = document;
			    	i = i+1;
		        }
  
        
	        //------------------------------------------------------------
	        // Set Up email template
	        //------------------------------------------------------------
        	
        	//variables
        	string emailTemplateDevName;
        	EmailTemplate emailTemplateId; 
            
            //Use the DocuSign Recipients to set email template.
            if(docuSignRecipient_List.size() > 0) {         	

            	//setup email template name 
            	decimal inter;         	
            	for(Apttus_DocuApi__DocuSignDefaultRecipient2__c a : docuSignRecipient_List) {
            		if(a.Apttus_DocuApi__EmailTemplateUniqueName__c != null) {
            			if(inter == null) {
            				emailTemplateDevName = a.Apttus_DocuApi__EmailTemplateUniqueName__c;
            				inter = a.Apttus_DocuApi__SigningOrder__c;
            			}else{
            				if(a.Apttus_DocuApi__SigningOrder__c < inter) {
            					emailTemplateDevName = a.Apttus_DocuApi__EmailTemplateUniqueName__c;
            					inter = a.Apttus_DocuApi__SigningOrder__c;
            				}
            			}
            		}
            	}
            	
            	//setup email template id
	        	if(emailTemplateDevName != null) {	        		
						        		
	            	emailTemplateId = [Select id, developerName, subject, body 
	            						from EmailTemplate 
	            						where developerName =: emailTemplateDevName
	            						limit 1]; 
	        	
	            	envelope.Subject = emailTemplateId.subject;
	        		envelope.EmailBlurb = emailTemplateId.body;	        	
	        	}
	        	
				
		        //------------------------------------------------------------
		        // Set Up all Recipients
		        //------------------------------------------------------------				
					       			
        			list<DocuSignAPI.Recipient> recipList = new list<DocuSignAPI.Recipient>();
        			
        			integer recipCounter = 0;					
					for(Apttus_DocuApi__DocuSignDefaultRecipient2__c dsr : docuSignRecipient_List) {          
									        
        				DocuSignAPI.Recipient recip = new DocuSignAPI.Recipient();

						if(dsr.Apttus_DocuApi__ReadOnlyEmail__c != null) {        			                			        
							recip.ID = recipCounter + 1;
							recip.UserName = dsr.Contact_Full_Name__c;
						    recip.RequireIDLookup = false;
							recip.Type_x = dsr.Apttus_DocuApi__RecipientType__c.replaceAll( '\\s+', '');					        				   
							recip.RoutingOrder = integer.valueOf(dsr.Apttus_DocuApi__SigningOrder__c);
							recip.RoleName = dsr.Apttus_DocuApi__RoleName__c;			
													        	        
							//Logic to avoid sending mass emails external contacts from a sandbox	
							if( !isSandbox ) {
								recip.Email = dsr.Apttus_DocuApi__ReadOnlyEmail__c;
							}else {
							
								if(dsr.Apttus_DocuApi__ReadOnlyEmail__c.contains('@multiplan.com') || dsr.Apttus_DocuApi__ReadOnlyEmail__c.contains('@marsauditor.com') ) {
							    	recip.Email = dsr.Apttus_DocuApi__ReadOnlyEmail__c;
							    }else{
							    	recip.Email = dsr.Apttus_DocuApi__ReadOnlyEmail__c + '.sandboxNoSend';
							    }
							}
							        
							recipList.add(recip);
							recipCounter++;       
						}

        			}
 
        			
        			//If there are no valid DocuSign Recipients throw error, else create Recipient List.
        			envelope.Recipients = new DocuSignAPI.ArrayOfRecipient(); 
       				
        			if(recipCounter > 0) {
        				envelope.Recipients.Recipient = new DocuSignAPI.Recipient[recipList.size()];
        				
        				integer counter = 0;
	        			for(DocuSignAPI.Recipient currR : recipList) {
	        				envelope.Recipients.Recipient[counter] = currR;
	        				counter++;
	        			}   				 
        			}else{
        				envWrapper.errorMsg = 'No DocuSign Recipients with Valid Email.'; return envWrapper;
        			}
        			
            }else{
            	envWrapper.errorMsg = 'DocuSign Recipients are Missing.'; return envWrapper;
            }		
            		 		     
        
        //Set the Notification, Reminders, and Expirations
        
        DocuSignAPI.Reminders reminders = new DocuSignAPI.Reminders();
         	reminders.ReminderEnabled = true;
        	reminders.ReminderDelay = 0; 
        	reminders.ReminderFrequency = 0;        
        
        DocuSignAPI.Expirations expirations = new DocuSignAPI.Expirations(); 
	        expirations.ExpireEnabled = false; 
	        expirations.ExpireAfter = 7; 
	        expirations.ExpireWarn = 0; 
        
        DocusignAPI.Notification notification = new DocuSignAPI.Notification();
        	notification.Reminders = reminders;
        	notification.Expirations = expirations;
        
        envelope.Notification = notification; 
        
                 
        try {
            if(!Test.isRunningTest()) {     	
                
                DocuSignAPI.EnvelopeStatus es = dsApiSend.CreateAndSendEnvelope(envelope);            
	                envWrapper.agreeRec = agreeRec;
	                envWrapper.envelopeId = es.envelopeID;
	                envWrapper.agreementId = agreeRec.Id;
	                envWrapper.recipientName = envelope.Recipients.Recipient[0].UserName;
	                envWrapper.recipientId = '';
	                envWrapper.recipientEmail = envelope.Recipients.Recipient[0].Email;
	                envWrapper.documentNames = documentNames;
	                envWrapper.errorMsg = '';
	                envWrapper.docCount = attList.size();
					

				//get Template
				list<Apttus__APTS_Template__c> templateApp = new list<Apttus__APTS_Template__c>();
				string tempN1 = 'Application-' + agreeRec.Market_State__c;
				string tempN2 = 'Application-Multiplan';
					templateApp = [select id, name, Apttus__IsActive__c, Market_State__c
					    				from Apttus__APTS_Template__c 
					       				where (name =: tempN1 
					       					OR name =: tempN2) 
					       				AND Apttus__IsActive__c = true];
				
				system.debug('templateApp=' + templateApp);
					
					map<string, id> appTempNameToAppTempId_map = new map<string, id>();
					for(Apttus__APTS_Template__c aat : templateApp){
						appTempNameToAppTempId_map.put(aat.name, aat.id);
					}
					system.debug('ppTempNameToAppTempId_map='+appTempNameToAppTempId_map);	       				
				
				//set actual Tempate id
				id finalTemplateId;
				if(appTempNameToAppTempId_map.get(tempN1) != null) {
					finalTemplateId = appTempNameToAppTempId_map.get(tempN1);
				}else{
					finalTemplateId = appTempNameToAppTempId_map.get(tempN2);
				}
				
				//build attachment for agreement insert
				Attachment a1 = [select id, name, parentid, body, contentType from Attachment where parentid =: finalTemplateId limit 1];
				Attachment b1 = new Attachment();
					b1.name = a1.name;
					b1.body = a1.body;
					b1.contentType = a1.contentType;
					b1.parentid = agreeRec.id;
				
				try{
					//insert b1;
				}catch(exception e){throw e;}
				
				
				//Set email file attachments
				List<Messaging.Emailfileattachment> fileAttachments = new List<Messaging.Emailfileattachment>();
				
				// Add to attachment file list
				Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
					efa.setFileName(b1.name);
					efa.setBody(b1.Body);
					//efa.setContentType(b1.contentType);
				    fileAttachments.add(efa);
				
				//setup email address to send to with blocks for externals in sandbox
				string em;
				if( !isSandbox ) { //not proper for multiple types of DSR
					em = docuSignRecipient_List[0].Apttus_DocuApi__ReadOnlyEmail__c;
				}else {
					if(docuSignRecipient_List[0].Apttus_DocuApi__ReadOnlyEmail__c.contains('@multiplan.com') || docuSignRecipient_List[0].Apttus_DocuApi__ReadOnlyEmail__c.contains('@marsauditor.com') ) {
						em = docuSignRecipient_List[0].Apttus_DocuApi__ReadOnlyEmail__c;
					}else{
						em = docuSignRecipient_List[0].Apttus_DocuApi__ReadOnlyEmail__c + '.sandboxNoSend';
					}
				}
				            
				//send email with Application Attachment
				system.debug('em='+em);
				Messaging.SingleEmailMessage mail1 = new Messaging.SingleEmailMessage();
					String[] toAddresses = new String[] {em}; //need right email address
				    mail1.setToAddresses(toAddresses);
				    mail1.setSubject('Application for ' + agreeRec.name);
				    mail1.setPlainTextBody('Please review the attached documents.');
				    mail1.setTargetObjectId(docuSignRecipient_List[0].Apttus_DocuApi__ContactId__c); //needs advanced logic
				    mail1.setWhatId(agreeRec.id);
				    mail1.saveAsActivity = true;  
				    mail1.setFileAttachments(fileAttachments);
				Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail1 });
				              
				
				//del pdf's created for Apttus docx convert
				if(attachDels.size() >0){
					delete attachDels;
				}
 
                return envWrapper;
                
            }else{
            	//used only for test coverage
                envWrapper.agreeRec = agreeRec;
                envWrapper.envelopeId = 'x';
                envWrapper.agreementId = agreeRec.Id;
                envWrapper.recipientName = envelope.Recipients.Recipient[0].UserName;
                envWrapper.recipientId = '';
                envWrapper.recipientEmail = 'x@x.com';
                envWrapper.documentNames = documentNames;
                envWrapper.errorMsg = 'X';
                envWrapper.docCount = attList.size();
            }
        
        }catch(CalloutException e) { system.debug('exceptionE=' + e); envelopeId = 'Exception - ' + e; envWrapper.errorMsg = 'Error 123 '+ e.getMessage() +'  ***  '+e; return envWrapper; }

        return envWrapper; //is this wrong spot move to else block
    }   
    */
           
}