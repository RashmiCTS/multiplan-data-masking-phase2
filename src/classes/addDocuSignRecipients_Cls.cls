/*
File Name:   addDocuSignRecipients_Cls
Author:      Roopak Mudili
Date:        08/31/2016
Description: Generate DocuSign Recipients via Visual Force Page - AgreementDocuSignRecipients.vf

Modification History
 
*/ 


/* 

Generate DocuSign Recipients from Agreement Detail Page
Objects : Apttus__APTS_Agreement__c, Apttus_DocuApi__DocuSignDefaultRecipient2__c
Related Lists : DocuSign Recipients
Page Layout Button : GenerateRecipients
Custom Meta Data Types : Agreement Record Type Setting, DocuSign Recipient Default

*/

public class addDocuSignRecipients_Cls
{
    public Apttus__APTS_Agreement__c agmnt{get;set;}
    public Agreement_Record_Type_Setting__mdt agmntrec{get;set;} 
    public List<Apttus_DocuApi__DocuSignDefaultRecipient2__c> dList {get; set;} 
    public List<Apttus_DocuApi__DocuSignDefaultRecipient2__c> docListtoinsert {get; set;} 
    public List<DocuSign_Recipient_Default__mdt> dsr_Defaults{get; set;} 
    public boolean isDlistAvaliable {get; set;}    
    public String agmId; 
    public Set<String> Assignees = new Set<String>(); 
    public Map<id,String> mapcon = new Map<id,String>(); 
    public Map<String,id> mapnames = new Map<String,id>(); 
    public Map<String,String> mapuserids = new Map<String,String>();
    public Map<String,String> mapaliasuserids = new Map<String,String>(); 
    public Map<String,String> mapusernames = new Map<String,String>();
    public Map<String,id> mapsignedgroupids = new Map<String,id>();     
    public List<DocusignWrapper> listwrap{get;set;}
    public List<Apttus_DocuApi__DocuSignDefaultRecipient2__c> listDSR {get;set;} //list of variables on docusign wrapper class
    public id rtCon {get;set;}
    public id rtEmail {get;set;}
    public id rtSG {get;set;}
    public id rtUser {get;set;}

	
    public addDocuSignRecipients_Cls(ApexPages.StandardController sc)
    {   
        rtCon = Schema.SObjectType.Apttus_DocuApi__DocuSignDefaultRecipient2__c.getRecordTypeInfosByName().get('Contact').getRecordTypeId();
        rtEmail = Schema.SObjectType.Apttus_DocuApi__DocuSignDefaultRecipient2__c.getRecordTypeInfosByName().get('Email').getRecordTypeId();
        rtSG = Schema.SObjectType.Apttus_DocuApi__DocuSignDefaultRecipient2__c.getRecordTypeInfosByName().get('Signing Group').getRecordTypeId();
        rtUser = Schema.SObjectType.Apttus_DocuApi__DocuSignDefaultRecipient2__c.getRecordTypeInfosByName().get('User').getRecordTypeId();
        docListtoinsert = new List<Apttus_DocuApi__DocuSignDefaultRecipient2__c>();
        
        agmId = ApexPages.CurrentPage().getParameters().get('agreementid');
        agmnt = [SELECT Id,RecordType.Name,RecordTypeId,Assignment_Work_Type__c,Assignor_Id__c,Apttus__Primary_Contact__c,Apttus__Primary_Contact__r.Name,Ownerid,Apttus__Requestor__c FROM Apttus__APTS_Agreement__c WHERE Id =: agmId];
           
        listDSR = ClatTriggerUtils.CreateAgreementRecipientsFromAgreements(new List<Apttus__APTS_Agreement__c>{agmnt});
        listDSR = addDocusignRecipientForAssignmentAgmt(listDSR);
        dList = [Select Id from Apttus_DocuApi__DocuSignDefaultRecipient2__c where Apttus_CMDSign__AgreementId__c = :agmId];
        
   }     
    public pageReference insertDSR()
    {
        if(dList.size()>0)
        {
            delete dList;
        }
        insert listDSR;
        Pagereference pRef = new Pagereference('/'+agmId);
        return pRef;    
    
    }
    
    Public PageReference Cancel()
    {
        Pagereference pRef = new Pagereference('/'+agmId);
        return pRef;
    }
    //wrapper class definition
    public class DocusignWrapper
    {
        public string assignee{get;set;}
        public string assigneenotes{get;set;}
        public string recordtypename{get;set;}
        public string recepienttype{get;set;}
        public string rolename{get;set;}
        public Decimal routingorder{get;set;}
        public string emailtemplate{get;set;}
        public DocusignWrapper()
        {
        }
    }
    
    private List<Apttus_DocuApi__DocuSignDefaultRecipient2__c> addDocusignRecipientForAssignmentAgmt(List<Apttus_DocuApi__DocuSignDefaultRecipient2__c> lstDocusignRecp){
        List<Apttus_DocuApi__DocuSignDefaultRecipient2__c> lstNewDSRecip = new List<Apttus_DocuApi__DocuSignDefaultRecipient2__c>();
        Apttus_DocuApi__DocuSignDefaultRecipient2__c ar = new Apttus_DocuApi__DocuSignDefaultRecipient2__c();
        if(!lstDocusignRecp.isEmpty() 
           && agmnt <> NULL 
           && agmnt.Assignment_Work_Type__c <> NULL 
           && ( agmnt.Assignment_Work_Type__c == 'Assignment and Amendment' || agmnt.Assignment_Work_Type__c == 'Assignment Only' ) 
           && agmnt.Assignor_Id__c <> NULL){      
            ar.RecordTypeid = Schema.SObjectType.Apttus_DocuApi__DocuSignDefaultRecipient2__c.getRecordTypeInfosByName().get('Contact').getRecordTypeId();   
            ar.Apttus_DocuApi__ContactId__c = ClatTriggerUtils.mergeTemplateName('{!Apttus__APTS_Agreement__c.Assignor_Id__c}', agmnt);                   
            ar.Apttus_DocuApi__RoleName__c = 'Signer 1'; 
            ar.Apttus_DocuApi__RecipientType__c = 'Signer';
            ar.Apttus_DocuApi__SigningOrder__c = 1;
            ar.Apttus_CMDSign__AgreementId__c = agmnt.id;
            ar.Apttus_DocuApi__IsTransient__c = false;
            ar.Email_Template__c = 'V5_Group_Email_Provider';                         
            ar.send_supplemental_email__c = false;    
            lstNewDSRecip.add(ar);
            for(Apttus_DocuApi__DocuSignDefaultRecipient2__c ds : lstDocusignRecp){
                Apttus_DocuApi__DocuSignDefaultRecipient2__c arClone = new Apttus_DocuApi__DocuSignDefaultRecipient2__c();
                arClone = ds;
                arClone.Apttus_DocuApi__SigningOrder__c = ds.Apttus_DocuApi__SigningOrder__c + 1;
                arClone.Apttus_DocuApi__RoleName__c = 'Signer '+arClone.Apttus_DocuApi__SigningOrder__c;
                lstNewDSRecip.add(arClone);
            }       
            return lstNewDSRecip;
        }
        return lstDocusignRecp;
    }
   
}