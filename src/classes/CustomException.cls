/*
File Name:   CustomException.cls
Author:      Jason Hartfield
Date:        8/17/2016
Description: CustomException utility class

Modification History
Modified By			Date			Description
*/

public class CustomException extends Exception {}