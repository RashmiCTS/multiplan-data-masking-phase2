/*
	Author: Kruti Shah, Apttus, 01/30/2017 - Test class for Approval Request History trigger logic.
*/

@isTest
private class APTS_ApprovalRequestHistoryTriggerTest {
    static Account testAccount;
	static Contact testContact;	
    static Apttus__APTS_Template__c testTemplate;
    static Apttus__APTS_Agreement__c testAgreement;
    static Apttus__Agreement_Clause__c agrClause;
    
	static{
        testAccount = (Account) APTS_TestDataFactory.createSObject(new Account());
        insert testAccount;

        testContact = (Contact) APTS_TestDataFactory.createSObject(new Contact());
        testContact.AccountId = testAccount.Id;
        insert testContact;
        
        testTemplate = new Apttus__APTS_Template__c();
        testTemplate.Name = 'Section 1.02 Billed Charges - Option 02 (Group 5)';
        testTemplate.Apttus__Type__c = 'Clause';
        testTemplate.Apttus__IsActive__c = true;
        testTemplate.Apttus__Agreement_Types__c = 'Group V5 - Amendment; Group - Region; Group Region - Amendment';
        insert testTemplate;
        
        Id group5RTId = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('Group V5').getRecordTypeId();
        
        testAgreement = new Apttus__APTS_Agreement__c();
		testAgreement.Apttus__Account__c = testAccount.id;
		testAgreement.recordtypeid = group5RTId;
		testAgreement.Apttus__Primary_Contact__c = testContact.id;		
		insert testAgreement;
        
        agrClause = new Apttus__Agreement_Clause__c();
		agrClause.Apttus__Agreement__c = testAgreement.id;
        agrclause.Apttus__Template__c = testTemplate.Id;
		agrClause.Apttus__Action__c = CONSTANTS.INSERTED;
		agrClause.Apttus__Clause__c = 'test clause to be insert';
		agrClause.Apttus__Active__c = true;       
        insert agrClause;
        
    }
    
     @isTest static void appRequestHistoryInsert_Test() {
        
        Apttus_Approval__Approval_Request_History__c appReqHistoryTest = new Apttus_Approval__Approval_Request_History__c();
        appReqHistoryTest.Apttus_Approval__Approval_Status__c = 'Cancelled';
        appReqHistoryTest.Apttus_Approval__ChildObjectId__c = String.valueOf(agrClause.Id);    
        appReqHistoryTest.Apttus_Approval__ChildObjectType__c = 'Apttus__Agreement_Clause__c';
        appReqHistoryTest.Apttus_Approval__ChildObjectName__c = 'Agreement Clause';
        appReqHistoryTest.Apttus_Approval__Active__c = true;
        appReqHistoryTest.Apttus_Approval__DateCancelled__c = System.today();
        insert appReqHistoryTest;
                
		appReqHistoryTest = [select id,APTS_Agreement_Clause_Name__c,APTS_Agreement_Clause_Name_Text__c
								FROM Apttus_Approval__Approval_Request_History__c
									WHERE Id = :appReqHistoryTest.ID];        
         
        system.debug('APTTUS--> appReqHistoryTest.APTS_Agreement_Clause_Name__c '+appReqHistoryTest.APTS_Agreement_Clause_Name_Text__c);
        system.assertEquals(agrClause.Apttus__Clause__c, appReqHistoryTest.APTS_Agreement_Clause_Name_Text__c);
        
        
    }
}