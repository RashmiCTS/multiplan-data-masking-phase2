/*
Author: Apttus, 01/27/2017 - Helper class for Approval Request History Trigger.
Modification log:
Kruti Shah, APTTUS, 01/27/2017 - Created Helper class for Approval Request History Trigger.
*/
public class APTS_ApprovalRequestHistoryHelper {
	public set<Id> setOfAgreementClauseId = new set<Id>();    
    public Map<Id,Apttus__Agreement_Clause__c> mapOfIdToAgreementClause = new Map<Id,Apttus__Agreement_Clause__c>();

    public APTS_ApprovalRequestHistoryHelper(){

    }

    //Derivation to populate field Agreement Clause Name(Text) from the Child Object(Agreement Clause)
    public void populateAgreementClauseName(Apttus_Approval__Approval_Request_History__c[] appReqList, Map<Id, Apttus_Approval__Approval_Request_History__c> appReqIdtoAppReqOldMap){        
        for(Apttus_Approval__Approval_Request_History__c appReq : appReqList)
		{
            Apttus_Approval__Approval_Request_History__c appReqOld = appReqIdtoAppReqOldMap.get(appReq.Id);
            
            if(appReqOld == null
					&& appReq.Apttus_Approval__ChildObjectType__c != null						
                    && appReq.Apttus_Approval__ChildObjectType__c == 'Apttus__Agreement_Clause__c')
			{
                setOfAgreementClauseId.add((Id)appReq.Apttus_Approval__ChildObjectId__c);                
            }            
        }

        system.debug('APTTUS--> setOfAgreementClauseId '+setOfAgreementClauseId);      

        if(setOfAgreementClauseId.size() >0 && !setOfAgreementClauseId.isEmpty()){                       
            for(Apttus__Agreement_Clause__c agrClause : [SELECT Id,Name,Apttus__Clause__c 
                            FROM Apttus__Agreement_Clause__c 
                            WHERE Id in :setOfAgreementClauseId]){
                mapOfIdToAgreementClause.put(agrClause.id, agrClause);      
            }
        }

        system.debug('APTTUS--> mapOfIdToAgreementClause '+mapOfIdToAgreementClause);

        for(Apttus_Approval__Approval_Request_History__c appReqHstoryObj : appReqList){
            Apttus__Agreement_Clause__c agrClauseRec = new Apttus__Agreement_Clause__c();
            agrClauseRec = mapOfIdToAgreementClause.get((ID)appReqHstoryObj.Apttus_Approval__ChildObjectId__c);
            system.debug('APTTUS--> Agreement Clause Record '+agrClauseRec);
            if(agrClauseRec != null){
                appReqHstoryObj.APTS_Agreement_Clause_Name_Text__c = agrClauseRec.Apttus__Clause__c;
            }                
        }       

    }
}