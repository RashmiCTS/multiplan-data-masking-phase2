/*==================================================================================
File Name:   NetworkDevUserConfig.cls
Author:      Michael Olson
Date:        5/19/2015
Description: 

Modification History
5/19/2015       Michael Olson       Created
9/28/2015		Michael Olson		Added support for FST auto assignment
10/15/2015		Michael olson		Added Auto Assign FST
====================================================================================*/

global with sharing class NetworkDevUserConfig {
    
    /* =======================================================
    Global Variables
    ========================================================= */
    String assignableUsers = Label.NetDevHCERequest;   
    list<User> configUsersUpdate {get;set;}

    /* =======================================================
    Constructor
    ========================================================= */    
    public NetworkDevUserConfig() { 
    }
    
    /* =======================================================
    Methods
    ========================================================= */        
        
        //------------------------------------------
        //Get List of Users
        //------------------------------------------
        public list<User> configUsers {
            get{
            	                
                list<string> uSet = new list<string>();
                uSet = assignableUsers.split(';');
            
                configUsers =    [select id, name, isactive, UserRole.Name, alias, No_Auto_Assign_HCE_Request_Start_Date__c, email,
                                    No_Auto_Assign_HCE_Request_End_Date__c, Auto_Assign_Rate_Operation_Request_Type__c,Rate_Operations_Backup_Email__c,
                                    Auto_Assign_Rate_Rev_HCFA_RT_Assignable__c, Auto_Assign_FST__c
                                 from User 
                                 where isactive = true 
                                 and alias IN : uSet]; 
                
                configUsersUpdate = configUsers;
                return configUsers;
            }    
            set;
        }
        
        //------------------------------------------
        //Save list of users
        //------------------------------------------
        public pageReference saveUsers() {
            try {
                update configUsersUpdate;
            }catch(exception e) { ApexPages.addMessages(e); return null; }
            
            PageReference newPage = new PageReference('/apex/NetworkDevUserConfig');    
            newPage.setRedirect(true);
            return newPage;        
        }

        //------------------------------------------
        //Cancel with no Save
        //------------------------------------------        
        public PageReference cancel(){        
            PageReference newPageC = new PageReference('/apex/NetworkDevUserConfig');
            newPageC.setRedirect(true);
            return newPageC;
        }  
    
    /* ===================================================
    TEST CLASS 
    ======================================================= */
    public void test() {
        Integer i = 0;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
    }    

    
}