/*
File Name:   MassEditHceMetricController.cls
Author:      Micahel Olson
Date:        11/19/2015
Description: Allow users to mass edit Metrics

Modification History

*/ 

public with sharing class MassEditHceMetricController {
     
    /* ===================================================
    * STRINGS, BOOLEAN...
    ====================================================== */
	public id ndRequestID {get;set;}
	public id copyFromID {get;set;}
    
    /* ====================================================
    *   LISTS
    ======================================================= */
	public NS_NIS_Request__c hceReq {get;set;}
	public list<HCE_Metric__c> allMetricsList {get;set;}


 	/* =====================================================
     CONSTRUCTOR
    ======================================================== */
    public MassEditHceMetricController() {
    
    	ndRequestID = ApexPages.currentPage().getParameters().get('ndRequestID');
    	copyFromID = ApexPages.currentPage().getParameters().get('copyFromID');
    	
    	hceReq = new NS_NIS_Request__c();
    	hceReq = [select id, name, NS_NIS_Req__c, Negotiation_Type__c, Negotiation_Sub_Type__c from NS_NIS_Request__c where id =: ndRequestID ];
   
    }   	 
  	
  	
	/* ================================================ 
    List of all Open Reivews for Current User
    =================================================== */    
    public list<HCE_Metric__c> getAllMetrics(){
    	
    	if(allMetricsList == null) {
    		
    		allMetricsList = new list<HCE_Metric__c>();
    		
    		allMetricsList = [select id, ACL_Volume__c, Current_P_of_Medicare__c, Current_P_of_Medicare_P__c, Current_Discount_P__c, HCE_Request_Negotiation_Sub_Type__c, 
								Metric_is_Complete__c, ND_HCE_Request__c, Negotiated_P_of_Medicare__c, Negotiated_P_of_Medicare_P__c, Negotiated_Discount_P__c, 
								Product__c, Revenue_Change__c, Revenue_Change_F__c, Revenue_Change_D__c, Revenue_Corridor__c, Revenue_Corridor_F__c, 
								Revenue_Corridor_P__c, Savings__c, Savings_Points__c, Savings_F__c, Copy_Metrics_to_Other_Lines__c
                    		from HCE_Metric__c
							where ND_HCE_Request__c =: ndRequestID ];
    		
    	}
    	return allMetricsList;
    }
 
    
    /* ================================================ 
    Save Records
    =================================================== */    
    public pagereference saveAll() {
    	
    	PageReference newPage = new PageReference('/apex/MassEditHceMetric?ndRequestID=' + ndRequestID);

    	try{
    		update AllMetricsList;
    		
    		newPage.setRedirect(true); 
    		return newPage; 
    		
    	}catch(Exception dmle) { ApexPages.addMessages(dmle); newPage.setRedirect(false); return null; }         

    }
    
    
    /* ================================================ 
    Cancel
    =================================================== */    
    public pagereference cancels() {        
		PageReference newPage = new PageReference('/' + ndRequestID);   
     	newPage.setRedirect(true);        
		return newPage;     
	}


    /* ================================================ 
    Copy From
    =================================================== */    
    public pagereference copyFrom() {
    	
    	if(copyFromID != null) {   
    	
	    	HCE_Metric__c copyR = new HCE_Metric__c();
	    		copyR = [select id, ACL_Volume__c, Current_P_of_Medicare__c, Current_P_of_Medicare_P__c, Current_Discount_P__c, HCE_Request_Negotiation_Sub_Type__c, 
										Metric_is_Complete__c, ND_HCE_Request__c, Negotiated_P_of_Medicare__c, Negotiated_P_of_Medicare_P__c, Negotiated_Discount_P__c, 
										Product__c, Revenue_Change__c, Revenue_Change_F__c, Revenue_Change_D__c, Revenue_Corridor__c, Revenue_Corridor_F__c, 
										Revenue_Corridor_P__c, Savings__c, Savings_Points__c, Savings_F__c, Copy_Metrics_to_Other_Lines__c
	                    			from HCE_Metric__c
									where id =:  copyFromID];					
	    
	    	for(HCE_Metric__c hce : getAllMetrics() ) {
	    		
	    		if(hce.id != copyFromID) {
					hce.ACL_Volume__c = copyR.ACL_Volume__c;
					hce.Current_P_of_Medicare__c = copyR.Current_P_of_Medicare__c;
					hce.Current_P_of_Medicare_P__c = copyR.Current_P_of_Medicare_P__c;
					hce.Current_Discount_P__c = copyR.Current_Discount_P__c;
					hce.Negotiated_P_of_Medicare__c = copyR.Negotiated_P_of_Medicare__c;
					hce.Negotiated_P_of_Medicare_P__c = copyR.Negotiated_P_of_Medicare_P__c;
					hce.Negotiated_Discount_P__c = copyR.Negotiated_Discount_P__c;
					hce.Revenue_Change__c = copyR.Revenue_Change__c;
					hce.Revenue_Change_D__c = copyR.Revenue_Change_D__c;
					hce.Revenue_Corridor__c = copyR.Revenue_Corridor__c;
					hce.Revenue_Corridor_P__c = copyR.Revenue_Corridor_P__c;
					hce.Savings__c = copyR.Savings__c; 
					hce.Savings_Points__c = copyR.Savings_Points__c;
	    		}
	 	 	
	    	}
	    	
	    	try{
	    		saveAll();
	    	}catch(exception ex) { ApexPages.addMessages(ex); return null; }
	    
			PageReference newPage = new PageReference('/apex/MassEditHceMetric?ndRequestID=' + ndRequestID);   
	     	newPage.setRedirect(true);        
			return newPage;     
    	
    	}else{
    		return null;
    	}
	}	

}