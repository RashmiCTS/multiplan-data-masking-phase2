public class NominationLoad_SendAgreementProcessJob {
/*
File Name:   NominationLoad_SendAgreementProcessJob
Author:      Rashmi T 
Date:        02/02/2017
Description: DocusignAPI used by apex batch

Modification History
5/25/2016		Michael Olson		Created
6/6/2016		Michale Olson		Add V5 Group Logic
7/6/2016		Michael Olson		For V5 Group; created multiple DocuSign Recipients; Auto Generate the Exhibits B,D, and SLCP - stateId
9/2/2016		Jason Hartfield	     This class is no longer used and has been absorbed into massSendDocusignBatch
*/

}