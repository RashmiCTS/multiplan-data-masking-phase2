/*
File Name:   Test_AttachmentTrigger
Author:      Michael Olson
Date:        4/6/2016
Description: Test Class for AttachmentTrigger

Modification History
3/24/2016		Michael Olson		Created email notifiation for HCE Requests in Final Rates Status where new documents have been uploaded.
4/20/2016		Michael Olson		Updated TC to cover a new validation Rule at close.
6/6/2016 		Michael Olson		Updated TC to handle new conditions.
*/

@istest 
public with sharing class Test_AttachmentTrigger {

	static testMethod void TestAttachmentEmail() { 
		//---------------------------------------------------
        // Variables
        //---------------------------------------------------
		Id rtRIUB = Schema.SObjectType.NS_NIS_Request__c.getRecordTypeInfosByName().get('Reimbursement and Implementation - UB').getRecordTypeId();        
        
        //--------------------------------------------------- 
        // Insert Records
        //---------------------------------------------------
        
            User newUapp2 = new User(); 
		    	newUapp2.TimeZoneSidKey = 'America/New_York';   
		    	newUapp2.LanguageLocaleKey = 'en_US';           
		      	newUapp2.LocaleSidKey = 'en_US';            
		     	newUapp2.EmailEncodingKey = 'UTF-8';                    
		  		newUapp2.ProfileId = '00e80000000lB1v'; //00e80000000lB1vAAE
		    	newUapp2.Title  = 'testtitle';
		      	newUapp2.FirstName = 'fname';
		       	newUapp2.LastName = 'lname';
		    	newUapp2.email = 'testtsyfh@kjdhfk.com';
		     	newUapp2.Username = 'testserd@tesdter1111.com'; 
		      	newUapp2.Alias = 'A1';
		       	newUapp2.CommunityNickname =  'nicakn2';
		       	newUapp2.Phone = '1234132';
		       	newUapp2.Auto_Assign_Rate_Operation_Request_Type__c = 'Fee Schedule Request;Non-Standard Rate Exhibit Lang Rev HCFA';
		       	newUapp2.Auto_Assign_Rate_Rev_HCFA_RT_Assignable__c = true;
	    	insert newUapp2; 
	    	
	    	 
       		list<NS_NIS_Request__c> reqList = new list<NS_NIS_Request__c>();    
	        
		        //req1
		        NS_NIS_Request__c req1 = new NS_NIS_Request__c();
		        	req1.recordtypeid = rtRIUB;
		        	req1.provider_Tins__c = '111111111';
		        	req1.hce_Analyst__c = newUapp2.id;
		        	req1.HCE_Analyst_Status__c = 'Pending Final Rates Request';
		        	req1.HCE_Analyst_Closed_Date__c = system.today(); 
		        reqList.add(req1);
		 	
		 	insert reqList;
		 	
		 	
		 	
		 	req1.Rate_Negotiation_Type__c = 'Final Rates';
		 	update req1;
		 	
		 	
        	Attachment attach = new Attachment();
		        attach.Name='Unit Test Attachment';
		        Blob bodyBlob = Blob.valueOf('Unit Test Attachment Body');
		        attach.body = bodyBlob;
		        attach.parentId = req1.id;
	        insert attach;

		 	    
		 	
	}
}