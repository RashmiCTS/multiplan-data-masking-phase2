/*==================================================================================
File Name:   TestNetworkDevUserConfig.cls
Author:      Michael Olson
Date:        5/19/2015
Description: 

Modification History
5/19/2015       Michael Olson       Created
====================================================================================*/

@isTest
public class TestNetworkDevUserConfig {
 
    static testmethod void test1() {
        
        //---------------------------------------------------
        // Variables
        //---------------------------------------------------
         Id rtRateOps = Schema.SObjectType.NS_NIS_Request__c.getRecordTypeInfosByName().get('Rate Operations').getRecordTypeId();
        
        //---------------------------------------------------
        // Record Insert
        //---------------------------------------------------
        User newU = new User(); 
            newU.TimeZoneSidKey = 'America/New_York';   
            newU.LanguageLocaleKey = 'en_US';           
            newU.LocaleSidKey = 'en_US';            
            newU.EmailEncodingKey = 'UTF-8';                    
            newU.ProfileId = UserInfo.getProfileId() ;
            newU.Title  = 'testtitle';
            newU.FirstName = 'fname';
            newU.LastName = 'lname';
            newU.email = 'testtyfh@kjdhfk.com';
            newU.Username = 'tester@tester1111.com'; 
            newU.Alias = 'Alias11';
            newU.CommunityNickname =  'nickn';
            newU.Phone = '1234132';
            newU.Auto_Assign_Rate_Operation_Request_Type__c = 'Fee Schedule Request';
        insert newU; 
        
        NS_NIS_Request__c HceReq = new NS_NIS_Request__c();
            HceReq.Rate_Operations_Request_Type__c = 'Fee Schedule Request';
            HceReq.recordtypeId = rtRateOps;
            HceReq.Provider_Tins__c = '111111111';
            HceReq.Desired_Completion_Date__c = system.today() + 10;
            HceReq.account__c = 'x';
        insert HceReq; 
        
        
        //---------------------------------------------------
        // Start Tests
        //---------------------------------------------------
        NetworkDevUserConfig nduc = new NetworkDevUserConfig();
        PageReference pageRef = Page.NetworkDevUserConfig;
        Test.setCurrentPage(pageRef);
       
        list<User> U = nduc.configUsers;
        nduc.saveUsers();
        nduc.cancel();        
        nduc.test();
        
    }
}