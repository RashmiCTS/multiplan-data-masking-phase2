<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Proposal_request_is_completed_notify_rep</fullName>
        <description>Proposal request is completed - notify rep</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ApprovalTemplatesJulieOnly/Proposalrequestcompletenotifyrep</template>
    </alerts>
    <rules>
        <fullName>Proposal Request Complete Notify Rep</fullName>
        <actions>
            <name>Proposal_request_is_completed_notify_rep</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Proposal_Request__c.Status__c</field>
            <operation>equals</operation>
            <value>Complete</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Proposal Request for Andrea Washington</fullName>
        <actions>
            <name>ProposalRequestforAndreaWashington</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Proposal_Request__c.Proposal_Writer__c</field>
            <operation>equals</operation>
            <value>Andrea Washington</value>
        </criteriaItems>
        <description>Assign task to Andrea Washington if she is selected as Proposal Writer</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Proposal Request for Jodi Nagel</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Proposal_Request__c.Proposal_Writer__c</field>
            <operation>equals</operation>
            <value>Jodi Nagel</value>
        </criteriaItems>
        <description>Assign task to Jodi Nagel if she is selected as Proposal Writer</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Proposal Request for Lisa Peterson</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Proposal_Request__c.Proposal_Writer__c</field>
            <operation>equals</operation>
            <value>Lisa Peterson</value>
        </criteriaItems>
        <description>Assign task to Lisa Peterson if she is selected as Proposal Writer</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Proposal Request for Lori Cooke</fullName>
        <actions>
            <name>ProposalRequestforLoriCooke</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Proposal_Request__c.Proposal_Writer__c</field>
            <operation>equals</operation>
            <value>Lori Cooke</value>
        </criteriaItems>
        <description>Assign task to Lori Cooke if she is selected as Proposal Writer</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Proposal Request for Mark Souder</fullName>
        <actions>
            <name>ProposalRequestforMarkSouder</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Proposal_Request__c.Proposal_Writer__c</field>
            <operation>equals</operation>
            <value>Mark Souder</value>
        </criteriaItems>
        <description>Assign task to Mark Souder if he is selected as Proposal Writer</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Proposal Request for Nicole Konkel</fullName>
        <actions>
            <name>ProposalRequestforNicoleKonkel</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Proposal_Request__c.Proposal_Writer__c</field>
            <operation>equals</operation>
            <value>Nicole Konkel</value>
        </criteriaItems>
        <description>Assign task to Nicole Konkel if she is selected as Proposal Writer</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Proposal Request for Sarah Bajjaly</fullName>
        <actions>
            <name>ProposalRequestforSarahBajjaly</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Proposal_Request__c.Proposal_Writer__c</field>
            <operation>equals</operation>
            <value>Sarah Bajjaly</value>
        </criteriaItems>
        <description>Assign task to Sarah Bajjaly if she is selected as Proposal Writer</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>ProposalRequestforAndreaWashington</fullName>
        <assignedTo>andrea.washington@multiplan.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Proposal_Request__c.Due_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Proposal Request for Andrea Washington</subject>
    </tasks>
    <tasks>
        <fullName>ProposalRequestforLoriCooke</fullName>
        <assignedTo>lori.cooke@multiplan.com</assignedTo>
        <assignedToType>user</assignedToType>
        <description>Assign task to Lori when she is selected as Proposal Writer</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Proposal_Request__c.Due_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Proposal Request for Lori Cooke</subject>
    </tasks>
    <tasks>
        <fullName>ProposalRequestforMarkSouder</fullName>
        <assignedTo>mark.souder@multiplan.com</assignedTo>
        <assignedToType>user</assignedToType>
        <description>Assign task to Lori when she is selected as Proposal Writer</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Proposal_Request__c.Due_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Proposal Request for Mark Souder</subject>
    </tasks>
    <tasks>
        <fullName>ProposalRequestforNicoleKonkel</fullName>
        <assignedTo>nkonkel@multiplan.com</assignedTo>
        <assignedToType>user</assignedToType>
        <description>Assign task to Nicole when she is selected as Proposal Writer</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Proposal_Request__c.Due_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Proposal Request for Nicole Konkel</subject>
    </tasks>
    <tasks>
        <fullName>ProposalRequestforSarahBajjaly</fullName>
        <assignedTo>sarah.bajjaly@multiplan.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Proposal_Request__c.Due_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Proposal Request for Sarah Bajjaly</subject>
    </tasks>
</Workflow>
