<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <rules>
        <fullName>NS-Annual CPI Increase</fullName>
        <actions>
            <name>StartRenegotiationsCPIincreasedateis120DaysOut</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product_Rate__c.Annual_CPI_Increase__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>NS-Incomplete Contract</fullName>
        <actions>
            <name>ObtainMissingProviderInformation</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product_Rate__c.Negotiation_Stage__c</field>
            <operation>equals</operation>
            <value>Incomplete Contract/Application  Received - Awaiting Credentialing</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>NS-Renewal Initiation</fullName>
        <actions>
            <name>InitiateRenewalDiscussions</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product_Rate__c.Anniversary_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Product_Rate__c.Annual_CPI_Increase__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>CompleteTerminationPaperwork</fullName>
        <assignedToType>owner</assignedToType>
        <description>Complete appropriate Termination paperwork in accordance with Policies and Procedures.

Generate utilization reports for complete terminations. 

Complete termination notice to Account Management 

Complete Stat Form Document</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Product_Rate__c.Termination_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Complete Termination Paperwork</subject>
    </tasks>
    <tasks>
        <fullName>InitiateRenewalDiscussions</fullName>
        <assignedToType>owner</assignedToType>
        <description>Contact this Account regarding renewal of the Product specified.</description>
        <dueDateOffset>-120</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Product_Rate__c.Anniversary_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Initiate Renewal Discussions</subject>
    </tasks>
    <tasks>
        <fullName>ObtainMissingProviderInformation</fullName>
        <assignedToType>owner</assignedToType>
        <description>Follow up with this Provider to obtain missing information.  Also schedule follow up task if necessary</description>
        <dueDateOffset>3</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Obtain Missing Provider Information</subject>
    </tasks>
    <tasks>
        <fullName>StartRenegotiationsCPIincreasedateis120DaysOut</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>-120</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Product_Rate__c.CPI_Increase_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Start Renegotiations - CPI increase date is 120 Days Out</subject>
    </tasks>
</Workflow>
