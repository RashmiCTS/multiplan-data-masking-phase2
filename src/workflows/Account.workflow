<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Account_Assignment_Change_Angela</fullName>
        <description>Account Assignment Change - Angela</description>
        <protected>false</protected>
        <recipients>
            <recipient>angela.robarge@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Auto_Notifications/AA_Assignment_Change</template>
    </alerts>
    <alerts>
        <fullName>Account_Assignment_Change_Calvin</fullName>
        <description>Account Assignment Change - Calvin</description>
        <protected>false</protected>
        <recipients>
            <recipient>csun@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Auto_Notifications/AA_Assignment_Change</template>
    </alerts>
    <alerts>
        <fullName>Account_Assignment_Change_Cheryl</fullName>
        <description>Account Assignment Change - Cheryl</description>
        <protected>false</protected>
        <recipients>
            <recipient>cheryl.janotta@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Auto_Notifications/AA_Assignment_Change</template>
    </alerts>
    <alerts>
        <fullName>Account_Assignment_Change_Ellen</fullName>
        <description>Account Assignment Change - Ellen</description>
        <protected>false</protected>
        <recipients>
            <recipient>ellen.walker@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Auto_Notifications/AA_Assignment_Change</template>
    </alerts>
    <alerts>
        <fullName>Account_Assignment_Change_Greg</fullName>
        <description>Account Assignment Change - Greg</description>
        <protected>false</protected>
        <recipients>
            <recipient>greg.hanson@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Auto_Notifications/AA_Assignment_Change</template>
    </alerts>
    <alerts>
        <fullName>Account_Assignment_Change_Jess</fullName>
        <description>Account Assignment Change - Jess</description>
        <protected>false</protected>
        <recipients>
            <recipient>jessica.rosewarne@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Auto_Notifications/AA_Assignment_Change</template>
    </alerts>
    <alerts>
        <fullName>Account_Assignment_Change_Marion</fullName>
        <description>Account Assignment Change - Marion</description>
        <protected>false</protected>
        <recipients>
            <recipient>marion.garbarino@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Auto_Notifications/AA_Assignment_Change</template>
    </alerts>
    <alerts>
        <fullName>Account_Assignment_Change_Martha</fullName>
        <description>Account Assignment Change - Martha</description>
        <protected>false</protected>
        <recipients>
            <recipient>martha.gonzalez@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Auto_Notifications/AA_Assignment_Change</template>
    </alerts>
    <alerts>
        <fullName>Account_Assignment_Change_Matt</fullName>
        <description>Account Assignment Change - Matt</description>
        <protected>false</protected>
        <recipients>
            <recipient>matt.ursitti@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Auto_Notifications/AA_Assignment_Change</template>
    </alerts>
    <alerts>
        <fullName>Account_Assignment_Change_Susan</fullName>
        <description>Account Assignment Change - Susan</description>
        <protected>false</protected>
        <recipients>
            <recipient>susan.haufe@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Auto_Notifications/AA_Assignment_Change</template>
    </alerts>
    <alerts>
        <fullName>Account_Assignment_Change_Tina</fullName>
        <description>Account Assignment Change - Tina</description>
        <protected>false</protected>
        <recipients>
            <recipient>tina.smith@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Auto_Notifications/AA_Assignment_Change</template>
    </alerts>
    <alerts>
        <fullName>Sales_Acct_Creation_Notification</fullName>
        <description>Sales Acct Creation Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>csun@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Auto_Notifications/SalesAcctCreationNotification</template>
    </alerts>
    <fieldUpdates>
        <fullName>CloneServiceLocationZip</fullName>
        <field>Service_Location_Zip_Code__c</field>
        <formula>ShippingPostalCode</formula>
        <name>Clone Service Location Zip</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CloneShippingAddress</fullName>
        <field>Service_Location_Street__c</field>
        <formula>ShippingStreet</formula>
        <name>Clone Shipping Address</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CloneShippingCity</fullName>
        <description>Clones Shipping City if Services Location is the same</description>
        <field>Service_Location_City__c</field>
        <formula>ShippingCity</formula>
        <name>Clone Shipping City</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CloneShippingCountry</fullName>
        <description>Clones Shipping Country when the Service Location Country is the same</description>
        <field>Service_Location_Country__c</field>
        <formula>ShippingCountry</formula>
        <name>Clone Shipping Country</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CloneShippingLocationCounty</fullName>
        <description>Clones Shipping Location to County when Services Location County is the same</description>
        <field>Service_Loc_County__c</field>
        <formula>Shipping_County__c</formula>
        <name>Clone Shipping Location County</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CloneShippingState</fullName>
        <field>ShippingState</field>
        <formula>ShippingState</formula>
        <name>Clone Shipping State</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CloneShippingState_d_2</fullName>
        <description>Clones the Shipping State when the Services Location State is the same</description>
        <field>Service_Location_State__c</field>
        <formula>ShippingState</formula>
        <name>Clone ShippingState</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Segment_Update_BCBS</fullName>
        <field>Segment__c</field>
        <literalValue>BCBS</literalValue>
        <name>Segment Update: BCBS</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Segment_Update_Government</fullName>
        <description>Update client segment to &quot;Government&quot; when specific ownership is assigned to an account.</description>
        <field>Segment__c</field>
        <literalValue>Government</literalValue>
        <name>Segment Update: Government</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Segment_Update_National_Health_Plan</fullName>
        <description>Update client segment to &quot;National Health Plan&quot; when specific ownership is assigned to an account.</description>
        <field>Segment__c</field>
        <literalValue>National Health Plan</literalValue>
        <name>Segment Update: National Health Plan</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Segment_Update_Regional_Health_Plan</fullName>
        <description>Update client segment to &quot;Regional Health Plan&quot; when specific ownership is assigned to an account.</description>
        <field>Segment__c</field>
        <literalValue>Regional Health Plan</literalValue>
        <name>Segment Update: Regional Health Plan</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Segment_Update_TPA</fullName>
        <description>Update client segment to &quot;TPA&quot; when specific ownership is assigned to an account.</description>
        <field>Segment__c</field>
        <literalValue>TPA</literalValue>
        <name>Segment Update: TPA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Segment_Update_United_Family</fullName>
        <description>Update client segment to &quot;United Family&quot; when specific ownership is assigned to an account.</description>
        <field>Segment__c</field>
        <literalValue>United Family</literalValue>
        <name>Segment Update: United Family</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Segment_Update_WC_Auto</fullName>
        <description>Update client segment to &quot;WC &amp; Auto&quot; when specific ownership is assigned to an account.</description>
        <field>Segment__c</field>
        <literalValue>WC &amp; AM</literalValue>
        <name>Segment Update: WC &amp; Auto</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>AA Assignment to Angela</fullName>
        <actions>
            <name>Account_Assignment_Change_Angela</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Associate__c</field>
            <operation>equals</operation>
            <value>Angela Robarge</value>
        </criteriaItems>
        <description>Email notification of account assignment</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AA Assignment to Calvin</fullName>
        <actions>
            <name>Account_Assignment_Change_Calvin</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Associate__c</field>
            <operation>equals</operation>
            <value>Calvin Sun</value>
        </criteriaItems>
        <description>Email notification of account assignment</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AA Assignment to Cheryl</fullName>
        <actions>
            <name>Account_Assignment_Change_Cheryl</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Associate__c</field>
            <operation>equals</operation>
            <value>Cheryl Janotta</value>
        </criteriaItems>
        <description>Email notification of account assignment</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AA Assignment to Ellen</fullName>
        <actions>
            <name>Account_Assignment_Change_Ellen</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Associate__c</field>
            <operation>equals</operation>
            <value>Ellen Walker</value>
        </criteriaItems>
        <description>Email notification of account assignment</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AA Assignment to Greg</fullName>
        <actions>
            <name>Account_Assignment_Change_Greg</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Associate__c</field>
            <operation>equals</operation>
            <value>Greg Hanson</value>
        </criteriaItems>
        <description>Email notification of account assignment</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AA Assignment to Jess</fullName>
        <actions>
            <name>Account_Assignment_Change_Jess</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Associate__c</field>
            <operation>equals</operation>
            <value>Jessica Rosewarne</value>
        </criteriaItems>
        <description>Email notification of account assignment</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AA Assignment to Marion</fullName>
        <actions>
            <name>Account_Assignment_Change_Marion</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Associate__c</field>
            <operation>equals</operation>
            <value>Marion Brown</value>
        </criteriaItems>
        <description>Email notification of account assignment</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AA Assignment to Martha</fullName>
        <actions>
            <name>Account_Assignment_Change_Martha</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Associate__c</field>
            <operation>equals</operation>
            <value>Martha Gonzalez</value>
        </criteriaItems>
        <description>Email notification of account assignment</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AA Assignment to Matt</fullName>
        <actions>
            <name>Account_Assignment_Change_Matt</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Associate__c</field>
            <operation>equals</operation>
            <value>Matt Ursitti</value>
        </criteriaItems>
        <description>Email notification of account assignment</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AA Assignment to Susan</fullName>
        <actions>
            <name>Account_Assignment_Change_Susan</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Associate__c</field>
            <operation>equals</operation>
            <value>Susan Haufe</value>
        </criteriaItems>
        <description>Email notification of account assignment</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AA Assignment to Tina</fullName>
        <actions>
            <name>Account_Assignment_Change_Tina</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Associate__c</field>
            <operation>equals</operation>
            <value>Tina Smith</value>
        </criteriaItems>
        <description>Email notification of account assignment</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Clone Service Location City</fullName>
        <actions>
            <name>CloneShippingCity</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Copy_Shipping_Add_to_Service_Location__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <description>Clones Shipping City if Service Location City is the same</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Clone Service Location Zip</fullName>
        <actions>
            <name>CloneServiceLocationZip</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Copy_Shipping_Add_to_Service_Location__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Clone Shipping Address</fullName>
        <actions>
            <name>CloneShippingAddress</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1</booleanFilter>
        <criteriaItems>
            <field>Account.Copy_Shipping_Add_to_Service_Location__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <description>Clones Shipping Address if Service Location Address is the same</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Clone Shipping County</fullName>
        <actions>
            <name>CloneShippingLocationCounty</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Copy_Shipping_Add_to_Service_Location__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <description>Clones Shipping County when the Services Location County is the same</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Clone Shipping Location Country</fullName>
        <actions>
            <name>CloneShippingCountry</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Copy_Shipping_Add_to_Service_Location__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <description>Clones Shipping Location Country when the Services Location Country is the same</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Clone Shipping State</fullName>
        <actions>
            <name>CloneShippingState_d_2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Copy_Shipping_Add_to_Service_Location__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <description>Clones Shipping State if Service Location State is the same</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Sales Acct Creation Notification</fullName>
        <actions>
            <name>Sales_Acct_Creation_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Sales/Acct Mgmt</value>
        </criteriaItems>
        <description>Notify admin of a new acct created for sales</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Segment Update%3A BCBS</fullName>
        <actions>
            <name>Segment_Update_BCBS</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.OwnerId</field>
            <operation>contains</operation>
            <value>madarang</value>
        </criteriaItems>
        <description>Update client segment to &quot;BCBS&quot; when specific ownership is assigned to an account.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Segment Update%3A Government</fullName>
        <actions>
            <name>Segment_Update_Government</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Account.OwnerId</field>
            <operation>contains</operation>
            <value>tackett,scoggins,blasingame,macnamee,mujica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.OwnerId</field>
            <operation>equals</operation>
            <value>karen lovett</value>
        </criteriaItems>
        <description>Update client segment to &quot;Government&quot; when specific ownership is assigned to an account.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Segment Update%3A National Health Plan</fullName>
        <actions>
            <name>Segment_Update_National_Health_Plan</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.OwnerId</field>
            <operation>contains</operation>
            <value>slowik,armstrong,mazzanti,williams,theresa</value>
        </criteriaItems>
        <description>Update client segment to &quot;National Health Plan&quot; when specific ownership is assigned to an account.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Segment Update%3A Regional Health Plan</fullName>
        <actions>
            <name>Segment_Update_Regional_Health_Plan</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.OwnerId</field>
            <operation>contains</operation>
            <value>asquith,o&apos;neill,payleitner,addison,taratunio,joe</value>
        </criteriaItems>
        <description>Update client segment to &quot;Regional Health Plan&quot; when specific ownership is assigned to an account.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Segment Update%3A TPA</fullName>
        <actions>
            <name>Segment_Update_TPA</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2 OR 3 OR 4 OR 5</booleanFilter>
        <criteriaItems>
            <field>Account.OwnerId</field>
            <operation>contains</operation>
            <value>markus,anzivino,pusic,gallagher,sherry,thair,nuland</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.OwnerId</field>
            <operation>contains</operation>
            <value>carrillo,barry,courville,dugan,wallace,skipper</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.OwnerId</field>
            <operation>contains</operation>
            <value>jareczek,castillo,ferro</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.OwnerId</field>
            <operation>contains</operation>
            <value>nappi,adams,fisher,gonzalez,meyers,nofsinger,steiner</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.OwnerId</field>
            <operation>equals</operation>
            <value>cheryl lovett</value>
        </criteriaItems>
        <description>Update client segment to &quot;TPA&quot; when specific ownership is assigned to an account.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Segment Update%3A United Family</fullName>
        <actions>
            <name>Segment_Update_United_Family</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.OwnerId</field>
            <operation>contains</operation>
            <value>Kienzle,edwards,johnson</value>
        </criteriaItems>
        <description>Update client segment to &quot;United Family&quot; when specific ownership is assigned to an account.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Segment Update%3A WC %26 Auto</fullName>
        <actions>
            <name>Segment_Update_WC_Auto</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.OwnerId</field>
            <operation>contains</operation>
            <value>jacobson,amato,basel,cromwell,leggero,clark,trevis,Dellagrotta</value>
        </criteriaItems>
        <description>Update client segment to &quot;WC &amp; Auto&quot; when specific ownership is assigned to an account.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Total Child Account Parity Utilization Dollars</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Account.Provider_Type__c</field>
            <operation>equals</operation>
            <value>Individual</value>
        </criteriaItems>
        <description>Totals the Parity Utilization Dollars for the Child/ Individual Provider Accounts</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
