<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>PNAPPROVEDbySalesAMMgmtNetSvcstoreviewandapprovereject</fullName>
        <ccEmails>SFcommunication@viant.com</ccEmails>
        <description>PN APPROVED by Sales/AM Mgmt - Net Svcs to review and approve/reject</description>
        <protected>false</protected>
        <recipients>
            <field>Submitted_to_Network_Services_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ApprovalTemplatesJulieOnly/PNSalesAMMgmtApprovalreceivednotifyNetSvcsMgmt</template>
    </alerts>
    <alerts>
        <fullName>PNHasNSmgrassignedcontractor</fullName>
        <description>PN - Has NS mgr assigned contractor?</description>
        <protected>false</protected>
        <recipients>
            <recipient>aliciaescamilla@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>NSAutonotificationadminsonly/PNobjHastheNSmgrassignedacontractor</template>
    </alerts>
    <alerts>
        <fullName>PNNotificationtocontractorofPNassignment</fullName>
        <ccEmails>SFcommunication@viant.com</ccEmails>
        <description>PN Notification to contractor of PN assignment</description>
        <protected>false</protected>
        <recipients>
            <field>Assigned_Contractor__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ApprovalTemplatesJulieOnly/PNNetSvcsMgmtApprovalRECEIVEDnotifyContractor</template>
    </alerts>
    <alerts>
        <fullName>PNREJECTEDbyNSMgrinformSalesAMmgrofREJECTION</fullName>
        <ccEmails>SFcommunication@viant.com</ccEmails>
        <description>PN REJECTED by NS Mgr - inform Sales/AM mgr of REJECTION</description>
        <protected>false</protected>
        <recipients>
            <field>LastModifiedById</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ApprovalTemplatesJulieOnly/PNNetSvcsMgmtREJECTEDnotifySalesAMMgr</template>
    </alerts>
    <alerts>
        <fullName>PNapprovalbySalesAMmgmtnotifyrequestor</fullName>
        <ccEmails>sfcommunication@viant.com</ccEmails>
        <description>PN approval by Sales/AM mgmt - notify requestor</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ApprovalTemplatesJulieOnly/PNapprovalRECEIVEDfromSalesAMMgmt</template>
    </alerts>
    <alerts>
        <fullName>PNapprovedandassignedtocontractor</fullName>
        <ccEmails>sfcommunication@viant.com</ccEmails>
        <description>PN approved and assigned to contractor</description>
        <protected>false</protected>
        <recipients>
            <field>Assigned_Contractor__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Auto_Notifications/PNassignmentnotificationtoCONTRACTOR</template>
    </alerts>
    <alerts>
        <fullName>PNapprovedbyNSMgrnotifyrequestor</fullName>
        <ccEmails>sfcommunication@viant.com</ccEmails>
        <description>PN approved by NS Mgr - notify requestor</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ApprovalTemplatesJulieOnly/PNAPPROVEDbyNSMgmtassignedtocontractor</template>
    </alerts>
    <alerts>
        <fullName>PNapprovedbyNSmgmtbutnocontractorassigned</fullName>
        <ccEmails>sfcommunication@viant.com</ccEmails>
        <description>PN - approved by NS mgmt but no contractor assigned</description>
        <protected>false</protected>
        <recipients>
            <recipient>aliciaescamilla@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>audrey.holmes@viant.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>cheryl.zellner@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>katrina.tucker@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>keri.gluckman@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sandra.manning@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>NSAutonotificationadminsonly/PNobjHastheNSmgrassignedacontractor</template>
    </alerts>
    <alerts>
        <fullName>PNassignedtoNScontractorbyNSmgmt</fullName>
        <ccEmails>SFCommunication@viant.com</ccEmails>
        <description>PN assigned to NS contractor by NS mgmt</description>
        <protected>false</protected>
        <recipients>
            <field>Assigned_Contractor__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ApprovalTemplatesJulieOnly/PNNetSvcsMgmtApprovalRECEIVEDnotifyContractor</template>
    </alerts>
    <alerts>
        <fullName>PNassignedtocontractornotification</fullName>
        <ccEmails>sfcommunication@viant.com</ccEmails>
        <description>PN assigned to contractor notification</description>
        <protected>false</protected>
        <recipients>
            <field>Assigned_Contractor__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Auto_Notifications/PNassignmentnotificationtoCONTRACTOR</template>
    </alerts>
    <alerts>
        <fullName>SMEPNdidtheNSmgrassignacontractor</fullName>
        <description>SME - PN did the NS mgr assign a contractor?</description>
        <protected>false</protected>
        <recipients>
            <recipient>aliciaescamilla@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>NSAutonotificationadminsonly/PNobjHastheNSmgrassignedacontractor</template>
    </alerts>
    <alerts>
        <fullName>SalesAMmgmtProvNomREJECTEDnotifyrequestor</fullName>
        <ccEmails>sfcommunication@viant.com</ccEmails>
        <description>Sales/AM mgmt Prov Nom REJECTED-notify requestor</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ApprovalTemplatesJulieOnly/PNapprovalREJECTEDfromSalesAMMgmt</template>
    </alerts>
    <alerts>
        <fullName>notifycontractorofPNassignedtothem</fullName>
        <ccEmails>sfcommunication@viant.com</ccEmails>
        <description>notify contractor of PN assigned to them</description>
        <protected>false</protected>
        <recipients>
            <field>Assigned_Contractor__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ApprovalTemplatesJulieOnly/PNNetSvcsMgmtApprovalRECEIVEDnotifyContractor</template>
    </alerts>
    <fieldUpdates>
        <fullName>ClosedProviderNominationRecTypeUpd</fullName>
        <description>Updates the provider nomination record type to closed.</description>
        <field>RecordTypeId</field>
        <lookupValue>ClosedProviderNomination</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Closed Provider Nomination Rec Type Upd</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NSnotifiedofPNsubmitdatetoNSMgmt</fullName>
        <description>Date Network Services management was notified of Provider Nomination by Sales/AM Mgmt</description>
        <field>Submit_Date_to_Net_Svcs_Mgmt__c</field>
        <formula>TODAY()</formula>
        <name>NS notified of PN submit date to NS Mgmt</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NetSvcsApprovedDate</fullName>
        <description>Date NS Management approved nomination and assigned a contractor</description>
        <field>Approved_by_Network_Services_Management__c</field>
        <formula>TODAY()</formula>
        <name>Net Svcs Approved Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PNrejectedbyNSmgr</fullName>
        <field>Rejected_by_NS_Mgmt__c</field>
        <formula>TODAY()</formula>
        <name>PN rejected by NS mgr</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PNrejectedbySalesAMMgmt</fullName>
        <description>PN rejected by Sales/AM Mgmt</description>
        <field>Rejected_by_Sales_AM_Mgmt__c</field>
        <formula>today()</formula>
        <name>PN rejected by Sales/AM Mgmt</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SalesAMMgmtapproveddate</fullName>
        <description>Date Prov Nom was approved by Sales/AM mgmt</description>
        <field>Approved_by_SalesAM_Management__c</field>
        <formula>TODAY()</formula>
        <name>Sales/AM Mgmt approved date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SubmittedtoSalesAMMgmt</fullName>
        <description>Date the Prov. Nom. was submitted to the Sales/AM Mgmt staff</description>
        <field>Submitted_to_Sales_AM_Mgmt__c</field>
        <formula>TODAY()</formula>
        <name>Submitted to Sales/AM Mgmt</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Has NS mgr assigned PN to contractor%3F</fullName>
        <actions>
            <name>SMEPNdidtheNSmgrassignacontractor</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Provider_Nomination_Client_Exclusion__c.Approved_by_Network_Services_Management__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Provider_Nomination_Client_Exclusion__c.Assigned_Contractor__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Has the NS manager assigned the PN to a contractor?</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>PNapprovedbyNSmgmtbutnocontractorassigned</name>
                <type>Alert</type>
            </actions>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>PN notification contractor</fullName>
        <actions>
            <name>PNassignedtocontractornotification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Provider_Nomination_Client_Exclusion__c.Assigned_Contractor__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
