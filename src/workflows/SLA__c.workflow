<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>New_SLA_Notification</fullName>
        <description>New SLA Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>rebecca.smith@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Auto_Notifications/New_SLA_Notification</template>
    </alerts>
    <alerts>
        <fullName>Notification_for_SLA_Record_Modification</fullName>
        <description>Notification for SLA Record Modification</description>
        <protected>false</protected>
        <recipients>
            <recipient>rebecca.smith@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Auto_Notifications/SLA_Record_Update_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>Check_Active_checkbox</fullName>
        <field>Active__c</field>
        <literalValue>1</literalValue>
        <name>Check Active checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SLA_Status_Update_to_Active</fullName>
        <description>Set PC status to Active based on the term date entered</description>
        <field>Active__c</field>
        <literalValue>1</literalValue>
        <name>SLA Status Update to Active</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SLA_Status_Update_to_Inactive</fullName>
        <field>Active__c</field>
        <literalValue>0</literalValue>
        <name>SLA Status Update to Inactive</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_active_checkbox</fullName>
        <field>Active__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck active checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Check Active checkbox</fullName>
        <active>true</active>
        <criteriaItems>
            <field>SLA__c.Effective_Date__c</field>
            <operation>greaterThan</operation>
            <value>TODAY</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Check_Active_checkbox</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>SLA__c.Effective_Date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>New SLA Notification</fullName>
        <actions>
            <name>New_SLA_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>SLA__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Notify Becky Smith when a new SLA entry has been created.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SLA Record Modification</fullName>
        <actions>
            <name>Notification_for_SLA_Record_Modification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Notify Becky Smith when an SLA record has been modified.</description>
        <formula>OR (
ISCHANGED( Performance_Standard__c  ) , 
ISCHANGED( Penalty__c ) ,
ISCHANGED( Penalty_Payment_Method__c  ) ,
ISCHANGED( Cure_Period__c  )  ,
ISCHANGED( Other__c  ) ,
ISCHANGED( Effective_Date__c  ) ,
ISCHANGED( Networks_Products__c  ) ,
ISCHANGED( Termination_Date__c  ) ,
ISCHANGED( Tracked_by__c  ) ,
ISCHANGED( Type__c  ) ,
ISCHANGED( Reporting_Frequency_External__c  ) ,
ISCHANGED( Reporting_Frequency_Internal__c ) 
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Status Update - Active</fullName>
        <actions>
            <name>SLA_Status_Update_to_Active</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>SLA__c.Effective_Date__c</field>
            <operation>lessOrEqual</operation>
            <value>TODAY</value>
        </criteriaItems>
        <criteriaItems>
            <field>SLA__c.Termination_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>SLA__c.Termination_Date__c</field>
            <operation>greaterThan</operation>
            <value>TODAY</value>
        </criteriaItems>
        <description>Set record to active if it has an eff date in the past and (1) term date is blank, or (2) it has a future term date</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Status Update - Inactive</fullName>
        <actions>
            <name>SLA_Status_Update_to_Inactive</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>SLA__c.Effective_Date__c</field>
            <operation>greaterThan</operation>
            <value>TODAY</value>
        </criteriaItems>
        <criteriaItems>
            <field>SLA__c.Termination_Date__c</field>
            <operation>lessOrEqual</operation>
            <value>TODAY</value>
        </criteriaItems>
        <description>Set record to inactive if (1) it has a future effective date, or (2) it has a term date in the past</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Uncheck Active checkbox</fullName>
        <active>true</active>
        <criteriaItems>
            <field>SLA__c.Termination_Date__c</field>
            <operation>greaterThan</operation>
            <value>TODAY</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Uncheck_active_checkbox</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>SLA__c.Termination_Date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
