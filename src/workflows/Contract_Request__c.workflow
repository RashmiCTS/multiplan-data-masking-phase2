<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>CRwNOopptyREJECTEDbySalesAMmgrnotifyrequestor</fullName>
        <ccEmails>SFcommunication@multiplan.com</ccEmails>
        <description>CR w/ NO oppty REJECTED by Sales/AM mgr - notify requestor</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ApprovalTemplatesJulieOnly/CRSalesAMmgrREJECTEDreqnotifyrep</template>
    </alerts>
    <alerts>
        <fullName>CRwNOopptyapprovedbySalesAMmgmtnotifyTP</fullName>
        <ccEmails>ViantClientContracting@multiplan.com</ccEmails>
        <description>CR w/ NO oppty approved by Sales/AM mgmt notify TP</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>ApprovalTemplatesJulieOnly/CRSalesAMmgrAPPROVEDreqnotifyTracee</template>
    </alerts>
    <alerts>
        <fullName>CRwithnoopptymgmtreviewedandapproved</fullName>
        <ccEmails>ViantClientContracting@multiplan.com</ccEmails>
        <description>CR with no oppty - mgmt reviewed and approved</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Auto_Notifications/CRNotificationtoTraceewnoopptymgmtapproval</template>
    </alerts>
    <alerts>
        <fullName>INACTIVEHasrepsenttasktotheirmgaskingforapprovalontheirCRwnooppty</fullName>
        <ccEmails>sfcommunication@multiplan.com</ccEmails>
        <description>INACTIVE - Has rep sent task to their mg asking for approval on their CR w/ no oppty?</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Auto_Notifications/HastherepaskedforapprovalfrommgmtforaCRwnooppty</template>
    </alerts>
    <alerts>
        <fullName>Lite_Amendment_Reminder</fullName>
        <description>Lite Amendment Reminder</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>SMClientContractingDirector</recipient>
            <type>role</type>
        </recipients>
        <senderAddress>claudia.major@multiplan.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Workflow_Auto_Notifications/Lite_Amendment_Reminder</template>
    </alerts>
    <alerts>
        <fullName>NotifyRepofMgmtapprovalofCRwnooppty</fullName>
        <description>Notify Rep of Mgmt approval of CR w/ no oppty</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Auto_Notifications/MgmtapprovalofCRwnooppty</template>
    </alerts>
    <alerts>
        <fullName>NotifyreptheSalesAMmgrhasAPPROVEDtheirCR</fullName>
        <description>Notify rep the Sales/AM mgr has APPROVED their CR.</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ApprovalTemplatesJulieOnly/CRSalesAMmgrAPPROVEDreqnotifyrep</template>
    </alerts>
    <alerts>
        <fullName>WhenaCRisrequestedNotifyClientContracting</fullName>
        <ccEmails>Multiplan_Client_Con@MultiPlan.com</ccEmails>
        <description>When a CR is requested, notify Client Contracting</description>
        <protected>false</protected>
        <recipients>
            <recipient>claudia.major@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mthompson@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Auto_Notifications/ContractRequestNotification</template>
    </alerts>
    <fieldUpdates>
        <fullName>DateCRsubmittedtomanagerforapprova</fullName>
        <description>Date the CR was submitted to the reps manager for approval</description>
        <field>CR_submitted_to_Sale_AM_Mgr_for_approval__c</field>
        <formula>TODAY()</formula>
        <name>Date CR submitted to manager for approva</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DateCRwasapprovedbySalesAMmgmt</fullName>
        <description>Date the CR w/ NO oppty was approved by Sales/AM mgmt.</description>
        <field>CR_approval_date__c</field>
        <formula>TODAY()</formula>
        <name>Date CR was approved by Sales/AM mgmt</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Date_Update</fullName>
        <field>Status_Date__c</field>
        <formula>TODAY()</formula>
        <name>Status Date Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>CR Notification</fullName>
        <actions>
            <name>WhenaCRisrequestedNotifyClientContracting</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contract_Request__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>When a contract is requested, notify Client Contracting.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CR assigned to Karina Lupercio</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Contract_Request__c.CA_Assigned_to_CR__c</field>
            <operation>equals</operation>
            <value>karina lupercio</value>
        </criteriaItems>
        <description>Contract request assigned to Karina Lupercio</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CR for Adria Ward</fullName>
        <actions>
            <name>CRAssignedtoAdriaWard</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contract_Request__c.CA_Assigned_to_CR__c</field>
            <operation>equals</operation>
            <value>Adria Ward</value>
        </criteriaItems>
        <description>Notification of a CR request is assigned to Adria Ward</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CR for Eden Peterson</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Contract_Request__c.CA_Assigned_to_CR__c</field>
            <operation>contains</operation>
            <value>eden</value>
        </criteriaItems>
        <description>Notification of a CR request is assigned to Eden Chorm-Peterson</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CR for Grace Ramos</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Contract_Request__c.CA_Assigned_to_CR__c</field>
            <operation>equals</operation>
            <value>Grace Ramos</value>
        </criteriaItems>
        <description>Notification of a CR request is assigned to Grace Ramos</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CR for Jayne Funk</fullName>
        <actions>
            <name>CR_assigned_to_Jayne_Funk</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contract_Request__c.CA_Assigned_to_CR__c</field>
            <operation>equals</operation>
            <value>Jayne Funk</value>
        </criteriaItems>
        <description>Notification of a CR request is assigned to Jayne Funk</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CR for Jerry Dungan</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Contract_Request__c.CA_Assigned_to_CR__c</field>
            <operation>equals</operation>
            <value>Jerry Dungan</value>
        </criteriaItems>
        <description>Notification of a CR request is assigned to Jerry Dungan</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CR for Karen Zutant</fullName>
        <actions>
            <name>CRAssignedtoKarenZutant</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contract_Request__c.CA_Assigned_to_CR__c</field>
            <operation>equals</operation>
            <value>Karen Zutant</value>
        </criteriaItems>
        <description>Notification of a CR request is assigned to Karen Zutant</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CR for Melissa Corpus</fullName>
        <actions>
            <name>CR_Assigned_to_Melissa_Corpus</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contract_Request__c.CA_Assigned_to_CR__c</field>
            <operation>equals</operation>
            <value>Melissa Corpus</value>
        </criteriaItems>
        <description>Notification of a CR request is assigned to Melissa</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CR for Michelle Thompson</fullName>
        <actions>
            <name>CRAssignedtoMichelleThompson</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contract_Request__c.CA_Assigned_to_CR__c</field>
            <operation>equals</operation>
            <value>Michelle Thompson</value>
        </criteriaItems>
        <description>Notification of a CR request is assigned to Michelle Thompson</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CR for Stephanie Hunt</fullName>
        <actions>
            <name>CRAssignedtoStephanieHunt</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contract_Request__c.CA_Assigned_to_CR__c</field>
            <operation>equals</operation>
            <value>Stephanie Hunt</value>
        </criteriaItems>
        <description>Notification of a CR request is assigned to Stephanie</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CR for Suzanne Ban</fullName>
        <actions>
            <name>CRAssignedtoSuzanneBan</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Contract_Request__c.CA_Assigned_to_CR__c</field>
            <operation>equals</operation>
            <value>Suzanne Ban</value>
        </criteriaItems>
        <description>Notification of a CR request is assigned to Suzanne Ban</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CR request for Heather Elliott</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Contract_Request__c.CA_Assigned_to_CR__c</field>
            <operation>equals</operation>
            <value>Heather Elliott</value>
        </criteriaItems>
        <description>Notification of a CR request is assigned to Heather Elliott</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CR request for Maronica Gibson</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Contract_Request__c.CA_Assigned_to_CR__c</field>
            <operation>equals</operation>
            <value>maronica gibson</value>
        </criteriaItems>
        <description>Notification of a CR request is assigned to Maronica Gibson</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CR request for Penny Sedlacek</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Contract_Request__c.CA_Assigned_to_CR__c</field>
            <operation>equals</operation>
            <value>Penny Sedlacek</value>
        </criteriaItems>
        <description>Notification of a CR request is assigned to Penny Sedlacek</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CR request for Shari Long</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Contract_Request__c.CA_Assigned_to_CR__c</field>
            <operation>equals</operation>
            <value>Shari Long</value>
        </criteriaItems>
        <description>Notification of a CR request is assigned to Shari Long</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CR request for Shelly Axe</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Contract_Request__c.CA_Assigned_to_CR__c</field>
            <operation>equals</operation>
            <value>Shelly Axe</value>
        </criteriaItems>
        <description>Notification of a CR request is assigned to Shelly Axe</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CR request for Susan Kirk-Velez</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Contract_Request__c.CA_Assigned_to_CR__c</field>
            <operation>equals</operation>
            <value>Susan Kirk-Velez</value>
        </criteriaItems>
        <description>Notification of a CR request is assigned to Susan Kirk-Velez</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Contract Request Status Date Update</fullName>
        <actions>
            <name>Status_Date_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Auto-update contract request &quot;Status Date&quot; field when the &quot;Status&quot; field has been changed.</description>
        <formula>ISCHANGED( Status__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Has rep asked for CR approval from their manager%3F</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Contract_Request__c.Contract_Request_approved_by__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contract_Request__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Contract Request with NO Oppty</value>
        </criteriaItems>
        <description>Has the rep sent a task to their mgr asking for approval of their CR w/ NO oppty?</description>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>INACTIVEHasrepsenttasktotheirmgaskingforapprovalontheirCRwnooppty</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Contract_Request__c.CreatedDate</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Lite Amendment Reminder</fullName>
        <actions>
            <name>Lite_Amendment_Reminder</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contract_Request__c.Agreement_Type__c</field>
            <operation>equals</operation>
            <value>Lite Amendment</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contract_Request__c.Prepare_New_CSA__c</field>
            <operation>equals</operation>
            <value>No</value>
        </criteriaItems>
        <description>Reminder to requester when Lite Amendment is requested that a CSA followup is required.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify Client Cont of a CR w%2F no oppty-mgr approved</fullName>
        <actions>
            <name>CRwithnoopptymgmtreviewedandapproved</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Contract_Request__c.OwnerId</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Notification of mgr approved CR with no Oppty.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Notify Rep of CR w%2F no oppty-mgr approved</fullName>
        <actions>
            <name>NotifyRepofMgmtapprovalofCRwnooppty</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Contract_Request__c.Management_Approved__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contract_Request__c.Contract_Request_approved_by__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Notification of mgr approved CR with no Oppty to Sales/AM Rep</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>VP has CR Approvals outstanding</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Contract_Request__c.Contract_Request_approved_by__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contract_Request__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Contract Request with NO Oppty</value>
        </criteriaItems>
        <description>Has the VP approved or denied an approval request for a CR w/ NO oppty?</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>notify record owner when mgr has approved CR w%2F no oppty</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Contract_Request__c.Contract_Request_approved_by__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contract_Request__c.Management_Approved__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <description>notify record owner when their mgr has approved the CR associated w/ NO oppty</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>CRAssignedtoAdriaWard</fullName>
        <assignedTo>adria.ward@multiplan.com</assignedTo>
        <assignedToType>user</assignedToType>
        <description>Notification of CR assigned to Adria Ward</description>
        <dueDateOffset>3</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>CR Assigned to Adria Ward</subject>
    </tasks>
    <tasks>
        <fullName>CRAssignedtoKarenZutant</fullName>
        <assignedTo>karen.zutant@multiplan.com</assignedTo>
        <assignedToType>user</assignedToType>
        <description>Notification of CR assigned to Karen Zutant</description>
        <dueDateOffset>3</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>CR Assigned to Karent Zutant</subject>
    </tasks>
    <tasks>
        <fullName>CRAssignedtoMichelleThompson</fullName>
        <assignedTo>mthompson@multiplan.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>3</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>CR Assigned to Michelle Thompson</subject>
    </tasks>
    <tasks>
        <fullName>CRAssignedtoStephanieHunt</fullName>
        <assignedTo>stephanie.hunt@multiplan.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>3</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>CR Assigned to Stephanie Hunt</subject>
    </tasks>
    <tasks>
        <fullName>CRAssignedtoSuzanneBan</fullName>
        <assignedTo>suzanne.ban@multiplan.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>3</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>CR Assigned to Suzanne Ban</subject>
    </tasks>
    <tasks>
        <fullName>CR_Assigned_to_Melissa_Corpus</fullName>
        <assignedTo>mcorpus@multiplan.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>3</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>CR Assigned to Melissa Corpus</subject>
    </tasks>
    <tasks>
        <fullName>CR_assigned_to_Jayne_Funk</fullName>
        <assignedTo>jayne.funk@multiplan.com</assignedTo>
        <assignedToType>user</assignedToType>
        <description>Notification of a CR assigned to Jayne Funk</description>
        <dueDateOffset>3</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>CR assigned to Jayne Funk</subject>
    </tasks>
</Workflow>
