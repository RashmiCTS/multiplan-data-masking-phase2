<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Client_Enhancement_notification_to_AM_that_imp_mgr_has_been_assigned</fullName>
        <description>Client Enhancement notification to AM that imp mgr has been assigned</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Auto_Notifications/Notification_to_Acct_mgmt_of_Client_Imp_mgr_assigned_to_Client_Enhancement_req</template>
    </alerts>
    <alerts>
        <fullName>NotifyClientImpofEDIImprequest</fullName>
        <description>Notify Client Imp of EDI Imp request</description>
        <protected>false</protected>
        <recipients>
            <recipient>Account Associate</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Account Manager</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>SMSalesSupportManager</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>csun@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Auto_Notifications/ClientEnhancementNotification</template>
    </alerts>
    <fieldUpdates>
        <fullName>DateTimeCIinformed</fullName>
        <field>Date_Sales_Ops_Informed__c</field>
        <formula>Now()</formula>
        <name>Date/Time CI informed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Notification to AM of assigned imp mgr</fullName>
        <actions>
            <name>Client_Enhancement_notification_to_AM_that_imp_mgr_has_been_assigned</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Client_Enhancement__c.Implementation_Manager__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Notification of assignment of Cliet Implementation manager for Client Enhancement requests.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify Client Imp on EDI Req</fullName>
        <actions>
            <name>NotifyClientImpofEDIImprequest</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>DateTimeCIinformed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Client_Enhancement__c.Ready_for_Implementation__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <description>When a rep wants to notify Client Imp of an EDI Implementation request.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
