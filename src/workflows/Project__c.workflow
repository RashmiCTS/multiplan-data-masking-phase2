<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>NeedeTimecodenotificationtoJolyne</fullName>
        <description>Need eTime code - notification to Jolyne</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>jennifer.forcash@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Auto_Notifications/NeedeTimecode</template>
    </alerts>
    <alerts>
        <fullName>ProjectCreationNotification1</fullName>
        <description>Project Creation Notification 1</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <recipient>SMSalesSupportManager</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>csun@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Auto_Notifications/ProjectCreationNotification</template>
    </alerts>
    <fieldUpdates>
        <fullName>Description_Updates_Summary</fullName>
        <field>Description_Updates_Summary__c</field>
        <formula>&quot;[&quot; + LEFT(LastModifiedBy.FirstName, 1) + LastModifiedBy.LastName + &quot; &quot; + TEXT(LastModifiedDate) + &quot;]&quot; + BR() +
Description_Updates__c + BR() + BR() +
PRIORVALUE( Description_Updates_Summary__c )</formula>
        <name>Description/Updates Summary</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Description%2FUpdates Summary</fullName>
        <actions>
            <name>Description_Updates_Summary</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED( Description_Updates__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Description%2FUpdates Summary Initial Input</fullName>
        <actions>
            <name>Description_Updates_Summary</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Project__c.Description_Updates__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Need eTime code - Jolyne</fullName>
        <actions>
            <name>NeedeTimecodenotificationtoJolyne</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Project__c.Project_Category__c</field>
            <operation>equals</operation>
            <value>Expense to Company (Viant),Capital,Expense to Client (billable)</value>
        </criteriaItems>
        <description>Notification to Jolyne requesting eTime code</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Project Creation Notification</fullName>
        <actions>
            <name>ProjectCreationNotification1</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Project__c.SFDC_Project_Name__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>When a Project is created, notify Sales Support Manager.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
