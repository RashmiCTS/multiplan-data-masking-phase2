<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>PDE_Completed_Request_Notification</fullName>
        <description>PDE Completed Request Notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>LastModifiedById</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Auto_Notifications/PDE_Request_Completed</template>
    </alerts>
    <alerts>
        <fullName>SO_New_Request_Notification</fullName>
        <description>SO New Request Notification</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>csun@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rebecca.smith@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Auto_Notifications/New_PDE_Order_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>PDE_Date_Sent_to_Network_Admin</fullName>
        <field>Date_Sent_to_Network_Admin__c</field>
        <formula>today()</formula>
        <name>PDE Date Sent to Network Admin</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PDE_Record_Type_to_default</fullName>
        <field>RecordTypeId</field>
        <lookupValue>New_Order</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>PDE Record Type to default</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PDE_SO_Assigned_Update</fullName>
        <field>SO_Assigned_to__c</field>
        <formula>$User.FirstName &amp; &quot; &quot; &amp;  $User.LastName</formula>
        <name>PDE SO Assigned Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PDE_Status_to_Requested</fullName>
        <field>Status__c</field>
        <literalValue>Requested</literalValue>
        <name>PDE Status to Requested</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PDE_Submitted_to_False</fullName>
        <field>Submitted__c</field>
        <literalValue>0</literalValue>
        <name>PDE Submitted to False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Request_Date</fullName>
        <field>Request_Date__c</field>
        <formula>NOW()</formula>
        <name>Request Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>PDE Completed Request</fullName>
        <actions>
            <name>PDE_Completed_Request_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>PDE_Order__c.Status__c</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>PDE_Order__c.Close_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Email notification of completed request.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PDE Order Cancel</fullName>
        <actions>
            <name>PDE_Submitted_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>PDE_Order__c.Status__c</field>
            <operation>equals</operation>
            <value>Cancelled</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PDE Request Submission</fullName>
        <actions>
            <name>SO_New_Request_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>PDE_Status_to_Requested</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Request_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>PDE_Order__c.Submitted__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SO Assigned Update</fullName>
        <actions>
            <name>PDE_SO_Assigned_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>PDE_Order__c.Status__c</field>
            <operation>equals</operation>
            <value>In Process</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>New_PDE_Request</fullName>
        <assignedTo>csun@multiplan.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>PDE_Order__c.Close_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>New PDE Request</subject>
    </tasks>
</Workflow>
