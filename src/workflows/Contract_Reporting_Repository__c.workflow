<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>CRR_Benefit_Diff_Other_Default_Val</fullName>
        <description>If Benefit Differential Requirement is 10% or 20% then this field is populated with default value</description>
        <field>Benefit_Differential_Other__c</field>
        <formula>&quot;availability of Network Provider listings or financial incentives that provide Participants or Users with savings when health care services are obtained from Network Providers&quot;</formula>
        <name>CRR Benefit Diff Other Default Val</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CRR_Benefit_Diff_Other_Non_Default_Val</fullName>
        <description>If Benefit Differential Requirements is Financial Incentive/Shared Savings then value of this field is blank</description>
        <field>Benefit_Differential_Other__c</field>
        <name>CRR Benefit Diff Other Non Default Val</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CRR_Cont_PSAV_Cln_Claim_Lang_False_Val</fullName>
        <description>Update field Contractual PSAV Clean Claim Lang with blank value</description>
        <field>Contractual_PSAV_Clean_Claim_Lang__c</field>
        <name>CRR Cont PSAV Cln Claim Lang False Val</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CRR_Cont_PSAV_Cln_Claim_Lang_True_Val</fullName>
        <description>Update field Contractual PSAV Clean Claim Lang with default value</description>
        <field>Contractual_PSAV_Clean_Claim_Lang__c</field>
        <formula>&quot;Unless otherwise required by law, Clean Claim means a completed UB 04 or HCFA 1500 (or successor form), as appropriate, or other standard billing format containing all information for adjudication&quot;</formula>
        <name>CRR Cont PSAV Cln Claim Lang True Val</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CRR_PSAV_Benefits_Differential_False_Val</fullName>
        <description>If PSAV is FALSE then value of PSAV Benefits Differential field is blank</description>
        <field>PSAV_Benefits_Differential__c</field>
        <name>CRR PSAV Benefits Differential False Val</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CRR_PSAV_Benefits_Differntial_True_Val</fullName>
        <description>If PSAV is True, PSAV Benefits Differential has default value as mentioned</description>
        <field>PSAV_Benefits_Differential__c</field>
        <formula>&quot;To the extent permitted by applicable law, access the  PHCS Savility Network only for Benefit Programs that have, at minimum, twenty percent (20%) differential between in- and out- of network benefits, including coinsurance, deductibles and annual out-of-pocket maximums&quot;</formula>
        <name>CRR PSAV Benefits Differntial True Val</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CRR_PSAV_Other_False_Val</fullName>
        <description>If PSAV is not checked this field is blank</description>
        <field>PSAV_Other__c</field>
        <name>CRR PSAV Other False Val</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CRR_PSAV_Other_True_Val</fullName>
        <description>If PSAV is checked this field is populated with the default value</description>
        <field>PSAV_Other__c</field>
        <formula>&quot;Submission of claims timeframe: 60 days but in no event longer than 180 days. Collections of Deduct/Coinsurance may only be collected at the time of services Unless otherwise required by law, PHCS Savility Gp shall refund any overpayment made by PHCS Savility Participants within 15 days of becoming aware, whether by notice or otherwise, that an overpayment was made.Contractual prompt payment days: 30 days after receipt of the Clean Claim.365 days to submit disputes regarding payment after issuance of payment or notice of denial&quot;</formula>
        <name>CRR PSAV Other True Val</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>CRR  PSAV Benefits Differential False</fullName>
        <actions>
            <name>CRR_PSAV_Benefits_Differential_False_Val</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If PSAV is not checked on Agreement then it is blank</description>
        <formula>PSAV_On_Agreement__c = FALSE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CRR  PSAV Benefits Differential True</fullName>
        <actions>
            <name>CRR_PSAV_Benefits_Differntial_True_Val</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If PSAV is checked on Agreement then value of field PSAV Benefit Differential is populated with the default value</description>
        <formula>PSAV_On_Agreement__c = TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CRR Benefit Differential Other Default</fullName>
        <actions>
            <name>CRR_Benefit_Diff_Other_Default_Val</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Contract_Reporting_Repository__c.Benefit_Differential_Requirement__c</field>
            <operation>equals</operation>
            <value>10% benefit differential</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contract_Reporting_Repository__c.Benefit_Differential_Requirement__c</field>
            <operation>equals</operation>
            <value>20% benefit differential</value>
        </criteriaItems>
        <description>If Benefit Differential Requirement is 10% or 20% then Benefit Differential Other is populated with the default text.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CRR Benefit Differential Other Non Default</fullName>
        <actions>
            <name>CRR_Benefit_Diff_Other_Non_Default_Val</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contract_Reporting_Repository__c.Benefit_Differential_Requirement__c</field>
            <operation>equals</operation>
            <value>Financial Incentive/Shared Savings</value>
        </criteriaItems>
        <description>If Benefit Differential Requirement is Financial Incentive/Shared Savings
then this field is blank</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CRR Contract PSAV Cln Claim Lang False</fullName>
        <actions>
            <name>CRR_Cont_PSAV_Cln_Claim_Lang_False_Val</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If PSAV is not checked on Agreement then it is blank</description>
        <formula>PSAV_On_Agreement__c = FALSE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CRR Contract PSAV Cln Claim Lang True</fullName>
        <actions>
            <name>CRR_Cont_PSAV_Cln_Claim_Lang_True_Val</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If PSAV is checked on Agreement then value of field Contractual PSAV Clean Claim Lang is the default value given</description>
        <formula>PSAV_On_Agreement__c = TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CRR PSAV Other False</fullName>
        <actions>
            <name>CRR_PSAV_Other_False_Val</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contract_Reporting_Repository__c.PSAV_On_Agreement__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>If PSAV on Agreement is not checked then this field is blank</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CRR PSAV Other True</fullName>
        <actions>
            <name>CRR_PSAV_Other_True_Val</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contract_Reporting_Repository__c.PSAV_On_Agreement__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>If PSAV on Agreement is checked then this field is populated with default value</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
