<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>ProviderTermAPPROVED</fullName>
        <ccEmails>SFcommunication@viant.com</ccEmails>
        <description>Provider Term - APPROVED</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>NSAutonotificationadminsonly/Prov_Term_APPROVED</template>
    </alerts>
    <alerts>
        <fullName>ProviderTermREJECTED</fullName>
        <description>Provider Term - REJECTED</description>
        <protected>false</protected>
        <recipients>
            <field>Negotiator__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Apttus__ApttusEmailTemplates/Termination_Approval_Reject</template>
    </alerts>
    <alerts>
        <fullName>Term_Record_RD_Approved</fullName>
        <description>Term Record RD Approved</description>
        <protected>false</protected>
        <recipients>
            <field>ParaLegal__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Apttus__ApttusEmailTemplates/Termination_Approval_Submit</template>
    </alerts>
    <alerts>
        <fullName>Termination_Internal_Notice</fullName>
        <ccEmails>Vamshi.takkalapalli@multiplan.com</ccEmails>
        <description>Termination Internal Notice</description>
        <protected>false</protected>
        <recipients>
            <field>Negotiator__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>ParaLegal__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Regional_Director__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Apttus__ApttusEmailTemplates/Termination_Internal_Notice</template>
    </alerts>
    <alerts>
        <fullName>Termination_submitted_for_approval</fullName>
        <ccEmails>vamshi.takkalapalli@multiplan.com</ccEmails>
        <description>Termination submitted for approval</description>
        <protected>false</protected>
        <recipients>
            <field>Regional_Director__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Apttus__ApttusEmailTemplates/Termination_Approval_Submit</template>
    </alerts>
    <fieldUpdates>
        <fullName>Date_submitted_to_manager</fullName>
        <field>Date_submitted_for_approval__c</field>
        <formula>today()</formula>
        <name>Date submitted to manager</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Final_Approval_Status_Update</fullName>
        <field>Workflow_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Final Approval Status Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Final_Rejection_Status_update</fullName>
        <field>Workflow_Status__c</field>
        <literalValue>Approval Rejected - Paralegal</literalValue>
        <name>Final Rejection Status update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Provider_Term_Read_only</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Provider_Term_Read_Only</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Provider Term Read only</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RD_field_Update</fullName>
        <field>Regional_Director__c</field>
        <name>RD field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RD_rejected_Status_Update</fullName>
        <field>Workflow_Status__c</field>
        <literalValue>Approval Rejected - RD</literalValue>
        <name>RD rejected Status Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recall_Status_Update</fullName>
        <field>Workflow_Status__c</field>
        <literalValue>Initiated</literalValue>
        <name>Recall Status Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Provider_Term_Read_Only</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Workflow_Status_Update</fullName>
        <field>Workflow_Status__c</field>
        <literalValue>In-Approval</literalValue>
        <name>Workflow Status Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Provider Termination Read only</fullName>
        <actions>
            <name>Provider_Term_Read_only</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Provider_Termination__c.Workflow_Status__c</field>
            <operation>equals</operation>
            <value>Approved,Terminated,Rescinded,Reversed</value>
        </criteriaItems>
        <description>Update the record type of the provider termination to read only layout</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
