<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Analytic_Request_Geo_Request_Assigned_to_Jenny_Santos</fullName>
        <ccEmails>Georequests@multiplan.com</ccEmails>
        <description>Analytic Request - Geo Request Assigned to Jenny Santos</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Auto_Notifications/Analytic_Request_Geo_Request_Assigned_to_Jenny_Santos</template>
    </alerts>
    <alerts>
        <fullName>Completed_Analytic_request_notify_requestor_and_Account_Owner</fullName>
        <description>Completed Analytic request - notify requestor and Account Owner</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Auto_Notifications/Analytic_request_complete_notify_rep</template>
    </alerts>
    <alerts>
        <fullName>Confirmation_of_a_repricing_analysis_submitted</fullName>
        <description>Confirmation of a repricing analysis submitted</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Auto_Notifications/Repricing_Analysis_Request_Confirmation</template>
    </alerts>
    <alerts>
        <fullName>Disruption_request_for_HCE_create_task_e_mail</fullName>
        <ccEmails>S&amp;DRequests@multiplan.com</ccEmails>
        <description>Disruption request for HCE - create task &amp; e-mail alert</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Auto_Notifications/Analytic_Request_S_D_Request</template>
    </alerts>
    <alerts>
        <fullName>Disruption_request_for_IHP_create_task_email_alert</fullName>
        <ccEmails>S&amp;DRequests@multiplan.com</ccEmails>
        <description>Disruption request for IHP - create task &amp; email alert</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Auto_Notifications/Analytic_Request_S_D_Request</template>
    </alerts>
    <alerts>
        <fullName>Email_alert_for_analytic_request_assigned_to_Brenda</fullName>
        <description>Email alert for analytic request assigned to Brenda</description>
        <protected>false</protected>
        <recipients>
            <recipient>brenda.binns@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Auto_Notifications/Analytic_Request_Assigned_to_Analyst</template>
    </alerts>
    <alerts>
        <fullName>Email_alert_for_analytic_request_assigned_to_Caren</fullName>
        <description>Email alert for analytic request assigned to Caren</description>
        <protected>false</protected>
        <recipients>
            <recipient>caren.connelly@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Auto_Notifications/Analytic_Request_Assigned_to_Analyst</template>
    </alerts>
    <alerts>
        <fullName>Email_alert_for_analytic_request_assigned_to_John</fullName>
        <description>Email alert for analytic request assigned to John</description>
        <protected>false</protected>
        <recipients>
            <recipient>john.vena@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Auto_Notifications/Analytic_Request_Assigned_to_Analyst</template>
    </alerts>
    <alerts>
        <fullName>Email_alert_for_analytic_request_assigned_to_Nicole</fullName>
        <description>Email alert for analytic request assigned to Nicole</description>
        <protected>false</protected>
        <recipients>
            <recipient>nkonkel@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Auto_Notifications/Analytic_Request_Assigned_to_Analyst</template>
    </alerts>
    <alerts>
        <fullName>Email_alert_for_analytic_request_assigned_to_Raul</fullName>
        <description>Email alert for analytic request assigned to Raul</description>
        <protected>false</protected>
        <recipients>
            <recipient>raul.mercado@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Auto_Notifications/Analytic_Request_Assigned_to_Analyst</template>
    </alerts>
    <alerts>
        <fullName>Email_alert_for_analytic_request_assigned_to_Sarah</fullName>
        <description>Email alert for analytic request assigned to Sarah</description>
        <protected>false</protected>
        <recipients>
            <recipient>sarah.bajjaly@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Auto_Notifications/Analytic_Request_Assigned_to_Analyst</template>
    </alerts>
    <alerts>
        <fullName>NotificationtoAndreaofHCEreq</fullName>
        <ccEmails>SFcommunication@viant.com</ccEmails>
        <description>Notification to Andrea of HCE req</description>
        <protected>false</protected>
        <recipients>
            <recipient>andrea.washington@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Auto_Notifications/NotificationtoAndrea</template>
    </alerts>
    <fieldUpdates>
        <fullName>CloneResetAnalyst</fullName>
        <field>HCE_Analyst__c</field>
        <name>CloneResetAnalyst</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CloneResetClosedDate</fullName>
        <field>Closed_Date__c</field>
        <name>CloneResetClosedDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CloneResetCompleteData</fullName>
        <field>Complete_Data__c</field>
        <literalValue>0</literalValue>
        <name>CloneResetCompleteData</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CloneResetExplanation</fullName>
        <field>Explanation__c</field>
        <name>CloneResetExplanation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CloneResetModifiedDueDate</fullName>
        <field>HCE_Modified_Due_Date__c</field>
        <name>CloneResetModifiedDueDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CloneResetStatus</fullName>
        <field>Status__c</field>
        <literalValue>Not Started</literalValue>
        <name>CloneResetStatus</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RushApprovalHold</fullName>
        <field>Status__c</field>
        <literalValue>On Hold for RUSH approval</literalValue>
        <name>Rush Approval Hold</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RushApproved</fullName>
        <field>Status__c</field>
        <literalValue>Not Started</literalValue>
        <name>Rush Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RushRejected</fullName>
        <field>Status__c</field>
        <literalValue>Canceled</literalValue>
        <name>Rush Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Ad Hoc Report%2FMap request for HCE - create task</fullName>
        <actions>
            <name>AdHocReprtMapRequest</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Analytic_Request__c.Refer_to_HCE__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Analytic_Request__c.Service_Requested_Proposals_Development__c</field>
            <operation>includes</operation>
            <value>Ad Hoc Report/Map</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Ad Hoc Report%2FMap request for IHP - create task</fullName>
        <actions>
            <name>AdHocReprtMapRequest_IHP</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Analytic_Request__c.Refer_to_IHP__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Analytic_Request__c.Service_Requested_Proposals_Development__c</field>
            <operation>includes</operation>
            <value>Ad Hoc Report/Map</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Analytic Request Assigned to Brenda</fullName>
        <actions>
            <name>Email_alert_for_analytic_request_assigned_to_Brenda</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Analytic_Request_Assigned_to_Brenda_Binns</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Analytic_Request__c.Assigned_Analyst__c</field>
            <operation>equals</operation>
            <value>Brenda Binns</value>
        </criteriaItems>
        <description>Assigns a task to Brenda and sends email alert when an analytic request is created with her name selected as the assigned analyst</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Analytic Request Assigned to Caren</fullName>
        <actions>
            <name>Email_alert_for_analytic_request_assigned_to_Caren</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Analytic_Request_Assigned_to_Caren_Connelly</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Analytic_Request__c.Assigned_Analyst__c</field>
            <operation>equals</operation>
            <value>Caren Connelly</value>
        </criteriaItems>
        <description>Assigns a task to Caren and sends email alert when an analytic request is created with her name selected as the assigned analyst</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Analytic Request Assigned to John</fullName>
        <actions>
            <name>Email_alert_for_analytic_request_assigned_to_John</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Analytic_Request_Assigned_to_John_Vena</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Analytic_Request__c.Assigned_Analyst__c</field>
            <operation>equals</operation>
            <value>John Vena</value>
        </criteriaItems>
        <description>Assigns a task to John and sends email alert when an analytic request is created with his name selected as the assigned analyst</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Analytic Request Assigned to Nicole</fullName>
        <actions>
            <name>Email_alert_for_analytic_request_assigned_to_Nicole</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Analytic_Request_Assigned_to_Nicole_Konkel</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Analytic_Request__c.Assigned_Analyst__c</field>
            <operation>equals</operation>
            <value>Nicole Konkel</value>
        </criteriaItems>
        <description>Assigns a task to Nicole and sends email alert when an analytic request is created with her name selected as the assigned analyst</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Analytic Request Assigned to Raul</fullName>
        <actions>
            <name>Email_alert_for_analytic_request_assigned_to_Raul</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Analytic_Request_Assigned_to_Raul_Mercado</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Analytic_Request__c.Assigned_Analyst__c</field>
            <operation>equals</operation>
            <value>Raul Mercado</value>
        </criteriaItems>
        <description>Assigns a task to Raul and sends email alert when an analytic request is created with his name selected as the assigned analyst</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Analytic Request Assigned to Sara</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Analytic_Request__c.Assigned_Analyst__c</field>
            <operation>equals</operation>
            <value>Sara Cole</value>
        </criteriaItems>
        <description>Assigns a task to Sara and sends email alert when an analytic request is created with her name selected as the assigned analyst</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Analytic Request Assigned to Sarah</fullName>
        <actions>
            <name>Email_alert_for_analytic_request_assigned_to_Sarah</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Analytic_Request_Assigned_to_Sarah_Bajjaly</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Analytic_Request__c.Assigned_Analyst__c</field>
            <operation>equals</operation>
            <value>Sarah Bajjaly</value>
        </criteriaItems>
        <description>Assigns a task to Sarah and sends email alert when an analytic request is created with her name selected as the assigned analyst</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Analytic Request Complete - Notify Rep</fullName>
        <actions>
            <name>Completed_Analytic_request_notify_requestor_and_Account_Owner</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Analytic_Request__c.Status__c</field>
            <operation>equals</operation>
            <value>Complete</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Analytic request for TTC - create tasks</fullName>
        <actions>
            <name>TTCAnalyticRequest_AW</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Analytic_Request__c.Refer_to_HCE__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Analytic_Request__c.TTC__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CPT Analysis request for HCE - create task</fullName>
        <actions>
            <name>CPTAnalysisRequest</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Analytic_Request__c.Refer_to_HCE__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Analytic_Request__c.Service_Requested_Proposals_Development__c</field>
            <operation>includes</operation>
            <value>CPT Analysis</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CPT Analysis request for IHP - create task</fullName>
        <actions>
            <name>CPTAnalysisRequest_IHP</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Analytic_Request__c.Refer_to_IHP__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Analytic_Request__c.Service_Requested_Proposals_Development__c</field>
            <operation>includes</operation>
            <value>CPT Analysis</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Clone Resets</fullName>
        <actions>
            <name>CloneResetAnalyst</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CloneResetClosedDate</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CloneResetCompleteData</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CloneResetExplanation</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CloneResetModifiedDueDate</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CloneResetStatus</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Analytic_Request__c.Status__c</field>
            <operation>equals</operation>
            <value>Complete,In Process,On Hold,Canceled</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Conversion Analysis request for HCE - create task</fullName>
        <actions>
            <name>Analytic_Request_Conversion_Analysis_HCE</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Analytic_Request__c.Refer_to_HCE__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Analytic_Request__c.Service_Requested_Proposals_Development__c</field>
            <operation>includes</operation>
            <value>Conversion Analysis</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Conversion Analysis request for IHP - create task</fullName>
        <actions>
            <name>Analytic_Request_Conversion_Analysis_IHP</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Analytic_Request__c.Refer_to_IHP__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Analytic_Request__c.Service_Requested_Proposals_Development__c</field>
            <operation>includes</operation>
            <value>Conversion Analysis</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>DIS Analysis request for NCN - create task</fullName>
        <actions>
            <name>Analytic_Request_NCN</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Analytic_Request__c.Refer_to_NCN__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Disruption request for HCE - create task</fullName>
        <actions>
            <name>Disruption_request_for_HCE_create_task_e_mail</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>DisruptionAnalyticRequest</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Analytic_Request__c.Refer_to_HCE__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Analytic_Request__c.Service_Requested_Proposals_Development__c</field>
            <operation>includes</operation>
            <value>Disruption Report</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Disruption request for IHP - create task</fullName>
        <actions>
            <name>Disruption_request_for_IHP_create_task_email_alert</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>DisruptionAnalyticRequest_IHP</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Analytic_Request__c.Refer_to_IHP__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Analytic_Request__c.Service_Requested_Proposals_Development__c</field>
            <operation>includes</operation>
            <value>Disruption Report</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>GEO Request for HCE - create task %26 email alert</fullName>
        <actions>
            <name>Analytic_Request_Geo_Request_Assigned_to_Jenny_Santos</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Analytic_Request_Geo_JS</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Analytic_Request__c.Refer_to_GEO_Requests__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>When a request is assigned to HCE, a task will be created and assigned to the HCE Analytics group, and an email alert will be sent to Georequests@multiplan.com mailbox.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>GEO request for HCE - create task</fullName>
        <actions>
            <name>GeoAnalyticRequest</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Analytic_Request__c.Refer_to_HCE__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Analytic_Request__c.Service_Requested_Proposals_Development__c</field>
            <operation>includes</operation>
            <value>Geographic Analysis</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>GEO request for IHP - create task</fullName>
        <actions>
            <name>GeoAnalyticRequest_IHP</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Analytic_Request__c.Refer_to_IHP__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Analytic_Request__c.Service_Requested_Proposals_Development__c</field>
            <operation>includes</operation>
            <value>Geographic Analysis</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notification to Andrea</fullName>
        <actions>
            <name>NotificationtoAndreaofHCEreq</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Analytic_Request__c.Service_Requested_Proposals_Development__c</field>
            <operation>includes</operation>
            <value>Ad Hoc Report/Map,Disruption Report,Project,CPT Analysis,Geographic Analysis,Repricing Analysis</value>
        </criteriaItems>
        <criteriaItems>
            <field>Analytic_Request__c.TTC__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Notification to Andrea Washington of Disruption, GeoRepricing analysis, CPT analysis, project or adhoc HCE requests</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notification to Maneekah</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Analytic_Request__c.Service_Requested_Proposals_Development__c</field>
            <operation>includes</operation>
            <value>Ad Hoc Report/Map,Project,CPT Analysis,Savings Analysis,Repricing Analysis</value>
        </criteriaItems>
        <criteriaItems>
            <field>Analytic_Request__c.TTC__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Notification to Maneekah of Savings/repricing or ad-hoc report</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Refer to MARS</fullName>
        <actions>
            <name>Clinical_Negotiation_Analytics_Requested</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Analytic_Request__c.Refer_to_MARS__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Repricing Analysis request for HCE - create task</fullName>
        <actions>
            <name>RepricingAnalyticRequest</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Analytic_Request__c.Refer_to_HCE__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Analytic_Request__c.Service_Requested_Proposals_Development__c</field>
            <operation>includes</operation>
            <value>Repricing Analysis</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Repricing Analysis request for IHP - create task</fullName>
        <actions>
            <name>RepricingAnalyticRequest_IHP</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Analytic_Request__c.Refer_to_IHP__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Analytic_Request__c.Service_Requested_Proposals_Development__c</field>
            <operation>includes</operation>
            <value>Repricing Analysis</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Repricing analysis request confirmation</fullName>
        <actions>
            <name>Confirmation_of_a_repricing_analysis_submitted</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Analytic_Request__c.VP_Approved__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Analytic_Request__c.Number_of_Claims__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Analytic_Request__c.Claims_Time_Period__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Send confirmation email to requester once a repricing analysis request has been received.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Savings request for HCE - create task</fullName>
        <actions>
            <name>AnalyticRequestSavings</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Analytic_Request__c.Refer_to_HCE__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Analytic_Request__c.Service_Requested_Proposals_Development__c</field>
            <operation>includes</operation>
            <value>Savings Analysis</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Savings request for IHP - create task</fullName>
        <actions>
            <name>AnalyticRequestSavings_IHP</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Analytic_Request__c.Refer_to_IHP__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Analytic_Request__c.Service_Requested_Proposals_Development__c</field>
            <operation>includes</operation>
            <value>Savings Analysis</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Savings-NotifyBReagan</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Analytic_Request__c.Service_Requested__c</field>
            <operation>equals</operation>
            <value>Savings Analysis</value>
        </criteriaItems>
        <criteriaItems>
            <field>Analytic_Request__c.Core_Services__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Waste%2FAbuse Analysis request for HCE - create task</fullName>
        <actions>
            <name>Analytic_Request_Waste_Abuse_Analysis_HCE</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Analytic_Request__c.Refer_to_HCE__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Analytic_Request__c.Service_Requested_Proposals_Development__c</field>
            <operation>includes</operation>
            <value>Waste/Abuse Non-Par Claim Analysis</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>AdHocReprtMapRequest</fullName>
        <assignedTo>evan.eckerle@multiplan.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>7</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Analytic_Request__c.Date_Request_Received__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Analytic Request - Ad Hoc Report/Map - HCE</subject>
    </tasks>
    <tasks>
        <fullName>AdHocReprtMapRequest_IHP</fullName>
        <assignedTo>brenda.binns@multiplan.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>7</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Analytic_Request__c.Date_Request_Received__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Analytic Request - Ad Hoc Report/Map - IHP</subject>
    </tasks>
    <tasks>
        <fullName>AnalyticRequestSavings</fullName>
        <assignedTo>evan.eckerle@multiplan.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>12</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Analytic_Request__c.Date_Request_Received__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Analytic Request - Savings - HCE</subject>
    </tasks>
    <tasks>
        <fullName>AnalyticRequestSavings_IHP</fullName>
        <assignedTo>brenda.binns@multiplan.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>12</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Analytic_Request__c.Date_Request_Received__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Analytic Request - Savings - IHP</subject>
    </tasks>
    <tasks>
        <fullName>Analytic_Request_Assigned_to_Brenda_Binns</fullName>
        <assignedTo>brenda.binns@multiplan.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Analytic_Request__c.Requested_Completion_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Analytic Request Assigned to Brenda Binns</subject>
    </tasks>
    <tasks>
        <fullName>Analytic_Request_Assigned_to_Caren_Connelly</fullName>
        <assignedTo>caren.connelly@multiplan.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Analytic_Request__c.Requested_Completion_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Analytic Request Assigned to Caren Connelly</subject>
    </tasks>
    <tasks>
        <fullName>Analytic_Request_Assigned_to_John_Vena</fullName>
        <assignedTo>john.vena@multiplan.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Analytic_Request__c.Requested_Completion_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Analytic Request Assigned to John Vena</subject>
    </tasks>
    <tasks>
        <fullName>Analytic_Request_Assigned_to_Nicole_Konkel</fullName>
        <assignedTo>nkonkel@multiplan.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Analytic_Request__c.Requested_Completion_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Analytic Request Assigned to Nicole Konkel</subject>
    </tasks>
    <tasks>
        <fullName>Analytic_Request_Assigned_to_Raul_Mercado</fullName>
        <assignedTo>raul.mercado@multiplan.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Analytic_Request__c.Requested_Completion_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Analytic Request Assigned to Raul Mercado</subject>
    </tasks>
    <tasks>
        <fullName>Analytic_Request_Assigned_to_Sarah_Bajjaly</fullName>
        <assignedTo>sarah.bajjaly@multiplan.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Analytic_Request__c.Requested_Completion_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Analytic Request Assigned to Sarah Bajjaly</subject>
    </tasks>
    <tasks>
        <fullName>Analytic_Request_Conversion_Analysis_HCE</fullName>
        <assignedTo>evan.eckerle@multiplan.com</assignedTo>
        <assignedToType>user</assignedToType>
        <description>Please pull Beech claims data for a Conversion Analysis.</description>
        <dueDateOffset>5</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Analytic_Request__c.Date_Request_Received__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Analytic Request  - Conversion Analysis - HCE</subject>
    </tasks>
    <tasks>
        <fullName>Analytic_Request_Conversion_Analysis_IHP</fullName>
        <assignedTo>brenda.binns@multiplan.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>12</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Analytic_Request__c.Date_Request_Received__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Analytic Request  - Conversion Analysis - IHP</subject>
    </tasks>
    <tasks>
        <fullName>Analytic_Request_Geo_JS</fullName>
        <assignedTo>evan.eckerle@multiplan.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>7</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Analytic_Request__c.Date_Request_Received__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Analytic Request - Geo Assigned to HCE Analytics</subject>
    </tasks>
    <tasks>
        <fullName>Analytic_Request_NCN</fullName>
        <assignedTo>tom.ralston@multiplan.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>12</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Analytic_Request__c.Date_Request_Received__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Analytic Request - Assigned to NCN</subject>
    </tasks>
    <tasks>
        <fullName>Analytic_Request_Waste_Abuse_Analysis_HCE</fullName>
        <assignedTo>evan.eckerle@multiplan.com</assignedTo>
        <assignedToType>user</assignedToType>
        <description>Please pull Beech claims data for a Conversion Analysis.</description>
        <dueDateOffset>12</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Analytic_Request__c.Date_Request_Received__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Analytic Request - Waste/Abuse Analysis - HCE</subject>
    </tasks>
    <tasks>
        <fullName>CPTAnalysisRequest</fullName>
        <assignedTo>evan.eckerle@multiplan.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>7</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Analytic_Request__c.Date_Request_Received__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Analytic Request - CPT Analysis - HCE</subject>
    </tasks>
    <tasks>
        <fullName>CPTAnalysisRequest_IHP</fullName>
        <assignedTo>brenda.binns@multiplan.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>7</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Analytic_Request__c.Date_Request_Received__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Analytic Request - CPT Analysis - IHP</subject>
    </tasks>
    <tasks>
        <fullName>Clinical_Negotiation_Analytics_Requested</fullName>
        <assignedTo>garyepstein@multiplan.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>28</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Analytic_Request__c.Date_Request_Received__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Clinical Negotiation Analytics Requested</subject>
    </tasks>
    <tasks>
        <fullName>DisruptionAnalyticRequest</fullName>
        <assignedTo>evan.eckerle@multiplan.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>7</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Analytic_Request__c.Date_Request_Received__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Analytic Request - Disruption - HCE</subject>
    </tasks>
    <tasks>
        <fullName>DisruptionAnalyticRequest_IHP</fullName>
        <assignedTo>evan.eckerle@multiplan.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>7</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Analytic_Request__c.Date_Request_Received__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Analytic Request - Disruption - IHP</subject>
    </tasks>
    <tasks>
        <fullName>GeoAnalyticRequest</fullName>
        <assignedTo>evan.eckerle@multiplan.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Analytic_Request__c.Desired_Completion_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Analytic Request - GEO - HCE</subject>
    </tasks>
    <tasks>
        <fullName>GeoAnalyticRequest_IHP</fullName>
        <assignedTo>brenda.binns@multiplan.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>7</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Analytic_Request__c.Date_Request_Received__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Analytic Request - GEO - IHP</subject>
    </tasks>
    <tasks>
        <fullName>RepricingAnalyticRequest</fullName>
        <assignedTo>evan.eckerle@multiplan.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>12</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Analytic_Request__c.Date_Request_Received__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Analytic Request - Repricing - HCE</subject>
    </tasks>
    <tasks>
        <fullName>RepricingAnalyticRequest_IHP</fullName>
        <assignedTo>brenda.binns@multiplan.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>12</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Analytic_Request__c.Date_Request_Received__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Analytic Request - Repricing - IHP</subject>
    </tasks>
    <tasks>
        <fullName>TTCAnalyticRequest_AW</fullName>
        <assignedTo>andrea.washington@multiplan.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>5</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Analytic_Request__c.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>TTC Analytic Request</subject>
    </tasks>
</Workflow>
