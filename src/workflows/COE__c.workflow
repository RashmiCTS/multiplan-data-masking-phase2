<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Comment_Summary_Field</fullName>
        <field>Comments_Summary__c</field>
        <formula>&quot;[&quot; + LEFT( LastModifiedBy.FirstName, 1) + LastModifiedBy.LastName + &quot; &quot; + TEXT( LastModifiedDate ) + &quot;]&quot; + BR() + 
Comments_Update__c + BR() + BR() + 
PRIORVALUE( Comments_Summary__c )</formula>
        <name>Update Comment Summary Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Comment Summary Initial Entry Update</fullName>
        <actions>
            <name>Update_Comment_Summary_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>COE__c.Comments_Update__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Comment Summary Updates</fullName>
        <actions>
            <name>Update_Comment_Summary_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED( Comments_Update__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
