<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Close_Date_in_the_Past</fullName>
        <description>Close Date in the Past</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>csun@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Auto_Notifications/Oppty_Close_Date_Expiring</template>
    </alerts>
    <alerts>
        <fullName>ITFOpptyAlert</fullName>
        <ccEmails>CNS_OAF_Profitability_Models@viant.com</ccEmails>
        <description>ITF Oppty Alert</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>ApprovalTemplatesJulieOnly/IntheFunnelStageAlert</template>
    </alerts>
    <alerts>
        <fullName>Opportunitycloseeffectivedatenotification</fullName>
        <description>Opportunity close/effective date notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Auto_Notifications/Opptycloseeffectivedates</template>
    </alerts>
    <alerts>
        <fullName>Oppty_is_Lost_or_Declined_to_propose_and_was_previously_ready_for_Imp_Mgr</fullName>
        <description>Oppty is Lost or Declined to propose and was previously ready for Imp Mgr</description>
        <protected>false</protected>
        <recipients>
            <recipient>Account Associate</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Account Manager</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>SMSalesSupportManager</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>csun@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Auto_Notifications/Oppty_lost_close_Project</template>
    </alerts>
    <alerts>
        <fullName>OpptyclosedatesBF</fullName>
        <description>Oppty close dates - B&amp;F</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Auto_Notifications/Opptycloseeffectivedates</template>
    </alerts>
    <alerts>
        <fullName>OpptynotifyclientimpReadyforanImplementationManageryes</fullName>
        <description>Oppty notify client imp - Ready for an Implementation Manager - yes</description>
        <protected>false</protected>
        <recipients>
            <recipient>Account Associate</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Account Manager</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>SMSalesSupportManager</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>csun@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Auto_Notifications/OpptyClientEnhancementNotification</template>
    </alerts>
    <alerts>
        <fullName>RVPRequestApproved</fullName>
        <description>RVP Request Approved</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ApprovalTemplatesJulieOnly/RVPRequestAccepted</template>
    </alerts>
    <alerts>
        <fullName>RVPRequestRejected</fullName>
        <description>RVP Request Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ApprovalTemplatesJulieOnly/RVPRequestRejected</template>
    </alerts>
    <fieldUpdates>
        <fullName>AboveFunnelRecTypeUpd</fullName>
        <description>When a stage is above the funnel update the record type to above funnel for all the required fields.</description>
        <field>RecordTypeId</field>
        <lookupValue>x2AboveFunnelOpportunity</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Above Funnel Rec Type Upd</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ApprovalSubmitDateUpd</fullName>
        <field>Approval_Requested_Date__c</field>
        <formula>TODAY()</formula>
        <name>Approval Submit Date Upd</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Awaiting_Decision_Rec_Type_Upd</fullName>
        <field>RecordTypeId</field>
        <lookupValue>X4_Awaiting_Decision</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Awaiting Decision Rec Type Upd</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BestFewRecTypeAutoUpd</fullName>
        <field>RecordTypeId</field>
        <lookupValue>x5BestandFewOpportunity</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Best &amp; Few Rec Type Auto Upd</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BestandFewRecTypeUpd</fullName>
        <description>When a stage is best and few update the record type to best and few for all the required fields.</description>
        <field>RecordTypeId</field>
        <lookupValue>x5BestandFewOpportunity</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Best and Few Rec Type Upd</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CloseDateExpired_Update</fullName>
        <description>Field update to &quot;close date expired&quot; field on the close date of an open oppty to cause the record to be saved and trigger the notification workflow.</description>
        <field>Close_Date_Expired__c</field>
        <formula>TODAY()</formula>
        <name>CloseDateExpired Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ClosedDeclinedRecTypeUpd</fullName>
        <description>When a stage is declined to propose update the record type to declined to propose for all the required fields.</description>
        <field>RecordTypeId</field>
        <lookupValue>x8DeclinedtoPropose</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Closed Declined Rec Type Upd</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ClosedLostRecTypeUpd</fullName>
        <description>When a stage is closed lost update the record type to closed lost for all the required fields.</description>
        <field>RecordTypeId</field>
        <lookupValue>x7ClosedLost</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Closed Lost Rec Type Upd</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ClosedSoldRecTypeUpd</fullName>
        <description>When a stage is closed sold update the record type to closed sold for all the required fields.</description>
        <field>RecordTypeId</field>
        <lookupValue>x6ClosedSoldOpportunity</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Closed Sold Rec Type Upd</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>HCEApprovedDateUpdate</fullName>
        <field>HCE_Approved_Date__c</field>
        <formula>TODAY()</formula>
        <name>HCE Approved Date Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>InFunnelApprovalRecTypeUpd</fullName>
        <field>RecordTypeId</field>
        <lookupValue>x35InFunnelApprovedOpportunity</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>In Funnel Approval Rec Type Upd</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>InFunnelRecTypeUpd</fullName>
        <description>When a stage is in funnel update the record type to in funnel for all the required fields.</description>
        <field>RecordTypeId</field>
        <lookupValue>x3InFunnelOpportunity</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>In Funnel Rec Type Upd</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NonStandardProdApprovalDateUpd</fullName>
        <field>Non_Standard_Product_Approved_Date__c</field>
        <formula>TODAY()</formula>
        <name>Non Standard Prod Approval Date Upd</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NonStandardProductApprovedUpd</fullName>
        <field>Non_Standard_Product_Approved_Date__c</field>
        <formula>TODAY()</formula>
        <name>Non Standard Product Approved Upd</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RVPApprovalDateUpd</fullName>
        <field>RVP_Oppty_Approved_Date__c</field>
        <formula>TODAY()</formula>
        <name>RVP Approval Date Upd</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SuspectRecTypeUpd</fullName>
        <description>When a stage is suspect update the record type to suspect for all the required fields.</description>
        <field>RecordTypeId</field>
        <lookupValue>x1SuspectOpportunity</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Suspect Rec Type Upd</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>insertCIreadydate</fullName>
        <field>Date_Sales_Ops_informed__c</field>
        <formula>NOW()</formula>
        <name>insert CI ready date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>AE Best and Few</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Best And Few</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>equals</operation>
            <value>Existing Client</value>
        </criteriaItems>
        <description>Assigns tasks when an existing client opportunity stage is updated to best and few</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AE Closed Sold</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Sold</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>equals</operation>
            <value>Exisitng Client</value>
        </criteriaItems>
        <description>Assigns tasks when the existing client opportunity has been updated to the closed sold opportunity stage</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AE In Funnel</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>equals</operation>
            <value>Existing Client</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>In Funnel</value>
        </criteriaItems>
        <description>Assigns tasks when an existing client opportunity stage is updated to In Funnel.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Above Funnel Stage Upd</fullName>
        <actions>
            <name>AboveFunnelRecTypeUpd</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>4-Awaiting Decision,1-Suspect,6-Closed Sold,8-Declined to Propose,3-In Funnel,5-Best and Few,7-Closed Lost,3.5-In Funnel Approved Opportunity</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Above Funnel</value>
        </criteriaItems>
        <description>Oppty Record Type field update when Oppty Stage has been changed.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Awaiting Decision Stage Upd</fullName>
        <actions>
            <name>Awaiting_Decision_Rec_Type_Upd</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>1-Suspect,5-Closed Sold,7-Declined to Propose,2-Above Funnel,3-In Funnel,4-Best and Few,6-Closed Lost,3.5-In Funnel Approved Opportunity</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Awaiting Decision</value>
        </criteriaItems>
        <description>Oppty Record Type field update when Oppty Stage has been changed.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Best and Few Stage Upd</fullName>
        <actions>
            <name>BestandFewRecTypeUpd</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>4-Awaiting Decision,1-Suspect,6-Closed Sold,8-Declined to Propose,2-Above Funnel,3-In Funnel,7-Closed Lost,3.5-In Funnel Approved Opportunity</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Best And Few</value>
        </criteriaItems>
        <description>Oppty Record Type field update when Oppty Stage has been changed.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Closed Declined Stage Upd</fullName>
        <actions>
            <name>ClosedDeclinedRecTypeUpd</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>4-Awaiting Decision,1-Suspect,6-Closed Sold,2-Above Funnel,3-In Funnel,5-Best and Few,7-Closed Lost,3.5-In Funnel Approved Opportunity</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Declined to Propose</value>
        </criteriaItems>
        <description>Oppty Record Type field update when Oppty Stage has been changed.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Closed Lost Stage Upd</fullName>
        <actions>
            <name>ClosedLostRecTypeUpd</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>4-Awaiting Decision,1-Suspect,6-Closed Sold,8-Declined to Propose,2-Above Funnel,3-In Funnel,5-Best and Few,3.5-In Funnel Approved Opportunity</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Lost</value>
        </criteriaItems>
        <description>Oppty Record Type field update when Oppty Stage has been changed.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Closed Sold Stage Upd</fullName>
        <actions>
            <name>ClosedSoldRecTypeUpd</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>4-Awaiting Decision,1-Suspect,8-Declined to Propose,2-Above Funnel,3-In Funnel,5-Best and Few,7-Closed Lost,3.5-In Funnel Approved Opportunity</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Sold</value>
        </criteriaItems>
        <description>Oppty Record Type field update when Oppty Stage has been changed.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>In Funnel Stage Upd</fullName>
        <actions>
            <name>InFunnelRecTypeUpd</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>4-Awaiting Decision,1-Suspect,6-Closed Sold,8-Declined to Propose,2-Above Funnel,5-Best and Few,7-Closed Lost</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>In Funnel</value>
        </criteriaItems>
        <description>Oppty Record Type field update when Oppty Stage has been changed.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Input date when ready for CI</fullName>
        <active>false</active>
        <formula>ISCHANGED( Ready_for_Implementation__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Lost Oppty notification</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Lost</value>
        </criteriaItems>
        <description>when an oppty moves to funnel stage Closed Lost, notify Allison Anderson</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>NSE %26 AE Above Funnel</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Above Funnel</value>
        </criteriaItems>
        <description>Assigns tasks when opportunity stage is updated to Above Funnel</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>NSE Best and Few</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Best And Few</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>equals</operation>
            <value>New Business</value>
        </criteriaItems>
        <description>Assigns tasks when a new business opportunity stage is updated to best and few</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>NSE Closed Sold</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Sold</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>equals</operation>
            <value>New Business</value>
        </criteriaItems>
        <description>Assigns tasks when new business opportunity stage is updated to Closed Sold</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>NSE In Funnel</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>In Funnel</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>equals</operation>
            <value>New Business</value>
        </criteriaItems>
        <description>Assigns tasks when new business opportunity stage is updated to In Funnel.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>NSE Suspect</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Suspect</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>equals</operation>
            <value>New Business</value>
        </criteriaItems>
        <description>Assign tasks when a new business opportunity stage is updated to Suspect stage</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notification to Cheryl Jenkins of Oppty stage ATF</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Funnel_Stage__c</field>
            <operation>equals</operation>
            <value>&lt;39% = Above the Funnel - fact finding/needs assessment</value>
        </criteriaItems>
        <description>Notify Cheryl Jenkins when an Oppty has been moved into the ATF category.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify Client Imp of lost oppty - Close Project</fullName>
        <actions>
            <name>Oppty_is_Lost_or_Declined_to_propose_and_was_previously_ready_for_Imp_Mgr</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Lost,Declined to Propose</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Ready_for_Implementation__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <description>Notify Sales Support Manager to close Project when an Oppty has been Lost/Declined to Propose, and implementation had been requested previously.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify Client Imp of ready for imp mgr YES</fullName>
        <actions>
            <name>OpptynotifyclientimpReadyforanImplementationManageryes</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>insertCIreadydate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Ready_for_Implementation__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <description>Notify Sales Support Manager when the YES selected for Ready for Implementation</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity Dates - Suspect%2C ATF%2C ITF</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Suspect,Above Funnel,In Funnel</value>
        </criteriaItems>
        <description>Remind rep of upcoming close/effective dates....are they accurate???????</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Opportunitycloseeffectivedatenotification</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.CloseDate</offsetFromField>
            <timeLength>-30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Opportunity Dates B%26F only</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Best And Few</value>
        </criteriaItems>
        <description>Remind rep of upcoming close/effective dates....are they accurate???????</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>OpptyclosedatesBF</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.CloseDate</offsetFromField>
            <timeLength>-15</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Oppty Close Date Expiring - Trigger %28Field Update%29</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.CloseDate</field>
            <operation>equals</operation>
            <value>TODAY</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>notEqual</operation>
            <value>Closed Sold,Closed Lost,Declined to Propose</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Network Services</value>
        </criteriaItems>
        <description>Field update on the close date of an open oppty to cause the oppty to update/save and thus trigger the notification workflow.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>CloseDateExpired_Update</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Opportunity.CloseDate</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Propritary Network YES</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Proprietary_Network__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SOLD Oppty notification</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Funnel_Stage__c</field>
            <operation>equals</operation>
            <value>100% = Sold -have received signed contract or LOA - will start to generate revenue</value>
        </criteriaItems>
        <description>Notify finance when an Oppty has been SOLD</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Suspect Stage Upd</fullName>
        <actions>
            <name>SuspectRecTypeUpd</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>4-Awaiting Decision,6-Closed Sold,8-Declined to Propose,2-Above Funnel,3-In Funnel,5-Best and Few,7-Closed Lost,3.5-In Funnel Approved Opportunity</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Suspect</value>
        </criteriaItems>
        <description>Oppty Record Type field update when Oppty Stage has been changed.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>notification of Oppty stage B%26F</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Funnel_Stage__c</field>
            <operation>equals</operation>
            <value>86%&gt; = Best &amp; Few-contract and deal points agreed to; awaiting signature,70 - 85% = Best &amp; Few-negotiating price; engaging in contract review</value>
        </criteriaItems>
        <description>Notify GHI when an Oppty has been moved into the B&amp;F category.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>notification of Oppty stage ITF</fullName>
        <actions>
            <name>ITFOpptyAlert</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Funnel_Stage__c</field>
            <operation>equals</operation>
            <value>60 - 69% = In the Funnel-proposal presented,&quot;50 - 59% = In the Funnel-Q&amp;A analytics, engaged in follow-up&quot;,40 - 49% = In the Funnel - face to face with follow-up</value>
        </criteriaItems>
        <description>Notify finance when an Oppty has been moved into the ITF category.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
