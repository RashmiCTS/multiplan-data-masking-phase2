<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>CER_AM_Approvals_Complete</fullName>
        <description>CER: AM Approvals Complete</description>
        <protected>false</protected>
        <recipients>
            <recipient>csun@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rebecca.smith@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>csun@multiplan.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Workflow_Auto_Notifications/CER_AM_Approval_Complete</template>
    </alerts>
    <alerts>
        <fullName>CER_Approval_Approved_w_ND</fullName>
        <description>CER Approval - Approved w ND</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <recipient>Account Manager</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>cheryl.zellner@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jlove@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rebecca.smith@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>csun@multiplan.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Workflow_Auto_Notifications/CER_Request_In_Process_9_19_16</template>
    </alerts>
    <alerts>
        <fullName>CER_Approval_Approved_wo_ND</fullName>
        <description>CER Approval - Approved wo ND</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <recipient>Account Manager</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderAddress>csun@multiplan.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Workflow_Auto_Notifications/CER_Request_In_Process</template>
    </alerts>
    <alerts>
        <fullName>CER_Approval_Rejected</fullName>
        <description>CER Approval - Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <recipient>Account Manager</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>rebecca.smith@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Auto_Notifications/CER_Request_Rejected</template>
    </alerts>
    <alerts>
        <fullName>CER_Approved_Notif_Contract</fullName>
        <description>CER Notify Requester Approved for Contract/Amendment</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>csun@multiplan.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Workflow_Auto_Notifications/CER_Request_Completed_Provider_Contract</template>
    </alerts>
    <alerts>
        <fullName>CER_Client_ID_Provided</fullName>
        <description>CER: Client ID Provided</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>csun@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rebecca.smith@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Auto_Notifications/CER_Client_ID_Provided</template>
    </alerts>
    <alerts>
        <fullName>CER_Client_Submission_Confirmation_Client</fullName>
        <description>CER Client Submission Confirmation</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <recipient>Account Manager</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderAddress>csun@multiplan.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Workflow_Auto_Notifications/CER_Client_Submission_Confirmation</template>
    </alerts>
    <alerts>
        <fullName>CER_Completed_Client</fullName>
        <description>CER Completed - Client</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <recipient>Account Manager</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>rebecca.smith@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Auto_Notifications/CER_Request_Completed_Client</template>
    </alerts>
    <alerts>
        <fullName>CER_Completed_Provider</fullName>
        <description>CER Completed - Provider</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>rebecca.smith@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Auto_Notifications/CER_Request_Completed_Provider</template>
    </alerts>
    <alerts>
        <fullName>CER_In_Process_9_19_16</fullName>
        <description>CER: In Process 9/19/16</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <recipient>Account Manager</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>csun@multiplan.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Workflow_Auto_Notifications/CER_Request_In_Process_9_19_16</template>
    </alerts>
    <alerts>
        <fullName>CER_In_Process_Notification_pre_9_19_16</fullName>
        <description>CER: In Process Notification pre-9/19/16</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <recipient>Account Manager</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>cheryl.zellner@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jlove@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rebecca.smith@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>csun@multiplan.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Workflow_Auto_Notifications/CER_Request_In_Process</template>
    </alerts>
    <alerts>
        <fullName>CER_ND_Entry_Complete</fullName>
        <description>CER ND Entry Complete</description>
        <protected>false</protected>
        <recipients>
            <recipient>csun@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jessica.rosewarne@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rebecca.smith@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Auto_Notifications/CER_ND_Complete_Notification</template>
    </alerts>
    <alerts>
        <fullName>CER_ND_Rejected</fullName>
        <description>CER ND Rejected</description>
        <protected>false</protected>
        <recipients>
            <recipient>csun@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jessica.rosewarne@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rebecca.smith@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Auto_Notifications/CER_Request_Rejected</template>
    </alerts>
    <alerts>
        <fullName>CER_Notif_Requester_Complete</fullName>
        <description>CER Notify Requester Complete - wo Notif to Becky</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>csun@multiplan.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Workflow_Auto_Notifications/CER_Request_Completed_Provider</template>
    </alerts>
    <alerts>
        <fullName>CER_Notification_to_negotiator_approved_new_contract</fullName>
        <description>CER: Notification to negotiator approved new contract</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>jlove@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rebecca.smith@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Auto_Notifications/CER_Provider_new_contract_approved</template>
    </alerts>
    <alerts>
        <fullName>CER_Notify_CIA_WA_Request_Approval_Complete</fullName>
        <description>CER Notify CIA - WA Request Approval Complete</description>
        <protected>false</protected>
        <recipients>
            <recipient>cheryl.zellner@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jlove@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>csun@multiplan.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Workflow_Auto_Notifications/CER_CIA_Notification_WA_Approval_Complete</template>
    </alerts>
    <alerts>
        <fullName>CER_On_Hold</fullName>
        <description>CER On Hold</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <recipient>Account Manager</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>rebecca.smith@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Auto_Notifications/CER_Request_On_Hold</template>
    </alerts>
    <alerts>
        <fullName>CER_PDM_Complete_Notification</fullName>
        <description>CER PDM Complete Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>cheryl.zellner@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jlove@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>csun@multiplan.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Workflow_Auto_Notifications/CER_PDM_Complete_Notification</template>
    </alerts>
    <alerts>
        <fullName>CER_PDM_Notification_MP3_Client</fullName>
        <ccEmails>tammie.warren@example.MultiPlan.com</ccEmails>
        <description>CER PDM Notification - MP3 - Client</description>
        <protected>false</protected>
        <recipients>
            <recipient>brenda.roberts@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>cheryl.zellner@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>csun@multiplan.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Workflow_Auto_Notifications/CER_PDM_Notification_MP3</template>
    </alerts>
    <alerts>
        <fullName>CER_PDM_Notification_MP3_Provider</fullName>
        <ccEmails>tammie.warren@example.MultiPlan.com</ccEmails>
        <description>CER PDM Notification - MP3 - Provider</description>
        <protected>false</protected>
        <recipients>
            <recipient>brenda.roberts@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jlove@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>csun@multiplan.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Workflow_Auto_Notifications/CER_PDM_Notification_MP3</template>
    </alerts>
    <alerts>
        <fullName>CER_PDM_Notification_eProvider_Client</fullName>
        <ccEmails>beth.voiles@example.MultiPlan.com</ccEmails>
        <description>CER PDM Notification - eProvider - Client</description>
        <protected>false</protected>
        <recipients>
            <recipient>cheryl.zellner@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>cindy.hunt@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>kim.russell@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>csun@multiplan.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Workflow_Auto_Notifications/CER_PDM_Notification_eProvider</template>
    </alerts>
    <alerts>
        <fullName>CER_PDM_Notification_eProvider_Provider</fullName>
        <ccEmails>Chattmailroom@example.Multiplan.com</ccEmails>
        <ccEmails>beth.voiles@example.MultiPlan.com</ccEmails>
        <description>CER PDM Notification - eProvider - Provider</description>
        <protected>false</protected>
        <recipients>
            <recipient>cindy.hunt@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jlove@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>kim.russell@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>csun@multiplan.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Workflow_Auto_Notifications/CER_PDM_Notification_eProvider</template>
    </alerts>
    <alerts>
        <fullName>CER_Provider_Submission_Confirmation_Provider</fullName>
        <description>CER Provider Submission Confirmation</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderAddress>csun@multiplan.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Workflow_Auto_Notifications/CER_Provider_Submission_Confirmation</template>
    </alerts>
    <alerts>
        <fullName>CER_Recall</fullName>
        <description>CER Recall</description>
        <protected>false</protected>
        <recipients>
            <recipient>Account Manager</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>rebecca.smith@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Auto_Notifications/CER_Request_Recall</template>
    </alerts>
    <fieldUpdates>
        <fullName>CER_Complete_Date</fullName>
        <field>Date_Request_Completed__c</field>
        <formula>TODAY()</formula>
        <name>CER: Complete Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CER_SO_Appoval_New</fullName>
        <field>Status__c</field>
        <literalValue>Closed - New Contract</literalValue>
        <name>CER: Status to Complete - SO Appoval_New</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CER_SO_Approved</fullName>
        <field>SO_Approved__c</field>
        <literalValue>1</literalValue>
        <name>CER: SO Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CER_Status_to_Approved_for_Contract</fullName>
        <field>Status__c</field>
        <literalValue>Approved for Contract/Amendment</literalValue>
        <name>CER: Status to Approved for Contract</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CER_Status_to_Approved_for_Contract1</fullName>
        <field>Status__c</field>
        <literalValue>Approved for Contract/Amendment</literalValue>
        <name>CER: Status to Approved for Contract1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CER_Status_to_Cancelled</fullName>
        <field>Status__c</field>
        <literalValue>Cancelled</literalValue>
        <name>CER: Status to Cancelled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CER_Status_to_Close_Push_Back</fullName>
        <field>Status__c</field>
        <literalValue>Closed - Successful Push Back</literalValue>
        <name>CER: Status to Close Push Back</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CER_Status_to_Closed_New_Contract</fullName>
        <field>Status__c</field>
        <literalValue>Closed - New Contract</literalValue>
        <name>CER: Status to Closed New Contract</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CER_Status_to_Complete</fullName>
        <field>Status__c</field>
        <literalValue>Complete</literalValue>
        <name>CER: Status to Complete</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CER_Status_to_Pending_CIA_Review</fullName>
        <field>Status__c</field>
        <literalValue>SO Approved, Pending CIA Review</literalValue>
        <name>CER: Status to Pending CIA Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CER_Status_to_Pending_EVP_ND_Review</fullName>
        <field>Status__c</field>
        <literalValue>Pending EVP/ND Review</literalValue>
        <name>CER: Status to Pending EVP/ND Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CER_Status_to_Pending_EVP_Ops_Review</fullName>
        <field>Status__c</field>
        <literalValue>Pending EVP/Ops Review</literalValue>
        <name>CER: Status to Pending EVP/Ops Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CER_Status_to_Pending_EVP_Sales_Review</fullName>
        <field>Status__c</field>
        <literalValue>Pending EVP/Sales Review</literalValue>
        <name>CER: Status to Pending EVP/Sales Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CER_Status_to_Pending_Revenue_Report</fullName>
        <field>Status__c</field>
        <literalValue>Not Submitted; Pending Revenue Impact Report</literalValue>
        <name>CER: Status to Pending Revenue Report</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CER_Status_to_Pending_SO_Review</fullName>
        <field>Status__c</field>
        <literalValue>Pending Sales Ops Review</literalValue>
        <name>CER: Status to Pending SO Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CER_Status_to_Pending_SVP_ND_Review</fullName>
        <field>Status__c</field>
        <literalValue>Pending SVP/ND Review</literalValue>
        <name>CER: Status to Pending SVP/ND Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CER_Status_to_Processing</fullName>
        <field>Status__c</field>
        <literalValue>Processing</literalValue>
        <name>CER: Status to Processing</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CER_Status_to_Rejected</fullName>
        <field>Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>CER: Status to Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CER_Status_to_Submitted_for_Review</fullName>
        <field>Status__c</field>
        <literalValue>Submitted for Review</literalValue>
        <name>CER: Status to Submitted for Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CER_Status_update_to_ID_requested</fullName>
        <field>Status__c</field>
        <literalValue>Not Submitted; Client ID Requested</literalValue>
        <name>CER: Status update to ID requested</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CER_Update_Request_Time_Stamp</fullName>
        <field>Time_Stamp_Request_Client_ID__c</field>
        <formula>NOW()</formula>
        <name>CER: Update Request Time Stamp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MP3_Status_to_Not_Started</fullName>
        <field>MP3_Status__c</field>
        <literalValue>Not Started</literalValue>
        <name>MP3 Status to Not Started</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MP3_Status_to_Processing</fullName>
        <field>MP3_Status__c</field>
        <literalValue>Processing</literalValue>
        <name>MP3 Status to Processing</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PDM_MP3_Assignment_Date_Update</fullName>
        <field>PDM_MP3_Assignment_Ddate__c</field>
        <formula>NOW()</formula>
        <name>PDM/MP3 Assignment Date Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PDM_eProvider_Assignment_Date_Update</fullName>
        <field>PDM_eProvider_Assignment_Ddate__c</field>
        <formula>NOW()</formula>
        <name>PDM/eProvider Assignment Date Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SO_Status_to_Not_Started</fullName>
        <field>SO_Status__c</field>
        <literalValue>Not Started</literalValue>
        <name>SO Status to Not Started</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_to_Cancelled</fullName>
        <field>Status__c</field>
        <literalValue>Cancelled</literalValue>
        <name>Status to Cancelled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_to_Processing</fullName>
        <field>Status__c</field>
        <literalValue>Processing</literalValue>
        <name>Status to Processing</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_to_Rejected</fullName>
        <field>Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Status to Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_to_Submitted_for_Review</fullName>
        <field>Status__c</field>
        <literalValue>Pending CIA Review</literalValue>
        <name>Status to Pending CIA Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Time_Stamp_CIA_Approval</fullName>
        <field>Time_Stamp_CIA_Approval__c</field>
        <formula>NOW()</formula>
        <name>Time Stamp - CIA Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Time_Stamp_EVP_ND_Approved</fullName>
        <field>Time_Stamp_EVP_ND_Approved__c</field>
        <formula>Now()</formula>
        <name>Time Stamp: EVP/ND Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Time_Stamp_EVP_Ops_Approved</fullName>
        <field>Time_Stamp_EVP_Ops_Approved__c</field>
        <formula>Now()</formula>
        <name>Time Stamp: EVP/Ops Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Time_Stamp_EVP_Sales_Approved</fullName>
        <field>Time_Stamp_EVP_Sales_Approved__c</field>
        <formula>Now()</formula>
        <name>Time Stamp: EVP/Sales Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Time_Stamp_ID_Complete</fullName>
        <field>Time_Stamp_Client_ID_Complete__c</field>
        <formula>NOW()</formula>
        <name>Time Stamp: ID Complete</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Time_Stamp_SO_Approval</fullName>
        <field>Time_Stamp_SO_Approval__c</field>
        <formula>NOW()</formula>
        <name>Time Stamp - SO Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Time_Stamp_SVP_Approval</fullName>
        <field>Time_Stamp_SVP_Approval__c</field>
        <formula>NOW()</formula>
        <name>Time Stamp - SVP Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Time_Stamp_Submitted</fullName>
        <field>Time_Stamp_Submitted__c</field>
        <formula>NOW ()</formula>
        <name>Time Stamp - Summitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Display_Alert_Flag</fullName>
        <description>Update Display Alert Flag to True to show alert to enter Agreement if required.</description>
        <field>Display_Alert_Flag__c</field>
        <literalValue>1</literalValue>
        <name>Update Display Alert Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>eProvider_Status_to_Not_Started</fullName>
        <field>eProvider_Status__c</field>
        <literalValue>Not Started</literalValue>
        <name>eProvider Status to Not Started</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>eProvider_Status_to_Processing</fullName>
        <field>eProvider_Status__c</field>
        <literalValue>Processing</literalValue>
        <name>eProvider Status to Processing</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>AM Approvals Complete</fullName>
        <actions>
            <name>CER_AM_Approvals_Complete</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>IF(Excluded_Clients__c &gt; 0 &amp;&amp; Excluded_Clients__c - Approved_Client_Records__c - Rejected_Client_Records__c = 0, TRUE, FALSE)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MP3 Assignment Date Update</fullName>
        <actions>
            <name>PDM_MP3_Assignment_Date_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED( PDM_MP3_Assigned_to__c ) &amp;&amp;  NOT( ISPICKVAL( PDM_MP3_Assigned_to__c , &quot;&quot;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>MP3 Status to Processing</fullName>
        <actions>
            <name>MP3_Status_to_Processing</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Client_Exclusion_Request__c.PDM_MP3_Assigned_to__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ND Complete Notification to Sales Ops</fullName>
        <actions>
            <name>CER_ND_Entry_Complete</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Client_Exclusion_Request__c.ND_Status__c</field>
            <operation>equals</operation>
            <value>Complete</value>
        </criteriaItems>
        <criteriaItems>
            <field>Client_Exclusion_Request__c.Request_Origin__c</field>
            <operation>equals</operation>
            <value>Client Requested</value>
        </criteriaItems>
        <description>Notify Sales Ops when exclusion has been marked as complete by ND.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PDM Complete Notification to CIA</fullName>
        <actions>
            <name>CER_PDM_Complete_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( ISPICKVAL( eProvider_Status__c, &quot;Complete&quot; ), OR( ISPICKVAL( MP3_Status__c, &quot;&quot; ), ISPICKVAL( MP3_Status__c, &quot;Cancelled&quot; ))) ||
 AND( OR( ISPICKVAL( eProvider_Status__c, &quot;&quot; ), ISPICKVAL( eProvider_Status__c, &quot;Cancelled&quot; )), ISPICKVAL( MP3_Status__c, &quot;Complete&quot; )) ||
 AND( ISPICKVAL( eProvider_Status__c, &quot;Complete&quot; ), ISPICKVAL( MP3_Status__c, &quot;Complete&quot; ))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Request Completed - Client</fullName>
        <actions>
            <name>CER_Completed_Client</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Client_Exclusion_Request__c.Status__c</field>
            <operation>equals</operation>
            <value>Complete</value>
        </criteriaItems>
        <criteriaItems>
            <field>Client_Exclusion_Request__c.Date_Request_Completed__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Client_Exclusion_Request__c.Request_Origin__c</field>
            <operation>equals</operation>
            <value>Client Requested</value>
        </criteriaItems>
        <description>Notification email to record creator and account owner when a request has been completed.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Request Completed - Provider</fullName>
        <actions>
            <name>CER_Completed_Provider</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Client_Exclusion_Request__c.Status__c</field>
            <operation>equals</operation>
            <value>Complete</value>
        </criteriaItems>
        <criteriaItems>
            <field>Client_Exclusion_Request__c.Date_Request_Completed__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Client_Exclusion_Request__c.Request_Origin__c</field>
            <operation>equals</operation>
            <value>Provider Requested</value>
        </criteriaItems>
        <description>Notification email to record creator and account owner when a request has been completed.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Request On Hold</fullName>
        <actions>
            <name>CER_On_Hold</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Client_Exclusion_Request__c.Status__c</field>
            <operation>equals</operation>
            <value>On Hold</value>
        </criteriaItems>
        <criteriaItems>
            <field>Client_Exclusion_Request__c.Status_Note__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Notification email to record creator and account owner when a request has been put on hold.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Request Rejected</fullName>
        <actions>
            <name>CER_Approval_Rejected</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Client_Exclusion_Request__c.Status__c</field>
            <operation>equals</operation>
            <value>Rejected</value>
        </criteriaItems>
        <description>Notification email to record creator and account owner when a request has been rejected (previously approved thru the approval process).</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Request for Client ID</fullName>
        <actions>
            <name>CER_Status_update_to_ID_requested</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CER_Update_Request_Time_Stamp</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Client_ID_Needed_for_CER_Becky</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Client_ID_Needed_for_CER_Calvin</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Client_ID_Needed_for_CER_Swarna</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Client_Exclusion_Request__c.Request_Client_ID__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SO Status Update</fullName>
        <actions>
            <name>SO_Status_to_Not_Started</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Client_Exclusion_Request__c.Request_Origin__c</field>
            <operation>equals</operation>
            <value>Client Requested</value>
        </criteriaItems>
        <criteriaItems>
            <field>Client_Exclusion_Request__c.May_Enter_SO_Process__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Client_Exclusion_Request__c.SO_Status__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Status to Closed Push Back</fullName>
        <actions>
            <name>CER_Status_to_Close_Push_Back</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Client_Exclusion_Request__c.Closed_Successful_Pushback__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Status to ID Provided</fullName>
        <actions>
            <name>CER_Client_ID_Provided</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>CER_Status_to_Pending_Revenue_Report</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Time_Stamp_ID_Complete</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Client_Exclusion_Request__c.Status__c</field>
            <operation>equals</operation>
            <value>Not Submitted; Client ID Requested</value>
        </criteriaItems>
        <criteriaItems>
            <field>Client_Exclusion_Request__c.Client_ID_Completed__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Change status to Pending Revenue Report.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>eProvider Assignment Date Update</fullName>
        <actions>
            <name>PDM_eProvider_Assignment_Date_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED( PDM_eProvider_Assigned_to__c ) &amp;&amp;  NOT( ISPICKVAL( PDM_eProvider_Assigned_to__c , &quot;&quot;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>eProvider Status to Processing</fullName>
        <actions>
            <name>eProvider_Status_to_Processing</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Client_Exclusion_Request__c.PDM_eProvider_Assigned_to__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>Client_ID_Needed_for_CER_Becky</fullName>
        <assignedTo>rebecca.smith@multiplan.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Client_Exclusion_Request__c.Time_Stamp_Request_Client_ID__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>CER: Client ID Needed - Becky</subject>
    </tasks>
    <tasks>
        <fullName>Client_ID_Needed_for_CER_Calvin</fullName>
        <assignedTo>csun@multiplan.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Client_Exclusion_Request__c.Time_Stamp_Request_Client_ID__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>CER: Client ID Needed - Calvin</subject>
    </tasks>
    <tasks>
        <fullName>Client_ID_Needed_for_CER_Swarna</fullName>
        <assignedTo>swarnalata.maruvad@multiplan.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Client_Exclusion_Request__c.Time_Stamp_Request_Client_ID__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>CER: Client ID Needed - Swarna</subject>
    </tasks>
</Workflow>
