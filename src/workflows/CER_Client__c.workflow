<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>AM_rejection_notif_to_negotiator</fullName>
        <description>AM rejection notif to negotiator</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Auto_Notifications/CER_AM_Rejection_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>AM_Approval_Update</fullName>
        <field>AM_Approval__c</field>
        <literalValue>Approved</literalValue>
        <name>AM Approval Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>AM_Approval_to_Pending</fullName>
        <field>AM_Approval__c</field>
        <literalValue>Pending</literalValue>
        <name>AM Approval to Pending</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>AM_Rejection_Update</fullName>
        <field>AM_Approval__c</field>
        <literalValue>Rejected</literalValue>
        <name>AM Rejection Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
