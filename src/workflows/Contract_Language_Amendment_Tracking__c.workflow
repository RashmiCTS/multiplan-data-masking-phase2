<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>CIInNegotiations</fullName>
        <description>Updates the Report Status Category for CI</description>
        <field>OLD_Report_Status_Category__c</field>
        <literalValue>To Be Contacted</literalValue>
        <name>CI - In Negotiations</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CIReportStatusCategory</fullName>
        <field>OLD_Report_Status_Category__c</field>
        <literalValue>To Be Contacted</literalValue>
        <name>CI Report Status Category</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CITotalAgreementDefault</fullName>
        <description>CI total Agreement will default to one (1) if Contract Integration is selected</description>
        <field>CI_Total_Agreement__c</field>
        <formula>1</formula>
        <name>CI Total Agreement Default</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CLATClosedforNS</fullName>
        <field>RecordTypeId</field>
        <lookupValue>CLOSEDNetworkServices</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>CLAT Closed for NS</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CloseCLATRecord</fullName>
        <description>Close CLAT record</description>
        <field>Closed_CLAT__c</field>
        <literalValue>1</literalValue>
        <name>Close CLAT Record</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ClosedCLAT</fullName>
        <field>RecordTypeId</field>
        <lookupValue>CLOSED_but_NOT_contracted</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Closed CLAT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>HNT_Name_Update</fullName>
        <field>HealthNet_Name__c</field>
        <formula>Target_Name__c</formula>
        <name>HNT Name Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>HNT_Provider_Type_Update</fullName>
        <field>HNT_Provider_Type__c</field>
        <formula>text(Provider_Types__c)</formula>
        <name>HNT Provider Type Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MgrApprovalreqdate</fullName>
        <description>Date CLA sent to mgr for approval</description>
        <field>Director_Approval_Requested__c</field>
        <formula>TODAY()</formula>
        <name>Mgr Approval req date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>VDCCLATreadonly</fullName>
        <description>When all details have been entered, make record read only</description>
        <field>RecordTypeId</field>
        <lookupValue>CLOSEDVDC</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>VDC CLAT read only</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>CI - Report Status Category</fullName>
        <actions>
            <name>CIReportStatusCategory</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND ( 
OR ( 
ISPICKVAL( Contract_Initiative__c , &quot;Contract Integration&quot;), 
ISPICKVAL( Contract_Initiative__c , &quot;Secondary Lift/Contract Integration&quot;)), 
ISPICKVAL(  OLD_In_Negotiations__c, &quot;None&quot; ) 
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <offsetFromField>Contract_Language_Amendment_Tracking__c.LastModifiedDate</offsetFromField>
            <timeLength>-1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>CLAT - CLOSED</fullName>
        <actions>
            <name>ClosedCLAT</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contract_Language_Amendment_Tracking__c.Closed_CLAT__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contract_Language_Amendment_Tracking__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Network Services</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CLAT - Record Conf</fullName>
        <actions>
            <name>CLATClosedforNS</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contract_Language_Amendment_Tracking__c.Record_Conf_all_contract_details_entered__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contract_Language_Amendment_Tracking__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Network Services</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Create Task When Date Sent To File Does Not Equal Null</fullName>
        <actions>
            <name>ReviewContract</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contract_Language_Amendment_Tracking__c.Date_Sent_to_File__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>HNT Name Update</fullName>
        <actions>
            <name>HNT_Name_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 4 AND ( 2 OR 3 or 5)</booleanFilter>
        <criteriaItems>
            <field>Contract_Language_Amendment_Tracking__c.Target_Name__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contract_Language_Amendment_Tracking__c.Contract_Initiative__c</field>
            <operation>equals</operation>
            <value>HealthNet VA</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contract_Language_Amendment_Tracking__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>VA Amendment - Group</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contract_Language_Amendment_Tracking__c.HealthNet_Name__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contract_Language_Amendment_Tracking__c.HealthNet_VA__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>HNT Provider Type Update</fullName>
        <actions>
            <name>HNT_Provider_Type_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 4 AND ( 2 OR 3 or 5)</booleanFilter>
        <criteriaItems>
            <field>Contract_Language_Amendment_Tracking__c.Provider_Types__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contract_Language_Amendment_Tracking__c.Contract_Initiative__c</field>
            <operation>equals</operation>
            <value>HealthNet VA</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contract_Language_Amendment_Tracking__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>VA Amendment - Group</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contract_Language_Amendment_Tracking__c.HNT_Provider_Type__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contract_Language_Amendment_Tracking__c.HealthNet_VA__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Record no longer editable</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Contract_Language_Amendment_Tracking__c.Record_Conf_all_contract_details_entered__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <description>When Record Conf field is YES, record is no longer editable</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>UD Report Status Category</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Contract_Language_Amendment_Tracking__c.OLD_In_Negotiations__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contract_Language_Amendment_Tracking__c.OLD_Negotiation_Type__c</field>
            <operation>equals</operation>
            <value>Renegotiation,Provider add</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contract_Language_Amendment_Tracking__c.OLD_Negotiation_Status__c</field>
            <operation>equals</operation>
            <value>Facility/Ancillary Already Participates in Wrap</value>
        </criteriaItems>
        <description>Updating Report Status Category based on Negotiations and Negotiation Type - cloned Account rule created by MMBT to CLAT.  JKG</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>VDC CLAT - Record Conf</fullName>
        <actions>
            <name>VDCCLATreadonly</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contract_Language_Amendment_Tracking__c.Record_Conf_all_contract_details_entered__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contract_Language_Amendment_Tracking__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>VDC</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>how many neg days</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Contract_Language_Amendment_Tracking__c.OLD_In_Negotiations__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>DateSenttoFileCompleted</fullName>
        <assignedToType>owner</assignedToType>
        <description>The agreement has been sent to File.  Please review the contract details and confirm that all necessary information has been entered   When completed, select &apos;Yes&apos; in the &apos;Record Confirmation - all contract details entered&apos; to lock the record.</description>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Contract_Language_Amendment_Tracking__c.Date_Sent_to_File__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Date Sent to File Completed</subject>
    </tasks>
    <tasks>
        <fullName>ReviewContract</fullName>
        <assignedToType>owner</assignedToType>
        <description>The CIM has sent this provider&apos;s contract to file.  Please review the data and confirm that all required contract details have been entered.</description>
        <dueDateOffset>7</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Contract_Language_Amendment_Tracking__c.Date_Sent_to_File__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Review Contract</subject>
    </tasks>
</Workflow>
