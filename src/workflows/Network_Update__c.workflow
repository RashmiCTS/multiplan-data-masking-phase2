<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Notification_of_Network_Update_record</fullName>
        <description>Notification of Network Update record</description>
        <protected>false</protected>
        <recipients>
            <recipient>aliciaescamilla@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/NU_Notification</template>
    </alerts>
    <rules>
        <fullName>Network Update Notification</fullName>
        <actions>
            <name>Notification_of_Network_Update_record</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Network_Update__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>When a Network Update has been created.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
