<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <rules>
        <fullName>AE Best and Few %3E 20 days</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Task.Status</field>
            <operation>equals</operation>
            <value>Discuss Contract Addendum</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Task_Age__c</field>
            <operation>equals</operation>
            <value>20</value>
        </criteriaItems>
        <description>Assign tasks when an existing client&apos;s opportunity stage was updated to Best and Few 20 days ago.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AE Closed Sold %3E 10 days</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>equals</operation>
            <value>Receive Signed Contract Addendum</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Task_Age__c</field>
            <operation>equals</operation>
            <value>10</value>
        </criteriaItems>
        <description>Assigns task for existing client opportunities 10 days after the opportunity stage has been updated to Closed Sold.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AE Closed Sold %3E 5 days</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>equals</operation>
            <value>Receive Signed Contract Addendum</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Task_Age__c</field>
            <operation>equals</operation>
            <value>5</value>
        </criteriaItems>
        <description>Assigns task for existing client opportunities 5 days after the opportunity stage has been updated to Closed Sold.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>NSE %26 AE Above Funnel %3E 5 days</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>equals</operation>
            <value>Update Above Funnel Data Fields</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Task_Age__c</field>
            <operation>equals</operation>
            <value>5</value>
        </criteriaItems>
        <description>Assign tasks when opportunity stage was updated to Above Funnel 5 days ago.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>NSE %26 AE Closed Sold %3E 10 days</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>equals</operation>
            <value>Complete LOI/Contract Neg,Receive Signed Contract Addendum</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Task_Age__c</field>
            <operation>equals</operation>
            <value>10</value>
        </criteriaItems>
        <description>Assigns task for existing client and new business opportunities 10 days after the opportunity stage has been updated to Closed Sold.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>NSE Best and Few %3E 20 days</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>equals</operation>
            <value>Discuss LOI/Contract Changes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Task_Age__c</field>
            <operation>equals</operation>
            <value>20</value>
        </criteriaItems>
        <description>Assign tasks for new business opportunities 20 days after the opportunity stage has been updated to best and few.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>NSE Closed Sold %3E 10 days</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>equals</operation>
            <value>Complete LOI/Contract Neg</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Task_Age__c</field>
            <operation>equals</operation>
            <value>10</value>
        </criteriaItems>
        <description>Assigns task for new business opportunities 10 days after the opportunity stage has been updated to Closed Sold.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>NSE Closed Sold %3E 5 days</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>equals</operation>
            <value>Complete LOI/Contract Neg</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Task_Age__c</field>
            <operation>equals</operation>
            <value>5</value>
        </criteriaItems>
        <description>Assigns task for new business opportunities 5 days after the opportunity stage has been updated to Closed Sold.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
