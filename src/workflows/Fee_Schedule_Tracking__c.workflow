<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>FST_Completed_Email</fullName>
        <description>FST Completed Email</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>HCE_Request_Email_Templates/FST_Completed_Email</template>
    </alerts>
    <alerts>
        <fullName>Fee_Tracking_Schedule_Assigned</fullName>
        <description>Fee Tracking Schedule - Assigned</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>HCE_Request_Email_Templates/FST_Assigned</template>
    </alerts>
    <fieldUpdates>
        <fullName>Assignment_Date_Clear</fullName>
        <field>Assignment_Date__c</field>
        <name>Assignment Date - Clear</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assignment_Date_Set</fullName>
        <field>Assignment_Date__c</field>
        <formula>Today()</formula>
        <name>Assignment Date - Set</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Completed_Date_Clear</fullName>
        <field>Completed_Date__c</field>
        <name>Completed Date - Clear</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Completed_Date_Set</fullName>
        <field>Completed_Date__c</field>
        <formula>today()</formula>
        <name>Completed Date - Set</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Pended_Date_Set</fullName>
        <field>Pended_Date__c</field>
        <formula>today()</formula>
        <name>Pended Date - Set</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Text_Searcher</fullName>
        <field>Text_Searcher__c</field>
        <formula>TEXT(Market_Id__c)</formula>
        <name>Text Searcher</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Assignment Date - Clear</fullName>
        <actions>
            <name>Assignment_Date_Clear</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>left(PRIORVALUE(OwnerId), 3) = &apos;005&apos; &amp;&amp; left(OwnerId, 3) != &apos;005&apos; &amp;&amp; NOT(ISBLANK(Assignment_Date__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Assignment Date - Set</fullName>
        <actions>
            <name>Assignment_Date_Set</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>(NOT(ISNEW())&amp;&amp; left(PRIORVALUE(OwnerId), 3) != &apos;005&apos; &amp;&amp; left(OwnerId, 3) = &apos;005&apos;  &amp;&amp; ISBLANK(Assignment_Date__c))  ||  ( ISNEW() &amp;&amp; Owner:Queue.QueueName != &apos;FST - Unassigned Q&apos; &amp;&amp; isblank(Call_Id__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Completed Date - Clear</fullName>
        <actions>
            <name>Completed_Date_Clear</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>NOT(Begins(TEXT(Status__c), &quot;Closed -&quot;)) &amp;&amp;  Begins(TEXT(PRIORVALUE(Status__c)), &quot;Closed -&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Completed Date - Set</fullName>
        <actions>
            <name>FST_Completed_Email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Completed_Date_Set</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Fee_Schedule_Tracking__c.Status__c</field>
            <operation>startsWith</operation>
            <value>Closed -</value>
        </criteriaItems>
        <criteriaItems>
            <field>Fee_Schedule_Tracking__c.Completed_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Fee Tracking Schedule - Assigned</fullName>
        <actions>
            <name>Fee_Tracking_Schedule_Assigned</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>NOT(ISNEW()) &amp;&amp;  ISCHANGED(OwnerId)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Pended Date - Set</fullName>
        <actions>
            <name>Pended_Date_Set</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Fee_Schedule_Tracking__c.Status__c</field>
            <operation>equals</operation>
            <value>Pended</value>
        </criteriaItems>
        <criteriaItems>
            <field>Fee_Schedule_Tracking__c.Pended_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Text Searcher</fullName>
        <actions>
            <name>Text_Searcher</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>( ISNEW() &amp;&amp;
!ISBLANK(TEXT(Market_Id__c)) )

||

( Not(ISNEW()) &amp;&amp;
Ischanged(Market_Id__c) )

||

( Not(ISNEW()) &amp;&amp;
!ISBLANK(TEXT(Market_Id__c)) &amp;&amp;
ISBLANK(Text_Searcher__c) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
