<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <rules>
        <fullName>Notify Viant PP of Post Pay oppty</fullName>
        <active>false</active>
        <booleanFilter>1 AND (2 or 3)</booleanFilter>
        <criteriaItems>
            <field>Product2.Family</field>
            <operation>equals</operation>
            <value>Post-Pay Services</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Funnel_Stage__c</field>
            <operation>equals</operation>
            <value>70 - 85% = Best &amp; Few-negotiating price; engaging in contract review</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Funnel_Stage__c</field>
            <operation>equals</operation>
            <value>86%&gt; = Best &amp; Few-contract and deal points agreed to; awaiting signature</value>
        </criteriaItems>
        <description>Notify Viant Post Pay group when an Oppty with a Post Pay product hits B&amp;F.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Oppty greater than 250K</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Amount</field>
            <operation>greaterOrEqual</operation>
            <value>250,000</value>
        </criteriaItems>
        <description>When an oppty annualized revenue is over 250K, notify finance.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
