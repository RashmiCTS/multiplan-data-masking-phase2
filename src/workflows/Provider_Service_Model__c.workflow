<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>PSM_Conduct_Web_Portal_Trn</fullName>
        <field>Conduct_Web_Portal_Training_if_required__c</field>
        <literalValue>1</literalValue>
        <name>PSM Conduct Web Portal Trn</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PSM_Provider_Trg_Resource_Mat</fullName>
        <field>Review_Provider_Training_Resource_Mat__c</field>
        <literalValue>1</literalValue>
        <name>PSM Provider Trg / Resource Mat</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PSM_Validate_Provider_Info_Rpts</fullName>
        <field>Validate_Provider_Information_Reports__c</field>
        <literalValue>1</literalValue>
        <name>PSM Validate Provider Info Rpts</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Select All</fullName>
        <actions>
            <name>PSM_Conduct_Web_Portal_Trn</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PSM_Provider_Trg_Resource_Mat</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PSM_Validate_Provider_Info_Rpts</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Provider_Service_Model__c.Select_All__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>When selected, selection will trigger other options automatically..Record example https://na6.salesforce.com/a1N800000001hZR</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
