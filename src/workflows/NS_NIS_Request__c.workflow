<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Enter_in_DTS_Assigned_Email</fullName>
        <description>Enter in DTS Assigned Email</description>
        <protected>false</protected>
        <recipients>
            <field>Entered_into_DTS_for_HCFA_By__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>HCE_Request_Email_Templates/Enter_in_DTS_Assigned</template>
    </alerts>
    <alerts>
        <fullName>Enter_into_DTS_Complete</fullName>
        <description>Enter into DTS Complete</description>
        <protected>false</protected>
        <recipients>
            <field>Requester__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>HCE_Request_Email_Templates/Enter_into_DTS_Approved</template>
    </alerts>
    <alerts>
        <fullName>Enter_into_DTS_Rejected</fullName>
        <description>Enter into DTS Rejected</description>
        <protected>false</protected>
        <recipients>
            <field>Requester__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>HCE_Request_Email_Templates/Enter_into_DTS_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Fee_Schedule_COO_Approved_Email</fullName>
        <description>Fee Schedule COO Approved Email</description>
        <protected>false</protected>
        <recipients>
            <field>Negotiator_Approver__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>HCE_Request_Email_Templates/Fee_Schedule_COO_Approved</template>
    </alerts>
    <alerts>
        <fullName>Fee_Schedule_COO_Rejected_Email</fullName>
        <description>Fee Schedule COO Rejected Email</description>
        <protected>false</protected>
        <recipients>
            <field>COO_Approver__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>EVP_Approver__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>HCE_VP_Approver__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Negotiator_Approver__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>RD_Approver__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>SVP_Approver__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>HCE_Request_Email_Templates/Fee_Schedule_COO_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Fee_Schedule_EVP_Approved_Email</fullName>
        <description>Fee Schedule EVP Approved Email</description>
        <protected>false</protected>
        <recipients>
            <field>Negotiator_Approver__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>HCE_Request_Email_Templates/Fee_Schedule_EVP_Approved</template>
    </alerts>
    <alerts>
        <fullName>Fee_Schedule_EVP_Rejected_Email</fullName>
        <description>Fee Schedule EVP Rejected Email</description>
        <protected>false</protected>
        <recipients>
            <field>EVP_Approver__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>HCE_VP_Approver__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Negotiator_Approver__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>RD_Approver__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>SVP_Approver__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>HCE_Request_Email_Templates/Fee_Schedule_EVP_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Fee_Schedule_HCE_VP_Approved_Email</fullName>
        <description>Fee Schedule HCE VP Approved Email</description>
        <protected>false</protected>
        <recipients>
            <field>Negotiator_Approver__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>HCE_Request_Email_Templates/Fee_Schedule_HCE_VP_Approved</template>
    </alerts>
    <alerts>
        <fullName>Fee_Schedule_HCE_VP_Rejected_Email</fullName>
        <description>Fee Schedule HCE VP Rejected Email</description>
        <protected>false</protected>
        <recipients>
            <field>HCE_VP_Approver__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Negotiator_Approver__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>RD_Approver__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>SVP_Approver__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>HCE_Request_Email_Templates/Fee_Schedule_HCE_VP_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Fee_Schedule_RD_Approved_Email</fullName>
        <description>Fee Schedule RD Approved Email</description>
        <protected>false</protected>
        <recipients>
            <field>Negotiator_Approver__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>HCE_Request_Email_Templates/Fee_Schedule_RD_Approved</template>
    </alerts>
    <alerts>
        <fullName>Fee_Schedule_RD_Rejected_Email</fullName>
        <description>Fee Schedule RD Rejected Email</description>
        <protected>false</protected>
        <recipients>
            <field>Negotiator_Approver__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>RD_Approver__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>HCE_Request_Email_Templates/Fee_Schedule_RD_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Fee_Schedule_SVP_Approved_Email</fullName>
        <description>Fee Schedule SVP Approved Email</description>
        <protected>false</protected>
        <recipients>
            <field>Negotiator_Approver__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>HCE_Request_Email_Templates/Fee_Schedule_SVP_Approved</template>
    </alerts>
    <alerts>
        <fullName>Fee_Schedule_SVP_Rejected_Email</fullName>
        <description>Fee Schedule SVP Rejected Email</description>
        <protected>false</protected>
        <recipients>
            <field>Negotiator_Approver__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>RD_Approver__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>SVP_Approver__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>HCE_Request_Email_Templates/Fee_Schedule_SVP_Rejected</template>
    </alerts>
    <alerts>
        <fullName>HCE_Analyst_Status_Closed</fullName>
        <description>HCE Analyst Status Closed</description>
        <protected>false</protected>
        <recipients>
            <field>Requester__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>HCE_Request_Email_Templates/HCE_Analyst_Status_Closed</template>
    </alerts>
    <alerts>
        <fullName>HCE_Analyst_Status_Closed_Rate_Ops</fullName>
        <description>HCE Analyst Status Closed (Rate Ops)</description>
        <protected>false</protected>
        <recipients>
            <field>HCE_Analyst__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Requester__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>HCE_Request_Email_Templates/HCE_Analyst_Status_Closed_RO</template>
    </alerts>
    <alerts>
        <fullName>HCE_Rate_Exhibit_Assigned_Notifiy_Requester</fullName>
        <description>HCE Rate Exhibit Assigned Notifiy Requester</description>
        <protected>false</protected>
        <recipients>
            <field>Requester__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>HCE_Request_Email_Templates/HCE_Rate_Exhibit_Assigned_Notifiy_Requester</template>
    </alerts>
    <alerts>
        <fullName>HCE_Rate_Exhibit_Assigned_Notifiy_Requester_v2</fullName>
        <description>HCE Rate Exhibit Assigned Notifiy Requester v2</description>
        <protected>false</protected>
        <recipients>
            <field>Requester__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>HCE_Request_Email_Templates/HCE_Rate_Exhibit_Assigned_Notifiy_Requester</template>
    </alerts>
    <alerts>
        <fullName>HCE_Req_UB_Final_Rates</fullName>
        <ccEmails>NSLFacRate=Example@multiplan.com</ccEmails>
        <description>HCE Req UB Final Rates</description>
        <protected>false</protected>
        <recipients>
            <field>HCE_Analyst__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>HCE_Request_Email_Templates/HCE_Req_UB_Final_Rates_Email</template>
    </alerts>
    <alerts>
        <fullName>HCE_Req_UB_Pending_Final_Rates</fullName>
        <description>HCE Req UB Pending Final Rates</description>
        <protected>false</protected>
        <recipients>
            <field>Requester__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>HCE_Request_Email_Templates/HCE_Req_UB_Pending_Final_Rates_Email</template>
    </alerts>
    <alerts>
        <fullName>HCE_Request_Assigned_to_HCE_Analyst_notify_Requster</fullName>
        <description>HCE Request  Assigned to HCE Analyst notify Requster</description>
        <protected>false</protected>
        <recipients>
            <field>Requester__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>HCE_Request_Email_Templates/HCE_Assigned_to_Analyst_Notifiy_Requester</template>
    </alerts>
    <alerts>
        <fullName>HCE_Request_Reassigned_to_HCE_Analyst</fullName>
        <description>HCE Request Reassigned to HCE Analyst</description>
        <protected>false</protected>
        <recipients>
            <field>HCE_Analyst__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>HCE_Request_Email_Templates/HCE_Request_Reassigned_to_HCE_Analyst</template>
    </alerts>
    <alerts>
        <fullName>HCE_Request_is_Closed_Canceled_v1</fullName>
        <description>HCE Request is Closed - Canceled v1</description>
        <protected>false</protected>
        <recipients>
            <field>Rate_Exhibit_Review_Approval_By__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>HCE_Request_Email_Templates/HCE_Request_is_Closed_Canceled</template>
    </alerts>
    <alerts>
        <fullName>HCE_Request_is_Closed_Canceled_v2</fullName>
        <description>HCE Request is Closed - Canceled v2</description>
        <protected>false</protected>
        <recipients>
            <field>Entered_into_DTS_for_HCFA_By__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>HCE_Request_Email_Templates/HCE_Request_is_Closed_Canceled</template>
    </alerts>
    <alerts>
        <fullName>HCE_Revenue_Impact_Request_is_Canceled</fullName>
        <description>HCE Revenue Impact Request is Canceled</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>HCE_Request_Email_Templates/HCE_Revenue_Impact_Request_is_Canceled</template>
    </alerts>
    <alerts>
        <fullName>HCE_Revenue_Impact_Request_is_Complete</fullName>
        <description>HCE Revenue Impact Request is Complete</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>HCE_Request_Email_Templates/HCE_Revenue_Impact_Request_is_Complete</template>
    </alerts>
    <alerts>
        <fullName>HCFA_ready_for_Approval</fullName>
        <description>HCFA ready for Approval</description>
        <protected>false</protected>
        <recipients>
            <field>Requester__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>HCE_Request_Email_Templates/HCFA_ready_for_Approval_Email</template>
    </alerts>
    <alerts>
        <fullName>ND_HCE_Request_Work_Comp_Model_HCFA</fullName>
        <description>ND - HCE Request - Work Comp – Model HCFA</description>
        <protected>false</protected>
        <recipients>
            <recipient>carol.mandernach@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>geralyn.wiechert@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>james.groat@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>joanna.skoczen@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>NSAutonotificationadminsonly/NS_NIS_Req_Notification_Work_Comp_Model_HCFA</template>
    </alerts>
    <alerts>
        <fullName>ND_HCE_Request_Work_Comp_Model_HCFA_TEST</fullName>
        <description>ND - HCE Request - Work Comp – Model HCFA TEST</description>
        <protected>false</protected>
        <recipients>
            <recipient>robert.mcgrath@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>NSAutonotificationadminsonly/NS_NIS_Req_Notification_Work_Comp_Model_HCFA</template>
    </alerts>
    <alerts>
        <fullName>ND_HCE_Request_Work_Comp_Model_IB</fullName>
        <description>ND - HCE Request - Work Comp – Model IB</description>
        <protected>false</protected>
        <recipients>
            <recipient>marc.olsson@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>NSAutonotificationadminsonly/NS_NIS_Req_Notification_Work_Comp_Model_IB</template>
    </alerts>
    <alerts>
        <fullName>ND_HCE_Request_Work_Comp_Model_IB2</fullName>
        <description>ND - HCE Request - Work Comp – Model IB</description>
        <protected>false</protected>
        <recipients>
            <recipient>marc.olsson@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>NSAutonotificationadminsonly/NS_NIS_Req_Notification_Work_Comp_Model_IB</template>
    </alerts>
    <alerts>
        <fullName>ND_HCE_Request_Work_Comp_Model_UB</fullName>
        <description>ND - HCE Request - Work Comp – Model UB</description>
        <protected>false</protected>
        <recipients>
            <recipient>marc.olsson@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>NSAutonotificationadminsonly/NS_NIS_Req_Notification_Work_Comp_Model_UB</template>
    </alerts>
    <alerts>
        <fullName>NS_HCE_request_Notification_Work_Comp_Model_HCFA_NE_NC</fullName>
        <description>_NS-HCE request Notification_Work Comp – Model HCFA_NE_NC</description>
        <protected>false</protected>
        <recipients>
            <recipient>joanna.skoczen@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>NSAutonotificationadminsonly/NS_NIS_Req_Notification_Work_Comp_Model_HCFA</template>
    </alerts>
    <alerts>
        <fullName>NS_HCE_request_Notification_Work_Comp_Model_HCFA_SE_SC</fullName>
        <description>_NS-HCE request Notification_Work Comp – Model HCFA_SE_SC</description>
        <protected>false</protected>
        <recipients>
            <recipient>geralyn.wiechert@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>NSAutonotificationadminsonly/NS_NIS_Req_Notification_Work_Comp_Model_HCFA</template>
    </alerts>
    <alerts>
        <fullName>New_Report_Notification_of_Adhoc_10_day</fullName>
        <description>New Report Notification of Adhoc 10-day</description>
        <protected>false</protected>
        <recipients>
            <recipient>aliciaescamilla@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/HCE_Adhoc_10_day_Notification</template>
    </alerts>
    <alerts>
        <fullName>NotificationofNSHCErequest</fullName>
        <ccEmails>HCEAnalysisRequests@MultiPlan.com</ccEmails>
        <description>Notification of NS-HCE request</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>NSAutonotificationadminsonly/NSNISReqNotification</template>
    </alerts>
    <alerts>
        <fullName>Rate_Exhibit_Review_Approval_Status_Closed_UB</fullName>
        <description>Rate Exhibit Review Approval Status Closed UB</description>
        <protected>false</protected>
        <recipients>
            <field>HCE_Analyst__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>HCE_Request_Email_Templates/Rate_Exhibit_Review_Approval_Status_Closed_UB_Email</template>
    </alerts>
    <alerts>
        <fullName>Rate_Exhibit_Review_Approved_Email</fullName>
        <description>Rate Exhibit Review: Approved Email</description>
        <protected>false</protected>
        <recipients>
            <field>Requester__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>HCE_Request_Email_Templates/Rate_Exhibit_Review_Closed</template>
    </alerts>
    <alerts>
        <fullName>Rate_Exhibit_Review_Assigned</fullName>
        <description>Rate Exhibit Review Assigned</description>
        <protected>false</protected>
        <recipients>
            <field>Rate_Exhibit_Review_Approval_By__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>HCE_Request_Email_Templates/Rate_Exhibit_Review_Assigned</template>
    </alerts>
    <alerts>
        <fullName>Rate_Exhibit_Review_Assigned_Notifiy_Requester</fullName>
        <description>Rate Exhibit Review Assigned Notifiy Requester</description>
        <protected>false</protected>
        <recipients>
            <field>Requester__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>HCE_Request_Email_Templates/HCE_Assigned_to_Analyst_Notifiy_Requester</template>
    </alerts>
    <alerts>
        <fullName>Rate_Exhibit_Review_Closed_v1</fullName>
        <description>Rate Exhibit Review: Closed v1</description>
        <protected>false</protected>
        <recipients>
            <field>Rate_Exhibit_Review_Approval_By__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>HCE_Request_Email_Templates/Rate_Exhibit_Review_Closed_V1</template>
    </alerts>
    <alerts>
        <fullName>Rate_Exhibit_Review_Closed_v2</fullName>
        <description>Rate Exhibit Review: Closed v2</description>
        <protected>false</protected>
        <recipients>
            <field>Rate_Exhibit_Review_Approval_By__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>HCE_Request_Email_Templates/Rate_Exhibit_Review_Closed_V2</template>
    </alerts>
    <alerts>
        <fullName>Rate_Exhibit_Review_Closed_v3</fullName>
        <description>Rate Exhibit Review: Closed v3</description>
        <protected>false</protected>
        <recipients>
            <field>Rate_Exhibit_Review_Approval_By__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>HCE_Request_Email_Templates/Rate_Exhibit_Review_Closed_V3</template>
    </alerts>
    <alerts>
        <fullName>TESTTE</fullName>
        <description>_NS-HCE request Notification_Work Comp – Model HCFA_West</description>
        <protected>false</protected>
        <recipients>
            <recipient>james.groat@multiplan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>NSAutonotificationadminsonly/NS_NIS_Req_Notification_Work_Comp_Model_HCFA</template>
    </alerts>
    <fieldUpdates>
        <fullName>All_HCE_Approvals_Complete</fullName>
        <field>Request_Status__c</field>
        <literalValue>Closed - Successful</literalValue>
        <name>All HCE Approvals Complete</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_HCE_Archive_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>HCE_Archived_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to HCE Archive Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>COO_Approval_Completed_Date</fullName>
        <field>COO_Approval_Completed_Date__c</field>
        <formula>now()</formula>
        <name>COO Approval - Completed Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>COO_Approval_Status_App</fullName>
        <field>COO_Approver_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>COO Approval - Status (App)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>COO_Approval_Status_Rej</fullName>
        <field>COO_Approver_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>COO Approval - Status (Rej)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EVP_Approval_Completed_Date</fullName>
        <field>EVP_Approval_Completed_Date__c</field>
        <formula>now()</formula>
        <name>EVP Approval - Completed Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EVP_Approval_Status_App</fullName>
        <field>EVP_Approver_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>EVP Approval - Status (App)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EVP_Approval_Status_Rej</fullName>
        <field>EVP_Approver_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>EVP Approval - Status (Rej)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Enter_into_DTS_Complete_Final_Closeout</fullName>
        <field>Request_Status__c</field>
        <literalValue>Closed - Successful</literalValue>
        <name>Enter into DTS Complete Final Closeout</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Enter_into_DTS_HCFA_Complete_Date</fullName>
        <field>Entered_into_DTS_for_HCFA_Date__c</field>
        <formula>today()</formula>
        <name>Enter into DTS HCFA Complete Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Enter_into_DTS_Rejected</fullName>
        <field>Request_Status__c</field>
        <literalValue>Closed - Unsuccessful</literalValue>
        <name>Enter into DTS Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Enter_into_DTS_for_HCFA_Requested_1</fullName>
        <field>Enter_into_DTS_for_HCFA_Status__c</field>
        <literalValue>New</literalValue>
        <name>Enter into DTS for HCFA: Requested 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Enter_into_DTS_for_HCFA_Requested_1_2</fullName>
        <field>Enter_into_DTS_for_HCFA_Requested_Date__c</field>
        <formula>today()</formula>
        <name>Enter into DTS for HCFA: Requested 1_2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Enter_into_DTS_for_HCFA_Requested_2</fullName>
        <field>Enter_into_DTS_for_HCFA_Requested_Date__c</field>
        <name>Enter into DTS for HCFA: Requested 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Final_Update_Set_Approval_Status_App</fullName>
        <field>FS_Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Final Update Set Approval Status (App)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Final_Update_Set_Approval_Status_Rej</fullName>
        <field>FS_Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Final Update Set Approval Status (Rej)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>HCE_Analyst_Status_Approval_Status_Pend</fullName>
        <field>HCE_Analyst_Status_Pended_Date__c</field>
        <formula>today()</formula>
        <name>HCE Analyst Status: Approval Status Pend</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>HCE_Analyst_Status_Closed</fullName>
        <field>HCE_Analyst_Closed_Date__c</field>
        <formula>today()</formula>
        <name>HCE Analyst Status Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>HCE_Analyst_Status_Reopened</fullName>
        <field>HCE_Analyst_Closed_Date__c</field>
        <name>HCE Analyst Status Reopened</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>HCE_Approval_Rejected</fullName>
        <field>Request_Status__c</field>
        <literalValue>Closed - Unsuccessful</literalValue>
        <name>HCE Approval Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>HCE_Request_Canceled_by_HCE_User</fullName>
        <field>Request_Status__c</field>
        <literalValue>Closed - Canceled</literalValue>
        <name>HCE Request Canceled by HCE User</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>HCE_Request_Canceled_by_Requester_1</fullName>
        <field>HCE_Analyst_Status__c</field>
        <literalValue>Closed - Canceled</literalValue>
        <name>HCE Request Canceled by Requester 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>HCE_Request_Canceled_by_Requester_1a2</fullName>
        <field>HCE_Analyst_Closed_Date__c</field>
        <formula>today()</formula>
        <name>HCE Request Canceled by Requester 1a2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>HCE_Request_Canceled_by_Requester_1b</fullName>
        <field>Rate_Exhibit_Review_Approval_Status__c</field>
        <literalValue>Closed - Canceled</literalValue>
        <name>HCE Request Canceled by Requester 1b</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>HCE_Request_Canceled_by_Requester_1b2</fullName>
        <field>Rate_Exhibit_Review_Approval_Date__c</field>
        <formula>Today()</formula>
        <name>HCE Request Canceled by Requester 1b2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>HCE_Request_Canceled_by_Requester_1c</fullName>
        <field>Enter_into_DTS_for_HCFA_Status__c</field>
        <literalValue>Closed - Canceled</literalValue>
        <name>HCE Request Canceled by Requester 1c</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>HCE_Request_Canceled_by_Requester_1c2</fullName>
        <field>Entered_into_DTS_for_HCFA_Date__c</field>
        <formula>today()</formula>
        <name>HCE Request Canceled by Requester 1c2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>HCE_VP_Approval_Completed_Date</fullName>
        <field>HCE_VP_Approval_Completed_Date__c</field>
        <formula>now()</formula>
        <name>HCE VP Approval - Completed Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>HCE_VP_Approval_Status_App</fullName>
        <field>HCE_VP_Approver_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>HCE VP Approval - Status (App)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>HCE_VP_Approval_Status_Rej</fullName>
        <field>HCE_VP_Approver_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>HCE VP Approval - Status (Rej)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NS_NIS_Req_Only</fullName>
        <field>NS_NIS_Req_N_Only__c</field>
        <formula>right(NS_NIS_Req__c,5 )&amp; &quot; &quot;&amp;  left(NS_NIS_Req__c,6) &amp; right(NS_NIS_Req__c,6)</formula>
        <name>NS-NIS Req. # Only</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Negotiator_Approval_Completed_Date</fullName>
        <field>Negotiator_Approval_Completed_Date__c</field>
        <formula>now()</formula>
        <name>Negotiator Approval - Completed Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Negotiator_Approval_Status_App</fullName>
        <field>Negotiator_Approver_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Negotiator Approval - Status (App)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Negotiator_Approval_Status_Rej</fullName>
        <field>Negotiator_Approver_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Negotiator Approval - Status (Rej)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RD_Approval_Completed_Date</fullName>
        <field>RD_Approval_Completed_Date__c</field>
        <formula>now()</formula>
        <name>RD Approval - Completed Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RD_Approval_Status_App</fullName>
        <field>RD_Approver_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>RD Approval - Status (App)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RD_Approval_Status_Rej</fullName>
        <field>RD_Approver_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>RD Approval - Status (Rej)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Rate_Exhibit_Review_Approval_Status_1</fullName>
        <field>Rate_Exhibit_Review_Approval_Date__c</field>
        <formula>today()</formula>
        <name>Rate Exhibit Review: Approval Status 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Rate_Exhibit_Review_Approval_Status_2</fullName>
        <field>Rate_Exhibit_Review_Approval_Date__c</field>
        <name>Rate Exhibit Review: Approval Status 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Rate_Exhibit_Review_Approval_Status_Pen</fullName>
        <field>Rate_Exhibit_Review_Status_Pended_Date__c</field>
        <formula>today()</formula>
        <name>Rate Exhibit Review: Approval Status Pen</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recall_Null_COO_Approval_Status</fullName>
        <field>COO_Approver_Status__c</field>
        <name>Recall: Null COO Approval Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recall_Null_EVP_Approval_Status</fullName>
        <field>EVP_Approver_Status__c</field>
        <name>Recall: Null EVP Approval Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recall_Null_HCE_VP_Approval_Status</fullName>
        <field>HCE_VP_Approver_Status__c</field>
        <name>Recall: Null HCE VP Approval Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recall_Null_Negotiator_Approval_Status</fullName>
        <field>Negotiator_Approver_Status__c</field>
        <name>Recall: Null Negotiator Approval Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recall_Null_RD_Approval_Status</fullName>
        <field>RD_Approver_Status__c</field>
        <name>Recall: Null RD Approval Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recall_Null_SVP_Approval_Status</fullName>
        <field>SVP_Approver_Status__c</field>
        <name>Recall: Null SVP Approval Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recall_Set_FS_Approval_Status</fullName>
        <field>FS_Approval_Status__c</field>
        <literalValue>Recalled</literalValue>
        <name>Recall: Set FS Approval Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Rej_Null_out_COO_Status</fullName>
        <field>COO_Approver_Status__c</field>
        <name>Rej Null out COO Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Rej_Null_out_EVP_Status</fullName>
        <field>EVP_Approver_Status__c</field>
        <name>Rej Null out EVP Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Rej_Null_out_HCE_VP_Status</fullName>
        <field>HCE_VP_Approver_Status__c</field>
        <name>Rej Null out HCE VP Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Rej_Null_out_RD_Status</fullName>
        <field>RD_Approver_Status__c</field>
        <name>Rej Null out RD Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Rej_Null_out_SVP_Status</fullName>
        <field>SVP_Approver_Status__c</field>
        <name>Rej Null out SVP Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Request_Rate_Exhibit_Review_1</fullName>
        <field>Rate_Exhibit_Review_Request_Date__c</field>
        <formula>today()</formula>
        <name>Request: Rate Exhibit Review 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Request_Rate_Exhibit_Review_1_1</fullName>
        <field>Rate_Exhibit_Review_Approval_Status__c</field>
        <literalValue>New</literalValue>
        <name>Request: Rate Exhibit Review 1_2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Request_Rate_Exhibit_Review_2</fullName>
        <field>Rate_Exhibit_Review_Request_Date__c</field>
        <name>Request: Rate Exhibit Review 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Request_Rate_Exhibit_Review_2b</fullName>
        <field>Rate_Exhibit_Review_Approval_Status__c</field>
        <name>Request: Rate Exhibit Review 2b</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Request_Rate_Exhibit_Review_2c</fullName>
        <field>Rate_Exhibit_Review_Approval_By__c</field>
        <name>Request: Rate Exhibit Review 2c</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Request_Rate_Exhibit_Review_2d</fullName>
        <field>Rate_Exhibit_Review_Approval_Date__c</field>
        <name>Request: Rate Exhibit Review 2d</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Request_Rate_Exhibit_Review_dt</fullName>
        <field>Rate_Exhibit_Review_Request_Date_Time__c</field>
        <formula>Now()</formula>
        <name>Request: Rate Exhibit Review dt</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Request_Rate_Exhibit_Review_dt_null</fullName>
        <field>Rate_Exhibit_Review_Request_Date_Time__c</field>
        <name>Request: Rate Exhibit Review dt null</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SVP_Approval_Completed_Date</fullName>
        <field>SVP_Approval_Completed_Date__c</field>
        <formula>now()</formula>
        <name>SVP Approval - Completed Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SVP_Approval_Status_App</fullName>
        <field>SVP_Approver_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>SVP Approval - Status (App)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SVP_Approval_Status_Rej</fullName>
        <field>SVP_Approver_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>SVP Approval - Status (Rej)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_FS_Approval_Status</fullName>
        <field>FS_Approval_Status__c</field>
        <literalValue>Pending Approvals</literalValue>
        <name>Set FS Approval Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UB_Final_Rate_update_Rate_Exhibit_Req</fullName>
        <field>Request_Rate_Exhibit_Review__c</field>
        <literalValue>Request Rate Exhibit Review</literalValue>
        <name>UB Final Rate update Rate Exhibit Req</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Enter in DTS Assigned</fullName>
        <actions>
            <name>Enter_in_DTS_Assigned_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>not(isblank(Entered_into_DTS_for_HCFA_By__c)) &amp;&amp; ischanged(Entered_into_DTS_for_HCFA_By__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Enter into DTS Complete</fullName>
        <actions>
            <name>Enter_into_DTS_Complete_Final_Closeout</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Enter_into_DTS_HCFA_Complete_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>text( Enter_into_DTS_for_HCFA_Status__c ) = &apos;Closed - Complete&apos; &amp;&amp;  text(PRIORVALUE(Enter_into_DTS_for_HCFA_Status__c)) != &apos;Closed - Complete&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Enter into DTS Rejected</fullName>
        <actions>
            <name>Enter_into_DTS_Rejected</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>text( Enter_into_DTS_for_HCFA_Status__c ) = &apos;Closed - Rejected&apos; &amp;&amp;  text(PRIORVALUE(Enter_into_DTS_for_HCFA_Status__c)) != &apos;Closed - Rejected&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Enter into DTS for HCFA%3A Requested 1</fullName>
        <actions>
            <name>Enter_into_DTS_for_HCFA_Requested_1</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Enter_into_DTS_for_HCFA_Requested_1_2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>( Enter_into_DTS_for_HCFA_Requested__c = true &amp;&amp; ischanged(Enter_into_DTS_for_HCFA_Requested__c) ) || 

( isnew() &amp;&amp; Enter_into_DTS_for_HCFA_Requested__c = true)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Enter into DTS for HCFA%3A Requested 2</fullName>
        <actions>
            <name>Enter_into_DTS_for_HCFA_Requested_2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Enter_into_DTS_for_HCFA_Requested__c = false &amp;&amp; ischanged(Enter_into_DTS_for_HCFA_Requested__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>HCE Analyst Status Closed %28Rate Ops%29</fullName>
        <actions>
            <name>HCE_Analyst_Status_Closed_Rate_Ops</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>contains(text(HCE_Analyst_Status__c), &apos;Closed -&apos;) &amp;&amp;  
not(contains(text(PRIORVALUE(HCE_Analyst_Status__c)), &apos;Closed -&apos;)) &amp;&amp;
RecordType.Name = &apos;Rate Operations&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>HCE Analyst Status Closed %28not Rate Ops%29</fullName>
        <actions>
            <name>HCE_Analyst_Status_Closed</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Deactivated per Rob 7/8/15</description>
        <formula>contains(text(HCE_Analyst_Status__c), &apos;Closed -&apos;) &amp;&amp;  
not(contains(text(PRIORVALUE(HCE_Analyst_Status__c)), &apos;Closed -&apos;)) &amp;&amp; 
RecordType.Name != &apos;Rate Operations&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>HCE Analyst Status Reopened</fullName>
        <actions>
            <name>HCE_Analyst_Status_Reopened</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>contains(text(PRIORVALUE(HCE_Analyst_Status__c)), &apos;Closed -&apos;) &amp;&amp; not(contains(text(HCE_Analyst_Status__c), &apos;Closed -&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>HCE Analyst Status%3A Approval Status Pended</fullName>
        <actions>
            <name>HCE_Analyst_Status_Approval_Status_Pend</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>text( HCE_Analyst_Status__c ) = &apos;Pended&apos; &amp;&amp;   isblank( HCE_Analyst_Status_Pended_Date__c  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>HCE Req UB Final Rates</fullName>
        <actions>
            <name>HCE_Req_UB_Final_Rates</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>UB_Final_Rate_update_Rate_Exhibit_Req</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>RecordType.Name = &apos;Reimbursement and Implementation - UB&apos; &amp;&amp; Ischanged(Rate_Negotiation_Type__c) &amp;&amp; text(Rate_Negotiation_Type__c) = &apos;Final Rates&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>HCE Req UB Pending Final Rates</fullName>
        <actions>
            <name>HCE_Req_UB_Pending_Final_Rates</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>RecordType.Name = &apos;Reimbursement and Implementation - UB&apos; &amp;&amp; Ischanged( HCE_Analyst_Status__c ) &amp;&amp; text(HCE_Analyst_Status__c) = &apos;Pending Final Rates Request&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>HCE Request  Assigned to HCE Analyst notify Requster</fullName>
        <actions>
            <name>HCE_Request_Assigned_to_HCE_Analyst_notify_Requster</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>(
IsNew() &amp;&amp; not(isblank(HCE_Analyst__c)) ) ||
( not(isNew()) &amp;&amp; isblank(PRIORVALUE(HCE_Analyst__c)) &amp;&amp;

not(isblank(HCE_Analyst__c))
) &amp;&amp;
RecordType.Name = &apos;Rate Operations&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>HCE Request Canceled by HCE User</fullName>
        <actions>
            <name>HCE_Request_Canceled_by_HCE_User</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>RecordType.Name != &apos;Legacy Request&apos; &amp;&amp; text(Request_Status__c ) != &apos;Closed - Canceled&apos; &amp;&amp;   ( text(HCE_Analyst_Status__c) = &apos;Closed - Canceled&apos; || text( Rate_Exhibit_Review_Approval_Status__c ) = &apos;Closed - Canceled&apos; || text( Enter_into_DTS_for_HCFA_Status__c ) = &apos;Closed - Canceled&apos;  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>HCE Request Canceled by Requester 1a</fullName>
        <actions>
            <name>HCE_Request_Canceled_by_Requester_1</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>HCE_Request_Canceled_by_Requester_1a2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>RecordType.Name != &apos;Legacy Request&apos; &amp;&amp; text(Request_Status__c ) = &apos;Closed - Canceled&apos; &amp;&amp;  not(contains(text(HCE_Analyst_Status__c), &apos;Closed -&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>HCE Request Canceled by Requester 1b</fullName>
        <actions>
            <name>HCE_Request_Canceled_by_Requester_1b</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>HCE_Request_Canceled_by_Requester_1b2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>RecordType.Name = &apos;Reimbursement and Implementation - HCFA&apos; &amp;&amp; text(Request_Status__c ) = &apos;Closed - Canceled&apos; &amp;&amp;  not(contains(text(Rate_Exhibit_Review_Approval_Status__c), &apos;Closed -&apos;)) &amp;&amp; not(isblank(text(Rate_Exhibit_Review_Approval_Status__c)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>HCE Request Canceled by Requester 1c</fullName>
        <actions>
            <name>HCE_Request_Canceled_by_Requester_1c</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>HCE_Request_Canceled_by_Requester_1c2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>RecordType.Name = &apos;Reimbursement and Implementation - HCFA&apos; &amp;&amp; text(Request_Status__c ) = &apos;Closed - Canceled&apos; &amp;&amp;  not(contains(text( Enter_into_DTS_for_HCFA_Status__c ), &apos;Closed -&apos;)) &amp;&amp; not(isblank(text(Enter_into_DTS_for_HCFA_Status__c)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>HCE Request Reassigned to HCE Analyst</fullName>
        <actions>
            <name>HCE_Request_Reassigned_to_HCE_Analyst</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Per Rob and Group: only send reassignment emails for Rate Operations. 7/9/15</description>
        <formula>not(isnew()) &amp;&amp; 
not(isblank(priorvalue(HCE_Analyst__c))) &amp;&amp; ischanged(HCE_Analyst__c) &amp;&amp;
RecordType.Name = &apos;Rate Operations&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>HCE Request%3A Closed - Canceled - notify v1</fullName>
        <actions>
            <name>HCE_Request_is_Closed_Canceled_v1</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>RecordType.Name = &apos;Reimbursement and Implementation - HCFA&apos; &amp;&amp;  text(Request_Status__c ) = &apos;Closed - Canceled&apos; &amp;&amp;  ischanged(Request_Status__c) &amp;&amp;   not(isblank( Rate_Exhibit_Review_Approval_By__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>HCE Request%3A Closed - Canceled - notify v2</fullName>
        <actions>
            <name>HCE_Request_is_Closed_Canceled_v2</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>RecordType.Name = &apos;Reimbursement and Implementation - HCFA&apos; &amp;&amp;  text(Request_Status__c ) = &apos;Closed - Canceled&apos; &amp;&amp;  ischanged(Request_Status__c) &amp;&amp;   not(isblank( Entered_into_DTS_for_HCFA_By__c )) &amp;&amp;  Entered_into_DTS_for_HCFA_By__c != Rate_Exhibit_Review_Approval_By__c</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>HCE Revenue Impact Request is Canceled</fullName>
        <actions>
            <name>HCE_Revenue_Impact_Request_is_Canceled</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>CONTAINS( RecordType.Name, &quot;Revenue Impact&quot; ) &amp;&amp;
ISPICKVAL( Request_Status__c, &quot;Canceled&quot;) &amp;&amp;
ISPICKVAL( HCE_Analyst_Status__c, &quot;Closed - Canceled&quot; )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>HCE Revenue Impact Request is Complete</fullName>
        <actions>
            <name>HCE_Revenue_Impact_Request_is_Complete</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>CONTAINS( RecordType.Name, &quot;Revenue Impact&quot; ) &amp;&amp;
ISPICKVAL( Request_Status__c, &quot;Complete&quot;) &amp;&amp;
ISPICKVAL( HCE_Analyst_Status__c, &quot;Closed - Complete&quot; )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>HCFA ready for Approval</fullName>
        <actions>
            <name>HCFA_ready_for_Approval</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>RecordType.Name = &apos;Reimbursement and Implementation - HCFA&apos; &amp;&amp; text(HCE_Analyst_Status__c) = &apos;Closed - Complete&apos; &amp;&amp; text(Rate_Exhibit_Review_Approval_Status__c) = &apos;Closed - Approved&apos; &amp;&amp;  (ischanged(Rate_Exhibit_Review_Approval_Status__c) || ischanged(HCE_Analyst_Status__c) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>NS-HCE request Notification</fullName>
        <actions>
            <name>NotificationofNSHCErequest</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>NS_NIS_Request__c.NS_NIS_Req__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>NS_NIS_Request__c.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Rate Operations</value>
        </criteriaItems>
        <description>When an NS-HCE request has been submitted - notification.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>NS-HCE request Notification - Adhoc 10-day</fullName>
        <actions>
            <name>New_Report_Notification_of_Adhoc_10_day</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>NS_NIS_Request__c.Request__c</field>
            <operation>equals</operation>
            <value>Adhoc 10-day</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>NS-HCE request Notification_Work Comp %E2%80%93 Model HCFA</fullName>
        <actions>
            <name>ND_HCE_Request_Work_Comp_Model_HCFA</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>NS_NIS_Request__c.Request__c</field>
            <operation>equals</operation>
            <value>Work Comp – Model HCFA,Work Comp – Model UB</value>
        </criteriaItems>
        <description>When an NS-HCE (request type: Work Comp – Model HCFA) request has been submitted, a notification to be sent to: joanna.skoczen@multiplan.com, james.groat@multiplan.com, Geralyn.Wiechert@multiplan.com, Carol.Mandernach@multiplan.com</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>NS-HCE request Notification_Work Comp %E2%80%93 Model HCFA_NE_NC</fullName>
        <actions>
            <name>NS_HCE_request_Notification_Work_Comp_Model_HCFA_NE_NC</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>NS_NIS_Request__c.Request__c</field>
            <operation>equals</operation>
            <value>Work Comp – Model HCFA,Work Comp – Model UB</value>
        </criteriaItems>
        <criteriaItems>
            <field>NS_NIS_Request__c.Region_Name__c</field>
            <operation>equals</operation>
            <value>Northeast,North Central</value>
        </criteriaItems>
        <description>When an NS-HCE (request type: Work Comp – Model HCFA) request has been submitted, a notification to be sent to: Joanna.Skoczen@multiplan.com</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>NS-HCE request Notification_Work Comp %E2%80%93 Model HCFA_SE_SC</fullName>
        <actions>
            <name>NS_HCE_request_Notification_Work_Comp_Model_HCFA_SE_SC</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>NS_NIS_Request__c.Request__c</field>
            <operation>equals</operation>
            <value>Work Comp – Model HCFA,Work Comp – Model UB</value>
        </criteriaItems>
        <criteriaItems>
            <field>NS_NIS_Request__c.Region_Name__c</field>
            <operation>equals</operation>
            <value>South Central,Southeast</value>
        </criteriaItems>
        <description>When an NS-HCE (request type: Work Comp – Model HCFA) request has been submitted, a notification to be sent to: Geralyn.Wiechert@multiplan.com</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>NS-HCE request Notification_Work Comp %E2%80%93 Model HCFA_West</fullName>
        <actions>
            <name>TESTTE</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>NS_NIS_Request__c.Request__c</field>
            <operation>equals</operation>
            <value>Work Comp – Model HCFA,Work Comp – Model UB</value>
        </criteriaItems>
        <criteriaItems>
            <field>NS_NIS_Request__c.Region_Name__c</field>
            <operation>equals</operation>
            <value>West</value>
        </criteriaItems>
        <description>When an NS-HCE (request type: Work Comp – Model HCFA) request has been submitted, a notification to be sent to:</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>NS-HCE request Notification_Work Comp %E2%80%93 Model IB</fullName>
        <actions>
            <name>ND_HCE_Request_Work_Comp_Model_IB2</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>NS_NIS_Request__c.Request__c</field>
            <operation>equals</operation>
            <value>Work Comp – Model IB</value>
        </criteriaItems>
        <description>When an NS-HCE (request type: Work Comp – Model IB) request has been submitted, a notification to be sent to: Marc.Olsson@multiplan.com</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>NS-HCE request Notification_Work Comp %E2%80%93 Model UB</fullName>
        <actions>
            <name>ND_HCE_Request_Work_Comp_Model_UB</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>NS_NIS_Request__c.Request__c</field>
            <operation>equals</operation>
            <value>Work Comp – Model UB</value>
        </criteriaItems>
        <description>When an NS-HCE (request type: Work Comp – Model UB) request has been submitted, a notification to be sent to: Marc.Olsson@multiplan.com</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>NS-NIS Req%2E %23 Only</fullName>
        <actions>
            <name>NS_NIS_Req_Only</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>NS_NIS_Request__c.NS_NIS_Req__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>NS_NIS_Request__c.NS_NIS_Req_N_Only__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Rate Exhibit Review Assigned</fullName>
        <actions>
            <name>Rate_Exhibit_Review_Assigned</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>(not(isnew()) &amp;&amp;
not(isblank( Rate_Exhibit_Review_Approval_By__c )) &amp;&amp; 
ischanged(Rate_Exhibit_Review_Approval_By__c))

||

( isnew() &amp;&amp; 
not(isblank( Rate_Exhibit_Review_Approval_By__c )) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Rate Exhibit Review Assigned - to Requester</fullName>
        <actions>
            <name>HCE_Rate_Exhibit_Assigned_Notifiy_Requester_v2</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>(not(isnew()) &amp;&amp;
not(isblank( Rate_Exhibit_Review_Approval_By__c )) &amp;&amp;
ischanged(Rate_Exhibit_Review_Approval_By__c) &amp;&amp;
isblank(PRIORVALUE(Rate_Exhibit_Review_Approval_By__c) ) )

||

(isnew() &amp;&amp;
not(isblank( Rate_Exhibit_Review_Approval_By__c )) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Rate Exhibit Review%3A Approval Status Closed UB</fullName>
        <actions>
            <name>Rate_Exhibit_Review_Approval_Status_Closed_UB</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Rate_Exhibit_Review_Approval_Status_1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>RecordType.Name = &apos;Reimbursement and Implementation - UB&apos; &amp;&amp; text( Rate_Exhibit_Review_Approval_Status__c ) = &apos;Closed - Approved&apos; &amp;&amp;  ischanged(Rate_Exhibit_Review_Approval_Status__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Rate Exhibit Review%3A Approval Status Closed v1</fullName>
        <actions>
            <name>Rate_Exhibit_Review_Closed_v1</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Rate_Exhibit_Review_Approval_Status_1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>RecordType.Name = &apos;Reimbursement and Implementation - HCFA&apos; &amp;&amp;  text( Rate_Exhibit_Review_Approval_Status__c ) = &apos;Closed - Approved&apos; &amp;&amp;  ischanged(Rate_Exhibit_Review_Approval_Status__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Rate Exhibit Review%3A Approval Status Closed v2</fullName>
        <actions>
            <name>Rate_Exhibit_Review_Closed_v2</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Rate_Exhibit_Review_Approval_Status_1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>text( Rate_Exhibit_Review_Approval_Status__c ) = &apos;Closed - Not Approved (new analysis needed)&apos; &amp;&amp;  ischanged(Rate_Exhibit_Review_Approval_Status__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Rate Exhibit Review%3A Approval Status Closed v3</fullName>
        <actions>
            <name>Rate_Exhibit_Review_Closed_v3</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Rate_Exhibit_Review_Approval_Status_1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>text( Rate_Exhibit_Review_Approval_Status__c ) = &apos;Closed - Canceled&apos; &amp;&amp;  
ischanged(Rate_Exhibit_Review_Approval_Status__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Rate Exhibit Review%3A Approval Status Pended</fullName>
        <actions>
            <name>Rate_Exhibit_Review_Approval_Status_Pen</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>text(Rate_Exhibit_Review_Approval_Status__c) = &apos;Pended&apos; &amp;&amp;   isblank(Rate_Exhibit_Review_Status_Pended_Date__c )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Rate Exhibit Review%3A Approval Status Reopen</fullName>
        <actions>
            <name>Rate_Exhibit_Review_Approval_Status_2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>4/21/16 Rule Reversed, as it was working improperly

contains(text( Rate_Exhibit_Review_Approval_Status__c ), &apos;Closed -&apos;) &amp;&amp;  
not(contains(text(PRIORVALUE(Rate_Exhibit_Review_Approval_Status__c)), &apos;Closed -&apos;))</description>
        <formula>not(contains(text( Rate_Exhibit_Review_Approval_Status__c ), &apos;Closed -&apos;)) &amp;&amp;  contains(text(PRIORVALUE(Rate_Exhibit_Review_Approval_Status__c)), &apos;Closed -&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Request%3A Rate Exhibit Review 1</fullName>
        <actions>
            <name>Request_Rate_Exhibit_Review_1</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Request_Rate_Exhibit_Review_1_1</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Request_Rate_Exhibit_Review_dt</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>7/9/15 Per Group only update the status when = &apos;Request Rate Exhibit Review&apos;</description>
        <formula>( isnew() &amp;&amp;
text(Request_Rate_Exhibit_Review__c) = &apos;Request Rate Exhibit Review&apos; )

||

( not(isnew()) &amp;&amp;

text(Request_Rate_Exhibit_Review__c) = &apos;Request Rate Exhibit Review&apos; &amp;&amp;
text(PRIORVALUE(Request_Rate_Exhibit_Review__c)) != &apos;Request Rate Exhibit Review&apos; )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Request%3A Rate Exhibit Review 2</fullName>
        <actions>
            <name>Request_Rate_Exhibit_Review_2</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Request_Rate_Exhibit_Review_2b</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Request_Rate_Exhibit_Review_2c</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Request_Rate_Exhibit_Review_2d</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Request_Rate_Exhibit_Review_dt_null</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>not(isblank(text(PRIORVALUE(Request_Rate_Exhibit_Review__c)))) &amp;&amp;
text(PRIORVALUE(Request_Rate_Exhibit_Review__c)) = &quot;Request Rate Exhibit Review&quot; &amp;&amp;

( isblank(text(Request_Rate_Exhibit_Review__c)) ||
text(Request_Rate_Exhibit_Review__c) = &quot;Rate Exhibit Not Applicable&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Transfer to Locked Queue</fullName>
        <actions>
            <name>Assign_to_HCE_Archive_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>RecordType.Name != &quot;Legacy Request&quot; &amp;&amp;   contains(text(Request_Status__c), &apos;Closed -&apos;) &amp;&amp;  Owner:Queue.QueueName != &apos;HCE Archived Queue&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
