<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <rules>
        <fullName>MPI Remote Status Check</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Sales_Ops__c.Repricing_Type_MPI__c</field>
            <operation>includes</operation>
            <value>Remote</value>
        </criteriaItems>
        <description>When Remote is selected for MPI pricing method in Sales Ops, update checkbox on Acct page.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MPI Remote Status UnCheck</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Sales_Ops__c.Repricing_Type_MPI__c</field>
            <operation>excludes</operation>
            <value>Remote</value>
        </criteriaItems>
        <description>When Remote is deselected for MPI pricing method in Sales Ops, update checkbox on Acct page.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
