<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <rules>
        <fullName>Update contact record in HEAT</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Account.Relationship_Standing__c</field>
            <operation>equals</operation>
            <value>Account - Secure,Account - In Jeopardy,Existing Account,Account - Dormant</value>
        </criteriaItems>
        <description>Updating contact information in HEAT</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
