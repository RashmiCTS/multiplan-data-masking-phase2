/*==================================================================================
File Name:   SentDateTrigger.trg
Author:      Swarnalata Maruvad
Date:        04/14/2017
Description: This trigger populates the sent date for Signer 4. This is needed to calculate executive TAT

Modification History

Date        Editor          Description
====================================================================================*/


trigger SentDateTrigger on Apttus_DocuApi__DocuSignEnvelopeRecipientStatus__c (after insert) 
{
    list<Apttus_DocuApi__DocuSignEnvelopeRecipientStatus__c> recp = new list<Apttus_DocuApi__DocuSignEnvelopeRecipientStatus__c>();
    datetime sentDate;
    recp = [SELECT id,name,Apttus_DocuApi__DocuSignEnvelopeId__c,Apttus_DocuApi__RoleName__c,Sent_DateTime__c,
                   Apttus_DocuApi__DeliveredDateTime__c,Apttus_DocuApi__SignedDateTime__c,Apttus_DocuApi__EnvelopeParentId__c
                   FROM Apttus_DocuApi__DocuSignEnvelopeRecipientStatus__c
                   WHERE id IN :trigger.new];

    for(Apttus_DocuApi__DocuSignEnvelopeRecipientStatus__c rs : recp)
    {
        if(rs.Apttus_DocuApi__RoleName__c == 'Signer3' && rs.Apttus_DocuApi__SignedDateTime__c != null) //Contract Admin signed document
            sentDate = rs.Apttus_DocuApi__SignedDateTime__c;     
            
        if(rs.Apttus_DocuApi__RoleName__c == 'Signer4' && rs.Apttus_DocuApi__SignedDateTime__c != null) //signed document sent to executives 
        {
            rs.Sent_DateTime__c = sentDate;
            update rs;  
        }
    }
}