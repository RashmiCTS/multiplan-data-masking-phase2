/*
	Author: Apttus, 02/24/2017 - Merge Events for after insert.

	Modification log:
	Kruti Shah, APTTUS, 02/24/2017 - Added Derivation for updating Agreement Clause field.
	
*/
trigger APTS_MergeEventsTrigger on Apttus__MergeEvent__c (after insert) {

	APTS_MergeEventsTriggerHelper helper = new APTS_MergeEventsTriggerHelper();

	if(trigger.isInsert && trigger.isAfter){
		helper.updateAgreementClauseAsDeleted(Trigger.New);
	}

}