/*
	Author: Apttus, 12/27/2016 - AgreementClauseTrigger for before insert.

	Modification log:
	Kruti Shah, APTTUS, 12/27/2016 - Added Derivation for updating fields(SVP,Director,Regulatory Compliance,Legal Support) on Agreement clause based on Agreement's fields.
	Kruti Shah, APTTUS, 01/10/2017 - Added Derivation for before Update event.
*/
trigger APTS_AgreementClausetrigger on Apttus__Agreement_Clause__c (before insert,before update) {
    
    APTS_AgreementClauseTriggerHelper helper = new APTS_AgreementClauseTriggerHelper();
    
    //Before Insert
    if(Trigger.isInsert && Trigger.isBefore){
        helper.populateApproversFromAgreement(Trigger.New);        
    }   
    if(Trigger.isUpdate && Trigger.isBefore){        
        helper.updateAgreementClauseAsCancelled(Trigger.New);
    } 
}