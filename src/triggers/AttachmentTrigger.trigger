/*
File Name:   AttachmentTrigger.trigger
Author:      Michael Olson  
Date:        3/24/2016
Description: Responsible for assigning the owner of the settlement by looking up the TIN assignment object.

Modification History
3/24/2016       Michael Olson       Created email notifiation for HCE Requests in Final Rates Status where new documents have been uploaded.
4/13/2016       Michael Olson       Added NSL to Attachment Email
*/

trigger AttachmentTrigger on Attachment (after insert) {

    //-----------------------------------------------------------------------
    //Global Variables
    //-----------------------------------------------------------------------       
    Id rtRIUB = Schema.SObjectType.NS_NIS_Request__c.getRecordTypeInfosByName().get('Reimbursement and Implementation - UB').getRecordTypeId();
    string hceReqPrefix = Schema.SObjectType.NS_NIS_Request__c.getKeyPrefix();

    //-----------------------------------------------------------------------
    //Insert Triggers
    //-----------------------------------------------------------------------               
    if(trigger.isInsert) {
        
        //-------------------------------------------
        //General Query get all inserted attachments
        //-------------------------------------------
        list<Attachment> allAttchments = new list<Attachment>(); 
            
            allAttchments = [select id, name, parentid, body
                                from Attachment 
                                where id IN : trigger.new];                     
        
        //----------------------------------------------------------------------------
        // Send Email for Attchments added to UB HCE Requests in Final Rates Status
        //----------------------------------------------------------------------------
        
        //setup lists and sets
        map<id, attachment> attachParentIdMap = new map<id, attachment>();
        list<NS_NIS_Request__c> hceReqsWithAttach = new list<NS_NIS_Request__c>();
            
            //filter out none HCE request uploads for effiency
            for(Attachment A : allAttchments) {
                string s = A.parentid;
                if(s.startsWith(hceReqPrefix)) {
                    attachParentIdMap.put(A.parentid, A);
                }
            }
        
        
        //Begin process of identifying if email notificatoin is needed
        if(attachParentIdMap.size() > 0) {
            
            hceReqsWithAttach = [select id, name,Rate_Negotiation_Type__c, HCE_Analyst__c, HCE_Analyst__r.email, HCE_Analyst__r.name, 
                                        NS_NIS_Req__c, Rate_Exhibit_Review_Approval_By__c, Rate_Exhibit_Review_Approval_By__r.email
                                    from NS_NIS_Request__c 
                                    where id IN : attachParentIdMap.keyset()
                                    AND Rate_Negotiation_Type__c = 'Final Rates'
                                    AND RecordTypeID =: rtRIUB];
                        
            if(hceReqsWithAttach.size() > 0) {
                
                list<Messaging.SingleEmailMessage> mails = new list<Messaging.SingleEmailMessage>();
                
                for(NS_NIS_Request__c hce : hceReqsWithAttach) {
                    
                    if(hce.HCE_Analyst__c != null || hce.Rate_Exhibit_Review_Approval_By__c != null) {
                            
                            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                            
                            //setup email variables
                            String sfdcBaseURL = URL.getSalesforceBaseUrl().toExternalForm();
                            list<string> toAddList = new list<string>();
                                if(hce.HCE_Analyst__c != null){
                                    toAddList.add(hce.HCE_Analyst__r.email);
                                }   
                                if(hce.Rate_Exhibit_Review_Approval_By__c != null){
                                    toAddList.add(hce.Rate_Exhibit_Review_Approval_By__r.email);
                                }                                                   
                            //String[] toAddresses = new String[] {hce.HCE_Analyst__r.email}; 
                            //String[] ccAddresses = backupEmail.get(x.HCE_Analyst__c) != null ? new String[] {backupEmail.get(x.HCE_Analyst__c)} : null;
                            string Subject = 'An \'Attachment\' has been uploaded to \'HCE Request\' =\''+ hce.name +'\' where the \'Rate Negotiation Type\' equals \'Final Rates\'. NIS# = \'' +hce.NS_NIS_Req__c +'\' (source: AttachmentTrigger)';
                            string Body = 'Please login to Salesforce to view full details. <br><br>' +
                                            'Link to HCE Request Record: ' + sfdcBaseURL + '/' + hce.id + '<br/><br>' +
                                            'File Name: ' + attachParentIdMap.get(hce.id).name + '<br/>' +
                                            //'Request Description:' + x.Request_Description__c + '<br/><br>' +
                                            'Link to File: ' + sfdcBaseURL + '/' + attachParentIdMap.get(hce.id).id;
                                                        
                            //set email values
                            //mail.setToAddresses(toAddresses);
                            mail.setToAddresses(toAddList);
                            //if(ccAddresses != null) { 
                                //mail.setCcAddresses(ccAddresses);
                            //}
                            mail.setSubject( Subject );
                            mail.setHtmlBody(Body);
                            
                            
                            //Set email file attachments
                            List<Messaging.Emailfileattachment> fileAttachments = new List<Messaging.Emailfileattachment>();
                
                            // Add to attachment file list
                            Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
                                efa.setFileName(attachParentIdMap.get(hce.id).name);
                                efa.setBody(attachParentIdMap.get(hce.id).Body);
                            fileAttachments.add(efa);
                            
                            mail.setFileAttachments(fileAttachments);                       
                            
                            mails.add(mail);
                        }
                
                }
                
                //Send email
                if(mails.size() > 0) {
                    try{                
                        Messaging.sendEmail ( mails );                      
                    }catch(exception e){ throw e; }
                }
            }           
        }                   
    }

    
}