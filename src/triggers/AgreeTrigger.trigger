/*
File Name:   AgreementTrigger.trigger
Author:      Michael Olson  
Date:        4/11/2016
Description: Agreement

Modification History
4/11/2016       Michael Olson       Updates Contact to Recipient 
5/26/2016       Michael Olson       Added logic for Tricare
6/6/2016        Michale Olson       Add V5 Group Logic
07/20/2016      Harish Emmadi       Added doCheck for approvals whenever a new agreement is created/updated.
03/23/2017      Kruti Shah          Added condition that if Agreement status is Activated then no approval check is required (it was causing error for agreement lock functionality).
*/

trigger AgreeTrigger on Apttus__APTS_Agreement__c (after insert, after update) {

    if(Trigger.isAfter)
    {
       if((Trigger.isInsert 
            || Trigger.isUpdate)
            && Trigger.new.size() == 1
            && APTS_Agreement_Helper.approvalCheckisInvoked == false){
            // Approval check is applicable only if single agreemet is created/updated at a time.
            // dont invoke the API if approval status, status or status category is updated.
            Apttus__APTS_Agreement__c agr = Trigger.new.get(0);
            if(Trigger.isUpdate
                && (agr.Apttus_Approval__Approval_Status__c != Trigger.OldMap.get(agr.Id).Apttus_Approval__Approval_Status__c
                    || agr.Apttus__Workflow_Trigger_Viewed_Final__c != Trigger.OldMap.get(agr.Id).Apttus__Workflow_Trigger_Viewed_Final__c
                    || agr.Apttus__Status_Category__c != Trigger.OldMap.get(agr.Id).Apttus__Status_Category__c
                    || agr.Apttus__Status__c != Trigger.OldMap.get(agr.Id).Apttus__Status__c)
                    || Test.isRunningTest()
                    || agr.Apttus__Status__c  == CONSTANTS.STATUS_ACTIVATED)
                    //|| agr.Apttus__Status__c  == 'Being Amended')
                   
                return;
            /* This API takes up the IDs of all the records of your context object
                and checks if these records satisfy the Entry Criteria defined at the Search Filter
                (Approvals). If the Entry Criteria is satisfied, this API returns true, otherwise false.
                For example, if you have approval processes defined for your Agreement header fields
                and Agreement Line Items, this API checks if your Agreement record and Agreement
                Line Items satisfy the Entry Criteria (RecordType == NDA) returns True or False, and
                sets the Approval status of filtered records to Approval Required. Also, if there is a
                change in the value of your Agreement header or Agreement Line Item, and the entire
                approval request is re-submitted, this API considers all record IDs for Approval
                Required Check and re-evaluates them.
            */
            try{
                APTS_Agreement_Helper.approvalCheckisInvoked = true;
                Apttus_Approval.ApprovalRequiredCheck checker = new Apttus_Approval.ApprovalRequiredCheck();
                Boolean result = checker.doCheck(agr.Id);    
            }catch(Exception ex){
                System.debug('Error Message : '+ ex.getMessage());
                agr.addError(' Approval process currently in progress. No changes allowed at this time.');
                return;
            }
            
        }
         
    }
    
    
}