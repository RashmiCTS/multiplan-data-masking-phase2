/* APTS_RelatedAgreementTrigger. 
 * Trigger for APTS_RelatedAgreementTrigger
 *
 * Developer: Kruti Shah, APTTUS
 * Business Owner: 
 *
 * Scenario:
 * 
 * History: 
 * 03/24/2017, Kruti Shah, APTTUS - Created APTS_RelatedAgreementTrigger To populate field APTS_Original_Agreement__c on Amended Agreement record.
 */
trigger APTS_RelatedAgreementTrigger on Apttus__APTS_Related_Agreement__c (after insert) {
    
    APTS_RelatedAgreementHandler handler = new APTS_RelatedAgreementHandler();
    
    if(Trigger.isInsert && Trigger.isAfter)
    {
        handler.populateOriginalAgreementAfterAmend(Trigger.New);
    }

}