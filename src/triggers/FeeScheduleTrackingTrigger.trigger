/*==================================================================================
File Name:   FeeScheduleTrackingTrigger.trg
Author:      Michael Olson
Date:        9/28/2015
Description: 

Modification History

Date				Editor					Description
2/16/2016			Michael Olson			Changed to send mass emails. 
====================================================================================*/

trigger FeeScheduleTrackingTrigger on Fee_Schedule_Tracking__c (after insert) {

	//-----------------------------------------------------------------------
	//Global Variables
	//-----------------------------------------------------------------------		
		String assignableRateOpsUsers = Label.NetDevHCERequest; 	
	
	//----------------------------------------------------------------------------------------------------------------------------------
	//All Insert Triggers
	//----------------------------------------------------------------------------------------------------------------------------------
	
	if(trigger.isInsert) { 
		
		//----------------------------------------------------------------------------
		// One Query for all NS_NIS_Requests in Insert - can be refernced below
		//----------------------------------------------------------------------------	
			list<Fee_Schedule_Tracking__c> allFSTs = new list<Fee_Schedule_Tracking__c>(); 
			
				allFSTs = [select id, name, ownerID, Provider_Name__c
									from Fee_Schedule_Tracking__c 
									where id IN : trigger.new];					
									
									
			//----------------------------------------------------------------------------
			// Assign New ND - HCE Requests for Rate Ops
			//----------------------------------------------------------------------------	
				
				//Variables;Maps;Sets;Lists	
				id fstUnQ;
				list<Group> gList = [select id, name from Group where name = 'FST - Unassigned Q' limit 1];
					for(Group g : gList) {
						fstUnQ = g.id;
					}
				
				
				list<Fee_Schedule_Tracking__c> assignableFST = new list<Fee_Schedule_Tracking__c>();
				list<Fee_Schedule_Tracking__c> nonAssignableFST = new list<Fee_Schedule_Tracking__c>();
				list<string> userAssignL = new list<string>();
				set<string> userAssignSet = new set<string>();
	            	userAssignL = assignableRateOpsUsers.split(';');
	           		for(string s : userAssignL) {
	           			userAssignSet.add(s);
	           		}
	           	
	           	map<id, User> fstScheduleIdToUserMap = new map<id,User>();
	           	map<User, datetime> fstScheduleMaxDateAssignMap = new map<User, datetime>(); 		

	           	map<id,string> assigneeEmail = new map<id, string>();
	           	map<id,string> backupEmail = new map<id, string>();
	            
	            list<User> allUsers = new list<User>([select id,alias, Auto_Assign_Rate_Operation_Request_Type__c, isactive,
	            												No_Auto_Assign_HCE_Request_Start_Date__c, No_Auto_Assign_HCE_Request_End_Date__c,
	            												Do_not_Auto_Assign_Rate_Operations__c, email, Rate_Operations_Backup_Email__c, Auto_Assign_FST__c
	            											from User where isactive = true]);
	            											
	            list<User> userAssignList = new list<User>();
	            
		            for(User U : allUsers) {
		             	assigneeEmail.put(U.id, u.email);
		             	backupEmail.put(U.id, u.Rate_Operations_Backup_Email__c);
		             	
		             	if(userAssignSet.contains(U.alias) && U.Do_not_Auto_Assign_Rate_Operations__c == false && U.Auto_Assign_FST__c == true) {
		             		userAssignList.add(U);
		             	}	            	
		            }
	            
	                      
	             for(User U : userAssignList){
	             	fstScheduleMaxDateAssignMap.put(U,null);
	             	fstScheduleIdToUserMap.put(U.id, U);          
	             }											
	
					
				//Get a list of applicable Requests based on Recordtype
				for(Fee_Schedule_Tracking__c fstReq : allFSTs) {
					if(!string.valueOF(fstReq.ownerid).startsWith('005') ) { //005 is a user
						assignableFST.add(fstReq);
					}else{
						nonAssignableFST.add(fstReq);
					}
				}
								
				//Assign Rate Operations Users
				if(assignableFST.size() > 0) {
					
					//Get list of Recent HCE Requests based on appliable Assignees.
					list<Fee_Schedule_Tracking__c> allHistoricalFST = new list<Fee_Schedule_Tracking__c>();
						allHistoricalFST = [select id, ownerId, createddate
												from Fee_Schedule_Tracking__c
												where ownerid !=: fstUnQ
												order by OwnerId, createddate asc];
						
						for(Fee_Schedule_Tracking__c r : allHistoricalFST) {					
							User U;
							
							if(string.valueOf(r.ownerid).startswith('005') ) {
								U = fstScheduleIdToUserMap.get(r.ownerid);
								if(U != null && fstScheduleIdToUserMap.ContainsKey(U.id)) {
									fstScheduleMaxDateAssignMap.put(U, r.createddate);	
								}
							}
						}								


					list<User> fsUsers = new list<User>();
						//not populated date
						for(User np: fstScheduleMaxDateAssignMap.keyset()) {
							if(fstScheduleMaxDateAssignMap.get(np) == null) {
								fsUsers.add(np);
							}
						}
												
						//populated date
						for(User p: fstScheduleMaxDateAssignMap.keyset()) {
							if(fstScheduleMaxDateAssignMap.get(p) != null) {
								fsUsers.add(p);
							}
						}

					
					//Setup ordered List of Assignees for Fee Schedule
					integer n = fsUsers.Size();
					
					for(integer k = 0; k < n - 1; k++) {
					    for(integer i2 = 0; i2 < n - k - 1; i2++) {					    
					        if(fstScheduleMaxDateAssignMap.get(fsUsers[i2]) > fstScheduleMaxDateAssignMap.get(fsUsers[i2 + 1]) ) {
					            integer temp = i2;
					            User uTemp = fsUsers[i2];
					            fsUsers[i2] = fsUsers[i2 + 1];
					            fsUsers[i2 + 1] = uTemp;
					        }
					    }
					}
					
					
					//Assign records		
					for(Fee_Schedule_Tracking__c hceReq2 : assignableFST) {
						
						User moveToEnd;
						
							hceReq2.ownerId = fsUsers[0].id;

							//reset order
							moveToEnd = fsUsers[0];
							fsUsers.remove(0);
							fsUsers.add(moveToEnd);										

					}

				}


				//setup emails to send in Mass Send
				map<id, list<Fee_Schedule_Tracking__c>> uidToFSTList = new map<id, list<Fee_Schedule_Tracking__c>>();
				
					if(assignableFST.size() > 0) {
						for(Fee_Schedule_Tracking__c fstUp : assignableFST) {        					
							
							//prepare mass email list
							if(uidToFSTList.keyset().contains(fstUp.ownerid)) {
								uidToFSTList.get(fstUp.ownerid).add(fstUP);
							}else {
								list<Fee_Schedule_Tracking__c> tempFstInsList = new list<Fee_Schedule_Tracking__c>();
									tempFstInsList.add(fstup);
									
								uidToFSTList.put(fstUp.ownerid, tempFstInsList);
							}	        					        						        					       				
						}
					}
					if(nonAssignableFST.size() > 0) {
						for(Fee_Schedule_Tracking__c fstUp : nonAssignableFST) {        					
							
							//prepare mass email list
							if(uidToFSTList.keyset().contains(fstUp.ownerid)) {
								uidToFSTList.get(fstUp.ownerid).add(fstUP);
							}else {
								list<Fee_Schedule_Tracking__c> tempFstInsList = new list<Fee_Schedule_Tracking__c>();
									tempFstInsList.add(fstup);
									
								uidToFSTList.put(fstUp.ownerid, tempFstInsList);
							}	        					        						        					        				
						}
					}
			        
			    
			    //Send email Notifiaction to Assignees			        

		        list<Messaging.SingleEmailMessage> mailsToMassSend = new list<Messaging.SingleEmailMessage>(); 
				
				//Create and Send emails
		        if(uidToFSTList.size() > 0) {
		        				
		        	map<id, string> uidToEmail = new map<id, string>();
		        	list<User> U = new list<User>();
		        	string cUserEmail;				        					
		        		
		        		//get users
		        		U = [select id, email, name from User where id IN : uidToFSTList.keySet() OR id =: UserInfo.getUserId() ];
		        		
		        		//setup email addresses for use later		
		        		for(User Usr : U) {
		        			uidToEmail.put(Usr.id, Usr.email);
		        			
		        			//setup current user as the email's CC
		        			if(Usr.id == UserInfo.getUserId()) {
		        				cUserEmail = Usr.email;
		        			}
		        		}	
		        		
		        		//Loop though map to generate the individual emails and set variables				
		        		for(id i : uidToFSTList.keySet()) {

		        			Messaging.SingleEmailMessage mail1 = new Messaging.SingleEmailMessage();
		        					
		        			string Subject = '';			        							        			
		        			string tempBody = '';
		        			
		        			for(Fee_Schedule_tracking__c fst : uidToFSTList.get(i)) {
		        				
		        				Subject = 'You were Assigned \'Fee Schedule Tracking\' records ('+ uidToFSTList.get(i).size() +').  Please see below for details. (source: FeeScheduleTrackingTrigger)';	
		        				
		        				tempBody = tempBody + 'FST #: ' + fst.name + ' for ' + fst.Provider_Name__c + '<br/>' +
		        										'Link to Record: ' + System.URL.getSalesforceBaseUrl().toExternalForm() + '/' + fst.id + '<br/><br>';
		        			}		        																
				 						
				 			list<string> toAddress = new list<string>();
				 				toAddress.add(uidToEmail.get(i));
				 			list<string> ccAddress = new list<string>();
				 				ccAddress.add(cUserEmail);
				 				if(backupEmail.get(i) != null) {
				 					ccAddress.add(backupEmail.get(i));
				 				}	
							                
							mail1.setToAddresses(toAddress);
							mail1.setccAddresses(ccAddress);
							mail1.setSubject(Subject);
							mail1.setHtmlBody(tempBody); 
							mail1.saveAsActivity = false;
											
							mailsToMassSend.add(mail1);										        									        				
		        		}
		        }	


			//Final Update for Assignements
				try{
					if(assignableFST.size() > 0) { 
						update assignableFST;
					}
					if(mailsToMassSend.size() > 0) {	 
						Messaging.sendEmail( mailsToMassSend);     		
					}	
				}catch(exception e){ throw e; }
																									
	}
}