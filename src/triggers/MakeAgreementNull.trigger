trigger MakeAgreementNull on Client_Exclusion_Request__c (before insert, before update)
{
    for(Client_Exclusion_Request__c CER:trigger.new)
    {
        if(CER.Status__c == 'Cancelled' && CER.Agreement__c!=null)
            CER.Agreement__c =null;
    }
}