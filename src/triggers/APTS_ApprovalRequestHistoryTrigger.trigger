/*
Author: Apttus, 01/27/2017 - Approval Request History Trigger for before Insert.
Modification log:
Kruti Shah, APTTUS, 01/27/2017 - populate field Agreement Clause Name when Child Object is Agreement Clause on before Insert.
*/
trigger APTS_ApprovalRequestHistoryTrigger on Apttus_Approval__Approval_Request_History__c (before Insert) {
    APTS_ApprovalRequestHistoryHelper helper = new APTS_ApprovalRequestHistoryHelper();
    
     //Before Insert
    if (Trigger.isInsert && Trigger.isBefore) {
        helper.populateAgreementClauseName(Trigger.new, new Map<Id, Apttus_Approval__Approval_Request_History__c>());
    }

}