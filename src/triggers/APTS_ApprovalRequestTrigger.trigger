/*
Author: Apttus, 01/25/2017 - Approval Request Trigger for before update.
Modification log:
Kruti Shah, APTTUS, 01/25/2017 - populate field Agreement Clause Name when Child Object is Agreement Clause on before update.
*/
trigger APTS_ApprovalRequestTrigger on Apttus_Approval__Approval_Request__c (before Update) {
	APTS_ApprovalRequestHelper helper = new APTS_ApprovalRequestHelper();
    
     //Before Update
    if (Trigger.isUpdate && Trigger.isBefore) {
        helper.populateAgreementClauseName(Trigger.new, Trigger.oldMap);
    }
    
    /*
    if (Trigger.isUpdate && Trigger.isBefore) {
        helper.populateSubmissionComments(Trigger.new);
    }
    */
}