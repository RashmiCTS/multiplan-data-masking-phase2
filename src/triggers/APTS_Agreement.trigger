/*
    Author: Apttus, 06/16/2016 - AgreementTrigger for before insert and before update.
    Modification log:
    Harish Emmadi, APTTUS, 08/01/2016 - extended updateAgreementFields for before insert.
    Kruti Shah, APTTUS, 03/31/2017 - added method lockFieldsOnActivateTerminateCancel for before update.
 */
trigger APTS_Agreement on Apttus__APTS_Agreement__c (before update, before insert, after update) {
    APTS_Agreement_Helper helper = new APTS_Agreement_Helper();
    if(Trigger.isBefore){
        if(Trigger.isUpdate){            
            helper.updateAgreementFields(Trigger.new, Trigger.oldMap);            
            helper.lockFieldsOnActivateTerminateCancel(Trigger.new, Trigger.oldMap);
            //helper.setApprovalFields(Trigger.new, Trigger.oldMap);
            //helper.createAgreementClauses(Trigger.newMap, Trigger.oldMap);            
        }else if(Trigger.isInsert){
            helper.updateAgreementFields(Trigger.new, new Map<Id, Apttus__APTS_Agreement__c>());
            helper.updateRecordType(Trigger.new);
        }
    }
    
    //After Update
    if (Trigger.isUpdate && Trigger.isAfter) {
        helper.updateApproversOnClauses(Trigger.oldMap, Trigger.newMap); 
        //helper.printOldNewAgreementValues(Trigger.oldMap, Trigger.newMap);
    }
}