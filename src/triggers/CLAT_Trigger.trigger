/*
File Name:   CLAT_Trigger.trigger
Author:      Michael Olson  
Date:        4/11/2016
Description: All CLAT Triggers

Modification History
4/11/2016		Michael Olson		Added trigger to create Agreement after CLAT inserted
5/26/2016		Michael Olson		Udated trigger to include Tricare 
6/6/2016		Michale Olson		Add V5 Group Logic
7/5/2016        Michael Olson       Added Tin Mapping
7/6/2016		Michael Olson		For V5 Group; created multiple DocuSign Recipients; Auto Generate the Exhibits B,D, and SLCP - stateId
8/17/2016 		Jason Hartfield		Removed logic from trigger and moved to ClatTriggerUtils class
*/

trigger CLAT_Trigger on Contract_Language_Amendment_Tracking__c (
	before insert, 
	before update, 
	before delete, 
	after insert, 
	after update, 
	after delete, 
	after undelete) {
	
	if(TriggerCommonUtils.BypassAllTriggers) return;

	if (Trigger.isAfter && Trigger.isInsert) {
	   
	    ClatTriggerUtils.AutoConstructAgreements(trigger.New,trigger.newMap);
	}
}