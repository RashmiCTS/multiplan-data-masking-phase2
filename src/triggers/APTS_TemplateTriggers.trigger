/*
File Name:   APTS_TemplateTriggers.cls
Author:      Jason Hartfield
Date:        8/9/2016
Description: Template triggers

Modification History
Modified By			Date			Description

*/

trigger APTS_TemplateTriggers on Apttus__APTS_Template__c (
	before insert, 
	before update, 
	before delete, 
	after insert, 
	after update, 
	after delete, 
	after undelete) {
		if(TriggerCommonUtils.BypassAllTriggers) return;

		if (Trigger.isBefore && (Trigger.isInsert || trigger.isUpdate)) {
	    	// Prevent templates with duplicate names.
	    	TriggerCommonUtils.PreventDuplicateNames(trigger.new,trigger.oldMap);	    
		} 
}