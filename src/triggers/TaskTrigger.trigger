/*
File Name:   TaskTrigger
Author:      Michael Olson  
Date:        4/11/2016
Description: task Trigger

Modification History
4/11/2016       Michael Olson       created
6/11/2016       Michael Olson       Updated new record types for Tricare and V5
6/6/2016        Michale Olson       Add V5 Group Logic
*/

trigger TaskTrigger on Task (after insert) {
 
     //-----------------------------------------------------------------------
    //Global Variables
    //-----------------------------------------------------------------------       
    Id rtAgreeVAAG = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('VA Amendment - Group').getRecordTypeId();
    Id rtAgreeTC = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('Tricare Amendment - Group').getRecordTypeId();
    Id rtAgreeV5G = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('Group V5').getRecordTypeId();
    
    //create a set of Rt ids to use later
    //When new RTs should be added to this list to turn off
    set<id> controlsRtTaskClosure = new set<id>();
        controlsRtTaskClosure.add(rtAgreeVAAG);
        controlsRtTaskClosure.add(rtAgreeTC);
        controlsRtTaskClosure.add(rtAgreeV5G);    
    
    //-----------------------------------------------------------------------
    //Insert Triggers
    //-----------------------------------------------------------------------               
    if(trigger.isInsert) {    

        //-------------------------------------------
        //General Query get all inserted attachments
        //-------------------------------------------
        list<Task> allTasks = new list<Task>([select id, Account.Name, AccountId, What.Type, what.id, subject, status, type, 
                                                    Description, ownerid 
                                                from Task
                                                where id IN : trigger.new]); 

        //-------------------------------------------
        //Close out unneeded Apttus Task
        //-------------------------------------------
        string sObj = 'Apttus__APTS_Agreement__c';
        string sObjectPrefix;
        map<id, id> taskIdToAgreeID = new map<id, id>();
        list<Task> tasksToClose = new list<Task>();
        set<id> agreesId = new set<id>();
        list<Apttus__APTS_Agreement__c> agrees = new list<Apttus__APTS_Agreement__c>();
        set<id> agreeVART = new set<id>();


        //setup prefix name
        map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
        for( Schema.SObjectType objectInstance : gd.values() ) {
            if(objectInstance.getDescribe().getName() == sObj) {
                sObjectPrefix = objectInstance.getDescribe().getKeyPrefix();
            }
        }
        
        //Get set of possible tasks ids to close
        for(Task t : allTasks) {
            if(t.what.id != null) {
                if( string.valueOf(t.what.id).substring(0,3) == sObjectPrefix) {
                    agreesId.add(t.what.id);
                }
            }
        }
        
        //Query possible applicable Agreements 
        if(agreesId.size() > 0 ) {
            
            agrees = [select id, recordtypeid 
                        from Apttus__APTS_Agreement__c 
                        where id IN: agreesId
                        AND recordtypeid IN: controlsRtTaskClosure];
           
            //Process Applicable Agreements to Close Tasks
            if(agrees.size() > 0) {

                for(Apttus__APTS_Agreement__c a : agrees) {
                    agreeVART.add(a.id);
                }               
                
                for(Task t : allTasks) {
                    if( agreeVART.contains(t.what.id) &&
                        (t.subject == 'Get Internal Signatures' || 
                        t.subject == 'Get External Signatures' || 
                        t.subject == 'Scan & Attach Agreement') && 
                        t.status == 'Not Started') {
                                                                                            
                        tasksToClose.add(t);
                    } 
                }
                
                if(tasksToClose.size() > 0) {
                    
                    for(Task t : tasksToClose) {
                        t.type = 'Other Task';
                        t.status = 'Closed';
                        string descip = t.Description;
                        t.Description = 'Auto Closed Task - ' + t.Description;
                    }
                    
                    try{
                        update tasksToClose;
                    }catch(exception e){throw e;}
                    
                }
            }
        } 
                        
    }
    
}