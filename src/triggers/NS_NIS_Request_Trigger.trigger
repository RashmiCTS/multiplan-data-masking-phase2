/*==================================================================================
File Name:   NS_NIS_Request_Trigger.trg
Author:      Michael Olson
Date:        5/19/2015
Description: 

Modification History

Date            Editor              Description
5/19/2015       Michael Olson       Created
7/7/2015        Michael Olson       Added Rate Exhibit Request assignement at insert
8/18/2015       Michael Olson       Added Logic to assign Rate Approval By based on Assinged date vs Created date.
3/24/2016       Michael Olson       Added Logic for Fee Schedule Approval Processs - Apttus Project Phase 0
03/13/2017		Rashmi T			Adding logic to update Zip Codes based on the Rate Year 
====================================================================================*/

trigger NS_NIS_Request_Trigger on NS_NIS_Request__c (before insert, before update ,after insert, after update) {
	
    /*******************Start Functionality to update Locality_Name_ZipCode__c based on the Rate_Data_Source_Year__c *********/
    if(Trigger.isBefore && Trigger.isInsert){
        NS_NIS_Request_TriggerHandler.updateZipCodeBasedOnRateDataSourceYear(Trigger.new);
    }
    else if(Trigger.isBefore && Trigger.isUpdate){
        List<NS_NIS_Request__c> lstTriggerNew = new List<NS_NIS_Request__c>();
        for(Id nid : Trigger.newMap.keySet()){
            if(Trigger.oldMap.get(nid).Rate_Data_Source_Year__c <> Trigger.newMap.get(nid).Rate_Data_Source_Year__c
               || Trigger.oldMap.get(nid).Zip_Code__c <> Trigger.newMap.get(nid).Zip_Code__c){
            	lstTriggerNew.add(Trigger.newMap.get(nid));       
            }
        }
        NS_NIS_Request_TriggerHandler.updateZipCodeBasedOnRateDataSourceYear(Trigger.new);
    }
    
    /*******************End Functionality to update Locality_Name_ZipCode__c based on the Rate_Data_Source_Year__c *********/
    //-----------------------------------------------------------------------
    //Global Variables
    //-----------------------------------------------------------------------       
        Id rtRateOps = Schema.SObjectType.NS_NIS_Request__c.getRecordTypeInfosByName().get('Rate Operations').getRecordTypeId();
        Id rtRIHCFA = Schema.SObjectType.NS_NIS_Request__c.getRecordTypeInfosByName().get('Reimbursement and Implementation - HCFA').getRecordTypeId();
        Id rtRIUB = Schema.SObjectType.NS_NIS_Request__c.getRecordTypeInfosByName().get('Reimbursement and Implementation - UB').getRecordTypeId();
        Id rtV = Schema.SObjectType.NS_NIS_Request__c.getRecordTypeInfosByName().get('Valuation').getRecordTypeId();
        Id rtL = Schema.SObjectType.NS_NIS_Request__c.getRecordTypeInfosByName().get('Legacy Request').getRecordTypeId();
        String assignableRateOpsUsers = Label.NetDevHCERequest;     
    
    //----------------------------------------------------------------------------------------------------------------------------------
    //All Insert Triggers
    //----------------------------------------------------------------------------------------------------------------------------------
    
    if(trigger.isInsert) { 
        
        //----------------------------------------------------------------------------
        // One Query for all NS_NIS_Requests in Insert - can be refernced below
        //----------------------------------------------------------------------------  
            List<NS_NIS_Request__c> allNdRequests = new List<NS_NIS_Request__c>(); 
            
                allNdRequests = [select id, name, ownerID, recordtypeID, Rate_Operations_Request_Type__c, HCE_Analyst__c, Product_Selection__c, 
                                        HCE_Analyst__r.email, HCE_Analyst__r.Rate_Operations_Backup_Email__c, Request_Description__c,
                                        Request_Rate_Exhibit_Review__c
                                    from NS_NIS_Request__c 
                                    where id IN : trigger.new];                 
                                    
                                    
            //----------------------------------------------------------------------------
            // Assign New ND - HCE Requests for Rate Ops
            //----------------------------------------------------------------------------  
                
                //Variables;Maps;Sets;Lists 
                list<NS_NIS_Request__c> assignableHCEReqRateOps = new list<NS_NIS_Request__c>();
                list<NS_NIS_Request__c> assignRateExApper = new list<NS_NIS_Request__c>();
                list<string> userAssignL = new list<string>();
                    userAssignL = assignableRateOpsUsers.split(';');
                
                map<id, User> feeScheduleIdToUserMap = new map<id,User>();
                map<User, datetime> feeScheduleMaxDateAssignMap = new map<User, datetime>();        
                map<id, User> rateOpsRateExhibitIdToUserMap = new map<id,User>();
                map<User, datetime> rateOpsRateExhibitMaxDateAssignMap = new map<User, datetime>(); 
                map<id,string> assigneeEmail = new map<id, string>();
                map<id,string> backupEmail = new map<id, string>();
                
                list<User> userAssignList = new list<User>([select id,alias, Auto_Assign_Rate_Operation_Request_Type__c, isactive,
                                                                No_Auto_Assign_HCE_Request_Start_Date__c, No_Auto_Assign_HCE_Request_End_Date__c,
                                                                Do_not_Auto_Assign_Rate_Operations__c, email, Rate_Operations_Backup_Email__c
                                                            from User 
                                                            where alias IN: userAssignL // use user field instead
                                                            AND Do_not_Auto_Assign_Rate_Operations__c = false
                                                            AND Auto_Assign_Rate_Operation_Request_Type__c != ''
                                                            AND isactive = true]); 
                 
                 
                 for(User U : userAssignList){
                    if(U.Auto_Assign_Rate_Operation_Request_Type__c.contains('Fee Schedule Request') || U.Auto_Assign_Rate_Operation_Request_Type__c.contains('Fee Schedule Question')) {
                        feeScheduleMaxDateAssignMap.put(U,null);
                        feeScheduleIdToUserMap.put(U.id, U);
                    }
                    if(U.Auto_Assign_Rate_Operation_Request_Type__c.contains('Non-Standard Rate Exhibit Lang Rev HCFA') ) {
                        rateOpsRateExhibitMaxDateAssignMap.put(U,null);
                        rateOpsRateExhibitIdToUserMap.put(U.id, U);                                             
                    }
                    assigneeEmail.put(U.id, u.email);
                    backupEmail.put(U.id, u.Rate_Operations_Backup_Email__c);
                 
                 }                                          
    
                    
                //Get a list of applicable Requests based on Recordtype
                for(NS_NIS_Request__c hceReq : allNdRequests) {
                    if(hceReq.recordtypeid == rtRateOps ) {
                        assignableHCEReqRateOps.add(hceReq);
                    }
                    if(hceReq.Request_Rate_Exhibit_Review__c == 'Request Rate Exhibit Review' && hceReq.recordtypeId == rtRIHCFA) {
                        assignRateExApper.add(hcereq);                  
                    }
                }
                
                
                //Assign Rate Operations Users
                if(assignableHCEReqRateOps.size() > 0) {
                    
                    //Get list of Recent HCE Requests based on appliable Assignees.
                    list<NS_NIS_Request__c> allHistoricalRequests = new list<NS_NIS_Request__c>();
                        allHistoricalRequests = [select id, HCE_Analyst__c, createddate, recordtypeid, Rate_Operations_Request_Type__c
                                                from NS_NIS_Request__c
                                                where HCE_Analyst__c != null
                                                and  recordtypeid =: rtRateOps
                                                and Rate_Operations_Request_Type__c != ''
                                                order by HCE_Analyst__c, createddate asc];
                        
                        for(NS_NIS_Request__c r : allHistoricalRequests) {                  
                            User U;
                            
                            if(r.rate_Operations_request_Type__c == 'Fee Schedule Request' || r.rate_Operations_request_Type__c == 'Fee Schedule Question' ) {
                                U = feeScheduleIdToUserMap.get(r.HCE_Analyst__c);
                                if(U != null && feeScheduleIdToUserMap.ContainsKey(U.id)) {
                                    feeScheduleMaxDateAssignMap.put(U, r.createddate);  
                                }
                            }else if(r.rate_Operations_request_Type__c == 'Non-Standard Rate Exhibit Lang Rev HCFA') {
                                U = rateOpsRateExhibitIdToUserMap.get(r.HCE_Analyst__c);
                                if(U != null && rateOpsRateExhibitIdToUserMap.ContainsKey(U.id)) {
                                    rateOpsRateExhibitMaxDateAssignMap.put(U, r.createddate);   
                                }                           
                            }       
                        }                               


                    list<User> fsUsers = new list<User>();
                        //not populated date
                        for(User np: feeScheduleMaxDateAssignMap.keyset()) {
                            if(feeScheduleMaxDateAssignMap.get(np) == null) {
                                fsUsers.add(np);
                            }
                        }
                                                
                        //populated date
                        for(User p: feeScheduleMaxDateAssignMap.keyset()) {
                            if(feeScheduleMaxDateAssignMap.get(p) != null) {
                                fsUsers.add(p);
                            }
                        }
                    
                    list<User> rateExhibitUsers = new list<User>();
                        //not populated date
                        for(User np: rateOpsRateExhibitMaxDateAssignMap.keyset()) {
                            if(rateOpsRateExhibitMaxDateAssignMap.get(np) == null) {
                                rateExhibitUsers.add(np);
                            }
                        }
                                            
                        //populated date
                        for(User p: rateOpsRateExhibitMaxDateAssignMap.keyset()) {
                            if(rateOpsRateExhibitMaxDateAssignMap.get(p) != null) {
                                rateExhibitUsers.add(p);
                            }
                        }

                    
                    //Setup ordered List of Assignees for Fee Schedule
                    integer n = fsUsers.Size();
                    
                    for(integer k = 0; k < n - 1; k++) {
                        for(integer i2 = 0; i2 < n - k - 1; i2++) {                     
                            if(feeScheduleMaxDateAssignMap.get(fsUsers[i2]) > feeScheduleMaxDateAssignMap.get(fsUsers[i2 + 1]) ) {
                                integer temp = i2;
                                User uTemp = fsUsers[i2];
                                fsUsers[i2] = fsUsers[i2 + 1];
                                fsUsers[i2 + 1] = uTemp;
                            }
                        }
                    }


                    //Setup ordered List of Assignees for Fee Schedule
                    integer n3 = rateExhibitUsers.Size();
                    
                    for(integer k3 = 0; k3 < n3 - 1; k3++) {
                        for(integer i3 = 0; i3 < n3 - k3 - 1; i3++) {
                            if(rateOpsRateExhibitMaxDateAssignMap.get(rateExhibitUsers[i3]) > rateOpsRateExhibitMaxDateAssignMap.get(rateExhibitUsers[i3 + 1]) ) {
                                integer temp = i3;
                                User uTemp = rateExhibitUsers[i3];
                                rateExhibitUsers[i3] = rateExhibitUsers[i3 + 1];
                                rateExhibitUsers[i3 + 1] = uTemp;
                            }
                        }
                    }
                    
                    
                    //Assign records        
                    for(NS_NIS_Request__c hceReq2 : assignableHCEReqRateOps) {
                        
                        User moveToEnd;
                        
                        if(hceReq2.Rate_Operations_Request_Type__c == 'Fee Schedule Request' || hceReq2.Rate_Operations_Request_Type__c == 'Fee Schedule Question') {
                            hceReq2.HCE_Analyst__c = fsUsers[0].id;

                            //reset order
                            moveToEnd = fsUsers[0];
                            fsUsers.remove(0);
                            fsUsers.add(moveToEnd);
                            
                            
                        }else if(hceReq2.Rate_Operations_Request_Type__c == 'Non-Standard Rate Exhibit Lang Rev HCFA') {
                            hceReq2.HCE_Analyst__c = rateExhibitUsers[0].id;

                            //reset order
                            moveToEnd = rateExhibitUsers[0];
                            rateExhibitUsers.remove(0);
                            rateExhibitUsers.add(moveToEnd);
                        }
                    }


                    //Send email Notifiaction                   
                    List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
                    
                    for(NS_NIS_Request__c x : assignableHCEReqRateOps) {
                        if(x.HCE_Analyst__c != null) {
                            
                            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                            
                            //setup email variables
                            String sfdcBaseURL = URL.getSalesforceBaseUrl().toExternalForm();                       
                            String[] toAddresses = new String[] {assigneeEmail.get(x.HCE_Analyst__c)};
                            String[] ccAddresses = backupEmail.get(x.HCE_Analyst__c) != null ? new String[] {backupEmail.get(x.HCE_Analyst__c)} : null;
                            string Subject = 'You have been Auto-Assigned an ND - HCE Request='+ x.name +'. (source: NS_NIS_Request_Trigger)';
                            string Body = 'Please login to view your Full Record. <br><br>' +
                                            'Request Type:' + x.Rate_Operations_Request_Type__c + '<br/><br>' +
                                            'Request Description:' + x.Request_Description__c + '<br/><br>' +
                                            'Link to Record: ' + sfdcBaseURL + '/' + x.id;
                                                        
                            //set email values
                            mail.setToAddresses(toAddresses);
                            if(ccAddresses != null) { 
                                mail.setCcAddresses(ccAddresses);
                            }
                            mail.setSubject( Subject );
                            mail.setHtmlBody(Body);
                            
                            mails.add(mail);
                        }
                    }


                    //Final Update for Assignements
                    try{ 
                        update assignableHCEReqRateOps;                 
                        Messaging.sendEmail ( mails );                      
                    }catch(exception e){ throw e; }
                                        
                }



                //----------------------------------------------------------------------------
                // Round Robin Assignment for Rate Exhibit Review Approval By
                //----------------------------------------------------------------------------  
                    if(assignRateExApper.size() > 0) {
                        
                        map<id, User> HcfaToUserMap = new map<id,User>();
                        map<User, datetime> hcfaMaxDateAssignMap = new map<User, datetime>();       
                        //map<id,string> assigneeEmail = new map<id, string>();
                        //map<id,string> backupEmail = new map<id, string>();
                        
                        list<User> userAssignListRateExhibit = new list<User>([select id,alias, Auto_Assign_Rate_Operation_Request_Type__c, isactive,
                                                                        No_Auto_Assign_HCE_Request_Start_Date__c, No_Auto_Assign_HCE_Request_End_Date__c,
                                                                        Do_not_Auto_Assign_Rate_Operations__c, email, Rate_Operations_Backup_Email__c,
                                                                        Auto_Assign_Rate_Rev_HCFA_RT_Assignable__c
                                                                    from User 
                                                                    //where alias IN: userAssignL // use user field instead
                                                                    where Auto_Assign_Rate_Rev_HCFA_RT_Assignable__c = true
                                                                    AND Do_not_Auto_Assign_Rate_Operations__c = false
                                                                    AND isactive = true]); 
                         
                         //Setup maps
                         for(User U : userAssignListRateExhibit){
                            hcfaMaxDateAssignMap.put(U,null);
                            HcfaToUserMap.put(U.id, U);
                            //assigneeEmail.put(U.id, u.email);
                            //backupEmail.put(U.id, u.Rate_Operations_Backup_Email__c);             
                         }                                          
            
                            
                            //Get list of Recent HCE Requests based on appliable Assignees.
                            list<NS_NIS_Request__c> allHistoricalRequestsHcfa = new list<NS_NIS_Request__c>();
                                allHistoricalRequestsHcfa = [select id, Rate_Exhibit_Review_Approval_By__c, createddate, recordtypeid, 
                                                                    Rate_Operations_Request_Type__c, Rate_Exhibit_Review_Request_Date_Time__c
                                                                from NS_NIS_Request__c
                                                                where Rate_Exhibit_Review_Approval_By__c != null
                                                                and  recordtypeid =: rtRIHCFA
                                                                and Rate_Exhibit_Review_Request_Date_Time__c != null
                                                                order by Rate_Exhibit_Review_Approval_By__c, Rate_Exhibit_Review_Request_Date_Time__c asc];
                                
                                for(NS_NIS_Request__c r : allHistoricalRequestsHcfa) {                  
                                    User U;
                                    
                                    U = HcfaToUserMap.get(r.Rate_Exhibit_Review_Approval_By__c);
                                    if(U != null && HcfaToUserMap.ContainsKey(U.id)) {
                                        hcfaMaxDateAssignMap.put(U, r.Rate_Exhibit_Review_Request_Date_Time__c);    
                                    }
                                }                               
        
        
                            list<User> rxUsers = new list<User>();
                                //not populated date
                                for(User np: hcfaMaxDateAssignMap.keyset()) {
                                    if(hcfaMaxDateAssignMap.get(np) == null) {
                                        rxUsers.add(np);
                                    }
                                }
                                                        
                                //populated date
                                for(User p: hcfaMaxDateAssignMap.keyset()) {
                                    if(hcfaMaxDateAssignMap.get(p) != null) {
                                        rxUsers.add(p);
                                    }
                                }
        
                            
                            //Setup ordered List of Assignees for Fee Schedule
                            integer n = rxUsers.Size();
                            
                            for(integer k = 0; k < n - 1; k++) {
                                for(integer i2 = 0; i2 < n - k - 1; i2++) {                     
                                    if(hcfaMaxDateAssignMap.get(rxUsers[i2]) > hcfaMaxDateAssignMap.get(rxUsers[i2 + 1]) ) {
                                        integer temp = i2;
                                        User uTemp = rxUsers[i2];
                                        rxUsers[i2] = rxUsers[i2 + 1];
                                        rxUsers[i2 + 1] = uTemp;
                                    }
                                }
                            }
                            
                            
                            //Assign records        
                            for(NS_NIS_Request__c hceReq2 : assignRateExApper) {
                                
                                User moveToEnd;                     
                                hceReq2.Rate_Exhibit_Review_Approval_By__c = rxUsers[0].id;
        
                                //reset order
                                moveToEnd = rxUsers[0];
                                rxUsers.remove(0);
                                rxUsers.add(moveToEnd);
                                                                
                            }

        
                            //Final Update for Assignements
                            try{ 
                                update assignRateExApper;                   
                                //Messaging.sendEmail ( mailsH );                       
                            }catch(exception e){ throw e; }
                                                    
                    }           
                        
    
            //----------------------------------------------------------------------------
            // ADD HCE Metrics records when Product selected on the Request.
            //----------------------------------------------------------------------------      
            
            list<HCE_Metric__c> hceMetricInsert = new list<HCE_Metric__c>();
            map<string, Id> metricToReqId = new map<string, Id>();
            
            for(NS_NIS_Request__c hceReq : allNdRequests){
                list<string> metricList = new list<string>();
                
                if(hceReq.Product_Selection__c != null && 
                        (hceReq.recordtypeid == rtRIHCFA || 
                        hceReq.recordtypeid == rtRIUB) ) {
                    
                    metricList = hceReq.Product_Selection__c.split(';');
                    
                    if(metricList.size() > 0) {

                        for(string s : metricList) {
                            if(s != 'AM' && s != 'W/C') {
                                HCE_Metric__c x = new HCE_Metric__c(Product__c = s, ND_HCE_Request__c = hceReq.id);
                                hceMetricInsert.add(x); 
                            }                       
                        }
                    }
                }
            }
            
            if(hceMetricInsert.size() > 0) {
                try{
                    insert hceMetricInsert;
                }catch(exception ex) { throw ex; }              
            }       
                                                                    
    }
    
    //----------------------------------------------------------------------------------------------------------------------------------
    //All Update Triggers
    //----------------------------------------------------------------------------------------------------------------------------------
    
    if(trigger.isUpdate) {
    
        //----------------------------------------------------------------------------
        // One Query for all NS_NIS_Requests in Update - can be refernced below
        //----------------------------------------------------------------------------  
            list<NS_NIS_Request__c> allNdUpRequests = new list<NS_NIS_Request__c>(); 
            
                allNdUpRequests = [select id, ownerID, recordtypeID, Rate_Operations_Request_Type__c, 
                                        HCE_Analyst__c, Product_Selection__c, Rate_Exhibit_Review_Approval_By__c, 
                                        Request_Rate_Exhibit_Review__c, Entered_into_DTS_for_HCFA_By__c, 
                                        Enter_into_DTS_for_HCFA_Requested__c, HCE_Analyst_Status__c,
                                        Requester__c, Rate_Exhibit_Review_Approval_Status__c,
                                        Negotiation_Type__c, Negotiation_Sub_Type__c,
                                        Negotiator_Approver__c, Negotiator_Approver_Status__c, Negotiator_Approval_Completed_Date__c,
                                        RD_Approver__c, RD_Approver_Status__c, RD_Approval_Completed_Date__c,
                                        SVP_Approver__c, SVP_Approver_Status__c, SVP_Approval_Completed_Date__c,
                                        HCE_VP_Approver__c, HCE_VP_Approver_Status__c, HCE_VP_Approval_Completed_Date__c,
                                        COO_Approver__c, COO_Approver_Status__c, COO_Approval_Completed_Date__c,
                                        Workers_Comp_Rate_of_the_State_FS__c, Auto_Medical_Rate_of_the_State_FS__c,
                                        Charge_Master_Cap__c, Charge_Master_Cap_P__c, FS_Approval_Status__c, Rate_Negotiation_Type__c,
                                        Requester__r.HCE_Request_Approver__c, Requester__r.HCE_Request_Approver__r.HCE_Request_Approver__c               
                                    from NS_NIS_Request__c 
                                    where id IN : trigger.new]; 
                                    
                                    
        //----------------------------------------------------------------------------
        // Set the Entered into DTS Analyst Field
        //----------------------------------------------------------------------------  
            list<NS_NIS_Request__c> updateEnteredToDTSuser = new list<NS_NIS_Request__c>();                 
            list<NS_NIS_Request__c> allChangedHCE = new list<NS_NIS_Request__c>(); 
            list<NS_NIS_Request__c> assignRateExApper = new list<NS_NIS_Request__c>(); 
            list<NS_NIS_Request__c> metricValidation = new list<NS_NIS_Request__c>(); 
            list<NS_NIS_Request__c> hceReqsNeedApprovers = new list<NS_NIS_Request__c>(); 
            
            //setup all update lists
            for(NS_NIS_Request__c HCE : allNdUpRequests) {
                NS_NIS_Request__c oldHCE = Trigger.oldMap.get(hce.id);
                
                // After update of the HCE Product selection update the Metrics list.
                if(HCE.Product_Selection__c != oldHCE.Product_Selection__c && 
                        (HCE.recordtypeid == rtRIHCFA || HCE.recordtypeid == rtRIUB) ) {
                    
                    allChangedHCE.add(HCE);
                }
                
                // Set the Entered into DTS Analyst Field
                if(HCE.Enter_into_DTS_for_HCFA_Requested__c != oldHCE.Enter_into_DTS_for_HCFA_Requested__c &&
                        HCE.Enter_into_DTS_for_HCFA_Requested__c == true && HCE.Rate_Exhibit_Review_Approval_By__c != null) {
                    
                    updateEnteredToDTSuser.add(HCE);
                }
                
                // Round Robin Assignment for Rate Exhibit Review Approval By
                if(HCE.Request_Rate_Exhibit_Review__c == 'Request Rate Exhibit Review' && 
                        oldHCE.Request_Rate_Exhibit_Review__c != 'Request Rate Exhibit Review' &&
                        HCE.Rate_Exhibit_Review_Approval_By__c == null &&
                        HCE.recordtypeId == rtRIHCFA) {
                    
                    assignRateExApper.add(HCE);
                }
                
                //Metric Validation
                if( (HCE.HCE_Analyst_Status__c == 'Closed - Complete' && (HCE.recordtypeid == rtRIHCFA || HCE.recordtypeid == rtRIUB)) 
                        ||
                        (HCE.HCE_Analyst_Status__c == 'Pending Final Rates Request' && HCE.recordtypeid == rtRIUB) ) {
                    
                    metricValidation.add(HCE);
                }

                //Update the Required Approvers fields for HCFA
                if( HCE.recordtypeid == rtRIHCFA && 
                        ( (HCE.Rate_Exhibit_Review_Approval_Status__c == 'Closed - Approved' &&
                        HCE.Rate_Exhibit_Review_Approval_Status__c != oldHCE.Rate_Exhibit_Review_Approval_Status__c &&
                        HCE.HCE_Analyst_Status__c == 'Closed - Complete')       
                        ||      
                        (HCE.HCE_Analyst_Status__c == 'Closed - Complete' &&
                        HCE.HCE_Analyst_Status__c != oldHCE.HCE_Analyst_Status__c &&
                        HCE.Rate_Exhibit_Review_Approval_Status__c == 'Closed - Approved') ) ) {
                        
                    hceReqsNeedApprovers.add(HCE);
                }
                
                //Update the Required Approvers fields for UB
                if( HCE.recordtypeid == rtRIUB && HCE.Rate_Negotiation_Type__c == 'Final Rates' &&
                        ( (HCE.Rate_Exhibit_Review_Approval_Status__c == 'Closed - Approved' &&
                        HCE.Rate_Exhibit_Review_Approval_Status__c != oldHCE.Rate_Exhibit_Review_Approval_Status__c &&
                        HCE.HCE_Analyst_Status__c == 'Closed - Complete')       
                        ||      
                        (HCE.HCE_Analyst_Status__c == 'Closed - Complete' &&
                        HCE.HCE_Analyst_Status__c != oldHCE.HCE_Analyst_Status__c &&
                        HCE.Rate_Exhibit_Review_Approval_Status__c == 'Closed - Approved') ) ) {
                        
                    hceReqsNeedApprovers.add(HCE);
                }               
            
            }


        //----------------------------------------------------------------------------
        // Metric are complete validation
        //----------------------------------------------------------------------------  
            if(metricValidation.size() > 0) {
                set<id> errorRequestsIDs = new set<id>();
                
                list<HCE_Metric__c> allIncompleteRelatedMetrics = new list<HCE_Metric__c>();
                    allIncompleteRelatedMetrics = [select id, Metric_is_Complete__c, 
                                                ND_HCE_Request__c, Current_P_of_Medicare__c, Negotiated_P_of_Medicare__c, Revenue_Change__c, 
                                                ACL_Volume__c
                                            from HCE_Metric__c
                                            where ND_HCE_Request__c IN: metricValidation
                                            AND Metric_is_Complete__c = false];
                
                for(HCE_Metric__c m : allIncompleteRelatedMetrics) {
                    errorRequestsIDs.add(m.ND_HCE_Request__c);
                }
                
                for( NS_NIS_Request__c r : trigger.new) {
                    if(errorRequestsIDs.contains(r.id)) {
                        trigger.new[0].addError('The \'HCE Metrics\' are not fully Complete. The \'HCE Status\' may not be set to \'Closed - Complete\' or \'Pending Final Rates Request\' until all Metrics are entered.  (Source: NS_NIS_Request.trg)');
                    }
                    
                }
            }

        //----------------------------------------------------------------------------
        // Round Robin Assignment for Rate Exhibit Review Approval By
        //----------------------------------------------------------------------------  
            if(assignRateExApper.size() > 0) {
                
                map<id, User> HcfaToUserMap = new map<id,User>();
                map<User, datetime> hcfaMaxDateAssignMap = new map<User, datetime>();       
                map<id,string> assigneeEmail = new map<id, string>();
                map<id,string> backupEmail = new map<id, string>();
                
                list<User> userAssignList = new list<User>([select id,alias, Auto_Assign_Rate_Operation_Request_Type__c, isactive,
                                                                No_Auto_Assign_HCE_Request_Start_Date__c, No_Auto_Assign_HCE_Request_End_Date__c,
                                                                Do_not_Auto_Assign_Rate_Operations__c, email, Rate_Operations_Backup_Email__c,
                                                                Auto_Assign_Rate_Rev_HCFA_RT_Assignable__c
                                                            from User 
                                                            //where alias IN: userAssignL // use user field instead
                                                            where Auto_Assign_Rate_Rev_HCFA_RT_Assignable__c = true
                                                            AND Do_not_Auto_Assign_Rate_Operations__c = false
                                                            AND isactive = true]); 
                 
                 //Setup maps
                 for(User U : userAssignList){
                    hcfaMaxDateAssignMap.put(U,null);
                    HcfaToUserMap.put(U.id, U);
                    assigneeEmail.put(U.id, u.email);
                    backupEmail.put(U.id, u.Rate_Operations_Backup_Email__c);              
                 }                                          
    
                    
                    //Get list of Recent HCE Requests based on appliable Assignees.
                    list<NS_NIS_Request__c> allHistoricalRequestsHcfa = new list<NS_NIS_Request__c>();
                        allHistoricalRequestsHcfa = [select id, Rate_Exhibit_Review_Approval_By__c, createddate, recordtypeid, 
                                                            Rate_Operations_Request_Type__c, Rate_Exhibit_Review_Request_Date_Time__c
                                                        from NS_NIS_Request__c
                                                        where Rate_Exhibit_Review_Approval_By__c != null
                                                        and  recordtypeid =: rtRIHCFA
                                                        and Rate_Exhibit_Review_Request_Date_Time__c != null
                                                        order by Rate_Exhibit_Review_Approval_By__c, Rate_Exhibit_Review_Request_Date_Time__c asc];
                        
                        for(NS_NIS_Request__c r : allHistoricalRequestsHcfa) {                  
                            User U;
                            
                            U = HcfaToUserMap.get(r.Rate_Exhibit_Review_Approval_By__c);
                            if(U != null && HcfaToUserMap.ContainsKey(U.id)) {
                                hcfaMaxDateAssignMap.put(U, r.Rate_Exhibit_Review_Request_Date_Time__c);    
                            }
                        }                               


                    list<User> rxUsers = new list<User>();
                        //not populated date
                        for(User np: hcfaMaxDateAssignMap.keyset()) {
                            if(hcfaMaxDateAssignMap.get(np) == null) {
                                rxUsers.add(np);
                            }
                        }
                                                
                        //populated date
                        for(User p: hcfaMaxDateAssignMap.keyset()) {
                            if(hcfaMaxDateAssignMap.get(p) != null) {
                                rxUsers.add(p);
                            }
                        }

                    
                    //Setup ordered List of Assignees for Fee Schedule
                    integer n = rxUsers.Size();
                    
                    for(integer k = 0; k < n - 1; k++) {
                        for(integer i2 = 0; i2 < n - k - 1; i2++) {                     
                            if(hcfaMaxDateAssignMap.get(rxUsers[i2]) > hcfaMaxDateAssignMap.get(rxUsers[i2 + 1]) ) {
                                integer temp = i2;
                                User uTemp = rxUsers[i2];
                                rxUsers[i2] = rxUsers[i2 + 1];
                                rxUsers[i2 + 1] = uTemp;
                            }
                        }
                    }
                    
                    
                    //Assign records        
                    for(NS_NIS_Request__c hceReq2 : assignRateExApper) {
                        
                        User moveToEnd;                     
                        hceReq2.Rate_Exhibit_Review_Approval_By__c = rxUsers[0].id;

                        //reset order
                        moveToEnd = rxUsers[0];
                        rxUsers.remove(0);
                        rxUsers.add(moveToEnd);
                                                        
                    }

                    //Final Update for Assignements
                    try{ 
                        update assignRateExApper;                   
                        //Messaging.sendEmail ( mailsH );                       
                    }catch(exception e){ throw e; }
                                            
            }
        //----------------------------------------------------------------------------
        // Set the Entered into DTS Analyst Field
        //----------------------------------------------------------------------------              
            
            if(updateEnteredToDTSuser.size() > 0) {
                
                for(NS_NIS_Request__c a : updateEnteredToDTSuser) {
                    a.Entered_into_DTS_for_HCFA_By__c = a.Rate_Exhibit_Review_Approval_By__c;
                }
                
                //update the User field         
                if(updateEnteredToDTSuser.size() > 0 ) {
                    try{
                        update updateEnteredToDTSuser;
                    }catch(exception e){ throw e; }
                        
                }
            }

        //----------------------------------------------------------------------------
        // After update of the HCE Product selection update the Metrics list.
        //----------------------------------------------------------------------------              
            
            if(allChangedHCE.size() > 0) {
                
                list<HCE_Metric__c> metric = new list<HCE_Metric__c>();
                    
                    if(allChangedHCE.size() > 0) {
                        
                        metric = [select id, product__c, ND_HCE_Request__c
                                    from HCE_Metric__c 
                                    where ND_HCE_Request__c IN : allChangedHCE];
                        
                    }
                
                //List for Insert and Delete
                list<HCE_Metric__c> insMetric = new list<HCE_Metric__c>();
                list<HCE_Metric__c> delMetric = new list<HCE_Metric__c>();
                
                
                //find deltas in the HCE products and add or delete.
                if(allChangedHCE.size() > 0) {
                    
                    for(NS_NIS_Request__c HCEreq2 : allChangedHCE) {
                        list<string> currentHeaderProducts = new list<string>();
                        map<string,id> mapCurrentHeaderProducts = new map<string,id>();
                        
                        if(HCEreq2.Product_Selection__c != null ){
                            currentHeaderProducts = HCEreq2.Product_Selection__c.split(';');
                        }
                        if(currentHeaderProducts.size() > 0) {
                            for(string s: currentHeaderProducts) {
                                mapCurrentHeaderProducts.put(s, HCEreq2.id);
                            }
                        }
                        
                        
                        if(currentHeaderProducts.size() > 0) {
                            list<HCE_Metric__c> currentMetricProducts = new list<HCE_Metric__c>();
                            map<string,id> mapCurrentMetricProducts = new map<string,id>();
                            
                            for(HCE_Metric__c m : metric) {
                                if(HCEreq2.id == m.ND_HCE_Request__c) {
                                    mapCurrentMetricProducts.put(m.product__c, m.id);
                                    currentMetricProducts.add(m);
                                }                       
                            }
                            
                            //add new products, newly listed
                            for(string s : currentHeaderProducts) {
                                if(!mapCurrentMetricProducts.containsKey(s) && s != 'AM' && s != 'W/C') {
                                    HCE_Metric__c x = new HCE_Metric__c(Product__c = s, ND_HCE_Request__c = HCEreq2.id);                                
                                    insMetric.add(x);   
                                }   
                            }
                            
                            //remove old product, newly removed
                            for(HCE_Metric__c hce : currentMetricProducts) {
                                if(!mapCurrentHeaderProducts.containsKey(hce.product__c) ) {
                                    delMetric.add(hce);
                                }
                            }
                            
                        }else {
                            for(HCE_Metric__c met : metric) {
                                if(met.ND_HCE_Request__c == HCEreq2.id) {
                                    delMetric.add(met);
                                }
                            }
                        }
                    
                    }
                }
                
                
                //Update the Metric records
                if(delMetric.size() > 0) {
                    try{
                        delete delMetric;
                    }catch(exception e){ throw e; }
                }
                
                if(insMetric.size() > 0 ) {
                    try{
                        insert insMetric;
                    }catch(exception e){ throw e; }
                        
                }
            
            }



        //----------------------------------------------------------------------------
        // Set Approvers based on Analysis
        //----------------------------------------------------------------------------  

            if(hceReqsNeedApprovers.size() > 0) {
                
                //VARIABLES
                string COOalias = system.Label.HCE_Req_Approver_COO_User_Alias;
                string EVPalias = system.Label.HCE_Req_Approver_EVP_User_Alias;
                string HCEVPalias = system.Label.HCE_Req_Approver_HCE_VP_User_Alias;        
                id HCEVPid;
                id EVPid;
                id COOid;
                
                //Sets,Lists, Maps  
                set<id> hceReqIds = new set<id>();
                set<id> requesterIds = new set<id>();
                set<id> requesterApproverIds = new set<id>();
                list<User> applicableUsers = new list<User>();
                list<HCE_Metric__c> allMetrics = new list<HCE_Metric__c>();
                map<id, HCE_Metric__c> hceReqToKeyMetric= new map<id, HCE_Metric__c>();
            
            
                    //populate sets and map
                    for(NS_NIS_Request__c HCE : hceReqsNeedApprovers) {
                        hceReqIds.add(HCE.id);
                        requesterIds.add(HCE.Requester__c);
                        requesterApproverIds.add(HCE.Requester__r.HCE_Request_Approver__c); 
                        requesterApproverIds.add(HCE.Requester__r.HCE_Request_Approver__r.HCE_Request_Approver__c);         
                    }   
                
                    //get list of all users
                    applicableUsers = [select id, name, isactive, alias, HCE_Request_Approver__c, HCE_Request_Approver__r.HCE_Request_Approver__c 
                                        from User
                                        where ( id IN : requesterIds OR
                                                id IN : requesterApproverIds OR                 
                                                alias = : COOalias OR
                                                alias = : EVPalias OR
                                                alias = : HCEVPalias )
                                                //and isactive = true //doesn't need to be active to assign
                                        ];
                    
                
                    
                    //set Approver Ids
                    for(user U : applicableUsers) {
                        //Check to ensure all users are active, throw error if inactive
                        if(U.isActive == false) {
                            trigger.new[0].addError('This \'HCE Request\' has an inactive \'Approver\' ('+ U.Name +').  The Approval process can not begin until the inactive \'Approver\' have been resolved.  (Source: NS_NIS_Request.trg)');     
                        }
                        //set executive approvers
                        if(U.alias == HCEVPalias) { HCEVPid = U.id; }
                        if(U.alias == EVPalias) { EVPid = U.id; }
                        if(U.alias == COOalias) { COOid = U.id; }                   
                    }   
                                    
                
                    //get list of all Metrics related to Requests   
                    allMetrics = [select Product__c, Negotiated_P_of_Medicare__c, Negotiated_P_of_Medicare_P__c, ND_HCE_Request__c,
                                        Revenue_Corridor_P__c,Revenue_Corridor__c, Negotiated_Discount_P__c, Revenue_Change__c, Revenue_Change_D__c,
                                        Savings__c,Savings_Points__c, Current_P_of_Medicare__c, Current_P_of_Medicare_P__c, Product_Sort_Order__c 
                                    from HCE_Metric__c 
                                    where ND_HCE_Request__c IN : hceReqIds
                                    order by ND_HCE_Request__c, Product_Sort_Order__c desc nulls first];

                
                    //set key Product used for Approval Routing (logic requires "HCE_Metric__c.Product_Sort _Order__c" to be set properly.)
                    for(HCE_Metric__c m : allMetrics) {
                        hceReqToKeyMetric.put(m.ND_HCE_Request__c, m);
                    }           
            
                        
                    // START ASSIGNMENT LOGIC   
                        for(NS_NIS_Request__c HCE : hceReqsNeedApprovers) {
                            
                            //dynamic approver ids
                            id negAid = HCE.Requester__c;
                            id RDid;
                            id SVPid;
                
                            //list of required Approval levels
                            boolean negA = true;
                            boolean RD;
                            boolean SVP;        
                            boolean HCEVP;
                            boolean EVP;
                            boolean COO;    
                            
                                //set the RD and SVP ids
                                for(User U : applicableUsers) {
                                    if(U.id == HCE.Requester__c ) {
                                        RDid = U.HCE_Request_Approver__c;
                                        SVPid = U.HCE_Request_Approver__r.HCE_Request_Approver__c;
                                    }       
                                }
            
                            
                            
                            //START OF ANALYSIS TO SEE WHAT APPROVALS ARE NEEDED
                                
                                //List of Exceptions or advanced Approval Requirements Product Not Accounted For
                                
                                list<string> productSelectionList = new list<string>();
                                productSelectionList = HCE.Product_Selection__c.split(';');                             
                                
                                set<string> productSelectionSet = new set<string>();
                                for(string s : productSelectionList) {
                                    productSelectionSet.add(s);
                                }

                                
                                //Exception RULE: W/C % of State FS 
                                if( productSelectionSet.contains('W/C') ) {
                                    if( HCE.Workers_Comp_Rate_of_the_State_FS__c != 'Excluded' && decimal.valueOf(HCE.Workers_Comp_Rate_of_the_State_FS__c) > 85) { 
                                        RD = true; SVP = true;                          
                                    }   
                                }   
                                    
                                        
                                //Exception RULE: AM % of State FS                                                                  
                                if( productSelectionSet.contains('AM') ) {
                                    if( HCE.Auto_Medical_Rate_of_the_State_FS__c != 'Excluded' && decimal.valueOf(HCE.Auto_Medical_Rate_of_the_State_FS__c) > 90 ) { 
                                        RD = true; SVP = true;                              
                                    }
                                }           
                    
                        
                                //Exception RULE: Product selection contains MPI and Not WC or AM
                                //Split out per RobM 4/20/16
                                //Rules were combined by Christy 4/27/16
                                //if( HCE.RecordTypeID == rtRIHCFA || HCE.RecordTypeID == rtRIUB ) {
                                    if( productSelectionSet.contains('MPI') || productSelectionSet.contains('Beech') ) {
                                        if( !productSelectionSet.contains('AM') || !productSelectionSet.contains('W/C')  ) {
                                            RD = true; SVP = true;                                              
                                        }
                                    }/*
                                }else if( HCE.RecordTypeID == rtRIUB ) {
                                    if( !productSelectionSet.contains('AM') || !productSelectionSet.contains('W/C')  ) {
                                        RD = true; SVP = true;                          
                                    }
                                }*/
                                
                                
                                //Exception RULE: AM or W/C are excluded                        
                                if( HCE.RecordTypeID == rtRIUB ) {
                                    if( HCE.Negotiation_Type__c == 'Add' || HCE.Negotiation_Type__c == 'ACL' ) {
                                        if( productSelectionSet.contains('AM') || productSelectionSet.contains('W/C') ) { 
                                            if( HCE.Workers_Comp_Rate_of_the_State_FS__c == 'Excluded' || HCE.Auto_Medical_Rate_of_the_State_FS__c == 'Excluded') {
                                                RD = true; SVP = true;          
                                            }
                                        }
                                    }   
                                }else if( HCE.RecordTypeID == rtRIHCFA ) {
                                    if( productSelectionSet.contains('AM') || productSelectionSet.contains('W/C') ) {
                                        if( HCE.Workers_Comp_Rate_of_the_State_FS__c == 'Excluded' || HCE.Auto_Medical_Rate_of_the_State_FS__c == 'Excluded') {
                                            RD = true; SVP = true;          
                                        }
                                    }
                                }
                                

                                //Exception RULE: Charge Master Cap Logic for UB
                                //Per Christy Brander 4/27/16 CMC is now 3%
                                if(HCE.RecordTypeID == rtRIUB && (HCE.Negotiation_Type__c == 'Add' || HCE.Negotiation_Type__c == 'ACL')) {
                                    
                                    //Exception RULE: Charge Master Cap Exclude Logic
                                    if( HCE.Charge_Master_Cap__c == 'Exclude' ) { 
                                        RD = true; SVP = true;          
                                    }
                                    
                                    //Exception RULE: Charge Master Cap 3% Logic
                                    if(HCE.Charge_Master_Cap__c == 'Percent') {
                                        if(HCE.Charge_Master_Cap_P__c > 3) {
                                            RD = true; SVP = true;              
                                        }
                                    }
                                
                                }
                                
                            
                                //Exception RULE: Charge Master Cap Logic for HCFA
                                //Per Christy Brander 4/27/16 CMC is now 3%
                                if(HCE.RecordTypeID == rtRIHCFA) {
                                    
                                    //Exception RULE: Charge Master Cap Exclude Logic
                                    if(HCE.Charge_Master_Cap__c == 'Exclude') {
                                        RD = true; SVP = true;              
                                    }
                        
                                    //Exception RULE: Charge Master Cap 3% Logic
                                    if(HCE.Charge_Master_Cap__c == 'Percent') {
                                        if(HCE.Charge_Master_Cap_P__c > 3) {
                                            RD = true; SVP = true;              
                                        }
                                    }
                                    
                                }   
                                
                
                    
                            //START OF SUB-TYPE BASED APPROVAL RULES (only applicable if a Key metric is selected)
                            
                                if( hceReqToKeyMetric.get(HCE.id) != null) {
                                    
                                    //list of local variables
                                    decimal neg_MDCR_P = hceReqToKeyMetric.get(HCE.id).Negotiated_P_of_Medicare_P__c;
                                    string neg_MDCR_Type = hceReqToKeyMetric.get(HCE.id).Negotiated_P_of_Medicare__c;           
                                    decimal RevCorr_P = hceReqToKeyMetric.get(HCE.id).Revenue_Corridor_P__c;
                                    string RevCorr_Type = hceReqToKeyMetric.get(HCE.id).Revenue_Corridor__c;            
                                    decimal NegDisc_P = hceReqToKeyMetric.get(HCE.id).Negotiated_Discount_P__c;
                                    string RevChange_Type = hceReqToKeyMetric.get(HCE.id).Revenue_Change__c;
                                    decimal RevChange_D = hceReqToKeyMetric.get(HCE.id).Revenue_Change_D__c;
                                    string Save_Type = hceReqToKeyMetric.get(HCE.id).Savings__c;
                                    decimal Save_P = hceReqToKeyMetric.get(HCE.id).Savings_Points__c;
                                    decimal curr_MDCR_P = hceReqToKeyMetric.get(HCE.id).Current_P_of_Medicare_P__c;
                                    string curr_MDCR_Type = hceReqToKeyMetric.get(HCE.id).Current_P_of_Medicare__c; 
                                
                                                            
                                    //Analayze Required Approval levels based on Negotiation Sub-Type                                                       
                                    if( (HCE.RecordTypeID == rtRIHCFA && HCE.Negotiation_Sub_Type__c == 'New Agreement')
                                        ||
                                        (HCE.RecordTypeID == rtRIUB && 
                                            ((HCE.Negotiation_Sub_Type__c == 'New Agreement' && HCE.Negotiation_Type__c != 'Term' && HCE.Negotiation_Type__c != 'Renegotiate')
                                            ||
                                            (HCE.Negotiation_Sub_Type__c == 'Renegotiation / Add Product' && HCE.Negotiation_Type__c == 'ACL'))
                                        )  ) { 
                                        
                                        
                                        //NewNeg RULE: Medicare % Approvals
                                        if( neg_MDCR_Type == 'Percent') {                   
                                            if( neg_MDCR_P >= 200 && neg_MDCR_P < 400 ) {
                                                RD = true;
                                            }else if( neg_MDCR_P >= 400 && neg_MDCR_P < 500 ) {
                                                RD = true; SVP = true;
                                            }else if( neg_MDCR_P >= 500 ) {
                                                RD = true; SVP = true; HCEVP = true; EVP = true; COO = true;
                                            }                       
                                        }else if( neg_MDCR_Type == 'No Medicare % able to be calculated') {
                                            RD = true; SVP = true; HCEVP = true;
                                        }else {
                                            trigger.new[0].addError('The \'HCE Request\' approvers can\'t be set. Invalid \'Negotiated % of Medicare\'.  (Source: NS_NIS_Request.trg)'); // does this even work?
                                        }
                                        
                    
                                        //NewNeg RULE: Revenue Corridor Approvals
                                        if( RevCorr_Type == 'Negative Revenue Impact') {    
                                            RD = true; SVP = true; HCEVP = true; EVP = true; COO = true;                                        
                                        }else if(RevCorr_Type == 'Positive Revenue Impact') {
                                            if( RevCorr_P <= 10 && RevCorr_P >= 0) { 
                                                RD = true; SVP = true; HCEVP = true;
                                            }       
                                        }else {
                                            //RevCorr_Type is optional for UB
                                            if(HCE.Recordtypeid != rtRIUB) {
                                                trigger.new[0].addError('The \'HCE Request\' approvers can\'t be set. Invalid \'Revenue Corridor\'.  (Source: NS_NIS_Request.trg)'); // does this even work?
                                            }
                                        }
                                                                                
                                        
                                        //NewNeg RULE: Neg Discount %
                                        if( NegDisc_P < 5) {
                                            RD = true; SVP = true; HCEVP = true; EVP = true; COO = true;                            
                                        }       
                    
                                                                
                                    }else if( (HCE.RecordTypeID == rtRIHCFA && HCE.Negotiation_Sub_Type__c == 'Renegotiation / Add Product') 
                                                ||
                                                (HCE.RecordTypeID == rtRIUB && 
                                                    ((HCE.Negotiation_Sub_Type__c == 'Renegotiation / Add Product' && HCE.Negotiation_Type__c != 'ACL')
                                                    || 
                                                    (HCE.Negotiation_Sub_Type__c == 'New Agreement' && (HCE.Negotiation_Type__c == 'Term' || HCE.Negotiation_Type__c == 'Renegotiate')) )
                                                ) ) {
                    
                    
                                        //ReNeg RULE: Medicare %, No Medicare % able to be calculated Approvals
                                        if( curr_MDCR_Type == 'No Medicare % able to be calculated' || neg_MDCR_Type == 'No Medicare % able to be calculated' ) {
                                            RD = true; SVP = true; HCEVP = true;
                                        }
                                        
                                    
                                        //ReNeg RULE: Medicare 500% Approvals 
                                        if( curr_MDCR_P >= 500 && neg_MDCR_P < 500) {
                                            RD = true; SVP = true; HCEVP = true; EVP = true;
                                        }else if( neg_MDCR_P >= 500 ) {                                         
                                            RD = true; SVP = true; HCEVP = true; EVP = true; COO = true;        
                                        }
                    
                
                                        //ReNeg RULE: Revenue Change Approvals
                                        //RevChange is optional for UB
                                        if( RevChange_Type == 'Decrease in Revenue') {      
                                            if( RevChange_D > -15000 ) {  
                                                RD = true; SVP = true;
                                            }else if(RevChange_D <= -15000) {
                                                RD = true; SVP = true; HCEVP = true; EVP = true; COO = true;                            
                                            }                               
                                        }else if(HCE.RecordTypeID == rtRIHCFA && RevChange_Type != 'Decrease in Revenue' && RevChange_Type != 'Increase in Revenue' && RevChange_Type != 'No Change') {
                                            trigger.new[0].addError('The \'HCE Request\' approvers can\'t be set. Invalid \'Revenue Change \'.  (Source: NS_NIS_Request.trg)'); 
                                        }           
                    
                    
                                        //ReNeg RULE: Savings Points Approvals  
                                        // Changes should also be made for the exception Rule below; is this needed?
                                        if( Save_Type == 'No Change' ) {
                                            RD = true;                      
                                        }else if(Save_Type == 'Point Decrease in Savings') {
                                            
                                            if(Save_P >= -1 && Save_P < 0) {
                                                RD = true;
                                            }
                                            else if(Save_P < -1 && Save_P > -10 ) { 
                                                RD = true; SVP = true;
                                            }else if(Save_P <= -10) {
                                                RD = true; SVP = true;  HCEVP = true; EVP = true; COO = true;                           
                                            }
                                        }                                                   
                                                        
                                    }else if( HCE.Negotiation_Sub_Type__c != 'Renegotiation / Add Product' && HCE.Negotiation_Sub_Type__c != 'New Agreement') {
                                        trigger.new[0].addError('The \'HCE Request\' approvers can\'t be set.  Invalid \'Negotiation Sub-Type\'.  (Source: NS_NIS_Request.trg)');
                                    }
                                
                                }       
                            
                            
                                            
                                //-----------------------------------------------------------------------------
                                //BEGIN APPROVER ASSIGNMENTS    
                                //------------------------------------------------------------------------------
                                    
                                    //Set baseline Status' and ownership
                                    HCE.FS_Approval_Status__c = 'Pending Submission';
                                    HCE.Request_Status__c = 'In Process';
                                    
                                    if( !string.valueOf(HCE.OwnerID).startsWith('005') ) {
                                        HCE.Ownerid = HCE.Requester__c;
                                    }
                                   
                                    if(HCE.Rate_Exhibit_Review_Approval_Status__c != null) {
                                        if( HCE.Rate_Exhibit_Review_Approval_Status__c.contains('Closed - ') && HCE.Rate_Exhibit_Review_Approval_Date__c == null ) {                                        
                                            HCE.Rate_Exhibit_Review_Approval_Date__c = system.today();
                                        }
                                    }
                                       
                                    
                                    //set Negotiator
                                    if(negA == true) {
                                        if(negAid != null) {
                                            HCE.Negotiator_Approver__c = negAid;
                                        }else {
                                            trigger.new[0].addError('The \'Negotiator\' is missing.  Approval process can not begin.  (Source: NS_NIS_Request.trg)');
                                        }
                                    }else { 
                                        HCE.Negotiator_Approver__c = null; 
                                    }                   
                                    HCE.Negotiator_Approver_Status__c = null;
                                    HCE.Negotiator_Approval_Completed_Date__c = null;  
                                        
                                            
                                    //set RD
                                    if(RD == true) {
                                        if(RDid != null) {
                                            HCE.RD_Approver__c = RDid;
                                        }else{
                                            trigger.new[0].addError('The \'RD\' is missing.  Approval process can not begin.  (Source: NS_NIS_Request.trg)');
                                        }
                                    }else { 
                                        HCE.RD_Approver__c = null; 
                                    }
                                    HCE.RD_Approver_Status__c = null;
                                    HCE.RD_Approval_Completed_Date__c = null;  
                                    
                                    
                                    //set SVP
                                    if(SVP == true) {
                                        if(SVPid != null) {
                                            HCE.SVP_Approver__c = SVPid;
                                        }else{
                                            trigger.new[0].addError('The \'SVP\' is missing.  Approval process can not begin.  (Source: NS_NIS_Request.trg)');
                                        }
                                    }else { 
                                        HCE.SVP_Approver__c = null; 
                                    }
                                    HCE.SVP_Approver_Status__c = null;
                                    HCE.SVP_Approval_Completed_Date__c = null;  
                                    
                                    
                                    //set HCE VP
                                    if(HCEVP == true) {
                                        if(HCEVPid != null) {
                                            HCE.HCE_VP_Approver__c = HCEVPid;
                                        }else{
                                            trigger.new[0].addError('The \'HCE VP\' is missing.  Approval process can not begin.  (Source: NS_NIS_Request.trg)');
                                        }
                                    }else { 
                                        HCE.HCE_VP_Approver__c = null; 
                                    }
                                    HCE.HCE_VP_Approver_Status__c = null;
                                    HCE.HCE_VP_Approval_Completed_Date__c = null;  
                                    
                                    
                                    //set EVP
                                    if(EVP == true) {
                                        if(EVPid != null) {
                                            HCE.EVP_Approver__c = EVPid;
                                        }else{
                                            trigger.new[0].addError('The \'EVP\' is missing.  Approval process can not begin.  (Source: NS_NIS_Request.trg)');
                                        }
                                    }else { 
                                        HCE.EVP_Approver__c = null; 
                                    }
                                    HCE.EVP_Approver_Status__c = null;
                                    HCE.EVP_Approval_Completed_Date__c = null;  
                                    
                                    
                                    //set COO
                                    if(COO == true) {
                                        if(COOid != null) {
                                            HCE.COO_Approver__c = COOid;
                                        }else{
                                            trigger.new[0].addError('The \'COO\' is missing.  Approval process can not begin.  (Source: NS_NIS_Request.trg)');
                                        }
                                    }else { 
                                        HCE.COO_Approver__c = null; 
                                    }
                                    HCE.COO_Approver_Status__c = null;
                                    HCE.COO_Approval_Completed_Date__c = null;                          

                        }
                        
                //BEGIN FINAL UPDATES
                try{
                    update hceReqsNeedApprovers;
                }catch(exception e){ throw e; }
                                
            }                       
    }
}