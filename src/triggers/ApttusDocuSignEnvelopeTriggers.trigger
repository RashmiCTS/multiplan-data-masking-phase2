trigger ApttusDocuSignEnvelopeTriggers on Apttus_DocuApi__DocuSignEnvelope__c (
    before insert, 
    before update, 
    before delete, 
    after insert, 
    after update, 
    after delete, 
    after undelete) {
        // After inserting an envelope, we need to see if this is part of our automated
        // contract load and send process.  If it is, we need to continue that process.
        // By sending out supplemental documentation to go with the Docusign document.
        if (Trigger.isAfter && trigger.isInsert && trigger.New.size() == 1) {
            // Check if this envelope is attached to an agreement that is waiting for the docusign 
            // envelope to be sent. 
            Apttus_DocuApi__DocuSignEnvelope__c envelope = trigger.new[0];
            massSendDocusignBatch.CheckAndSendSupplementalEmail(envelope.Apttus_CMDSign__Agreement__c);
        
        }
        
        /************************* Start Code to Reset Agreement EmailTemplates for Apttus_DocuApi__DocuSignEnvelope__c **********/
        if(Trigger.isAfter &&  Trigger.isUpdate){
           DocuSignResetAgrmtEmailTemplatesHandler.resetEmailTemplates(Trigger.New);
        }
        
        /************************* End Code to Reset Agreement EmailTemplates for Apttus_DocuApi__DocuSignEnvelope__c *************/
}